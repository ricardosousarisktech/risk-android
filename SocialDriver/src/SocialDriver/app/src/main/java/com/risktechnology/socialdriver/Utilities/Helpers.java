package com.risktechnology.socialdriver.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.risktechnology.socialdriver.activities.FacebookLoginActivity;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

@SuppressWarnings("unused")
public class Helpers
{

    public static String convertStreamToString(InputStream is) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null)
        {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public static JSONArray JSONSort(JSONArray array, Comparator c)
    {
        List asList = new ArrayList(array.length());
        for (int i = 0; i < array.length(); i++)
        {
            Object object = array.opt(i);
            if(object != null)
            {
                //noinspection unchecked
                asList.add(object);
            }
        }
        Collections.sort(asList, c);
        JSONArray res = new JSONArray();
        for (Object o : asList)
        {
            res.put(o);
        }
        return res;
    }

    public static String readableDate(String created)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = created;
        try
        {
            Date baseDate = sdf.parse(created);
            sdf.setTimeZone(TimeZone.getDefault());
            sdf = new SimpleDateFormat("HH:mm dd-MMM", Locale.getDefault());
            date = sdf.format(baseDate);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    public static boolean isEmpty(@Nullable String value)
    {
        if (value == null || value.trim().length() == 0 || value.equals("null"))
            return true;
        else
            return false;
    }

    public static void errorAlert(Activity activity, String title, String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg)
                .setTitle(title)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.stat_notify_error);
        // Create the AlertDialog object and return it
        builder.create().show();
    }


    public static void warningAlert(Activity activity, String title, String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg)
                .setTitle(title)
                .setPositiveButton(android.R.string.ok, null);
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    public static void successAlert(Activity activity, String title, String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg)
                .setTitle(title)
                .setPositiveButton(android.R.string.ok, null);
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    public static boolean isEmailValid(@Nullable String email)
    {
        return !Helpers.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(@Nullable String password)
    {
        return !Helpers.isEmpty(password) && (password.length() > 8 && password.length() < 16);
    }

    public static void showServerError(Activity activity)
    {
        Helpers.errorAlert(activity, "Can't connect", "The server failed to respond, please try again later");
    }

    public static String joinWithString(ArrayList<String> components, String delimiter)
    {
        String outputString = "";

        for (String component : components)
        {
            outputString += component + delimiter;
        }

        if(outputString.length() >= delimiter.length())
        {
            outputString = outputString.substring(0, outputString.length() - delimiter.length());
        }
        return outputString;
    }
}
