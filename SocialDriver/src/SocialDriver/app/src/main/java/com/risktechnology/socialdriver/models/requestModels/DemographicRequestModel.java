package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


@SuppressWarnings("unused")
public class DemographicRequestModel extends BaseRequestModel
{
    private String mUserId;
    private String mDemographicType;
    private String mMonth;

    public DemographicRequestModel()
    {
        populateDetailsWithDate(new Date());
    }

    public DemographicRequestModel(Date date)
    {
        populateDetailsWithDate(date);
    }

    private void populateDetailsWithDate(Date date)
    {
        mUserId = "";
        mDemographicType = "";

        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        Date firstDayOfMonth = cal.getTime();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date lastDayOfMonth = cal.getTime();

        mMonth = "/Date(" + firstDayOfMonth.getTime() + ")/";
    }

    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException
    {
        if(!Helpers.isEmpty(mDemographicType))
        {
            jsonObject.put("DemographicType", mDemographicType);
        }

        jsonObject.put("Month", mMonth);

        if(!Helpers.isEmpty(mUserId))
        {
            jsonObject.put("UserId", mUserId);
        }

    }

    @Override
    public void describeModel()
    {

    }

    /**
     * Getter for property 'userId'.
     *
     * @return Value for property 'userId'.
     */
    public String getUserId()
    {
        return mUserId;
    }

    /**
     * Setter for property 'userId'.
     *
     * @param userId Value to set for property 'userId'.
     */
    public void setUserId(String userId)
    {
        mUserId = userId;
    }

    /**
     * Getter for property 'demographicType'.
     *
     * @return Value for property 'demographicType'.
     */
    public String getDemographicType()
    {
        return mDemographicType;
    }

    /**
     * Setter for property 'demographicType'.
     *
     * @param demographicType Value to set for property 'demographicType'.
     */
    public void setDemographicType(String demographicType)
    {
        mDemographicType = demographicType;
    }
}
