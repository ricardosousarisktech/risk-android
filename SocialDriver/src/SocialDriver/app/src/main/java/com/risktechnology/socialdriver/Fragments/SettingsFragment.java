package com.risktechnology.socialdriver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.activities.MainActivity;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.core.JourneyManager;
import com.risktechnology.socialdriver.utilities.UserPreferences;

/**
 * Created by Matthew on 23/07/15.
 */
public class SettingsFragment extends BaseFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener
{

    private LinearLayout mWifiNetworksHolder;
    private LinearLayout mBluetoothDevicesHolder;
    private Switch mAutoJourneyDetectSwitch;
    private Switch mLocationServicesSwitch;
    private Switch mNotificationsSwitch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setTitle("Settings");

        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        mWifiNetworksHolder = (LinearLayout) view.findViewById(R.id.wifiNetworksHolder);
        mBluetoothDevicesHolder = (LinearLayout) view.findViewById(R.id.bluetoothDevicesHolder);
        mAutoJourneyDetectSwitch = (Switch) view.findViewById(R.id.autoJourneyDetectSwitch);
        mLocationServicesSwitch = (Switch) view.findViewById(R.id.locationServicesSwitch);
        mNotificationsSwitch = (Switch) view.findViewById(R.id.notificationsSwitch);

        mWifiNetworksHolder.setOnClickListener(this);
        mBluetoothDevicesHolder.setOnClickListener(this);
        mNotificationsSwitch.setOnClickListener(this);

        mAutoJourneyDetectSwitch.setChecked(UserPreferences.getSharedInstance().getOptBoolForKey(Constants.SettingsKeyAutoJourneyDetect, true));
        mLocationServicesSwitch.setChecked(UserPreferences.getSharedInstance().getOptBoolForKey(Constants.SettingsKeyLocationServices, true));
        mNotificationsSwitch.setChecked(UserPreferences.getSharedInstance().getOptBoolForKey(Constants.SettingsKeyNotifications, true));

        mAutoJourneyDetectSwitch.setOnCheckedChangeListener(this);
        mLocationServicesSwitch.setOnCheckedChangeListener(this);
        mNotificationsSwitch.setOnCheckedChangeListener(this);

        return view;
    }

    @Override
    public void onClick(View view)
    {
        if(view == mWifiNetworksHolder)
        {
            MainActivity.sharedInstance().showFragment(new WifiNetworksFragment(), true, true);
        }
        else if(view == mBluetoothDevicesHolder)
        {
            MainActivity.sharedInstance().showFragment(new BluetoothDevicesFragment(), true, true);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        if(buttonView== mAutoJourneyDetectSwitch)
        {
            UserPreferences.getSharedInstance().putBoolean(isChecked, Constants.SettingsKeyAutoJourneyDetect);
            if(!isChecked)
            {
                if(JourneyManager.isInAutoMode())
                {
                    JourneyManager.stopMonitoring();
                }
            }
            else
            {
                if(!JourneyManager.isRunning())
                {
                    JourneyManager.startMonitoring(true);
                }
            }
        }
        else if(buttonView == mLocationServicesSwitch)
        {
            UserPreferences.getSharedInstance().putBoolean(isChecked, Constants.SettingsKeyLocationServices);
        }
        else if(buttonView == mNotificationsSwitch)
        {
            UserPreferences.getSharedInstance().putBoolean(isChecked, Constants.SettingsKeyNotifications);
        }
    }
}
