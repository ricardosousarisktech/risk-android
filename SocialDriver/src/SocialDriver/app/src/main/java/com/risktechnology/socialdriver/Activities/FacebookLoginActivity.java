package com.risktechnology.socialdriver.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.internal.Utility;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.models.requestModels.CreateUserRequestModel;
import com.risktechnology.socialdriver.models.requestModels.LoginRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.CreateUserRequest;
import com.risktechnology.socialdriver.requests.LoginRequest;
import com.risktechnology.socialdriver.ui_elements.DimmableButton;
import com.risktechnology.socialdriver.ui_elements.DimmableImageButton;
import com.risktechnology.socialdriver.ui_elements.ProgressHUD;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.UiUtilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by Dev on 21/07/2015.
 */
public class FacebookLoginActivity extends BaseActivity{

    DimmableImageButton mFacebookLoginButton;
    CallbackManager mCallbackManager;
    DimmableButton mJoinButton;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();

        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            // Activity was brought to front and not created,
            // Thus finishing this will get us to the last viewed activity
            finish();
            return;
        }

        setContentView(R.layout.activity_facebook_login_screen);

        mJoinButton = (DimmableButton) findViewById(R.id.joinButton);


        UiUtilities.setMultifontStringOnButton(mJoinButton, getApplicationContext());

        mJoinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadSDLogin();
            }
        });

        mFacebookLoginButton = (DimmableImageButton) findViewById(R.id.facebookLoginButton);

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>()
        {
            @Override
            public void onSuccess(LoginResult loginResult)
            {
                // App code

                getFacebookUserProfile();

            }

            @Override
            public void onCancel()
            {
                // App code
            }

            @Override
            public void onError(FacebookException exception)
            {
                // App code
                Helpers.errorAlert(FacebookLoginActivity.this, "Error", exception.getLocalizedMessage());
            }
        });

        mFacebookLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(FacebookLoginActivity.this, Arrays.asList("public_profile", "user_friends"));
            }
        });

        if(!Helpers.isEmpty(UserModel.getInstance().getSecretKey()) && UserModel.getInstance().getUserId() > 0)
        {
            launchMain();
        }
        else
        {
            //clear this just to be safe as encryption will be used if set
            UserModel.getInstance().setSecretKey("");
        }
    }

    private void getFacebookUserProfile() {

        final ProgressHUD progressHUD = ProgressHUD.show(this, "Fetching profile...", true, false, null);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken == null) {
            Profile.setCurrentProfile(null);
        } else {
            Utility.getGraphMeRequestWithCacheAsync(accessToken.getToken(), new Utility.GraphMeRequestWithCacheCallback() {
                public void onSuccess(JSONObject userInfo) {
                    progressHUD.dismiss();
                    String id = userInfo.optString("id");
                    if (id != null) {
                        String link = userInfo.optString("link");
                        Profile profile = new Profile(id, userInfo.optString("first_name"), userInfo.optString("middle_name"), userInfo.optString("last_name"), userInfo.optString("name"), link != null ? Uri.parse(link) : null);
                        Profile.setCurrentProfile(profile);

                        UserModel.getInstance().setFirstName(profile.getFirstName());
                        UserModel.getInstance().setLastName(profile.getLastName());
                        UserModel.getInstance().setImageUrl(profile.getProfilePictureUri(120, 120).toString());
                        login();
                    }
                }

                public void onFailure(FacebookException error) {
                    progressHUD.dismiss();
                    Helpers.errorAlert(FacebookLoginActivity.this, "Error", "Unable to retrieve facebook profile. Please try again");
                }
            });
        }

    }

    private void login()
    {
        LoginRequestModel model = new LoginRequestModel();
        model.setOauthId(Profile.getCurrentProfile().getId());
        model.setAccessToken(AccessToken.getCurrentAccessToken().getToken());

        final LoginRequest request = new LoginRequest(model);


        final ProgressHUD progressHUD = ProgressHUD.show(this, "Fetching user...", true, true, new DialogInterface.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialog)
            {
                request.cancelRequest();
            }
        });

        request.setRequestHandler(new BaseRequest.RequestHandler()
        {
            @Override
            public void requestFinished(BaseRequest request)
            {
                progressHUD.dismiss();
                if(request.getHTTPStatusCode() == 0)
                {
                    Helpers.showServerError(FacebookLoginActivity.this);
                    return;
                }

                if(request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                {
                    try
                    {
                        JSONObject userSocialDriver = request.getResponseObject().getJSONObject("UserSocialDriver");
                        UserModel.getInstance().setUserId(userSocialDriver.getInt("UserId"));
                        UserModel.getInstance().setSecretKey(userSocialDriver.getString("Secret"));
                        UserModel.getInstance().save();
                        launchMain();
                    } catch (JSONException e)
                    {
                        Helpers.errorAlert(FacebookLoginActivity.this, "User error", "An error occured retrieving your profile. Please try again");
                    }

                }
                else if(request.getErrorStatusCode() == Constants.LoginErrorCode.OAUTH_NOT_FOUND)
                {
                    createAccount();
                }
                else {
                    Helpers.errorAlert(FacebookLoginActivity.this, "Error", request.getErrorStatusMessage());
                }
                Log.i("Login", "Request Finished");
            }
        });

        request.executeRequest(this);
    }

    private void createAccount()
    {
        Profile currentProfile = Profile.getCurrentProfile();
        CreateUserRequestModel createUserRequestModel = new CreateUserRequestModel();
        createUserRequestModel.setOauthId(currentProfile.getId());
        createUserRequestModel.setFirstname(currentProfile.getFirstName());
        createUserRequestModel.setLastname(currentProfile.getLastName());

        CreateUserRequest request = new CreateUserRequest(createUserRequestModel);
        final ProgressHUD hud = ProgressHUD.show(this, "Creating account", true, false, null);
        request.setRequestHandler(new BaseRequest.RequestHandler()
        {
            @Override
            public void requestFinished(BaseRequest request)
            {
                hud.dismiss();
                if (request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                {
                    try
                    {
                        UserModel.getInstance().setUserId(request.getResponseObject().getInt("UserId"));
                        UserModel.getInstance().setSecretKey(request.getResponseObject().getString("SecretKey"));
                        UserModel.getInstance().save();
                        launchMain();
                    } catch (JSONException e)
                    {
                        Helpers.errorAlert(FacebookLoginActivity.this, "User error", "Unable to load user. Please try again");
                    }
                }
                else
                {
                    Helpers.errorAlert(FacebookLoginActivity.this, "Error", request.getErrorStatusMessage());
                }
            }
        });

        request.executeRequest(this);
    }

    private void loadSDRegister() {
        Intent intent = new Intent(FacebookLoginActivity.this, SocialDriverRegisterActivity.class);
        FacebookLoginActivity.this.startActivity(intent);
//        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void loadSDLogin() {
        Intent intent = new Intent(FacebookLoginActivity.this, SocialDriverLoginActivity.class);
        FacebookLoginActivity.this.startActivity(intent);
//        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void launchMyInformation()
    {
        Intent intent = new Intent(FacebookLoginActivity.this, MyInformationActivity.class);
        FacebookLoginActivity.this.startActivity(intent);
        FacebookLoginActivity.this.finish();
    }

    private void launchMain() {

        UserModel.getInstance().download();
        if(UserModel.getInstance().requiresConfiguration())
        {
            launchMyInformation();
        }
        else
        {
            Intent intent = new Intent(FacebookLoginActivity.this, MainActivity.class);
            FacebookLoginActivity.this.startActivity(intent);
            FacebookLoginActivity.this.finish();
//        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}