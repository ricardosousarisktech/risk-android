package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.Logger;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginRequestModel extends BaseRequestModel
{

    /*
    Username	The username (email address) that the user would use to log into the customer portal
    Password	The password associated with the user that they would use to log into the customer portal
    OAuthId	    Id returned from facebook login done on client
    AppID   	A unique identifier for the app making the request
     */

    private String mUsername;
    private String mPassword;
    private String mOauthId;
    private String mAccessToken;

    public LoginRequestModel() {
        mUsername = "";
        mPassword = "";
        mAccessToken = "";
        mOauthId = "";
    }

    public void describeModel() {
        Logger.LogMessage("Registration Model", "================ BEGIN =================");
        Logger.LogMessage("Registration Model", "Email: " + mUsername);
        Logger.LogMessage("Registration Model", "PinCode: " + mPassword);
        Logger.LogMessage("Registration Model", "================== END =================");
    }

    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException {
        if(!Helpers.isEmpty(mUsername))
        {
            jsonObject.put("Username", mUsername);
        }

        if(!Helpers.isEmpty(mPassword))
        {
            jsonObject.put("Password", mPassword);
        }

        if(!Helpers.isEmpty(mAccessToken))
        {
            jsonObject.put("AccessToken", mAccessToken);
        }

        if(!Helpers.isEmpty(mOauthId))
        {
            jsonObject.put("OauthId", mOauthId);
        }

        jsonObject.put("AppID", Constants.APP_ID);

    }

    /**
     * Getter for property 'username'.
     *
     * @return Value for property 'username'.
     */
    public String getUsername()
    {
        return mUsername;
    }

    /**
     * Setter for property 'username'.
     *
     * @param username Value to set for property 'username'.
     */
    public void setUsername(String username)
    {
        mUsername = username;
    }

    /**
     * Getter for property 'password'.
     *
     * @return Value for property 'password'.
     */
    public String getPassword()
    {
        return mPassword;
    }

    /**
     * Setter for property 'password'.
     *
     * @param password Value to set for property 'password'.
     */
    public void setPassword(String password)
    {
        mPassword = password;
    }

    public String getAccessToken()
    {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken)
    {
        mAccessToken = accessToken;
    }

    public String getOauthId() {
        return mOauthId;
    }

    public void setOauthId(String oauthId) {
        mOauthId = oauthId;
    }
}
