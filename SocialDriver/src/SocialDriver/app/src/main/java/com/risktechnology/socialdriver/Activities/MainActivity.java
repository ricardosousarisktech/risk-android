package com.risktechnology.socialdriver.activities;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.application.SDApplication;
import com.risktechnology.socialdriver.core.BluetoothAccessManager;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.core.JourneyDetailsManager;
import com.risktechnology.socialdriver.core.JourneyManager;
import com.risktechnology.socialdriver.core.RegistrationIntentService;
import com.risktechnology.socialdriver.core.WifiAccessManager;
import com.risktechnology.socialdriver.fragments.DashboardFragment;
import com.risktechnology.socialdriver.fragments.JourneyListFragment;
import com.risktechnology.socialdriver.fragments.MyInformationFragment;
import com.risktechnology.socialdriver.fragments.NavigationDrawerFragment;
import com.risktechnology.socialdriver.fragments.SettingsFragment;
import com.risktechnology.socialdriver.listeners.OnFragmentInteractionListener;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.utilities.UserPreferences;

import junit.framework.Assert;

import java.util.Date;


public class MainActivity extends BaseActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, OnFragmentInteractionListener
{

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private static NavigationDrawerFragment sNavigationDrawerFragment;
    private static MainActivity sMainActivity;

    private long mLastSeconds;
    private final int kExitTimeDelay = 2;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private final String TAG = "MainActivity";

    public static MainActivity sharedInstance()
    {
        if (sMainActivity == null)
        {
            try
            {
                throw new Exception("MainActivity does not yet exist. Check that you are not trying to use this out of the main app flow");
            } catch (Exception e)
            {
                e.printStackTrace();
                Assert.assertNotNull(sMainActivity);
            }
        }
        return sMainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        JourneyManager.sharedManager().prepare();
        JourneyManager.sharedManager().setActivityContext(this);
        boolean autoJourneyEnabled = UserPreferences.getSharedInstance().getOptBoolForKey(Constants.SettingsKeyAutoJourneyDetect, true);
        if(autoJourneyEnabled)
        {
            JourneyManager.startMonitoring(true);
        }
        else
        {
            JourneyManager.stopMonitoring();
        }

        JourneyDetailsManager.sharedManager().getStatisticsForDate(new Date());
        sMainActivity = this;

        setContentView(R.layout.activity_main);

        sNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        sNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position)
    {
        // update the main content by replacing fragments
        Constants.PageIndexes index = Constants.PageIndexes.values()[position];
        switch (index)
        {
            case PAGE_INDEX_HOME:
                showDashboard();
                break;
            case PAGE_INDEX_JOURNEYS:
                showJourneyList();
                break;
            case PAGE_INDEX_SETTINGS:
                showSettings();
                break;
            case PAGE_INDEX_MY_INFORMATION:
                showMyInformation();
                break;
            case PAGE_INDEX_LOGOUT:
                logout();
                break;
            case PAGE_INDEX_COUNT:
                //This one does not but is here to satisfy the switch
                break;
        }
    }

    private void showMyInformation()
    {
        showFragment(new MyInformationFragment(), false, false);

    }

    private void showJourneyList()
    {
        showFragment(new JourneyListFragment(), false, false);
    }

    private void showDashboard()
    {
        showFragment(new DashboardFragment(), false, false);

    }

    public void showFragment(android.app.Fragment fragment, boolean addToBackStack)
    {
        showFragment(fragment, addToBackStack, false);
    }

    public void showFragment(android.app.Fragment fragment, boolean addToBackStack, boolean hideActionBar)
    {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        android.app.Fragment existingFragment = fragmentManager.findFragmentByTag(fragment.getClass().getName());
        if (existingFragment != null)
        {
            //fragment is already in the stack so ignore the change request
            return;
        }

        if (getActionBar() != null)
        {
            if (hideActionBar)
            {
                getActionBar().hide();
            }
            else
            {
                getActionBar().show();
            }
        }

        if (!addToBackStack)
        {
            fragmentManager.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        fragmentTransaction = fragmentTransaction.replace(R.id.container, fragment, fragment.getClass().getName());
        if (addToBackStack)
        {
            fragmentTransaction = fragmentTransaction.addToBackStack("backstack");
        }
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void logout()
    {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.section_title_logout)
                .setMessage(R.string.really_logout)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (!FacebookSdk.isInitialized())
                        {
                            FacebookSdk.sdkInitialize(SDApplication.getAppContext());
                        }
                        LoginManager.getInstance().logOut();
                        UserModel.getInstance().logout();
                        Intent intent = new Intent(MainActivity.this, FacebookLoginActivity.class);
                        MainActivity.this.startActivity(intent);
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();

    }

    public static NavigationDrawerFragment navigationDraw()
    {
        return sNavigationDrawerFragment;
    }

    private void showSettings()
    {
        showFragment(new SettingsFragment(), false, false);
    }

    public void onSectionAttached(int number)
    {

    }

    public void restoreActionBar()
    {
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (!sNavigationDrawerFragment.isDrawerOpen())
        {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }

    public void hideKeyboard()
    {
        View view = getCurrentFocus();
        if (view != null)
        {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            view.clearFocus();
        }
    }

    @Override
    public void onBackPressed()
    {

        hideKeyboard();

        if (navigationDraw().isDrawerOpen())
        {
            navigationDraw().closeDraw();
            return;
        }

        FragmentManager fragmentManager = getFragmentManager();

        if (fragmentManager.getBackStackEntryCount() > 0)
        {
            fragmentManager.popBackStack();
        }
        else
        {
            long seconds = System.currentTimeMillis() / 1000L;

            if (seconds - mLastSeconds <= kExitTimeDelay)
            {
                mLastSeconds = 0;
                cleanUp();
                finish();
                System.exit(0);
            }
            else
            {
                Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
                mLastSeconds = seconds;
            }
        }
    }

    private void cleanUp()
    {
        JourneyManager.sharedManager().shutdown();
        BluetoothAccessManager.sharedManager().shutdown();
        WifiAccessManager.sharedManager().shutdown();
    }

    public boolean isOpenAllowed()
    {

        return true;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {

//        if(BuildConfig.DEBUG)
//        {
//            return true;
//        }

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
