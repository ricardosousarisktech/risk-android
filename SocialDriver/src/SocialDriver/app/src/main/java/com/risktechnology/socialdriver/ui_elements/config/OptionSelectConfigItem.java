package com.risktechnology.socialdriver.ui_elements.config;

import android.content.Context;

/**
 * Created by Dev on 03/08/2015.
 */
public class OptionSelectConfigItem extends BaseConfigItem {

    public OptionSelectConfigItem(Context context, int iconResId, String configName) {
        setConfigName(configName);
        setIconResId(iconResId);
        setContext(context);
    }
}
