package com.risktechnology.socialdriver.fragments;

import android.animation.Animator;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.listeners.OnFragmentInteractionListener;


@SuppressWarnings("unused")
public class BaseFragment extends Fragment
{

    private OnFragmentInteractionListener mListener;
    private OnFragmentInteractionListener mFragmentListener; //used to allow a fragment to listen to the response

    public void setFragmentListener(OnFragmentInteractionListener fragmentListener)
    {
        mFragmentListener = fragmentListener;
    }

    public BaseFragment()
    {
    }

    public OnFragmentInteractionListener getListener()
    {
        return (mFragmentListener != null ? mFragmentListener : mListener);
    }

    public void setTitle(String title)
    {
        if (getActivity().getActionBar() != null)
        {
            getActivity().getActionBar().setTitle(title);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @SuppressWarnings("deprecation") // Used in API Level < 23
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Context context) // Used in API Level 23
    {
        super.onAttach(context);
        try
        {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
        mFragmentListener = null;
    }

    public void showLoadingDialog()
    {
        hideLoadingDialog();
    }

    public void showLoadingDialog(String message)
    {
        hideLoadingDialog();
    }

    public void hideLoadingDialog()
    {

    }

    public void hideKeyboard()
    {
        View view = this.getActivity().getCurrentFocus();
        if (view != null)
        {
            InputMethodManager inputManager = (InputMethodManager) this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
    {

        View view = getView();

        if (view != null)
        {
            if (enter)
            {
                view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }
            else
            {
                view.setLayerType(View.LAYER_TYPE_NONE, null);
            }
        }

        return super.onCreateAnimator(transit, enter, nextAnim);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        final View finalView = view;
        final Bundle finalSavedInstanceState = savedInstanceState;
        ViewTreeObserver observer = view.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            public void onGlobalLayout()
            {
                ViewTreeObserver observer = finalView.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                {
                    observer.removeOnGlobalLayoutListener(this);
                }
                else
                {
                    observer.removeGlobalOnLayoutListener(this);
                }

                onViewLaidOut(finalView, finalSavedInstanceState);
            }
        });
    }

    public void onViewLaidOut(View view, Bundle savedInstanceState)
    {

    }
}
