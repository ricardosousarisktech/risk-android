package com.risktechnology.socialdriver.adapters;

/**
 * Created by Dev on 03/08/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.WifiAccessManager;
import com.risktechnology.socialdriver.ui_elements.DimmableImageButton;
import com.risktechnology.socialdriver.ui_elements.view_holders.DeviceItemViewHolderItem;
import com.risktechnology.socialdriver.ui_elements.view_holders.TitleHeaderViewHolderItem;

public class WifiListAdapter extends BaseExpandableListAdapter
{

    private final String ALLOWED_WIFI_NETWORKS_HEADER = "Networks Used For Auto Journey";
    private final String BLOCKED_WIFI_NETWORKS_HEADER = "Known Wifi Networks";

    private final int ALLOWED_WIFI_NETWORKS_GROUP = 1;
    private final int BLOCKED_WIFI_NETWORKS_GROUP = 0;
    private final int WIFI_NETWORKS_GROUP_COUNT = 2;

    private Context mContext;

    public WifiListAdapter(Context context)
    {
        this.mContext = context;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
        if (groupPosition == ALLOWED_WIFI_NETWORKS_GROUP)
        {
            return WifiAccessManager.sharedManager().getAllowedWifiDevices().get(childPosition);
        }
        else if (groupPosition == BLOCKED_WIFI_NETWORKS_GROUP)
        {
            return WifiAccessManager.sharedManager().getBlockedWifiDevices().get(childPosition);
        }
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent)
    {

        DeviceItemViewHolderItem viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitem_wifi_item, parent, false);

            viewHolder = new DeviceItemViewHolderItem();
            viewHolder.setEntryLabel((TextView) convertView.findViewById(R.id.entryTextView));
            viewHolder.setSwapButton((DimmableImageButton) convertView.findViewById(R.id.swapButton));
            convertView.setTag(viewHolder);

        }
        else
        {
            viewHolder = (DeviceItemViewHolderItem) convertView.getTag();
        }

        String childItem = (String) getChild(groupPosition, childPosition);


        viewHolder.getEntryLabel().setText(childItem);

        if (groupPosition == ALLOWED_WIFI_NETWORKS_GROUP)
        {
            //noinspection deprecation
            viewHolder.getSwapButton().setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_remove_circle_black_36dp));
            //noinspection deprecation
            viewHolder.getSwapButton().setColorFilter(mContext.getResources().getColor(R.color.light_red));
        }
        else
        {
            //noinspection deprecation
            viewHolder.getSwapButton().setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_add_circle_black_36dp));
            //noinspection deprecation
            viewHolder.getSwapButton().setColorFilter(mContext.getResources().getColor(R.color.light_green));
        }

        viewHolder.getSwapButton().setTag(childItem);
        viewHolder.getSwapButton().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                WifiAccessManager.sharedManager().swapDevice((String) v.getTag());
                WifiListAdapter.this.notifyDataSetChanged();
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        if (groupPosition == ALLOWED_WIFI_NETWORKS_GROUP)
        {
            return WifiAccessManager.sharedManager().getAllowedWifiDevices().size();
        }
        else if (groupPosition == BLOCKED_WIFI_NETWORKS_GROUP)
        {
            return WifiAccessManager.sharedManager().getBlockedWifiDevices().size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        if (groupPosition == ALLOWED_WIFI_NETWORKS_GROUP)
        {
            return ALLOWED_WIFI_NETWORKS_HEADER;
        }
        else if (groupPosition == BLOCKED_WIFI_NETWORKS_GROUP)
        {
            return BLOCKED_WIFI_NETWORKS_HEADER;
        }
        return null;
    }

    @Override
    public int getGroupCount()
    {
        return WIFI_NETWORKS_GROUP_COUNT;
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent)
    {

        TitleHeaderViewHolderItem viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitemheader_settings_header, parent, false);

            viewHolder = new TitleHeaderViewHolderItem();

            viewHolder.setTitleLabel((TextView) convertView.findViewById(R.id.titleLabel));

            convertView.setTag(viewHolder);

        }
        else
        {
            viewHolder = (TitleHeaderViewHolderItem) convertView.getTag();
        }

        String headerTitle = (String) getGroup(groupPosition);
        viewHolder.getTitleLabel().setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }

}