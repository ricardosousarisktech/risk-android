package com.risktechnology.socialdriver.ui_elements;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by Matthew on 23/07/15.
 */
public class WhiteCircle extends RelativeLayout implements ViewTreeObserver.OnGlobalLayoutListener {

    private int mAspectRatioWidth;
    private int mAspectRatioHeight;

    public WhiteCircle(Context context) {
        super(context);
    }

    public WhiteCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        Init(context, attrs);
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public WhiteCircle(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        Init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void Init(Context context, AttributeSet attrs) {
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    private void Init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }


    @Override
    public void onGlobalLayout() {
        ViewGroup.LayoutParams layout = getLayoutParams();

        int width = getWidth();
        int height = getHeight();

        if(width < height)
        {
            width = height;
        }
        else
        {
            height = width;
        }

        layout.height = height;
        layout.width = width;

        setLayoutParams(layout);
    }
}
