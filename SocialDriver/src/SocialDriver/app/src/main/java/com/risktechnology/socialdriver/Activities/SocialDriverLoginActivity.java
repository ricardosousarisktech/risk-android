package com.risktechnology.socialdriver.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.models.requestModels.LoginRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.LoginRequest;
import com.risktechnology.socialdriver.ui_elements.DimmableButton;
import com.risktechnology.socialdriver.ui_elements.ProgressHUD;
import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

/**
 * Created by Dev on 22/07/2015.
 */
public class SocialDriverLoginActivity extends BaseActivity {


    DimmableButton mRegisterButton;
    DimmableButton mLoginbutton;
    EditText mEmailEditText;
    EditText mPasswordEditText;
    TextView mForgottenPasswordLink;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_social_driver_login_screen);

        mRegisterButton = (DimmableButton) findViewById(R.id.registerButton);
        mLoginbutton = (DimmableButton) findViewById(R.id.loginButton);
        mEmailEditText = (EditText) findViewById(R.id.emailTextField);
        mPasswordEditText = (EditText) findViewById(R.id.passwordTextField);
        mForgottenPasswordLink = (TextView) findViewById(R.id.forgottenPassword);

        mRegisterButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                loadRegistration();
            }
        });
        mLoginbutton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                login();
            }
        });
        mForgottenPasswordLink.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showForgottenPassword();
            }
        });

    }

    private void showForgottenPassword()
    {
        Intent intent = new Intent(SocialDriverLoginActivity.this, ForgottenPasswordActivity.class);
        SocialDriverLoginActivity.this.startActivity(intent);
    }

    private void loadRegistration() {
        Intent intent = new Intent(SocialDriverLoginActivity.this, SocialDriverRegisterActivity.class);
        SocialDriverLoginActivity.this.startActivity(intent);
    }

    private void login()
    {
        if(Helpers.isEmpty(mEmailEditText.getText().toString()))
        {
            Helpers.errorAlert(this, "Error", "Please enter your email address");
            return;
        }

        if(Helpers.isEmpty(mPasswordEditText.getText().toString()))
        {
            Helpers.errorAlert(this, "Error", "Please enter your password");
            return;
        }

        LoginRequestModel model = new LoginRequestModel();
        model.setUsername(mEmailEditText.getText().toString());
        model.setPassword(mPasswordEditText.getText().toString());

        final LoginRequest request = new LoginRequest(model);


        final ProgressHUD progressHUD = ProgressHUD.show(this, "Fetching user...", true, true, new DialogInterface.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialog)
            {
                request.cancelRequest();
            }
        });

        request.setRequestHandler(new BaseRequest.RequestHandler()
        {
            @Override
            public void requestFinished(BaseRequest request)
            {
                progressHUD.dismiss();
                if(request.getHTTPStatusCode() == 0)
                {
                    Helpers.showServerError(SocialDriverLoginActivity.this);
                    return;
                }

                if(request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                {
                    try
                    {
                        JSONObject userSocialDriver = request.getResponseObject().getJSONObject("UserSocialDriver");
                        UserModel.getInstance().setUserId(userSocialDriver.getInt("UserId"));
                        UserModel.getInstance().setSecretKey(userSocialDriver.getString("Secret"));
                        UserModel.getInstance().save();
                        launchMain();
                    } catch (JSONException e)
                    {
                        Helpers.errorAlert(SocialDriverLoginActivity.this, "Error", e.getLocalizedMessage());
                    }

                }
                else if(request.getErrorStatusCode() == Constants.LoginErrorCode.USER_INACTIVE)
                {
                    Helpers.errorAlert(SocialDriverLoginActivity.this, "Account inactive", "Please check your email account for your activate email.");
                }
                else {
                    Helpers.errorAlert(SocialDriverLoginActivity.this, "Error", request.getErrorStatusMessage());
                }
                Log.i("Login", "Request Finished");
            }
        });

        request.executeRequest(this);
    }

    private void launchMain()
    {
        UserModel.getInstance().download();
        if(UserModel.getInstance().requiresConfiguration())
        {
            launchMyInformation();
        }
        else
        {
            Intent intent = new Intent(SocialDriverLoginActivity.this, MainActivity.class);
            SocialDriverLoginActivity.this.startActivity(intent);
            SocialDriverLoginActivity.this.finish();
//        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }

    private void launchMyInformation()
    {
        Intent intent = new Intent(SocialDriverLoginActivity.this, MyInformationActivity.class);
        SocialDriverLoginActivity.this.startActivity(intent);
        SocialDriverLoginActivity.this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
