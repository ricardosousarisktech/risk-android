package com.risktechnology.socialdriver.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Dev on 10/09/2015.
 */
public class JourneyDetailsModel extends BaseModel
{
    private ArrayList<JourneyEventModel> mEvents;
    private Double mDistanceTravelled;
    private String mDistanceUnits;
    private Integer mRiskScore;
    private Integer mAccelScore;
    private Integer mDecelScore;
    private Integer mSpeedScore;
    private String mIdlingtime;
    private String mStartAddress;
    private String mEndAddress;
    private Date mStartTime;
    private Date mEndTime;
    private String mDuration;



    public void populateFromJSON(JSONObject object)
    {
        mEvents = new ArrayList<>();

        try
        {
            mDistanceTravelled = object.getDouble("DistanceTravelled");
            mDistanceUnits = object.getString("DistanceUnits");
            mRiskScore = object.getInt("RiskScore");
            mAccelScore = object.getInt("AccelScore");
            mDecelScore = object.getInt("DecelScore");
            mSpeedScore = object.getInt("SpeedScore");
            mIdlingtime = object.getString("IdlingTime");
            mStartAddress = object.getString("StartAddress");
            mEndAddress = object.getString("EndAddress");

            String startTimeString = object.getString("StartTime");
            String endTimeString = object.getString("EndTime");

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
            format.setTimeZone(TimeZone.getTimeZone("UTC"));


            mStartTime = format.parse(startTimeString);
            mEndTime = format.parse(endTimeString);

            mDuration = object.getString("Duration");
        } catch (JSONException e)
        {
            e.printStackTrace();
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property 'events'.
     *
     * @return Value for property 'events'.
     */
    public ArrayList<JourneyEventModel> getEvents()
    {
        return mEvents;
    }

    /**
     * Setter for property 'events'.
     *
     * @param events Value to set for property 'events'.
     */
    public void setEvents(ArrayList<JourneyEventModel> events)
    {
        mEvents = events;
    }

    /**
     * Getter for property 'distanceTravelled'.
     *
     * @return Value for property 'distanceTravelled'.
     */
    public Double getDistanceTravelled()
    {
        return mDistanceTravelled;
    }

    /**
     * Setter for property 'distanceTravelled'.
     *
     * @param distanceTravelled Value to set for property 'distanceTravelled'.
     */
    public void setDistanceTravelled(Double distanceTravelled)
    {
        mDistanceTravelled = distanceTravelled;
    }

    /**
     * Getter for property 'distanceUnits'.
     *
     * @return Value for property 'distanceUnits'.
     */
    public String getDistanceUnits()
    {
        return mDistanceUnits;
    }

    /**
     * Setter for property 'distanceUnits'.
     *
     * @param distanceUnits Value to set for property 'distanceUnits'.
     */
    public void setDistanceUnits(String distanceUnits)
    {
        mDistanceUnits = distanceUnits;
    }

    /**
     * Getter for property 'riskScore'.
     *
     * @return Value for property 'riskScore'.
     */
    public Integer getRiskScore()
    {
        return mRiskScore;
    }

    /**
     * Setter for property 'riskScore'.
     *
     * @param riskScore Value to set for property 'riskScore'.
     */
    public void setRiskScore(Integer riskScore)
    {
        mRiskScore = riskScore;
    }

    /**
     * Getter for property 'accelScore'.
     *
     * @return Value for property 'accelScore'.
     */
    public Integer getAccelScore()
    {
        return mAccelScore;
    }

    /**
     * Setter for property 'accelScore'.
     *
     * @param accelScore Value to set for property 'accelScore'.
     */
    public void setAccelScore(Integer accelScore)
    {
        mAccelScore = accelScore;
    }

    /**
     * Getter for property 'decelScore'.
     *
     * @return Value for property 'decelScore'.
     */
    public Integer getDecelScore()
    {
        return mDecelScore;
    }

    /**
     * Setter for property 'decelScore'.
     *
     * @param decelScore Value to set for property 'decelScore'.
     */
    public void setDecelScore(Integer decelScore)
    {
        mDecelScore = decelScore;
    }

    /**
     * Getter for property 'speedScore'.
     *
     * @return Value for property 'speedScore'.
     */
    public Integer getSpeedScore()
    {
        return mSpeedScore;
    }

    /**
     * Setter for property 'speedScore'.
     *
     * @param speedScore Value to set for property 'speedScore'.
     */
    public void setSpeedScore(Integer speedScore)
    {
        mSpeedScore = speedScore;
    }

    /**
     * Getter for property 'idlingtime'.
     *
     * @return Value for property 'idlingtime'.
     */
    public String getIdlingtime()
    {
        return mIdlingtime;
    }

    /**
     * Setter for property 'idlingtime'.
     *
     * @param idlingtime Value to set for property 'idlingtime'.
     */
    public void setIdlingtime(String idlingtime)
    {
        mIdlingtime = idlingtime;
    }

    /**
     * Getter for property 'startAddress'.
     *
     * @return Value for property 'startAddress'.
     */
    public String getStartAddress()
    {
        return mStartAddress;
    }

    public String getFormattedStartAddress() {
        return getStartAddress().replace(",","\n");
    }

    /**
     * Setter for property 'startAddress'.
     *
     * @param startAddress Value to set for property 'startAddress'.
     */
    public void setStartAddress(String startAddress)
    {
        mStartAddress = startAddress;
    }

    /**
     * Getter for property 'endAddress'.
     *
     * @return Value for property 'endAddress'.
     */
    public String getEndAddress()
    {
        return mEndAddress;
    }

    public String getFormattedEndAddress() {
        return getEndAddress().replace(",","\n");
    }

    /**
     * Setter for property 'endAddress'.
     *
     * @param endAddress Value to set for property 'endAddress'.
     */
    public void setEndAddress(String endAddress)
    {
        mEndAddress = endAddress;
    }

    /**
     * Getter for property 'startTime'.
     *
     * @return Value for property 'startTime'.
     */
    public Date getStartTime()
    {
        return mStartTime;
    }

    /**
     * Setter for property 'startTime'.
     *
     * @param startTime Value to set for property 'startTime'.
     */
    public void setStartTime(Date startTime)
    {
        mStartTime = startTime;
    }

    /**
     * Getter for property 'endTime'.
     *
     * @return Value for property 'endTime'.
     */
    public Date getEndTime()
    {
        return mEndTime;
    }

    /**
     * Setter for property 'endTime'.
     *
     * @param endTime Value to set for property 'endTime'.
     */
    public void setEndTime(Date endTime)
    {
        mEndTime = endTime;
    }

    /**
     * Getter for property 'duration'.
     *
     * @return Value for property 'duration'.
     */
    public String getDuration()
    {
        return mDuration;
    }

    /**
     * Setter for property 'duration'.
     *
     * @param duration Value to set for property 'duration'.
     */
    public void setDuration(String duration)
    {
        mDuration = duration;
    }

    public String getReadableDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy", Locale.getDefault());
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(mStartTime);
    }


    public String getStyledReadableDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("MMMM '<b>'HH:mm'</b>'", Locale.getDefault());
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String monthYearTime = formatter.format(mStartTime);
        formatter = new SimpleDateFormat("d", Locale.getDefault());
        String day = formatter.format(mStartTime);
        day = ordinal_suffix_of(Integer.parseInt(day));
        return day + " " + monthYearTime;
    }

    public String ordinal_suffix_of(int i) {
        int j = i % 10,
                k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }
}
