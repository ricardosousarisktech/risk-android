package com.risktechnology.socialdriver.adapters;

/**
 * Created by Dev on 03/08/2015.
 */

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.BluetoothAccessManager;
import com.risktechnology.socialdriver.ui_elements.DimmableImageButton;
import com.risktechnology.socialdriver.ui_elements.view_holders.DeviceItemViewHolderItem;
import com.risktechnology.socialdriver.ui_elements.view_holders.TitleHeaderViewHolderItem;

public class BluetoothListAdapter extends BaseExpandableListAdapter
{

    private final String ALLOWED_BLUETOOTH_DEVICES_HEADER = "Devices Used For Auto Journey";
    private final String BLOCKED_BLUETOOTH_DEVICES_HEADER = "Known Bluetooth Devices";

    private final int ALLOWED_BLUETOOTH_DEVICES_GROUP = 1;
    private final int BLOCKED_BLUETOOTH_DEVICES_GROUP = 0;
    private final int BLUETOOTH_DEVICES_GROUP_COUNT = 2;

    private Context mContext;

    public BluetoothListAdapter(Context context)
    {
        this.mContext = context;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
        if (groupPosition == ALLOWED_BLUETOOTH_DEVICES_GROUP)
        {
            return BluetoothAccessManager.sharedManager().getAllowedBluetoothDevices().get(childPosition);
        }
        else if (groupPosition == BLOCKED_BLUETOOTH_DEVICES_GROUP)
        {
            return BluetoothAccessManager.sharedManager().getBlockedBluetoothDevices().get(childPosition);
        }
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent)
    {

        DeviceItemViewHolderItem viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitem_bluetooth_item, parent, false);

            viewHolder = new DeviceItemViewHolderItem();
            viewHolder.setEntryLabel((TextView) convertView.findViewById(R.id.entryTextView));
            viewHolder.setSwapButton((DimmableImageButton) convertView.findViewById(R.id.swapButton));
            convertView.setTag(viewHolder);

        }
        else
        {
            viewHolder = (DeviceItemViewHolderItem) convertView.getTag();
        }

        BluetoothDevice childItem = (BluetoothDevice) getChild(groupPosition, childPosition);

        viewHolder.getEntryLabel().setText(childItem.getName());

        if(groupPosition == ALLOWED_BLUETOOTH_DEVICES_GROUP)
        {
            viewHolder.getSwapButton().setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_remove_circle_black_36dp));
            viewHolder.getSwapButton().setColorFilter(mContext.getResources().getColor(R.color.light_red));
        }
        else
        {
            viewHolder.getSwapButton().setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_add_circle_black_36dp));
            viewHolder.getSwapButton().setColorFilter(mContext.getResources().getColor(R.color.light_green));
        }

        viewHolder.getSwapButton().setTag(childItem);
        viewHolder.getSwapButton().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                BluetoothAccessManager.sharedManager().swapDevice((BluetoothDevice)v.getTag());
                BluetoothListAdapter.this.notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        if (groupPosition == ALLOWED_BLUETOOTH_DEVICES_GROUP)
        {
            return BluetoothAccessManager.sharedManager().getAllowedBluetoothDevices().size();
        }
        else if (groupPosition == BLOCKED_BLUETOOTH_DEVICES_GROUP)
        {
            return BluetoothAccessManager.sharedManager().getBlockedBluetoothDevices().size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        if (groupPosition == ALLOWED_BLUETOOTH_DEVICES_GROUP)
        {
            return ALLOWED_BLUETOOTH_DEVICES_HEADER;
        }
        else if (groupPosition == BLOCKED_BLUETOOTH_DEVICES_GROUP)
        {
            return BLOCKED_BLUETOOTH_DEVICES_HEADER;
        }
        return null;
    }

    @Override
    public int getGroupCount()
    {
        return BLUETOOTH_DEVICES_GROUP_COUNT;
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent)
    {

        TitleHeaderViewHolderItem viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitemheader_settings_header, parent, false);

            viewHolder = new TitleHeaderViewHolderItem();

            viewHolder.setTitleLabel((TextView) convertView.findViewById(R.id.titleLabel));

            convertView.setTag(viewHolder);

        }
        else
        {
            viewHolder = (TitleHeaderViewHolderItem) convertView.getTag();
        }


        String headerTitle = (String) getGroup(groupPosition);
        viewHolder.getTitleLabel().setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }

}

