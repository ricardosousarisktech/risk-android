package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


@SuppressWarnings("unused")
public class ProcessJourneyRequestModel extends BaseRequestModel
{
    private Integer mUserId;
    private ArrayList<MobileTelemetryRequestModel> mMobileTelemetry;
    private Integer mMinsToTrim;

    public ProcessJourneyRequestModel()
    {
        mUserId = 0;
        mMobileTelemetry = new ArrayList<>();
        mMinsToTrim = 5;
    }

    /**
     * Getter for property 'userId'.
     *
     * @return Value for property 'userId'.
     */
    public Integer getUserId()
    {
        return mUserId;
    }

    /**
     * Setter for property 'userId'.
     *
     * @param userId Value to set for property 'userId'.
     */
    public void setUserId(Integer userId)
    {
        mUserId = userId;
    }

    /**
     * Getter for property 'mobileTelemetry'.
     *
     * @return Value for property 'mobileTelemetry'.
     */
    public ArrayList<MobileTelemetryRequestModel> getMobileTelemetry()
    {
        return mMobileTelemetry;
    }

    /**
     * Setter for property 'mobileTelemetry'.
     *
     * @param mobileTelemetry Value to set for property 'mobileTelemetry'.
     */
    public void setMobileTelemetry(ArrayList<MobileTelemetryRequestModel> mobileTelemetry)
    {
        mMobileTelemetry = mobileTelemetry;
    }

    /**
     * Getter for property 'minsToTrim'.
     *
     * @return Value for property 'minsToTrim'.
     */
    public Integer getMinsToTrim()
    {
        return mMinsToTrim;
    }

    /**
     * Setter for property 'minsToTrim'.
     *
     * @param minsToTrim Value to set for property 'minsToTrim'.
     */
    public void setMinsToTrim(Integer minsToTrim)
    {
        mMinsToTrim = minsToTrim;
    }

    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException
    {
        jsonObject.put("MinsToTrim", mMinsToTrim);

        JSONArray mobileTelemetryArray = new JSONArray();
        for(MobileTelemetryRequestModel mobileTelemetryModel : mMobileTelemetry)
        {
            JSONObject mobileTelemetryJSON = new JSONObject();
            mobileTelemetryModel.addToJSONObject(mobileTelemetryJSON);
            mobileTelemetryArray.put(mobileTelemetryJSON);
        }

        jsonObject.put("MobileTelemetry", mobileTelemetryArray);

        if(mUserId > 0)
        {
            jsonObject.put("UserId", mUserId);
        }
    }

    @Override
    public void describeModel()
    {

    }
}
