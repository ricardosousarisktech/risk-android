package com.risktechnology.socialdriver.ui_elements.view_holders;

import android.widget.TextView;

public class NavigationMenuViewHolderItem {
    // public ImageView imageView;
    private TextView textView;

    /**
     * Getter for property 'textView'.
     *
     * @return Value for property 'textView'.
     */
    public TextView getTextView()
    {
        return textView;
    }

    /**
     * Setter for property 'textView'.
     *
     * @param textView Value to set for property 'textView'.
     */
    public void setTextView(TextView textView)
    {
        this.textView = textView;
    }
}
