package com.risktechnology.socialdriver.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.risk.socialdriver.journeyapp.events.JourneyStartEvent;
import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.core.JourneyDetailsManager;
import com.risktechnology.socialdriver.core.JourneyManager;
import com.risktechnology.socialdriver.events.BluetoothConnectionChangedEvent;
import com.risktechnology.socialdriver.events.JourneyDetailsUpdatedEvent;
import com.risktechnology.socialdriver.events.UserDetailsUpdatedEvent;
import com.risktechnology.socialdriver.events.WifiConnectionChangedEvent;
import com.risktechnology.socialdriver.models.JourneyStatisticsModel;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.ui_elements.MonthYearPickerDialogFragment;
import com.risktechnology.socialdriver.ui_elements.WhiteCircle;
import com.risktechnology.socialdriver.ui_elements.dash_wheel.DashWheel;
import com.risktechnology.socialdriver.ui_elements.dash_wheel.DashWheelCallback;
import com.risktechnology.socialdriver.ui_elements.dash_wheel.DashWheelSegment;
import com.risktechnology.socialdriver.utilities.UiUtilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.greenrobot.event.EventBus;

/**
 * Created by Matthew on 23/07/15.
 */
public class DashboardFragment extends BaseFragment implements DashWheelCallback
{
    TextView mDateLabel;
    TextView mInfoLabel;
    TextView mPointsLabel;
    TextView mYourScoreLabel;
    TextView mActionLabel;
    WhiteCircle mScoreHolder;
    DashWheel mDashWheel;

    int kNumberOfSegments = Constants.DashWheelSegments.SEGMENT_COUNT.ordinal();
    private Date mCurrentDate;
    private Runnable mJourneyTextUpdateTask;
    private Handler mJourneyUpdateTextHandler;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        setTitle("Dashboard");

        EventBus bus = EventBus.getDefault();
        bus.register(this, -1);

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        mScoreHolder = (WhiteCircle) view.findViewById(R.id.scoreHolder);
        mDateLabel = (TextView) view.findViewById(R.id.dateLabel);
        mInfoLabel = (TextView) view.findViewById(R.id.infoLabel);
        mActionLabel = (TextView) view.findViewById(R.id.actionLabel);
        mPointsLabel = (TextView) view.findViewById(R.id.pointsLabel);
        mYourScoreLabel = (TextView) view.findViewById(R.id.yourScoreLabel);
        mDashWheel = (DashWheel) view.findViewById(R.id.dashWheel);


        UiUtilities.setMultifontStringOnTextView(mPointsLabel, getActivity());
        UiUtilities.setMultifontStringOnTextView(mYourScoreLabel, getActivity());

        mCurrentDate = new Date();

        UiUtilities.setMultifontStringOnTextView(mInfoLabel, getActivity().getApplicationContext());
        UiUtilities.setDefaultFontStringOnTextView(mInfoLabel, getActivity().getApplicationContext());
        UiUtilities.setMultifontStringOnTextView(mActionLabel, getActivity().getApplicationContext());

        mDashWheel.setDashWheelCallback(this);

        setupDateSelector();

        JourneyDetailsManager.sharedManager().getStatisticsForDate(mCurrentDate, true, getActivity());

        return view;
    }

    @Override
    public void onViewLaidOut(View view, Bundle savedInstanceState)
    {
        super.onViewLaidOut(view, savedInstanceState);


        if (kNumberOfSegments > 0)
        {
            int segmentSize = 360 / kNumberOfSegments;
            int startPosition = 0;

            for (int i = 0; i < kNumberOfSegments; i++)
            {
                int icon = R.drawable.car_icon;
                String segmentName = "Unknown";
                String infoText = "Unknown";

                Constants.DashWheelSegments dashWheelSegment = Constants.DashWheelSegments.values()[i];

                switch (dashWheelSegment)
                {
                    case SEGMENT_CAR:
                    icon = R.drawable.car_icon;
                    segmentName = "CarType";
                    infoText = "<b>Information</b> not available for <b>Car Type</b>";
                    break;
                    case SEGMENT_AGE:
                        icon = R.drawable.age_icon;
                        segmentName = "Age";
                        infoText = "<b>Information</b>  not available for <b>Age</b>";
                        break;
                    case SEGMENT_LOCATION:
                        icon = R.drawable.journeys_icon;
                        segmentName = "Location";
                        infoText = "<b>Information</b>  not available for <b>Location</b>";
                        break;
                    case SEGMENT_GENDER:
                        icon = R.drawable.my_information;
                        segmentName = "Gender";
                        infoText = "<b>Information</b>  not available for <b>Gender</b>";
                        break;
                    default:
                        //should never reach here, only here to satisfy switch
                        break;
                }

                mDashWheel.invalidate();

                mDashWheel.addSegment(startPosition, segmentSize, icon, segmentName, infoText, dashWheelSegment);
                startPosition += segmentSize;
            }
        }

        updateUI();
    }

    private void setupDateSelector()
    {
        mDateLabel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                MonthYearPickerDialogFragment fragment = new MonthYearPickerDialogFragment();

                Bundle args = new Bundle();
                args.putString(SpinnerDialogFragment.KEY_TITLE, "Select Date");
                args.putLong(SpinnerDialogFragment.KEY_CURRENT_VALUE, mCurrentDate.getTime());
                fragment.setArguments(args);
                fragment.setAlertResponseHandler(new MonthYearPickerDialogFragment.AlertResponseHandler()
                {
                    @Override
                    public void buttonPressed(MonthYearPickerDialogFragment alert)
                    {
                        if (!alert.wasCancelPressed())
                        {
                            Calendar calendar = alert.getCalendar();
                            mCurrentDate = calendar.getTime();
                            UserModel.getInstance().downloadDemographics(mCurrentDate, true, getActivity());
                            JourneyDetailsManager.sharedManager().getStatisticsForDate(mCurrentDate, true, getActivity());
                        }
                    }
                });
                fragment.show(getFragmentManager(), "spinner");
            }
        });
    }


    @Override
    public void onResume()
    {
        super.onResume();
        mDashWheel.updateUI();
        updateJourneyText();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if(mJourneyTextUpdateTask != null && mJourneyUpdateTextHandler != null)
        {
            //cancel updating is screen hidden
            mJourneyUpdateTextHandler.removeCallbacks(mJourneyTextUpdateTask);
        }
    }

    @Override
    public void segmentSelected(DashWheelSegment segment)
    {

        final DashWheelSegment fSegment = segment;

        ObjectAnimator fadeOutAnimator = ObjectAnimator.ofFloat(mInfoLabel, "alpha", mInfoLabel.getAlpha(), 0.0f);
        fadeOutAnimator.setDuration(200);
        fadeOutAnimator.addListener(new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animation)
            {

            }

            @Override
            public void onAnimationEnd(Animator animation)
            {
                String text = fSegment.getName();
                String infoText = fSegment.getInfoText();
                switch (text)
                {
                    case "Age":
                        int ageDemographic = UserModel.getInstance().getAgeDemographic();
                        if (ageDemographic > -1)
                        {
                            infoText = "<b>" + ageDemographic + "%</b> of users are in your <b>age group</b>";
                        }
                        break;
                    case "CarType":
                        int carTypeDemographic = UserModel.getInstance().getCarTypeDemographic();
                        if (carTypeDemographic > -1)
                        {
                            infoText = "<b>" + carTypeDemographic + "%</b> of users have your <b>car type</b>";
                        }
                        break;
                    case "Gender":
                        int genderDemographic = UserModel.getInstance().getGenderDemographic();
                        if (genderDemographic > -1)
                        {
                            infoText = "<b>" + genderDemographic + "%</b> of users are the same <b>gender</b> as you";
                        }
                        break;
                    case "Location":
                        int locationDemographic = UserModel.getInstance().getLocationDemographic();
                        if (locationDemographic > -1)
                        {
                            infoText = "<b>" + locationDemographic + "%</b> of users are in your <b>location</b>";
                        }
                        break;
                    default:
                        break;
                }
                mInfoLabel.setText(infoText);
                UiUtilities.setMultifontStringOnTextView(mInfoLabel, getActivity().getApplicationContext());
                ObjectAnimator fadeInAnimator = ObjectAnimator.ofFloat(mInfoLabel, "alpha", mInfoLabel.getAlpha(), 1.0f);
                fadeInAnimator.setDuration(250);
                fadeInAnimator.start();
            }

            @Override
            public void onAnimationCancel(Animator animation)
            {

            }

            @Override
            public void onAnimationRepeat(Animator animation)
            {

            }
        });
        fadeOutAnimator.start();
    }

    @Override
    public void onDetach()
    {
        EventBus.getDefault().unregister(this);
        mDashWheel.setDashWheelCallback(null);
        mDateLabel = null;
        mInfoLabel = null;
        mPointsLabel = null;
        mYourScoreLabel = null;
        mActionLabel = null;
        mScoreHolder = null;
        mDashWheel = null;
        super.onDetach();
    }


    @SuppressWarnings("unused")
    public void onEvent(UserDetailsUpdatedEvent event)
    {
        updateUI();
        mDashWheel.updateUI();
    }

    @SuppressWarnings("unused")
    public void onEvent(JourneyDetailsUpdatedEvent event)
    {
        mDashWheel.updateUI();
        updateUI();
    }

    @SuppressWarnings("unused")
    public void onEvent(JourneyStartEvent event)
    {
        mDashWheel.updateUI();
        updateUI();
        updateJourneyText();
    }

    @SuppressWarnings("unused")
    public void onEvent(BluetoothConnectionChangedEvent event)
    {
        mDashWheel.updateUI();
    }

    @SuppressWarnings("unused")
    public void onEvent(WifiConnectionChangedEvent event)
    {
        mDashWheel.updateUI();
    }

    private void updateUI()
    {
        JourneyStatisticsModel journeyStatisticsModel = JourneyDetailsManager.sharedManager().getJourneyStatisticsModel();
        mPointsLabel.setText(String.format("%d", journeyStatisticsModel.getMonthlyScore()));

        String formattedDate = new SimpleDateFormat("MMMM yyyy", Locale.getDefault()).format(mCurrentDate);
        mDateLabel.setText(String.format("<b>%s<b>", formattedDate));
        UiUtilities.setMultifontStringOnTextView(mDateLabel, getActivity().getApplicationContext());

        UiUtilities.setMultifontStringOnTextView(mActionLabel, getActivity().getApplicationContext());
        updateJourneyText();
        mDashWheel.updateUI();
    }

    private void updateJourneyText()
    {
        if(mJourneyTextUpdateTask != null && mJourneyUpdateTextHandler != null)
        {
            //prevent duplicate calls from stacking
            mJourneyUpdateTextHandler.removeCallbacks(mJourneyTextUpdateTask);
        }


        if (JourneyManager.isRunning() && JourneyManager.sharedManager().getStartTime() != 0)
        {
            Date currentDate = new Date();
            long currentSeconds = currentDate.getTime();

            long millisecondsDiff = currentSeconds - JourneyManager.sharedManager().getStartTime();

            int secondsDiff = Math.round(millisecondsDiff / 1000);

            int hours = (int) Math.floor(secondsDiff / 60 / 60);
            int minutes = (secondsDiff / 60 ) - (hours * 60);
            int seconds = secondsDiff - (minutes * 60);

            mActionLabel.setText(String.format("JOURNEY RUNNING: <b>%02d:%02d:%02d</b>", hours, minutes, seconds));
        }
        else if(JourneyManager.isInAutoMode())
        {
            mActionLabel.setText("AUTO JOURNEY DETECTION IS <b>RUNNING</b>");
        }
        else
        {
            mActionLabel.setText("PRESS <b>PLAY</b> TO START YOUR JOURNEY");
        }
        UiUtilities.setMultifontStringOnTextView(mActionLabel, getActivity().getApplicationContext());

        if(mJourneyTextUpdateTask == null)
        {
            mJourneyTextUpdateTask = new Runnable()
            {
                public void run()
                {
                    updateJourneyText();
                }
            };
        }
        if(mJourneyUpdateTextHandler == null)
        {
            mJourneyUpdateTextHandler = new Handler();
        }

        mJourneyUpdateTextHandler.postDelayed(mJourneyTextUpdateTask, 1000);
    }
}
