package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.Logger;

import org.json.JSONException;
import org.json.JSONObject;


public class ForgottenPasswordRequestModel extends BaseRequestModel
{

    private String mEmail;

    public ForgottenPasswordRequestModel() {
        mEmail = "";
    }

    public void describeModel() {
    }

    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException {

        jsonObject.put("AppID", Constants.APP_ID);

        if(!Helpers.isEmpty(mEmail))
        {
            jsonObject.put("Username", mEmail);
        }
    }

    /**
     * Getter for property 'email'.
     *
     * @return Value for property 'email'.
     */
    public String getEmail()
    {
        return mEmail;
    }

    /**
     * Setter for property 'email'.
     *
     * @param email Value to set for property 'email'.
     */
    public void setEmail(String email)
    {
        mEmail = email;
    }
}
