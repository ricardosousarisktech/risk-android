package com.risktechnology.socialdriver.ui_elements.dash_wheel;

/**
 * Created by Dev on 24/07/2015.
 */

public interface DashWheelCallback {
    void segmentSelected(DashWheelSegment segment);
}
