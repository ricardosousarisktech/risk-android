package com.risktechnology.socialdriver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.adapters.WifiListAdapter;
import com.risktechnology.socialdriver.core.BluetoothAccessManager;
import com.risktechnology.socialdriver.core.WifiAccessManager;
import com.risktechnology.socialdriver.ui_elements.DimmableImageButton;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Dev on 25/08/2015.
 */
public class WifiNetworksFragment extends BaseFragment
{
    private View mRootView;
    private ExpandableListView mWifiListView;
    private WifiListAdapter mWifiListAdapter;
    private DimmableImageButton mRefreshButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        setTitle("Wifi Networks");

        WifiAccessManager.sharedManager().refreshDevices();

        if (mRootView == null)
        {
            mRootView = inflater.inflate(R.layout.fragment_wifi_networks, container, false);
            mWifiListView = (ExpandableListView) mRootView.findViewById(R.id.wifiListView);

            mRefreshButton = (DimmableImageButton) mRootView.findViewById(R.id.refreshButton);
            mRefreshButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    refresh();
                }
            });

            mWifiListAdapter = new WifiListAdapter(getActivity());

            mWifiListView.setAdapter(mWifiListAdapter);
            mWifiListView.setGroupIndicator(null);
            openGroups();
        }

        return mRootView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        WifiAccessManager.sharedManager().refreshDevices();
        if(mWifiListAdapter != null)
        {
            mWifiListAdapter.notifyDataSetChanged();
        }
    }

    public void openGroups()
    {
        for(int i = 0; i < mWifiListAdapter.getGroupCount(); i++)
        {
            mWifiListView.expandGroup(i);
        }

        mWifiListView.setGroupIndicator(null);
        mWifiListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener()
        {
            @Override
            public void onGroupCollapse(int i)
            {
                mWifiListView.expandGroup(i);
            }
        });
    }

    public void refresh()
    {
        WifiAccessManager.sharedManager().refreshDevices();
        mWifiListAdapter.notifyDataSetChanged();
    }
}
