package com.risktechnology.socialdriver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.activities.MainActivity;
import com.risktechnology.socialdriver.adapters.BluetoothListAdapter;
import com.risktechnology.socialdriver.core.BluetoothAccessManager;
import com.risktechnology.socialdriver.ui_elements.DimmableImageButton;

/**
 * Created by Dev on 25/08/2015.
 */
public class BluetoothDevicesFragment extends BaseFragment
{

    private View mRootView;
    private ExpandableListView mBluetoothListView;
    private BluetoothListAdapter mBluetoothListAdapter;
    private DimmableImageButton mRefreshButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        setTitle("Bluetooth Devices");

        BluetoothAccessManager.sharedManager().refreshDevices();

        if (mRootView == null)
        {
            mRootView = inflater.inflate(R.layout.fragment_bluetooth_devices, container, false);
            mBluetoothListView = (ExpandableListView) mRootView.findViewById(R.id.bluetoothListView);
            mBluetoothListAdapter = new BluetoothListAdapter(getActivity());

            mRefreshButton = (DimmableImageButton) mRootView.findViewById(R.id.refreshButton);
            mRefreshButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    refresh();
                }
            });

            mBluetoothListView.setAdapter(mBluetoothListAdapter);
            mBluetoothListView.setGroupIndicator(null);
            openGroups();
        }

        return mRootView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        BluetoothAccessManager.sharedManager().refreshDevices();
        if(mBluetoothListAdapter != null)
        {
            mBluetoothListAdapter.notifyDataSetChanged();
        }
    }

    public void openGroups()
    {
        for (int i = 0; i < mBluetoothListAdapter.getGroupCount(); i++)
        {
            mBluetoothListView.expandGroup(i);
        }

        mBluetoothListView.setGroupIndicator(null);
        mBluetoothListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener()
        {
            @Override
            public void onGroupCollapse(int i)
            {
                mBluetoothListView.expandGroup(i);
            }
        });
    }

    public void refresh()
    {
        BluetoothAccessManager.sharedManager().refreshDevices();
        mBluetoothListAdapter.notifyDataSetChanged();
    }

}
