package com.risktechnology.socialdriver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.facebook.Profile;
import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.models.requestModels.CreateUserRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.CreateUserRequest;
import com.risktechnology.socialdriver.ui_elements.DimmableButton;
import com.risktechnology.socialdriver.ui_elements.ProgressHUD;
import com.risktechnology.socialdriver.utilities.Helpers;

/**
 * Created by Dev on 22/07/2015.
 */
public class SocialDriverRegisterActivity extends BaseActivity implements View.OnClickListener
{

    private DimmableButton mJoinButton;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private EditText mConfirmPasswordEditText;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_social_driver_register_screen);

        mJoinButton = (DimmableButton) findViewById(R.id.joinButton);

        mJoinButton.setOnClickListener(this);
        mEmailEditText = (EditText) findViewById(R.id.emailTextField);
        mPasswordEditText = (EditText) findViewById(R.id.passwordTextField);
        mConfirmPasswordEditText = (EditText) findViewById(R.id.confirmPasswordTextField);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v)
    {
        if(v == mJoinButton)
        {
            if(!Helpers.isEmailValid(mEmailEditText.getText().toString()))
            {
                Helpers.errorAlert(this, "Error", "Please enter your email address");
                return;
            }

            if(!Helpers.isPasswordValid(mPasswordEditText.getText().toString()))
            {

                Helpers.errorAlert(this, "Error", "Please enter your password");
                return;
            }

            if(Helpers.isEmpty(mConfirmPasswordEditText.getText().toString()))
            {
                Helpers.errorAlert(this, "Error", "Please confirm your password");
                return;
            }

            if(!mPasswordEditText.getText().toString().equals(mConfirmPasswordEditText.getText().toString()))
            {
                Helpers.errorAlert(this, "Error", "Your passwords do not match");
                return;
            }

            CreateUserRequestModel createUserRequestModel = new CreateUserRequestModel();
            createUserRequestModel.setUsername(mEmailEditText.getText().toString());
            createUserRequestModel.setPassword(mPasswordEditText.getText().toString());

            CreateUserRequest request = new CreateUserRequest(createUserRequestModel);
            final ProgressHUD hud = ProgressHUD.show(this, "Creating account", true, false, null);
            request.setRequestHandler(new BaseRequest.RequestHandler()
            {
                @Override
                public void requestFinished(BaseRequest request)
                {
                    hud.dismiss();
                    if(request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                    {
                        launchMain();
                    }
                    else
                    {
                        Helpers.errorAlert(SocialDriverRegisterActivity.this, "Error", request.getErrorStatusMessage());
                    }
                }
            });

            request.executeRequest(this);
        }
    }

    private void launchMain()
    {
        UserModel.getInstance().download();
        if(UserModel.getInstance().requiresConfiguration())
        {
            launchMyInformation();
        }
        else
        {
            Intent intent = new Intent(SocialDriverRegisterActivity.this, MainActivity.class);
            SocialDriverRegisterActivity.this.startActivity(intent);
            SocialDriverRegisterActivity.this.finish();
//        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }

    private void launchMyInformation()
    {
        Intent intent = new Intent(SocialDriverRegisterActivity.this, MyInformationActivity.class);
        SocialDriverRegisterActivity.this.startActivity(intent);
        SocialDriverRegisterActivity.this.finish();
    }
}