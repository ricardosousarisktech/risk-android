package com.risktechnology.socialdriver.ui_elements.view_holders;

import android.widget.TextView;

import com.risktechnology.socialdriver.ui_elements.DimmableImageButton;

public class DeviceItemViewHolderItem
{
    private TextView mEntryLabel;
    private DimmableImageButton mSwapButton;

    /**
     * Getter for property 'entryLabel'.
     *
     * @return Value for property 'entryLabel'.
     */
    public TextView getEntryLabel()
    {
        return mEntryLabel;
    }

    /**
     * Setter for property 'entryLabel'.
     *
     * @param entryLabel Value to set for property 'entryLabel'.
     */
    public void setEntryLabel(TextView entryLabel)
    {
        mEntryLabel = entryLabel;
    }

    /**
     * Getter for property 'swapButton'.
     *
     * @return Value for property 'swapButton'.
     */
    public DimmableImageButton getSwapButton()
    {
        return mSwapButton;
    }

    /**
     * Setter for property 'swapButton'.
     *
     * @param swapButton Value to set for property 'swapButton'.
     */
    public void setSwapButton(DimmableImageButton swapButton)
    {
        mSwapButton = swapButton;
    }
}
