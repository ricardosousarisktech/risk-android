package com.risktechnology.socialdriver.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;

import java.util.ArrayList;

/**
 * Created by Matthew on 11/11/14.
 */
public class SpinnerTextDialogFragment extends DialogFragment {
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_MESSAGE = "KEY_MESSAGE";
    public static final String KEY_LIST_VALES = "KEY_LIST_VALES";
    public static final String KEY_TEXT = "KEY_TEXT";
    public static final String KEY_CURRENT_VALUE = "KEY_CURRENT_VALUE";
    public static final String KEY_VISIBLE_ON_VALUE = "KEY_VISIBLE_ON_VALUE";
    private AlertResponseHandler mAlertResponseHandler;

    private TextView mTitleView;
    private TextView mMessageView;
    private Button mPositiveButton;
    private Button mNegativeButton;
    private RadioGroup mRadioGroup;
    private boolean mCancelPressed = false;
    private ArrayList<String> mValues;
    private String mSelectedValue;
    private EditText mTextField;
    private String mVisibleTextOnValue;

    public String getSelectedValue() {
        return mSelectedValue;
    }
    public EditText getTextField() {
        return mTextField;
    }

    public boolean wasCancelPressed()
    {
        return mCancelPressed;
    }

    public SpinnerTextDialogFragment() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dialog.setContentView(R.layout.fragment_alert_spinner_text);

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        mTitleView = (TextView) dialog.findViewById(R.id.titleTextView);
        mMessageView = (TextView) dialog.findViewById(R.id.messageTextView);
        mRadioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroupSpinner);
        mPositiveButton = (Button) dialog.findViewById(R.id.positiveButton);
        mNegativeButton = (Button) dialog.findViewById(R.id.negativeButton);
        mTextField = (EditText) dialog.findViewById(R.id.editText);


        Bundle args = getArguments();
        String title = args.getString(KEY_TITLE, "");
        String message = args.getString(KEY_MESSAGE, "");
        final String text = args.getString(KEY_TEXT, "");
        String currentValue = args.getString(KEY_CURRENT_VALUE, "");
        mVisibleTextOnValue = args.getString(KEY_VISIBLE_ON_VALUE, "");
        mValues = args.getStringArrayList(KEY_LIST_VALES);

        mSelectedValue = currentValue;

        mTitleView.setText(title);
        mMessageView.setText(message);

        if(message.length() == 0)
        {
            mMessageView.setVisibility(View.GONE);
        }

        for(String value : mValues)
        {
            RadioButton button = (RadioButton) LayoutInflater.from(getActivity()).inflate(R.layout.template_radio_spinner, null);
            button.setText(value);
            mRadioGroup.addView(button, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if(value.equals(currentValue))
            {
                if(value.equals(mVisibleTextOnValue))
                {
                    mTextField.setText(text);
                    mTextField.setVisibility(View.VISIBLE);
                }
                else
                {
                    mTextField.setText("");
                    mTextField.setVisibility(View.GONE);
                }
                mRadioGroup.check(button.getId());
            }
        }

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton viewById = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
                mSelectedValue = viewById.getText().toString();
                if(mSelectedValue.equals(mVisibleTextOnValue))
                {
                    mTextField.setText(text);
                    mTextField.setVisibility(View.VISIBLE);
                }
                else
                {
                    mTextField.setText("");
                    mTextField.setVisibility(View.GONE);
                }
            }
        });

        mPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                dismiss();
                Fragment targetFragment = getTargetFragment();
                if (targetFragment != null) {
                    targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                }
                if (mAlertResponseHandler != null) {
                    mAlertResponseHandler.buttonPressed(SpinnerTextDialogFragment.this);
                }
            }
        });

        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                dismiss();
                Fragment targetFragment = getTargetFragment();
                if (targetFragment != null) {
                    targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                }
                if (mAlertResponseHandler != null) {
                    mCancelPressed = true;
                    mAlertResponseHandler.buttonPressed(SpinnerTextDialogFragment.this);
                }
            }
        });

        dialog.show();
        return dialog;
    }


    public void setAlertResponseHandler(AlertResponseHandler alertResponseHandler) {
        mAlertResponseHandler = alertResponseHandler;
    }


    public interface AlertResponseHandler {
        public void buttonPressed(SpinnerTextDialogFragment alert);
    }
}
