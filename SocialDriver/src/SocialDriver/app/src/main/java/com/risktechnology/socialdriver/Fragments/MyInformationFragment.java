
package com.risktechnology.socialdriver.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.activities.MainActivity;
import com.risktechnology.socialdriver.events.UserDetailsUpdatedEvent;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.models.requestModels.CarMakeRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.CarTypesRequest;
import com.risktechnology.socialdriver.ui_elements.DimmableButton;
import com.risktechnology.socialdriver.ui_elements.MenuButton;
import com.risktechnology.socialdriver.ui_elements.ProgressHUD;
import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.greenrobot.event.EventBus;

/**
 * Created by Matthew on 23/07/15.
 */

public class MyInformationFragment extends BaseFragment implements View.OnClickListener
{

    private MenuButton mMenuButton;
    private DimmableButton mNextButton;
    private View mRootView;

    private LinearLayout mNameHolder;
    private LinearLayout mGenderHolder;
    private LinearLayout mAgeHolder;
    private LinearLayout mLocationHolder;
    private LinearLayout mCarMakeHolder;

    private TextView mNameValue;
    private TextView mGenderValue;
    private TextView mAgeValue;
    private TextView mLocationValue;
    private TextView mCarMakeValue;

    private ArrayList<String> mCarTypes;
    private boolean mNextShouldSaveOnly;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        setTitle("My Information");
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this, -1);
        }
        mRootView = inflater.inflate(R.layout.fragment_my_information, container, false);

        mNameHolder = (LinearLayout) mRootView.findViewById(R.id.nameHolder);
        mGenderHolder = (LinearLayout) mRootView.findViewById(R.id.genderHolder);
        mAgeHolder = (LinearLayout) mRootView.findViewById(R.id.ageHolder);
        mLocationHolder = (LinearLayout) mRootView.findViewById(R.id.locationHolder);
        mCarMakeHolder = (LinearLayout) mRootView.findViewById(R.id.carMakeHolder);

        mNameValue = (TextView) mRootView.findViewById(R.id.nameValue);
        mGenderValue = (TextView) mRootView.findViewById(R.id.genderValue);
        mAgeValue = (TextView) mRootView.findViewById(R.id.ageValue);
        mLocationValue = (TextView) mRootView.findViewById(R.id.locationValue);
        mCarMakeValue = (TextView) mRootView.findViewById(R.id.carMakeValue);

        setupNameEntry();
        setupGenderPicker();
        setupAgePicker();
        setupLocationOption();
        setupCarMakePicker();

        mMenuButton = (MenuButton) mRootView.findViewById(R.id.myInfoMenuButton);
        mNextButton = (DimmableButton) mRootView.findViewById(R.id.nextButton);
        if (mMenuButton.isUsable())
        {
            mMenuButton.setVisibility(View.VISIBLE);
            mNextButton.setVisibility(View.VISIBLE);
            mNextButton.setOnClickListener(this);
            mNextButton.setText("Save");
            mNextShouldSaveOnly = true;
        }
        else
        {
            mMenuButton.setVisibility(View.GONE);
            mNextButton.setVisibility(View.VISIBLE);
            mNextButton.setOnClickListener(this);
            mNextButton.setText("Next");
            mNextShouldSaveOnly = false;
        }


        return mRootView;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        onResume();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updateUI();
    }

    private void updateUI()
    {
        mNameValue.setText(String.format(UserModel.getInstance().getFullName()));
        mGenderValue.setText(UserModel.getInstance().getGender());

        if(UserModel.getInstance().getDateOfBirth() != null)
        {
            try
            {
                mAgeValue.setText("");
                SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
                String dobString = format.format(UserModel.getInstance().getDateOfBirth());
                mAgeValue.setText(dobString);
            } catch (Exception e)
            {
                mAgeValue.setText("");
                UserModel.getInstance().setDateOfBirth(null);
                e.printStackTrace();
            }
        }

        mLocationValue.setText(UserModel.getInstance().formattedAddress());
        mCarMakeValue.setText(UserModel.getInstance().getCarType());
    }

    private void setupCarMakePicker()
    {
        mCarMakeHolder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (mCarTypes == null || mCarTypes.size() == 0)
                {
                    final ProgressHUD progressHUD = ProgressHUD.show(getActivity(), "Getting car types...", true, false, null);

                    CarMakeRequestModel model = new CarMakeRequestModel();
                    CarTypesRequest request = new CarTypesRequest(model);
                    request.setRequestHandler(new BaseRequest.RequestHandler()
                    {
                        @Override
                        public void requestFinished(BaseRequest request)
                        {
                            progressHUD.dismiss();
                            if (request.getResponseObject() != null)
                            {
                                JSONArray types = request.getResponseObject().optJSONArray("Types");
                                if (types != null)
                                {
                                    if (mCarTypes == null)
                                    {
                                        mCarTypes = new ArrayList<>();
                                    }
                                    for (int i = 0; i < types.length(); i++)
                                    {
                                        try
                                        {
                                            mCarTypes.add(types.getString(i));
                                        } catch (JSONException e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }

                                    if (mCarTypes.size() > 0)
                                    {
                                        onClick(null);
                                    }
                                }
                            }
                            else
                            {
                                Helpers.errorAlert(getActivity(), "Error", "Unable to get car types, please try again");
                            }
                        }
                    });
                    request.executeRequest(getActivity());
                }
                else
                {
                    SpinnerDialogFragment fragment = new SpinnerDialogFragment();

                    Bundle args = new Bundle();
                    args.putString(SpinnerDialogFragment.KEY_TITLE, "Car Type");
                    args.putString(SpinnerDialogFragment.KEY_CURRENT_VALUE, UserModel.getInstance().getCarType());
                    args.putStringArrayList(SpinnerDialogFragment.KEY_LIST_VALES, mCarTypes);
                    fragment.setArguments(args);
                    fragment.setAlertResponseHandler(new SpinnerDialogFragment.AlertResponseHandler()
                    {
                        @Override
                        public void buttonPressed(SpinnerDialogFragment alert)
                        {
                            if (!alert.wasCancelPressed())
                            {
                                UserModel.getInstance().setCarType(alert.getSelectedValue());
                                mCarMakeValue.setText(alert.getSelectedValue());
                                UserModel.getInstance().save();
                            }
                        }
                    });
                    fragment.show(getFragmentManager(), "spinner");
                }
            }
        });

    }

    private void setupLocationOption()
    {
        mLocationHolder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AddressEntryAlertDialogFragment fragment = new AddressEntryAlertDialogFragment();
                Bundle args = new Bundle();
                args.putString(SpinnerDialogFragment.KEY_TITLE, "Address");
                fragment.setArguments(args);
                fragment.setAlertResponseHandler(new AddressEntryAlertDialogFragment.AlertResponseHandler()
                {
                    @Override
                    public void buttonPressed(AddressEntryAlertDialogFragment alert)
                    {
                        updateUI();
                        UserModel.getInstance().save();
                    }
                });
                fragment.show(getFragmentManager(), "addressEntry");
            }
        });
    }

    private void setupAgePicker()
    {
        mAgeHolder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                DatePickerDialogFragment fragment = new DatePickerDialogFragment();

                Bundle args = new Bundle();
                args.putString(SpinnerDialogFragment.KEY_TITLE, "Date Of Birth");
                Date dateOfBirth = UserModel.getInstance().getDateOfBirth();
                if(dateOfBirth != null)
                {
                    args.putLong(SpinnerDialogFragment.KEY_CURRENT_VALUE, dateOfBirth.getTime());
                }
                fragment.setArguments(args);
                fragment.setAlertResponseHandler(new DatePickerDialogFragment.AlertResponseHandler()
                {
                    @Override
                    public void buttonPressed(DatePickerDialogFragment alert)
                    {
                        if (!alert.wasCancelPressed())
                        {
                            Calendar dobCalendar = alert.getCalendar();

                            SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
                            String dobString = format.format(dobCalendar.getTime());
                            mAgeValue.setText(dobString);
                            UserModel.getInstance().setDateOfBirth(dobCalendar.getTime());
                            UserModel.getInstance().save();
                        }
                    }
                });
                fragment.show(getFragmentManager(), "spinner");
            }
        });
    }


    private void setupNameEntry()
    {
        mNameHolder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                NameEntryAlertDialogFragment fragment = new NameEntryAlertDialogFragment();
                Bundle args = new Bundle();
                args.putString(SpinnerDialogFragment.KEY_TITLE, "Name");
                fragment.setArguments(args);
                fragment.setAlertResponseHandler(new NameEntryAlertDialogFragment.AlertResponseHandler()
                {
                    @Override
                    public void buttonPressed(NameEntryAlertDialogFragment alert)
                    {
                        mNameValue.setText(UserModel.getInstance().getFullName());
                        UserModel.getInstance().save();
                    }
                });
                fragment.show(getFragmentManager(), "nameEntry");
            }
        });
    }

    private void setupGenderPicker()
    {
        mGenderHolder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ArrayList<String> genderArray = new ArrayList<>();
                genderArray.add("Male");
                genderArray.add("Female");

                SpinnerDialogFragment fragment = new SpinnerDialogFragment();

                Bundle args = new Bundle();
                args.putString(SpinnerDialogFragment.KEY_TITLE, "Gender");
                args.putString(SpinnerDialogFragment.KEY_CURRENT_VALUE, UserModel.getInstance().getGender());
                args.putStringArrayList(SpinnerDialogFragment.KEY_LIST_VALES, genderArray);
                fragment.setArguments(args);
                fragment.setAlertResponseHandler(new SpinnerDialogFragment.AlertResponseHandler()
                {
                    @Override
                    public void buttonPressed(SpinnerDialogFragment alert)
                    {
                        if (!alert.wasCancelPressed())
                        {
                            UserModel.getInstance().setGender(alert.getSelectedValue());
                            mGenderValue.setText(alert.getSelectedValue());
                            UserModel.getInstance().save();
                        }
                    }
                });
                fragment.show(getFragmentManager(), "spinner");
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        if (v == mNextButton)
        {
            if (UserModel.getInstance().requiresConfiguration())
            {
                Helpers.errorAlert(getActivity(), "User details", "Please complete all the details on this page.");
            }
            else
            {
                UserModel.getInstance().upload(true, getActivity());
                if(!mNextShouldSaveOnly)
                {
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                }
            }
        }
    }

    @Override
    public void onDetach()
    {
        UserModel.getInstance().save();
        UserModel.getInstance().upload();
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }


    public void onEvent(UserDetailsUpdatedEvent event)
    {
        updateUI();
    }
}
