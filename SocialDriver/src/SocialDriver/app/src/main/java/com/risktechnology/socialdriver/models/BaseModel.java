package com.risktechnology.socialdriver.models;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.risktechnology.socialdriver.application.SDApplication;
import com.risktechnology.socialdriver.events.UserDetailsUpdatedEvent;
import com.risktechnology.socialdriver.utilities.FileManager;

import java.io.File;

import de.greenrobot.event.EventBus;

/**
 * Created by Dev on 07/09/2015.
 */

@SuppressWarnings("unused")
public class BaseModel
{
    private String mFilename;

    public void save()
    {
        if(mFilename != null)
        {
            Gson gson = new Gson();
            String gsonString = gson.toJson(this);
            FileManager.saveObject(gsonString, mFilename, SDApplication.getAppContext());
        }
    }

    public void load()
    {
        if(mFilename != null)
        {
            Object loadedObject = FileManager.getObject(mFilename, SDApplication.getAppContext());
            Gson gson = new Gson();
            if(loadedObject != null)
            {
                try
                {
                    parseLoadedObject(gson.fromJson((String) loadedObject, this.getClass()));
                } catch (JsonSyntaxException e)
                {
                    //The data is corrupt so delete it
                    delete();
                    e.printStackTrace();
                }
            }
        }
    }

    public void delete()
    {
        if(mFilename != null)
        {
            FileManager.deleteObject(mFilename, SDApplication.getAppContext());
        }
    }

    /**
     * Getter for property 'filename'.
     *
     * @return Value for property 'filename'.
     */
    public String getFilename()
    {
        return mFilename;
    }

    /**
     * Setter for property 'filename'.
     *
     * @param filename Value to set for property 'filename'.
     */
    public void setFilename(String filename)
    {
        mFilename = filename;
    }

    public void parseLoadedObject(BaseModel model)
    {

    }
}
