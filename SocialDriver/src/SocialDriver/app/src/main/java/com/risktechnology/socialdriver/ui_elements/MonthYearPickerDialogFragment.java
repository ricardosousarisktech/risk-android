package com.risktechnology.socialdriver.ui_elements;

/**
 * Created by Dev on 21/09/2015.
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.UiUtilities;

/**
 * The Class MonthYearPickerDialogFragment.
 *
 * @author SuRendra Reddy
 */
@SuppressLint("InflateParams")
public class MonthYearPickerDialogFragment extends DialogFragment implements DatePicker.OnDateChangedListener {

    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_MESSAGE = "KEY_MESSAGE";
    public static final String KEY_CURRENT_VALUE = "KEY_CURRENT_VALUE";
    public static final String KEY_POSITIVE_TEXT = "KEY_POSITIVE_TEXT";
    public static final String KEY_NEGATIVE_TEXT = "KEY_NEGATIVE_TEXT";
    public static final String KEY_MIN_YEAR = "KEY_MIN_YEAR";
    public static final String KEY_MAX_YEAR = "KEY_MAX_YEAR";

    private static final String[] PICKER_DISPLAY_MONTHS_NAMES = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct",
            "Nov", "Dec" };

    private static final String[] MONTHS = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September",
            "October", "November", "December" };

        private boolean build = false;
    private NumberPicker monthNumberPicker;
    private NumberPicker yearNumberPicker;
    private AlertResponseHandler mAlertResponseHandler;

    private int currentYear;

    private int currentMonth;
    private Calendar mCalendar;
    private TextView mTitleView;
    private TextView mMessageView;
    private Button mPositiveButton;
    private Button mNegativeButton;
    private boolean mCancelPressed;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dialog.setContentView(R.layout.fragment_alert_date_month_year_picker);

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        mCalendar = Calendar.getInstance();

        Bundle args = getArguments();
        String title = args.getString(KEY_TITLE, "");
        String message = args.getString(KEY_MESSAGE, "");
        String positiveMessage = args.getString(KEY_POSITIVE_TEXT, "Set");
        String negativeMessage = args.getString(KEY_NEGATIVE_TEXT, "Cancel");
        Integer minYear = args.getInt(KEY_MIN_YEAR, 2015);
        Integer maxYear = args.getInt(KEY_MAX_YEAR, mCalendar.get(Calendar.YEAR));


        mTitleView = (TextView) dialog.findViewById(R.id.titleTextView);
        mMessageView = (TextView) dialog.findViewById(R.id.messageTextView);
        mPositiveButton = (Button) dialog.findViewById(R.id.positiveButton);
        mNegativeButton = (Button) dialog.findViewById(R.id.negativeButton);

        long defaultValue = args.getLong(KEY_CURRENT_VALUE, new Date().getTime());

        mCalendar.setTimeInMillis(defaultValue);

        currentMonth = mCalendar.get(Calendar.MONTH);
        currentYear = mCalendar.get(Calendar.YEAR);


        monthNumberPicker = (NumberPicker) dialog.findViewById(R.id.monthNumberPicker);
        monthNumberPicker.setDisplayedValues(PICKER_DISPLAY_MONTHS_NAMES);

        monthNumberPicker.setMinValue(0);
        monthNumberPicker.setMaxValue(MONTHS.length - 1);

        yearNumberPicker = (NumberPicker) dialog.findViewById(R.id.yearNumberPicker);
        yearNumberPicker.setMinValue(minYear);
        yearNumberPicker.setMaxValue(maxYear);

        monthNumberPicker.setValue(currentMonth);
        yearNumberPicker.setValue(currentYear);

        monthNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        yearNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        mPositiveButton.setText(positiveMessage);
        mNegativeButton.setText(negativeMessage);


        mTitleView.setText(title);

        mPositiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                Date date = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy");
                int year = Integer.parseInt(format.format(date));
                format = new SimpleDateFormat("MM");
                int month = Integer.parseInt(format.format(date));

                int selectedYear = getSelectedYear();
                int selectedMonth = getSelectedMonth();

                if(selectedYear > year)
                {
                    Helpers.errorAlert(getActivity(), "Invalid date", "Please select the current date or one in the past");
                    return;
                }
                else if(year == selectedYear + 1)
                {
                    if(selectedMonth + 1 > month)
                    {
                        Helpers.errorAlert(getActivity(), "Invalid date", "Please select the current date or one in the past");
                        return;
                    }
                }

                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                mCalendar.set(selectedYear, selectedMonth, 1);
                dismiss();
                Fragment targetFragment = getTargetFragment();
                if (targetFragment != null)
                {
                    targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                }
                if (mAlertResponseHandler != null)
                {
                    mAlertResponseHandler.buttonPressed(MonthYearPickerDialogFragment.this);
                }
            }
        });

        mNegativeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                dismiss();
                Fragment targetFragment = getTargetFragment();
                if (targetFragment != null)
                {
                    targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                }
                if (mAlertResponseHandler != null)
                {
                    mCancelPressed = true;
                    mAlertResponseHandler.buttonPressed(MonthYearPickerDialogFragment.this);
                }
            }
        });

        dialog.show();
        return dialog;

    }

    /**
     * Gets the selected month.
     *
     * @return the selected month
     */
    public int getSelectedMonth() {
        return monthNumberPicker.getValue();
    }

    /**
     * Gets the selected month name.
     *
     * @return the selected month name
     */
    public String getSelectedMonthName() {
        return MONTHS[monthNumberPicker.getValue()];
    }

    /**
     * Gets the selected month name.
     *
     * @return the selected month short name i.e Jan, Feb ...
     */
    public String getSelectedMonthShortName() {
        return PICKER_DISPLAY_MONTHS_NAMES[monthNumberPicker.getValue()];
    }

    /**
     * Gets the selected year.
     *
     * @return the selected year
     */
    public int getSelectedYear() {
        return yearNumberPicker.getValue();
    }

    /**
     * Gets the current year.
     *
     * @return the current year
     */
    public int getCurrentYear() {
        return currentYear;
    }

    /**
     * Gets the current month.
     *
     * @return the current month
     */
    public int getCurrentMonth() {
        return currentMonth;
    }

    /**
     * Sets the month value changed listener.
     *
     * @param valueChangeListener
     *            the new month value changed listener
     */
    public void setMonthValueChangedListener(NumberPicker.OnValueChangeListener valueChangeListener) {
        monthNumberPicker.setOnValueChangedListener(valueChangeListener);
    }

    /**
     * Sets the year value changed listener.
     *
     * @param valueChangeListener
     *            the new year value changed listener
     */
    public void setYearValueChangedListener(NumberPicker.OnValueChangeListener valueChangeListener) {
        yearNumberPicker.setOnValueChangedListener(valueChangeListener);
    }

    /**
     * Sets the month wrap selector wheel.
     *
     * @param wrapSelectorWheel
     *            the new month wrap selector wheel
     */
    public void setMonthWrapSelectorWheel(boolean wrapSelectorWheel) {
        monthNumberPicker.setWrapSelectorWheel(wrapSelectorWheel);
    }

    /**
     * Sets the year wrap selector wheel.
     *
     * @param wrapSelectorWheel
     *            the new year wrap selector wheel
     */
    public void setYearWrapSelectorWheel(boolean wrapSelectorWheel) {
        yearNumberPicker.setWrapSelectorWheel(wrapSelectorWheel);
    }

    public void setAlertResponseHandler(AlertResponseHandler alertResponseHandler) {
        mAlertResponseHandler = alertResponseHandler;
    }

    @Override
    public void onDateChanged(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        mCalendar.set(year, monthOfYear, dayOfMonth);
    }

    public boolean wasCancelPressed()
    {
        return mCancelPressed;
    }

    public Calendar getCalendar()
    {
        return mCalendar;
    }

    public interface AlertResponseHandler {
        public void buttonPressed(MonthYearPickerDialogFragment alert);
    }
}