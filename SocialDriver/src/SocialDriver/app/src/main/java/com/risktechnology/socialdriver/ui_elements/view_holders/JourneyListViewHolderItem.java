package com.risktechnology.socialdriver.ui_elements.view_holders;

import android.widget.TextView;

import com.risktechnology.socialdriver.ui_elements.WhiteCircle;

public class JourneyListViewHolderItem {
    private TextView mStartAddressLabel;
    private TextView mEndAddressLabel;
    private TextView mDistanceLabel;
    private TextView mTravelTimeLabel;
    private TextView mPointsLabel;
    private TextView mAccelerationLabel;
    private TextView mDecelerationLabel;
    private TextView mSpeedLabel;
    private TextView mDateLabel;
    private WhiteCircle mPointsHolder;
    private WhiteCircle mAccelerationHolder;
    private WhiteCircle mDecelerationHolder;
    private WhiteCircle mSpeedHolder;

    /**
     * Getter for property 'startAddressLabel'.
     *
     * @return Value for property 'startAddressLabel'.
     */
    public TextView getStartAddressLabel()
    {
        return mStartAddressLabel;
    }

    /**
     * Setter for property 'startAddressLabel'.
     *
     * @param startAddressLabel Value to set for property 'startAddressLabel'.
     */
    public void setStartAddressLabel(TextView startAddressLabel)
    {
        mStartAddressLabel = startAddressLabel;
    }

    /**
     * Getter for property 'endAddressLabel'.
     *
     * @return Value for property 'endAddressLabel'.
     */
    public TextView getEndAddressLabel()
    {
        return mEndAddressLabel;
    }

    /**
     * Setter for property 'endAddressLabel'.
     *
     * @param endAddressLabel Value to set for property 'endAddressLabel'.
     */
    public void setEndAddressLabel(TextView endAddressLabel)
    {
        mEndAddressLabel = endAddressLabel;
    }

    /**
     * Getter for property 'distanceLabel'.
     *
     * @return Value for property 'distanceLabel'.
     */
    public TextView getDistanceLabel()
    {
        return mDistanceLabel;
    }

    /**
     * Setter for property 'distanceLabel'.
     *
     * @param distanceLabel Value to set for property 'distanceLabel'.
     */
    public void setDistanceLabel(TextView distanceLabel)
    {
        mDistanceLabel = distanceLabel;
    }

    /**
     * Getter for property 'travelTimeLabel'.
     *
     * @return Value for property 'travelTimeLabel'.
     */
    public TextView getTravelTimeLabel()
    {
        return mTravelTimeLabel;
    }

    /**
     * Setter for property 'travelTimeLabel'.
     *
     * @param travelTimeLabel Value to set for property 'travelTimeLabel'.
     */
    public void setTravelTimeLabel(TextView travelTimeLabel)
    {
        mTravelTimeLabel = travelTimeLabel;
    }

    /**
     * Getter for property 'pointsLabel'.
     *
     * @return Value for property 'pointsLabel'.
     */
    public TextView getPointsLabel()
    {
        return mPointsLabel;
    }

    /**
     * Setter for property 'pointsLabel'.
     *
     * @param pointsLabel Value to set for property 'pointsLabel'.
     */
    public void setPointsLabel(TextView pointsLabel)
    {
        mPointsLabel = pointsLabel;
    }

    /**
     * Getter for property 'accelLabel'.
     *
     * @return Value for property 'accelLabel'.
     */
    public TextView getAccelerationLabel()
    {
        return mAccelerationLabel;
    }

    /**
     * Setter for property 'accelerationLabel'.
     *
     * @param accelerationLabel Value to set for property 'accelerationLabel'.
     */
    public void setAccelerationLabel(TextView accelerationLabel)
    {
        mAccelerationLabel = accelerationLabel;
    }

    /**
     * Getter for property 'decelLabel'.
     *
     * @return Value for property 'decelLabel'.
     */
    public TextView getDecelerationLabel()
    {
        return mDecelerationLabel;
    }

    /**
     * Setter for property 'decelerationLabel'.
     *
     * @param decelerationLabel Value to set for property 'decelerationLabel'.
     */
    public void setDecelerationLabel(TextView decelerationLabel)
    {
        mDecelerationLabel = decelerationLabel;
    }

    /**
     * Getter for property 'speedLabel'.
     *
     * @return Value for property 'speedLabel'.
     */
    public TextView getSpeedLabel()
    {
        return mSpeedLabel;
    }

    /**
     * Setter for property 'speedLabel'.
     *
     * @param speedLabel Value to set for property 'speedLabel'.
     */
    public void setSpeedLabel(TextView speedLabel)
    {
        mSpeedLabel = speedLabel;
    }

    /**
     * Getter for property 'pointsHolder'.
     *
     * @return Value for property 'pointsHolder'.
     */
    public WhiteCircle getPointsHolder()
    {
        return mPointsHolder;
    }

    /**
     * Setter for property 'pointsHolder'.
     *
     * @param pointsHolder Value to set for property 'pointsHolder'.
     */
    public void setPointsHolder(WhiteCircle pointsHolder)
    {
        mPointsHolder = pointsHolder;
    }

    /**
     * Getter for property 'accelHolder'.
     *
     * @return Value for property 'accelHolder'.
     */
    public WhiteCircle getAccelerationHolder()
    {
        return mAccelerationHolder;
    }

    /**
     * Setter for property 'accelerationHolder'.
     *
     * @param accelerationHolder Value to set for property 'accelerationHolder'.
     */
    public void setAccelerationHolder(WhiteCircle accelerationHolder)
    {
        mAccelerationHolder = accelerationHolder;
    }

    /**
     * Getter for property 'decelHolder'.
     *
     * @return Value for property 'decelHolder'.
     */
    public WhiteCircle getDecelerationHolder()
    {
        return mDecelerationHolder;
    }

    /**
     * Setter for property 'decelerationHolder'.
     *
     * @param decelerationHolder Value to set for property 'decelerationHolder'.
     */
    public void setDecelerationHolder(WhiteCircle decelerationHolder)
    {
        mDecelerationHolder = decelerationHolder;
    }

    /**
     * Getter for property 'speedHolder'.
     *
     * @return Value for property 'speedHolder'.
     */
    public WhiteCircle getSpeedHolder()
    {
        return mSpeedHolder;
    }

    /**
     * Setter for property 'speedHolder'.
     *
     * @param speedHolder Value to set for property 'speedHolder'.
     */
    public void setSpeedHolder(WhiteCircle speedHolder)
    {
        mSpeedHolder = speedHolder;
    }

    public TextView getDateLabel() {
        return mDateLabel;
    }

    public void setDateLabel(TextView dateLabel) {
        mDateLabel = dateLabel;
    }
}
