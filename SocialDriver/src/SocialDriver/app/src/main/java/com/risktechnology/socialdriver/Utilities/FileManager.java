package com.risktechnology.socialdriver.utilities;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Dev on 07/08/2015.
 */
public class FileManager
{

    public static boolean saveObject(Object object, String filename, Context context) {

        final File suspend_f = context.getFileStreamPath(filename);

        FileOutputStream fos  = null;
        ObjectOutputStream oos  = null;
        boolean            keep = true;

        try {
            fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
        } catch (Exception e) {
            Log.e("BaseRequestModel", "Error: " + e.getLocalizedMessage() + "\n" + e.getStackTrace());
            keep = false;
        } finally {
            try {
                if (oos != null)   oos.close();
                if (fos != null)   fos.close();
                if (!keep) suspend_f.delete();
            } catch (Exception e) { /* do nothing */ }
        }

        return keep;
    }

    public static Object getObject(String filename, Context context) {

        final File suspend_f = context.getFileStreamPath(filename);

        Object object= null;
        FileInputStream fis = null;
        ObjectInputStream is = null;

        try {
            fis = new FileInputStream(suspend_f);
            is = new ObjectInputStream(fis);
            object = is.readObject();
        } catch(Exception e) {
            String val= e.getMessage();
        } finally {
            try {
                if (fis != null)   fis.close();
                if (is != null)   is.close();
            } catch (Exception e) { }
        }

        return object;
    }

    public static void deleteObject(String filename, Context context)
    {
        final File suspend_f = context.getFileStreamPath(filename);
        suspend_f.delete();
    }
}
