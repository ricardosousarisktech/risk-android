package com.risktechnology.socialdriver.ui_elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.activities.MainActivity;

/**
 * Created by Dev on 26/08/2015.
 */
public class MenuButton extends DimmableImageButton
{
    private Context mContext;
    public MenuButton(Context context)
    {
        super(context);
        mContext = context;
        init();
    }

    public MenuButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        mContext = context;
        init();
    }

    public MenuButton(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    private void init()
    {
        setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MainActivity.navigationDraw().openDraw();
            }
        });
    }

    public boolean isUsable()
    {
        return (MainActivity.navigationDraw() != null);
    }
}
