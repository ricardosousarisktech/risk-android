package com.risktechnology.socialdriver.requests;

import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.models.requestModels.BaseRequestModel;
import com.risktechnology.socialdriver.utilities.Logger;

import cz.msebera.android.httpclient.Header;

public class NotifyTokenRequest extends BaseRequest
{
    private static final String TAG = "NotifyTokenRequest";

    public NotifyTokenRequest(BaseRequestModel model)
    {
        super(model);
        setUrl(Constants.API_ENDPOINT + "setnotifytoken");
    }

    @Override
    public void onRetry(int retryNo) {
        super.onRetry(retryNo);
        Logger.LogDebugMessage(TAG, "onRetry");


    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        super.onSuccess(statusCode, headers, responseBody);
        Logger.LogDebugMessage(TAG, "onSuccess");
        fireRequestFinished();
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
        super.onFailure(statusCode, headers, errorResponse, e);
        Logger.LogDebugMessage(TAG, "onFailure");
        fireRequestFinished();
    }

}
