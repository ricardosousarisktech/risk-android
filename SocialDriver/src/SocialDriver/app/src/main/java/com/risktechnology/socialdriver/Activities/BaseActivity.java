package com.risktechnology.socialdriver.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.JourneyManager;
import com.risktechnology.socialdriver.listeners.OnFragmentInteractionListener;

/**
 * Created by Dev on 21/07/2015.
 */
public abstract class BaseActivity extends FragmentActivity implements OnFragmentInteractionListener
{

    @Override
    protected void onResume() {
        super.onResume();
        JourneyManager.setActivityContext(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        JourneyManager.setActivityContext(this);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }
}
