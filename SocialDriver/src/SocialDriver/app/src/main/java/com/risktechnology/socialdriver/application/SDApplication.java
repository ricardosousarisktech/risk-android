package com.risktechnology.socialdriver.application;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.risktechnology.socialdriver.BuildConfig;
import com.risktechnology.socialdriver.core.BluetoothAccessManager;
import com.risktechnology.socialdriver.core.WifiAccessManager;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Dev on 24/07/2015.
 */
public class SDApplication extends Application
{

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "SDApplication";
    private static Context context;

    public void onCreate(){
        super.onCreate();
        if(!BuildConfig.DEBUG)
        {
            Fabric.with(this, new Crashlytics());
        }
        SDApplication.context = getApplicationContext();
        BluetoothAccessManager.sharedManager();
        WifiAccessManager.sharedManager();
    }

    public static Context getAppContext() {
        return SDApplication.context;
    }
}