package com.risktechnology.socialdriver.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableWrapper;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.ui_elements.view_holders.NavigationMenuViewHolderItem;
import com.risktechnology.socialdriver.utilities.UiUtilities;

/**
 * Created by Matthew on 09/01/15.
 */

public class NavigationDrawerAdapter extends ArrayAdapter {
    Context mContext;
    Object[] mObjects;

    public NavigationDrawerAdapter(Context context, int resource, int textViewResourceId, Object[] objects) {
        super(context, resource, textViewResourceId, objects);
        mContext = context;
        mObjects = objects;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NavigationMenuViewHolderItem viewHolder;

        if(convertView==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitem_navigation_item, parent, false);

            viewHolder = new NavigationMenuViewHolderItem();

            viewHolder.setTextView((TextView) convertView.findViewById(R.id.menuTitle));

            convertView.setTag(viewHolder);

        }else{
            viewHolder = (NavigationMenuViewHolderItem) convertView.getTag();
        }


        Constants.PageIndexes index = Constants.PageIndexes.values()[position];

        UiUtilities.setMultifontStringOnTextView(viewHolder.getTextView(), "<b>" + mObjects[position].toString() + "</b>", mContext);
        Drawable d = null;

        switch (index)
        {
            case PAGE_INDEX_HOME:
                d = mContext.getResources().getDrawable(R.drawable.home_icon);
                break;
            case PAGE_INDEX_JOURNEYS:
                d = mContext.getResources().getDrawable(R.drawable.journeys_icon);
                break;
            case PAGE_INDEX_SETTINGS:
                d = mContext.getResources().getDrawable(R.drawable.settings_icon);
                break;
            case PAGE_INDEX_MY_INFORMATION:
                d = mContext.getResources().getDrawable(R.drawable.my_information);
                break;
            case PAGE_INDEX_LOGOUT:
                d = mContext.getResources().getDrawable(R.drawable.logout_icon);
                break;
            case PAGE_INDEX_COUNT:
                //nothing here
                break;
        }

        if(d != null)
        {
            d.setBounds(0, 0, UiUtilities.dpToPixel(mContext, 10), UiUtilities.dpToPixel(mContext, 10));
            int colour = Color.WHITE;
            UiUtilities.tintCompoundDrawable(viewHolder.getTextView(), d, colour);
        }


        return convertView;

    }
}

