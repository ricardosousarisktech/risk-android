package com.risktechnology.socialdriver.models.requestModels;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

@SuppressWarnings("unused")
public abstract class BaseRequestModel implements Serializable
{
    public abstract void addToJSONObject(JSONObject jsonObject) throws JSONException;
    public abstract void describeModel();
}
