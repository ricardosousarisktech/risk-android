package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.TimeZone;


@SuppressWarnings("unused")
public class CreateUserRequestModel extends BaseRequestModel
{

    /*
    Username	The user username (email address)
    Password	The user password
    OAuthid	    If a facebook user there oauthid
    Firstname	User firstname
    Lastname	User lastname
    DOB	        User date of birth
    Gender	    User gender
    Address	    User address
    Postcode	Users postcode
    Timezone	User Timezone eg Europe/London
    Culture	    User Culture eg en-GB
    CarType	    Type of user car
    Notifyuser	True/false. To notify user of journeys score
     */

    private String mUsername;
    private String mPassword;
    private String mOauthId;
    private String mAccessToken;
    private String mFirstname;
    private String mLastname;
    private String mDOB;
    private String mGender;
    private String mAddress;
    private String mPostcode;
    private String mTimezone;
    private String mCulture;
    private String mCarType;
    private String mNotifyuser;

    public CreateUserRequestModel() {
        mUsername = "";
        mPassword = "";
        mAccessToken = "";
        mFirstname = "";
        mLastname = "";
        mDOB = "";
        mGender = "";
        mAddress = "";
        mPostcode = "";
        mTimezone = TimeZone.getDefault().getID();
        mCulture = Locale.getDefault().toString();
        mCarType = "";
        mNotifyuser = "";
        mOauthId = "";

    }

    public void describeModel() {
        Logger.LogMessage("Registration Model", "================ BEGIN =================");
        Logger.LogMessage("Registration Model", "Email: " + mUsername);
        Logger.LogMessage("Registration Model", "PinCode: " + mPassword);
        Logger.LogMessage("Registration Model", "================== END =================");
    }

    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException {
        if(!Helpers.isEmpty(mUsername))
        {
            jsonObject.put("Username", mUsername);
        }

        if(!Helpers.isEmpty(mPassword))
        {
            jsonObject.put("Password", mPassword);
        }

        if(!Helpers.isEmpty(mOauthId))
        {
            jsonObject.put("FacebookId", mOauthId);
            jsonObject.put("OAuthID", mOauthId);
        }

        if(!Helpers.isEmpty(mAccessToken))
        {
            jsonObject.put("AccessToken", mAccessToken);
        }

        if(!Helpers.isEmpty(mFirstname))
        {
            jsonObject.put("Firstname", mFirstname);
        }

        if(!Helpers.isEmpty(mLastname))
        {
            jsonObject.put("Lastname", mLastname);
        }

        if(!Helpers.isEmpty(mDOB))
        {
            jsonObject.put("DOB", mDOB);
        }

        if(!Helpers.isEmpty(mGender))
        {
            jsonObject.put("Gender", mGender);
        }

        if(!Helpers.isEmpty(mAddress))
        {
            jsonObject.put("Address", mAddress);
        }

        if(!Helpers.isEmpty(mPostcode))
        {
            jsonObject.put("Postcode", mPostcode);
        }

        if(!Helpers.isEmpty(mTimezone))
        {
            jsonObject.put("Timezone", mTimezone);
        }

        if(!Helpers.isEmpty(mCulture))
        {
            jsonObject.put("Culture", mCulture);
        }

        if(!Helpers.isEmpty(mCarType))
        {
            jsonObject.put("CarType", mCarType);
        }

        if(!Helpers.isEmpty(mNotifyuser))
        {
            jsonObject.put("Notifyuser", mNotifyuser);
        }

        jsonObject.put("AppID", Constants.APP_ID);

    }

    /**
     * Getter for property 'username'.
     *
     * @return Value for property 'username'.
     */
    public String getUsername()
    {
        return mUsername;
    }

    /**
     * Setter for property 'username'.
     *
     * @param username Value to set for property 'username'.
     */
    public void setUsername(String username)
    {
        mUsername = username;
    }

    /**
     * Getter for property 'password'.
     *
     * @return Value for property 'password'.
     */
    public String getPassword()
    {
        return mPassword;
    }

    /**
     * Setter for property 'password'.
     *
     * @param password Value to set for property 'password'.
     */
    public void setPassword(String password)
    {
        mPassword = password;
    }

    /**
     * Getter for property 'firstname'.
     *
     * @return Value for property 'firstname'.
     */
    public String getFirstname()
    {
        return mFirstname;
    }

    /**
     * Setter for property 'firstname'.
     *
     * @param firstname Value to set for property 'firstname'.
     */
    public void setFirstname(String firstname)
    {
        mFirstname = firstname;
    }

    /**
     * Getter for property 'lastname'.
     *
     * @return Value for property 'lastname'.
     */
    public String getLastname()
    {
        return mLastname;
    }

    /**
     * Setter for property 'lastname'.
     *
     * @param lastname Value to set for property 'lastname'.
     */
    public void setLastname(String lastname)
    {
        mLastname = lastname;
    }

    /**
     * Getter for property 'DOB'.
     *
     * @return Value for property 'DOB'.
     */
    public String getDOB()
    {
        return mDOB;
    }

    /**
     * Setter for property 'DOB'.
     *
     * @param DOB Value to set for property 'DOB'.
     */
    public void setDOB(String DOB)
    {
        mDOB = DOB;
    }

    /**
     * Getter for property 'gender'.
     *
     * @return Value for property 'gender'.
     */
    public String getGender()
    {
        return mGender;
    }

    /**
     * Setter for property 'gender'.
     *
     * @param gender Value to set for property 'gender'.
     */
    public void setGender(String gender)
    {
        mGender = gender;
    }

    /**
     * Getter for property 'address'.
     *
     * @return Value for property 'address'.
     */
    public String getAddress()
    {
        return mAddress;
    }

    /**
     * Setter for property 'address'.
     *
     * @param address Value to set for property 'address'.
     */
    public void setAddress(String address)
    {
        mAddress = address;
    }

    /**
     * Getter for property 'postcode'.
     *
     * @return Value for property 'postcode'.
     */
    public String getPostcode()
    {
        return mPostcode;
    }

    /**
     * Setter for property 'postcode'.
     *
     * @param postcode Value to set for property 'postcode'.
     */
    public void setPostcode(String postcode)
    {
        mPostcode = postcode;
    }

    /**
     * Getter for property 'timezone'.
     *
     * @return Value for property 'timezone'.
     */
    public String getTimezone()
    {
        return mTimezone;
    }

    /**
     * Setter for property 'timezone'.
     *
     * @param timezone Value to set for property 'timezone'.
     */
    public void setTimezone(String timezone)
    {
        mTimezone = timezone;
    }

    /**
     * Getter for property 'culture'.
     *
     * @return Value for property 'culture'.
     */
    public String getCulture()
    {
        return mCulture;
    }

    /**
     * Setter for property 'culture'.
     *
     * @param culture Value to set for property 'culture'.
     */
    public void setCulture(String culture)
    {
        mCulture = culture;
    }

    /**
     * Getter for property 'carType'.
     *
     * @return Value for property 'carType'.
     */
    public String getCarType()
    {
        return mCarType;
    }

    /**
     * Setter for property 'carType'.
     *
     * @param carType Value to set for property 'carType'.
     */
    public void setCarType(String carType)
    {
        mCarType = carType;
    }

    /**
     * Getter for property 'notifyuser'.
     *
     * @return Value for property 'notifyuser'.
     */
    public String getNotifyuser()
    {
        return mNotifyuser;
    }

    /**
     * Setter for property 'notifyuser'.
     *
     * @param notifyuser Value to set for property 'notifyuser'.
     */
    public void setNotifyuser(String notifyuser)
    {
        mNotifyuser = notifyuser;
    }

    public String getOauthId() {
        return mOauthId;
    }

    public void setOauthId(String oauthId) {
        mOauthId = oauthId;
    }
}
