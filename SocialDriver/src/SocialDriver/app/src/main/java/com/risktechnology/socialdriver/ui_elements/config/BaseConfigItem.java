package com.risktechnology.socialdriver.ui_elements.config;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;

/**
 * Created by Dev on 03/08/2015.
 */
public abstract class BaseConfigItem {

    private ConfigCallback mConfigCallback;
    private String mConfigName;
    private int mIconResId;
    private Context mContext;

    public ConfigCallback getConfigCallback() {
        return mConfigCallback;
    }

    public void setConfigCallback(ConfigCallback configCallback) {
        mConfigCallback = configCallback;
    }

    public String getConfigName() {
        return mConfigName;
    }

    public void setConfigName(String settingName) {
        mConfigName = settingName;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public int getIconResId() {
        return mIconResId;
    }

    public void setIconResId(int iconResId) {
        mIconResId = iconResId;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Drawable getIcon()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return mContext.getDrawable(mIconResId);
        }
        else
        {
            //noinspection deprecation
            return mContext.getResources().getDrawable(getIconResId());
        }
    }

}
