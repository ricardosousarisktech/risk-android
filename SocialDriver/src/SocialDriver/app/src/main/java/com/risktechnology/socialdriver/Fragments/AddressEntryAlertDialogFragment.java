package com.risktechnology.socialdriver.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.events.UserDetailsUpdatedEvent;
import com.risktechnology.socialdriver.models.UserModel;

import de.greenrobot.event.EventBus;

/**
 * Created by Matthew on 11/11/14.
 */
public class AddressEntryAlertDialogFragment extends DialogFragment {
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_POSITIVE_BUTTON = "KEY_POSITIVE_BUTTON";
    public static final String KEY_NEGATIVE_BUTTON = "KEY_NEGATIVE_BUTTON";
    private AlertResponseHandler mAlertResponseHandler;

    private EditText mAddressEditText;
    private EditText mPostcodeEditText;
    private TextView mTitleView;
    private Button mPositiveButton;
    private Button mNegativeButton;


    public AddressEntryAlertDialogFragment() {
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dialog.setContentView(R.layout.fragment_alert_address);

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        mTitleView = (TextView) dialog.findViewById(R.id.titleTextView);
        mPositiveButton = (Button) dialog.findViewById(R.id.positiveButton);
        mNegativeButton = (Button) dialog.findViewById(R.id.negativeButton);

        Bundle args = getArguments();
        String title = args.getString(KEY_TITLE, "");
        String positiveButtonText = args.getString(KEY_POSITIVE_BUTTON, "Save");
        String negativeButtonText = args.getString(KEY_NEGATIVE_BUTTON, "Cancel");

        mAddressEditText = (EditText) dialog.findViewById(R.id.addressTextEntry);
        mPostcodeEditText = (EditText) dialog.findViewById(R.id.postcodeTextEntry);

        mTitleView.setText(title);
        if(positiveButtonText.length() > 0)
        {
            mPositiveButton.setText(positiveButtonText);
            mPositiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UserModel.getInstance().setAddress(mAddressEditText.getText().toString());
                    UserModel.getInstance().setPostcode(mPostcodeEditText.getText().toString());
                    UserModel.getInstance().save();
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    dismiss();
                    EventBus.getDefault().post(new UserDetailsUpdatedEvent());
                    Fragment targetFragment = getTargetFragment();
                    if(targetFragment != null) {
                        targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                    }
                    if(mAlertResponseHandler != null) {
                        mAlertResponseHandler.buttonPressed(AddressEntryAlertDialogFragment.this);
                    }
                }
            });
        }
        else
        {
            mPositiveButton.setVisibility(View.GONE);

        }

        if(negativeButtonText.length() > 0)
        {
            mNegativeButton.setText(negativeButtonText);
            mNegativeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    dismiss();
                    Fragment targetFragment = getTargetFragment();
                    if (targetFragment != null) {
                        targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                    }
                    if (mAlertResponseHandler != null) {
                        mAlertResponseHandler.buttonPressed(AddressEntryAlertDialogFragment.this);
                    }
                }
            });
        }
        else
        {
            mNegativeButton.setVisibility(View.GONE);
        }

        mAddressEditText.setText(UserModel.getInstance().getAddress());
        mPostcodeEditText.setText(UserModel.getInstance().getPostcode());

        dialog.show();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        return dialog;
    }


    public void setAlertResponseHandler(AlertResponseHandler alertResponseHandler) {
        mAlertResponseHandler = alertResponseHandler;
    }

    public interface AlertResponseHandler {
        void buttonPressed(AddressEntryAlertDialogFragment alert);
    }

}
