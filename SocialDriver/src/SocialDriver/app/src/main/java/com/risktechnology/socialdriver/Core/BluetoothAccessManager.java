package com.risktechnology.socialdriver.core;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.risktechnology.socialdriver.application.SDApplication;
import com.risktechnology.socialdriver.events.BluetoothConnectionChangedEvent;
import com.risktechnology.socialdriver.events.JourneyDetailsUpdatedEvent;
import com.risktechnology.socialdriver.utilities.UserPreferences;

import java.util.ArrayList;
import java.util.Set;

import de.greenrobot.event.EventBus;

/**
 * Created by Dev on 25/08/2015.
 */
public class BluetoothAccessManager
{
    private static final String ALLOWED_BLUETOOTH_DEVICE_KEY = "allowed_bluetooth_devices";
    private static final String BLOCKED_BLUETOOTH_DEVICE_KEY = "blocked_bluetooth_devices";
    private static final String TAG = "BluetoothAccessManager";
    private static BluetoothAccessManager sInstance;

    private ArrayList<BluetoothDevice> mAllowedBluetoothDevices;
    private ArrayList<BluetoothDevice> mBlockedBluetoothDevices;
    private BluetoothAdapter mBluetoothAdapter;
    private BroadcastReceiver mRegisteredReceiver;

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            if (this != mRegisteredReceiver)
            {
                //catch old receivers
                SDApplication.getAppContext().unregisterReceiver(this);
                return;
            }
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action))
            {
                Bundle extras = intent.getExtras();
                BluetoothDevice device = extras.getParcelable(BluetoothDevice.EXTRA_DEVICE);
                Log.i(TAG, "A new bluetooth device has been connected: " + device.getName());
                if (mAllowedBluetoothDevices.contains(device))
                {
                    Log.i(TAG, "Connected to available device: " + device.getName());
                    if(JourneyManager.isInAutoMode())
                    {
                        JourneyManager.stopMonitoring();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                JourneyManager.startMonitoring(false);
                            }
                        }, 1000);
                    }
                    else
                    {
                        if(JourneyManager.isRunning())
                        {
                            return;
                        }
                        else
                        {
                            JourneyManager.startMonitoring(false);
                        }
                    }
                }
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action))
            {
                Bundle extras = intent.getExtras();
                BluetoothDevice device = extras.getParcelable(BluetoothDevice.EXTRA_DEVICE);
                if (mAllowedBluetoothDevices.contains(device))
                {
                    Log.i(TAG, "A bluetooth device has been disconnected: " + device.getName());
                    JourneyManager.stopMonitoring();
                    boolean autoJourneyEnabled = UserPreferences.getSharedInstance().getOptBoolForKey(Constants.SettingsKeyAutoJourneyDetect, true);
                    if(autoJourneyEnabled)
                    {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                JourneyManager.startMonitoring(true);
                                EventBus.getDefault().post(new JourneyDetailsUpdatedEvent());
                            }
                        }, 1000);
                    }
                }
            }
            EventBus.getDefault().post(new BluetoothConnectionChangedEvent());
        }
    };

    public static synchronized BluetoothAccessManager sharedManager()
    {
        if (sInstance == null)
        {
            sInstance = new BluetoothAccessManager();
        }
        return sInstance;
    }

    public BluetoothAccessManager()
    {
        loadDevices();
        findDevices();
        setupListeners();
    }

    private void setupListeners()
    {
        // Create a BroadcastReceiver for ACTION_FOUND
        Log.i(TAG, "Adding intent filters for detecting bluetooth connections");
        IntentFilter connectedFilter = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        IntentFilter disconnectedFilter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);

        SDApplication.getAppContext().registerReceiver(mBroadcastReceiver, connectedFilter);
        SDApplication.getAppContext().registerReceiver(mBroadcastReceiver, disconnectedFilter);
        mRegisteredReceiver = mBroadcastReceiver;
    }


    @SuppressWarnings("unchecked")
    private void loadDevices()
    {
        mAllowedBluetoothDevices = (ArrayList<BluetoothDevice>) UserPreferences.getSharedInstance().getArrayForKey(ALLOWED_BLUETOOTH_DEVICE_KEY, BluetoothDevice[].class);
        if (mAllowedBluetoothDevices == null)
        {
            mAllowedBluetoothDevices = new ArrayList<>();
        }
        mBlockedBluetoothDevices = (ArrayList<BluetoothDevice>) UserPreferences.getSharedInstance().getArrayForKey(BLOCKED_BLUETOOTH_DEVICE_KEY, BluetoothDevice[].class);
        if (mBlockedBluetoothDevices == null)
        {
            mBlockedBluetoothDevices = new ArrayList<>();
        }
    }

    private void saveDevices()
    {
        UserPreferences.getSharedInstance().putArrayList(mAllowedBluetoothDevices, ALLOWED_BLUETOOTH_DEVICE_KEY);
        UserPreferences.getSharedInstance().putArrayList(mBlockedBluetoothDevices, BLOCKED_BLUETOOTH_DEVICE_KEY);
    }

    private void findDevices()
    {
        if (mBluetoothAdapter == null)
        {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        if(mBluetoothAdapter == null)
        {
            //bluetooth not available, handle this
            return;
        }
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices != null)
        {
            for (BluetoothDevice bluetoothDevice : pairedDevices)
            {
                if (!mAllowedBluetoothDevices.contains(bluetoothDevice) && !mBlockedBluetoothDevices.contains(bluetoothDevice))
                {
                    mBlockedBluetoothDevices.add(bluetoothDevice);
                }
            }
        }

        saveDevices();
    }

    /**
     * Getter for property 'allowedBluetoothDevices'.
     *
     * @return Value for property 'allowedBluetoothDevices'.
     */
    public ArrayList<BluetoothDevice> getAllowedBluetoothDevices()
    {
        return mAllowedBluetoothDevices;
    }

    /**
     * Getter for property 'blockedBluetoothDevices'.
     *
     * @return Value for property 'blockedBluetoothDevices'.
     */
    public ArrayList<BluetoothDevice> getBlockedBluetoothDevices()
    {
        return mBlockedBluetoothDevices;
    }

    public void swapDevice(BluetoothDevice bluetoothDevice)
    {
        if (mAllowedBluetoothDevices.contains(bluetoothDevice))
        {
            mAllowedBluetoothDevices.remove(bluetoothDevice);
            mBlockedBluetoothDevices.add(bluetoothDevice);
        }
        else if (mBlockedBluetoothDevices.contains(bluetoothDevice))
        {
            mBlockedBluetoothDevices.remove(bluetoothDevice);
            mAllowedBluetoothDevices.add(bluetoothDevice);
        }
        saveDevices();
    }

    public void refreshDevices()
    {
        findDevices();
    }


    public void shutdown()
    {
        Log.i(TAG, "Shutting down bluetooth manager");
        if (mRegisteredReceiver != null)
        {
            SDApplication.getAppContext().unregisterReceiver(mRegisteredReceiver);
        }
    }
}
