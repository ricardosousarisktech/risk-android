package com.risktechnology.socialdriver.ui_elements.config;

/**
 * Created by Dev on 03/08/2015.
 */
public interface ConfigCallback {

    void configOptionSelected(BaseConfigItem configItem);
    void configValueChanged(BaseConfigItem configItem);

}
