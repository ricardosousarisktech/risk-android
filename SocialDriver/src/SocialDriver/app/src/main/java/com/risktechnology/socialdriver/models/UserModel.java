package com.risktechnology.socialdriver.models;

import android.content.Context;
import android.support.annotation.Nullable;

import com.risktechnology.socialdriver.application.SDApplication;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.events.UserDetailsUpdatedEvent;
import com.risktechnology.socialdriver.models.requestModels.DemographicRequestModel;
import com.risktechnology.socialdriver.models.requestModels.UpdateUserRequestModel;
import com.risktechnology.socialdriver.models.requestModels.UserDetailsRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.DemographicRequest;
import com.risktechnology.socialdriver.requests.UpdateUserRequest;
import com.risktechnology.socialdriver.requests.UserDetailsRequest;
import com.risktechnology.socialdriver.ui_elements.ProgressHUD;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.UserPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.greenrobot.event.EventBus;

/**
 * Created by Dev on 31/07/2015.
 */
public class UserModel extends BaseModel
{
    private static final String USER_FILENAME = "user.sdf";

    private static UserModel sInstance;
    private String mSecretKey;
    private String mUsername;
    private String mFirstName;
    private String mLastName;
    private String mGender;
    private String mAddress;
    private String mPostcode;
    private String mTimezone;
    private String mCulture;
    private String mCarType;
    private String mNotifyuser;
    private Date mDateOfBirth;
    private String mImageUrl;
    private int mUserId;
    private int mAgeDemographic;
    private int mGenderDemographic;
    private int mLocationDemographic;
    private int mCarTypeDemographic;

    public static synchronized UserModel getInstance()
    {
        if (sInstance == null)
        {
            sInstance = new UserModel();
            sInstance.load();
        }
        return sInstance;
    }

    private UserModel()
    {
        setFilename(USER_FILENAME);
    }

    @Override
    public void parseLoadedObject(BaseModel model)
    {
        UserModel userModel = (UserModel) model;
        mSecretKey = userModel.getSecretKey();
        mUsername = userModel.getUsername();
        mFirstName = userModel.getFirstName();
        mLastName = userModel.getLastName();
        mGender = userModel.getGender();

        mAddress = userModel.getAddress();
        mPostcode = userModel.getPostcode();

        mTimezone = userModel.getTimezone();
        mCulture = userModel.getCulture();
        mCarType = userModel.getCarType();
        mNotifyuser = userModel.getNotifyuser();
        mDateOfBirth = userModel.getDateOfBirth();
        mImageUrl = userModel.getImageUrl();
        mUserId = userModel.getUserId();
    }


    /**
     * Getter for property 'secretKey'.
     *
     * @return Value for property 'secretKey'.
     */
    public String getSecretKey()
    {
        return mSecretKey;
    }

    /**
     * Setter for property 'secretKey'.
     *
     * @param secretKey Value to set for property 'secretKey'.
     */
    public void setSecretKey(String secretKey)
    {
        mSecretKey = secretKey;
    }

    /**
     * Getter for property 'username'.
     *
     * @return Value for property 'username'.
     */
    public String getUsername()
    {
        return mUsername;
    }

    /**
     * Setter for property 'username'.
     *
     * @param username Value to set for property 'username'.
     */
    public void setUsername(String username)
    {
        mUsername = username;
    }

    /**
     * Getter for property 'firstName'.
     *
     * @return Value for property 'firstName'.
     */
    public String getFirstName()
    {
        if(Helpers.isEmpty(mFirstName))
        {
            return "";
        }
        return mFirstName;
    }

    /**
     * Setter for property 'firstName'.
     *
     * @param firstName Value to set for property 'firstName'.
     */
    public void setFirstName(String firstName)
    {
        mFirstName = firstName;
    }

    /**
     * Getter for property 'lastName'.
     *
     * @return Value for property 'lastName'.
     */
    public String getLastName()
    {
        if(Helpers.isEmpty(mLastName))
        {
            return "";
        }
        return mLastName;
    }

    /**
     * Setter for property 'lastName'.
     *
     * @param lastName Value to set for property 'lastName'.
     */
    public void setLastName(String lastName)
    {
        mLastName = lastName;
    }

    /**
     * Getter for property 'gender'.
     *
     * @return Value for property 'gender'.
     */
    public String getGender()
    {
        return mGender;
    }

    /**
     * Setter for property 'gender'.
     *
     * @param gender Value to set for property 'gender'.
     */
    public void setGender(String gender)
    {
        if(gender.equals("M"))
        {
            gender = "Male";
        }

        if(gender.equals("F"))
        {
            gender = "Female";
        }

        mGender = gender;
    }


    /**
     * Getter for property 'postcode'.
     *
     * @return Value for property 'postcode'.
     */
    public String getPostcode()
    {
        return mPostcode;
    }

    /**
     * Setter for property 'postcode'.
     *
     * @param postcode Value to set for property 'postcode'.
     */
    public void setPostcode(String postcode)
    {
        mPostcode = postcode;
    }

    /**
     * Getter for property 'timezone'.
     *
     * @return Value for property 'timezone'.
     */
    public String getTimezone()
    {
        return mTimezone;
    }

    /**
     * Setter for property 'timezone'.
     *
     * @param timezone Value to set for property 'timezone'.
     */
    public void setTimezone(String timezone)
    {
        mTimezone = timezone;
    }

    /**
     * Getter for property 'culture'.
     *
     * @return Value for property 'culture'.
     */
    public String getCulture()
    {
        return mCulture;
    }

    /**
     * Setter for property 'culture'.
     *
     * @param culture Value to set for property 'culture'.
     */
    public void setCulture(String culture)
    {
        mCulture = culture;
    }

    /**
     * Getter for property 'carType'.
     *
     * @return Value for property 'carType'.
     */
    public String getCarType()
    {
        return mCarType;
    }

    /**
     * Setter for property 'carType'.
     *
     * @param carType Value to set for property 'carType'.
     */
    public void setCarType(String carType)
    {
        mCarType = carType;
    }

    /**
     * Getter for property 'notifyuser'.
     *
     * @return Value for property 'notifyuser'.
     */
    public String getNotifyuser()
    {
        return mNotifyuser;
    }

    /**
     * Setter for property 'notifyuser'.
     *
     * @param notifyuser Value to set for property 'notifyuser'.
     */
    public void setNotifyuser(String notifyuser)
    {
        mNotifyuser = notifyuser;
    }

    /**
     * Getter for property 'dateOfBirth'.
     *
     * @return Value for property 'dateOfBirth'.
     */
    public Date getDateOfBirth()
    {
        return mDateOfBirth;
    }

    /**
     * Setter for property 'dateOfBirth'.
     *
     * @param dateOfBirth Value to set for property 'dateOfBirth'.
     */
    public void setDateOfBirth(Date dateOfBirth)
    {
        mDateOfBirth = dateOfBirth;
    }

    /**
     * Getter for property 'imageUrl'.
     *
     * @return Value for property 'imageUrl'.
     */
    public String getImageUrl()
    {
        return mImageUrl;
    }

    /**
     * Setter for property 'imageUrl'.
     *
     * @param imageUrl Value to set for property 'imageUrl'.
     */
    public void setImageUrl(String imageUrl)
    {
        mImageUrl = imageUrl;
    }

    public boolean requiresConfiguration()
    {
        return false;
//        boolean requiresConfiguration = (Helpers.isEmpty(getFirstName()) ||
//                Helpers.isEmpty(getLastName()) ||
//                Helpers.isEmpty(getGender()) ||
//                Helpers.isEmpty(getAddress()) ||
//                Helpers.isEmpty(getPostcode()) ||
//                Helpers.isEmpty(getCarType()) ||
//                Helpers.isEmpty(getDateOfBirth()));
//        return requiresConfiguration;
    }

    public void setUserId(int userId)
    {
        mUserId = userId;
    }

    public int getUserId()
    {
        return mUserId;
    }

    public String getAddress()
    {
        return mAddress;
    }

    public void setAddress(String address)
    {
        mAddress = address;
    }

    public void logout()
    {
        sInstance = new UserModel();
        sInstance.save();
        sInstance.load();
    }

    public void load()
    {
        super.load();
        EventBus.getDefault().post(new UserDetailsUpdatedEvent());
    }

    public void upload()
    {
        upload(false, null);
    }

    public void upload(boolean showLoader, @Nullable Context context)
    {

        UpdateUserRequestModel model = new UpdateUserRequestModel();
        model.setUserId(getUserId() + "");
        model.setLastname(getLastName());
        model.setFirstname(getFirstName());
        model.setAddress(getAddress());
        model.setCarType(getCarType());
        model.setDOB(getDateOfBirth());
        model.setPostcode(getPostcode());
        model.setGender(getGender());

        boolean notifications = UserPreferences.getSharedInstance().getOptBoolForKey(Constants.SettingsKeyNotifications, false);

        model.setNotifyuser((notifications ? "1" : "0"));

        UpdateUserRequest request = new UpdateUserRequest(model);
        request.setUseEncryption(true);

        if(showLoader && context != null)
        {
            final ProgressHUD hud = ProgressHUD.show(context, "Saving user", true, false, null);
            request.setRequestHandler(new BaseRequest.RequestHandler()
            {
                @Override
                public void requestFinished(BaseRequest request)
                {
                    hud.dismiss();
                }
            });
        }

        request.executeRequest(SDApplication.getAppContext());
    }

    public String formattedAddress()
    {
        ArrayList<String> components = new ArrayList<>();
        if(!Helpers.isEmpty(UserModel.getInstance().getAddress()))
        {
            components.add(UserModel.getInstance().getAddress());
        }

        if(!Helpers.isEmpty(UserModel.getInstance().getPostcode()))
        {
            components.add(UserModel.getInstance().getPostcode());
        }

        String addressString = Helpers.joinWithString(components, "\n");

        if(Helpers.isEmpty(addressString))
        {
           addressString = "";
        }

        return addressString;
    }

    public void download()
    {
        download(false, null);
    }

    public void download(boolean showLoader, @Nullable Context context)
    {
        UserDetailsRequestModel model = new UserDetailsRequestModel();
        model.setUserId(getUserId() + "");

        UserDetailsRequest request = new UserDetailsRequest(model);
        request.setUseEncryption(true);

        ProgressHUD hud = null;

        if(showLoader && context != null)
        {
            hud = ProgressHUD.show(context, "Retrieving user...", true, false, null);
        }

        final ProgressHUD finalHud = hud;

        request.setRequestHandler(new BaseRequest.RequestHandler()
        {
            @Override
            public void requestFinished(BaseRequest request)
            {
                if(finalHud != null)
                {
                    finalHud.dismiss();
                }

                if(request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                {
                    /*{"Username":"1696083337270732","Firstname":"Matthew","Lastname":"Cawley",
                    "Dob":"1985-02-26T00:00:00Z","Gender":"","Address":"31","Postcode":"SK56XW","CarType":"Medium Car","NotifyUser":false,"OauthId":"1696083337270732","Success":true}*/
                    JSONObject responseObject = request.getResponseObject();
                    setUsername(responseObject.optString("Username", getUsername()));
                    setFirstName(responseObject.optString("Firstname", getFirstName()));
                    setLastName(responseObject.optString("Lastname", getLastName()));
                    setGender(responseObject.optString("Gender", getGender()));
                    setAddress(responseObject.optString("Address", getAddress()));
                    setPostcode(responseObject.optString("Postcode", getPostcode()));
                    setCarType(responseObject.optString("CarType", getCarType()));

                    String dobString = (responseObject.optString("Dob", ""));
                    if(!Helpers.isEmpty(dobString))
                        try
                        {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
                            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date dobDate = formatter.parse(dobString);
                            setDateOfBirth(dobDate);
                        } catch (ParseException e)
                        {
                            //invalid date was returned so simply nullify it.
                        }

                    boolean notify = responseObject.optBoolean("NotifyUser", UserPreferences.getSharedInstance().getBoolForKey(Constants.SettingsKeyNotifications));
                    UserPreferences.getSharedInstance().putBoolean(notify, Constants.SettingsKeyNotifications);
                    downloadDemographics(new Date());
                    EventBus.getDefault().post(new UserDetailsUpdatedEvent());

                }
            }
        });

        request.executeRequest(SDApplication.getAppContext());
    }

    public void downloadDemographics(@Nullable Date date)
    {
        if(date == null)
        {
            date = new Date();
        }
        downloadDemographics(date, false, null);
    }

    public void downloadDemographics(@Nullable Date date, boolean showLoader, @Nullable Context context)
    {
        DemographicRequestModel model = new DemographicRequestModel(date);
        model.setUserId(getUserId() + "");

        DemographicRequest request = new DemographicRequest(model);
        request.setUseEncryption(true);

        ProgressHUD hud = null;

        if(showLoader && context != null)
        {
            hud = ProgressHUD.show(context, "Retrieving demographics...", true, false, null);
        }

        final ProgressHUD finalHud = hud;

        request.setRequestHandler(new BaseRequest.RequestHandler()
        {
            @Override
            public void requestFinished(BaseRequest request)
            {
                if(finalHud != null)
                {
                    finalHud.dismiss();
                }

                if(request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                {
                    JSONObject responseObject = request.getResponseObject();

                    setAgeDemographic(responseObject.optInt("Age", -1));
                    setGenderDemographic(responseObject.optInt("Gender", -1));
                    setLocationDemographic(responseObject.optInt("Location", -1));
                    setCarTypeDemographic(responseObject.optInt("CarType", -1));

                }
                else
                {

                    setAgeDemographic(-1);
                    setGenderDemographic(-1);
                    setLocationDemographic(-1);
                    setCarTypeDemographic(-1);
                }
                EventBus.getDefault().post(new UserDetailsUpdatedEvent());
            }
        });

        request.executeRequest(SDApplication.getAppContext());
    }

    public void setAgeDemographic(int ageDemographic)
    {
        mAgeDemographic = ageDemographic;
    }

    public int getAgeDemographic()
    {
        return mAgeDemographic;
    }

    public void setGenderDemographic(int genderDemographic)
    {
        mGenderDemographic = genderDemographic;
    }

    public int getGenderDemographic()
    {
        return mGenderDemographic;
    }

    public void setLocationDemographic(int locationDemographic)
    {
        mLocationDemographic = locationDemographic;
    }

    public int getLocationDemographic()
    {
        return mLocationDemographic;
    }

    public void setCarTypeDemographic(int carTypeDemographic)
    {
        mCarTypeDemographic = carTypeDemographic;
    }

    public int getCarTypeDemographic()
    {
        return mCarTypeDemographic;
    }

    public String getFullName()
    {
        String fullName = "";
        if(!Helpers.isEmpty(mFirstName))
        {
            fullName += mFirstName + " ";
        }

        if(!Helpers.isEmpty(mLastName))
        {
            fullName += mLastName;
        }

        return fullName.trim();
    }
}
