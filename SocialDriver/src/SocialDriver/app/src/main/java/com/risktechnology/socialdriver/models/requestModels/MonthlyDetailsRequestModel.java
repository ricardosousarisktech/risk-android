package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings("unused")
public class MonthlyDetailsRequestModel extends BaseRequestModel
{
    private String mIMEI;
    private String mStartDateOfMonth;
    private int mUserId;

    public MonthlyDetailsRequestModel()
    {
        mIMEI = "";
        mStartDateOfMonth = "";
        mUserId = 0;
    }


    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException
    {
//
//        if(!Helpers.isEmpty(mIMEI))
//        {
//            jsonObject.put("IMEI", mIMEI);
//        }

        if(!Helpers.isEmpty(mStartDateOfMonth))
        {
            jsonObject.put("StartofMonth", mStartDateOfMonth);
        }

        if(mUserId > 0)
        {
            jsonObject.put("UserID", mUserId);
        }
    }

    @Override
    public void describeModel()
    {

    }

    /**
     * Getter for property 'IMEI'.
     *
     * @return Value for property 'IMEI'.
     */
    public String getIMEI()
    {
        return mIMEI;
    }

    /**
     * Setter for property 'IMEI'.
     *
     * @param IMEI Value to set for property 'IMEI'.
     */
    public void setIMEI(String IMEI)
    {
        mIMEI = IMEI;
    }

    /**
     * Getter for property 'startDateOfMonth'.
     *
     * @return Value for property 'startDateOfMonth'.
     */
    public String getStartDateOfMonth()
    {
        return mStartDateOfMonth;
    }

    /**
     * Setter for property 'startDateOfMonth'.
     *
     * @param startDateOfMonth Value to set for property 'startDateOfMonth'.
     */
    public void setStartDateOfMonth(String startDateOfMonth)
    {
        mStartDateOfMonth = startDateOfMonth;
    }

    /**
     * Getter for property 'userId'.
     *
     * @return Value for property 'userId'.
     */
    public int getUserId()
    {
        return mUserId;
    }

    /**
     * Setter for property 'userId'.
     *
     * @param userId Value to set for property 'userId'.
     */
    public void setUserId(int userId)
    {
        mUserId = userId;
    }
}
