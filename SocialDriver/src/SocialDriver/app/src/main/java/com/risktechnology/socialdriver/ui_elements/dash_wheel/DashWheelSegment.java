package com.risktechnology.socialdriver.ui_elements.dash_wheel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.UiUtilities;

public class DashWheelSegment extends LinearLayout {

    private Context mContext;
    private ImageView mIconView;
    private String mInfoText;
    private int mEndAngle;
    private int mStartAngle;
    private int mBaseRotation;
    private int mSegmentSize;
    private int mNormalSegmentColour;
    private int mSelectedSegmentColour;
    private int mUnavailableSegmentColour;
    private int mUnavailableSelectedSegmentColour;
    private int mSegmentBorderColour;
    private int mSegmentDimension;
    private boolean mIsSelected;
    private DashWheelSegmentDrawable mDashWheelSegmentDrawable;
    private String mName;

    private Constants.DashWheelSegments mSegmentTag = Constants.DashWheelSegments.SEGMENT_CAR;


    public DashWheelSegment(Context context) {
        super(context);
        mContext = context;
    }

    public DashWheelSegment(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public DashWheelSegment(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public void loadSegment(int startAngle, int endAngle, int width, int height, Constants.DashWheelSegments segmentTag) {

        mSegmentTag = segmentTag;
        mSegmentDimension = Math.min(width, height);

        mNormalSegmentColour = getResources().getColor(R.color.menu_segment);
        mSelectedSegmentColour = getResources().getColor(R.color.menu_selected_segment);
        mUnavailableSegmentColour = getResources().getColor(R.color.menu_unavailable_segment);
        mUnavailableSelectedSegmentColour = getResources().getColor(R.color.menu_unavailable_selected_segment);
        mSegmentBorderColour = getResources().getColor(R.color.menu_segment_border);

        mSegmentSize = endAngle;
        mStartAngle = 0 - (mSegmentSize / 2) - mSegmentSize;
        mEndAngle = mSegmentSize;
        mBaseRotation = (startAngle);
        mDashWheelSegmentDrawable = new DashWheelSegmentDrawable();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(mSegmentDimension, mSegmentDimension);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        setLayoutParams(layoutParams);
        setBackground(mDashWheelSegmentDrawable);
        setRotation(mBaseRotation);
    }

    public void setIcon(int image)
    {
        mIconView = new ImageView(mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(mSegmentDimension / 8, mSegmentDimension / 8);
        mIconView.setImageResource(image);
        mIconView.setColorFilter(Color.argb(255, 255, 255, 255));
        layoutParams.weight = 1.0f;
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        mIconView.setLayoutParams(layoutParams);
        addView(mIconView);
    }

    public void setName(String name)
    {
        mName = name;
    }

    public String getName()
    {
        return mName;
    }

    class DashWheelSegmentDrawable extends Drawable {

        private Canvas mCanvas;
        private Canvas mSelectedCanvas;
        private Bitmap mBitmap;
        private Bitmap mSelectedBitmap;
        private Paint mPaint;
        private RectF mRectF;

        public DashWheelSegmentDrawable() {

            mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mPaint.setColor(mNormalSegmentColour);
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setFilterBitmap(true);
            mRectF = new RectF();

        }

        @SuppressWarnings("SuspiciousNameCombination")
        public void generateBitmaps()
        {
            if(mBitmap != null && mSelectedBitmap != null)
            {
                return;
            }
            Rect bounds = getBounds();

            int width = bounds.width();
            int height = bounds.height();

            if (width > height) {
                width = height;
            } else {
                height = width;
            }

            if(width == 0|| height == 0)
            {
                return ;
            }

            bounds = new Rect(0, 0, width, height);

            LinearLayout.LayoutParams layoutParams = (LayoutParams) mIconView.getLayoutParams();
            layoutParams.topMargin = (mSegmentDimension / 16);
            mIconView.setLayoutParams(layoutParams);

            mBitmap = Bitmap.createBitmap(bounds.width(), bounds.height(), Bitmap.Config.ARGB_8888);
            mSelectedBitmap = Bitmap.createBitmap(bounds.width(), bounds.height(), Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            mSelectedCanvas = new Canvas(mSelectedBitmap);

            mRectF.set(bounds);

            mPaint.setXfermode(null);

            mPaint.setColor(mNormalSegmentColour);

            switch (mSegmentTag)
            {
                case SEGMENT_CAR:
                    if(Helpers.isEmpty(UserModel.getInstance().getCarType()))
                    {
                        mPaint.setColor(mUnavailableSegmentColour);
                    }
                    break;
                case SEGMENT_AGE:
                    if(UserModel.getInstance().getDateOfBirth() == null)
                    {
                        mPaint.setColor(mUnavailableSegmentColour);
                    }
                    break;
                case SEGMENT_LOCATION:
                    if(Helpers.isEmpty(UserModel.getInstance().getAddress()) && Helpers.isEmpty(UserModel.getInstance().getPostcode()))
                    {
                        mPaint.setColor(mUnavailableSegmentColour);
                    }
                    break;
                case SEGMENT_GENDER:
                    if(Helpers.isEmpty(UserModel.getInstance().getGender()))
                    {
                        mPaint.setColor(mUnavailableSegmentColour);
                    }
                    break;
                default:

                    break;
            }
            mPaint.setStyle(Paint.Style.FILL);

            mCanvas.drawArc(mRectF, mStartAngle, mEndAngle, true, mPaint);


            mPaint.setColor(mSelectedSegmentColour);
            switch (mSegmentTag)
            {
                case SEGMENT_CAR:
                    if(Helpers.isEmpty(UserModel.getInstance().getCarType()))
                    {
                        mPaint.setColor(mUnavailableSelectedSegmentColour);
                    }
                    break;
                case SEGMENT_AGE:
                    if(UserModel.getInstance().getDateOfBirth() == null)
                    {
                        mPaint.setColor(mUnavailableSelectedSegmentColour);
                    }
                    break;
                case SEGMENT_LOCATION:
                    if(Helpers.isEmpty(UserModel.getInstance().getAddress()) && Helpers.isEmpty(UserModel.getInstance().getPostcode()))
                    {
                        mPaint.setColor(mUnavailableSelectedSegmentColour);
                    }
                    break;
                case SEGMENT_GENDER:
                    if(Helpers.isEmpty(UserModel.getInstance().getGender()))
                    {
                        mPaint.setColor(mUnavailableSelectedSegmentColour);
                    }
                    break;
                default:

                    break;
            }

            mSelectedCanvas.drawArc(mRectF, mStartAngle, mEndAngle, true, mPaint);

            mPaint.setColor(mSegmentBorderColour);
            mPaint.setStrokeWidth(2);
            mPaint.setStyle(Paint.Style.STROKE);
            mCanvas.drawArc(mRectF, mStartAngle, mEndAngle, true, mPaint);
            mSelectedCanvas.drawArc(mRectF, mStartAngle, mEndAngle, true, mPaint);

            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(getResources().getColor(android.R.color.transparent));
            mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            mCanvas.drawCircle(width / 2, height / 2, width / 4, mPaint);
            mSelectedCanvas.drawCircle(width / 2, height / 2, width / 4, mPaint);

            mPaint.setXfermode(null);
            mPaint.setColor(mSegmentBorderColour);
            mPaint.setStrokeWidth(2);
            mPaint.setStyle(Paint.Style.STROKE);
            mCanvas.drawCircle(width / 2, height / 2, width / 4, mPaint);
            mSelectedCanvas.drawCircle(width / 2, height / 2, width / 4, mPaint);

        }

        @Override
        public void draw(Canvas canvas) {
            canvas.save();


            generateBitmaps();

            if(mBitmap != null) {
                if(mIsSelected) {
                    canvas.drawBitmap(mSelectedBitmap, 0, (getHeight() / 2) - (mBitmap.getHeight() / 2), mPaint);
                }
                else
                {
                    canvas.drawBitmap(mBitmap, 0, (getHeight() / 2) - (mBitmap.getHeight() / 2), mPaint);
                }
            }
        }

        @Override
        public void setAlpha(int alpha) {
            // Has no effect
        }

        @Override
        public void setColorFilter(ColorFilter cf) {
            // Has no effect
        }

        @Override
        public int getOpacity() {
            return 0;
        }


        public void destroy() {

            mCanvas = null;
            mSelectedCanvas = null;
            mBitmap.recycle();
            mBitmap = null;
            mSelectedBitmap.recycle();
            mSelectedBitmap = null;
            mPaint = null;
            mRectF = null;
        }

        public void refreshBitmaps()
        {
            if(mBitmap != null)
            {
                mBitmap.recycle();
                mBitmap = null;
            }

            if(mSelectedBitmap != null)
            {
                mSelectedBitmap.recycle();
                mSelectedBitmap = null;
            }
            generateBitmaps();
        }
    }

    public void refreshBitmaps()
    {
        if(getParent() != null)
        {
            if (mDashWheelSegmentDrawable != null)
            {
                mDashWheelSegmentDrawable.refreshBitmaps();
            }
        }
    }



    public int getBaseRotation() {
        return mBaseRotation;
    }

    public String getInfoText() {
        return mInfoText;
    }

    public void setInfoText(String infoText) {
        mInfoText = infoText;
    }

    public void selectSegment()
    {
        mIsSelected = true;
        invalidate();

    }

    public void deselectSegment()
    {
        mIsSelected = false;
        invalidate();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mContext = null;
        mIconView = null;
        mInfoText = null;
        mDashWheelSegmentDrawable.destroy();
    }

}