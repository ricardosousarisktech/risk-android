package com.risktechnology.socialdriver.models.requestModels;

import android.util.Log;

import com.risk.socialdriver.journeyapp.JourneyData;
import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class MobileTelemetryRequestModel extends BaseRequestModel
{
    private static final String TAG = "MobileTelemetry";
    private String mMsgType;
    private String mGpsTime;
    private String mGpsLatitude;
    private String mGpsLongitude;
    private String mGpsOrientation;
    private String mGpsSpeed;
    private String mGpsFix;
    private String mAccelValue;

    public MobileTelemetryRequestModel()
    {
        mMsgType = "";
        mGpsTime = "";
        mGpsLatitude = "";
        mGpsLongitude = "";
        mGpsOrientation = "";
        mGpsSpeed = "";
        mGpsFix = "";
    }


    public void populateFromJourneyDate(JourneyData data)
    {
        if(data != null)
        {
            mMsgType = data.type;

            Log.i("JOURNEY_TYPE", data.journey);
            try
            {
                String gpsTime = data.timestamp;
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date gpsDate = formatter.parse(gpsTime);
                mGpsTime = "/Date(" + gpsDate.getTime() + ")/";
            } catch (ParseException e)
            {
                e.printStackTrace();
            }
            mGpsLatitude = data.lat;
            mGpsLongitude = data.lon;
            mGpsOrientation = data.heading;
            mGpsSpeed = data.speed;
            mGpsFix = data.sats;
            mAccelValue = data.accel;
        }
    }

    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException
    {
        if(!Helpers.isEmpty(mAccelValue))
        {
            jsonObject.put("AccelDecel", mAccelValue);
        }

        if(!Helpers.isEmpty(mGpsFix))
        {
            jsonObject.put("GpsFix", mGpsFix);
        }

        if(!Helpers.isEmpty(mGpsLatitude))
        {
            jsonObject.put("GpsLatitude", mGpsLatitude);
        }

        if(!Helpers.isEmpty(mGpsLongitude))
        {
            jsonObject.put("GpsLongitude", mGpsLongitude);
        }

        if(!Helpers.isEmpty(mGpsOrientation))
        {
            jsonObject.put("GpsOrientation", mGpsOrientation);
        }

        if(!Helpers.isEmpty(mGpsSpeed))
        {
            jsonObject.put("GpsSpeed", mGpsSpeed);
        }

        if(!Helpers.isEmpty(mGpsTime))
        {
            jsonObject.put("GpsTime", mGpsTime);
        }

        if(!Helpers.isEmpty(mMsgType))
        {
            jsonObject.put("MsgType", mMsgType);
        }
    }

    @Override
    public void describeModel()
    {

    }

    /**
     * Getter for property 'msgType'.
     *
     * @return Value for property 'msgType'.
     */
    public String getMsgType()
    {
        return mMsgType;
    }

    /**
     * Setter for property 'msgType'.
     *
     * @param msgType Value to set for property 'msgType'.
     */
    public void setMsgType(String msgType)
    {
        mMsgType = msgType;
    }

    /**
     * Getter for property 'gpsTime'.
     *
     * @return Value for property 'gpsTime'.
     */
    public String getGpsTime()
    {
        return mGpsTime;
    }

    /**
     * Setter for property 'gpsTime'.
     *
     * @param gpsTime Value to set for property 'gpsTime'.
     */
    public void setGpsTime(String gpsTime)
    {
        mGpsTime = gpsTime;
    }

    /**
     * Getter for property 'gpsLatitude'.
     *
     * @return Value for property 'gpsLatitude'.
     */
    public String getGpsLatitude()
    {
        return mGpsLatitude;
    }

    /**
     * Setter for property 'gpsLatitude'.
     *
     * @param gpsLatitude Value to set for property 'gpsLatitude'.
     */
    public void setGpsLatitude(String gpsLatitude)
    {
        mGpsLatitude = gpsLatitude;
    }

    /**
     * Getter for property 'gpsLongitude'.
     *
     * @return Value for property 'gpsLongitude'.
     */
    public String getGpsLongitude()
    {
        return mGpsLongitude;
    }

    /**
     * Setter for property 'gpsLongitude'.
     *
     * @param gpsLongitude Value to set for property 'gpsLongitude'.
     */
    public void setGpsLongitude(String gpsLongitude)
    {
        mGpsLongitude = gpsLongitude;
    }

    /**
     * Getter for property 'gpsOrientation'.
     *
     * @return Value for property 'gpsOrientation'.
     */
    public String getGpsOrientation()
    {
        return mGpsOrientation;
    }

    /**
     * Setter for property 'gpsOrientation'.
     *
     * @param gpsOrientation Value to set for property 'gpsOrientation'.
     */
    public void setGpsOrientation(String gpsOrientation)
    {
        mGpsOrientation = gpsOrientation;
    }

    /**
     * Getter for property 'gpsSpeed'.
     *
     * @return Value for property 'gpsSpeed'.
     */
    public String getGpsSpeed()
    {
        return mGpsSpeed;
    }

    /**
     * Setter for property 'gpsSpeed'.
     *
     * @param gpsSpeed Value to set for property 'gpsSpeed'.
     */
    public void setGpsSpeed(String gpsSpeed)
    {
        mGpsSpeed = gpsSpeed;
    }

    /**
     * Getter for property 'gpsFix'.
     *
     * @return Value for property 'gpsFix'.
     */
    public String getGpsFix()
    {
        return mGpsFix;
    }

    /**
     * Setter for property 'gpsFix'.
     *
     * @param gpsFix Value to set for property 'gpsFix'.
     */
    public void setGpsFix(String gpsFix)
    {
        mGpsFix = gpsFix;
    }

}
