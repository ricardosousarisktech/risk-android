package com.risktechnology.socialdriver.core;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.risk.socialdriver.journeyapp.JourneyData;
import com.risk.socialdriver.journeyapp.JourneyService;
import com.risk.socialdriver.journeyapp.events.ActivityStatusEvent;
import com.risk.socialdriver.journeyapp.events.HighAccelerationEvent;
import com.risk.socialdriver.journeyapp.events.HighBrakingEvent;
import com.risk.socialdriver.journeyapp.events.JourneyDataEvent;
import com.risk.socialdriver.journeyapp.events.JourneyListEvent;
import com.risk.socialdriver.journeyapp.events.JourneyStartEvent;
import com.risk.socialdriver.journeyapp.events.NewLocationEvent;
import com.risk.socialdriver.journeyapp.events.SpeedEvent;
import com.risk.socialdriver.journeyapp.utils.Session;
import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.application.SDApplication;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.models.requestModels.MobileTelemetryRequestModel;
import com.risktechnology.socialdriver.models.requestModels.ProcessJourneyRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.ProcessJourneyRequest;
import com.risktechnology.socialdriver.utilities.FileManager;
import com.risktechnology.socialdriver.utilities.UserPreferences;

import java.util.ArrayList;
import java.util.Date;

import de.greenrobot.event.EventBus;

/**
 * Created by Dev on 24/08/2015.
 */

@SuppressWarnings("unused")
public class JourneyManager extends JourneyService
{
    private static final String TAG = "JourneyManager";
    private static final String PENDING_JOURNEYS = "pendingJourneys.sdf";
    private static final Integer MINS_TO_TRIM_AUTO = 0;
    private static final Integer MINS_TO_TRIM_MANUAL = 0;
    private static JourneyManager sInstance;
    private int mActivityStatus;
    private int mSpeed;
    private static Activity sActivityContext;

    private static long mStartTime = 0;

    private ArrayList<ProcessJourneyRequestModel> mJourneysToUpload;

    public static synchronized JourneyManager sharedManager()
    {
        if(sInstance == null)
        {
            sInstance = new JourneyManager();
        }
        return sInstance;
    }

    public JourneyManager()
    {
        loadPendingJourneys();
        if(mJourneysToUpload == null)
        {
            mJourneysToUpload = new ArrayList<>();
        }
    }

    @SuppressWarnings("unchecked")
    private void loadPendingJourneys()
    {
       mJourneysToUpload = (ArrayList<ProcessJourneyRequestModel>) FileManager.getObject(PENDING_JOURNEYS, SDApplication.getAppContext());
       processQueue();
    }

    public void prepare()
    {
        JourneyManager.init();

        JourneyManager.ConfigureNotification("Social Driver", "New journey started", "Journey currently in progress", R.drawable.social_driver_logo, R.drawable.social_driver_logo);

//        JourneyConfig.GpsConfig config = JourneyConfig.getGpsConfiguration();
//        config.snrThreshold = 4.0f;
//        config.minSatsCnt = 1;
//        JourneyConfig.setGpsConfiguration(config);
        Session.getInstance();
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this, -1);
        }
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        EventBus.getDefault().unregister(sharedManager());
    }

    private static void init()
    {
        init(SDApplication.getAppContext());
    }

    /* redirects with global context */
    public static void startMonitoring(boolean autoMode)
    {
        if(getActivityContext() == null)
        {
            return;
        }

        JourneyManager.sharedManager().setStartTime(0);

        if(autoMode)
        {
            if(!UserPreferences.getSharedInstance().getOptBoolForKey(Constants.SettingsKeyAutoJourneyDetect, true))
            {
                Log.i(TAG, "Auto journey detect disabled in settings");
                return;
            }
        }

        Log.i(TAG, "startMonitoring in " + (autoMode ? "auto" : "manual") + " mode");
        StartMonitoring(getActivityContext(), autoMode);
    }

    /* redirects with global context */
    public static void stopMonitoring()
    {
        if(getActivityContext() == null)
        {
            return;
        }
        Log.i(TAG, "stopMonitoring");
        StopMonitoring(getActivityContext());
    }

    /* redirects with global context */
    public static void deleteAllJourneys()
    {
        if(getActivityContext() == null)
        {
            return;
        }
        Log.i(TAG, "deleteAllJourneys");
        deleteAllJourneys(getActivityContext());
    }

    /* redirects with global context */
    public static ArrayList<JourneyData> getAllJourneys()
    {
        if(getActivityContext() == null)
        {
            return null;
        }
        Log.i(TAG, "getAllJourneys");
        return getAllJourneys(getActivityContext());
    }


    //Events
    public void onEvent(SpeedEvent event) {
        Log.i(TAG, "SpeedEvent");
        mSpeed = event.mSpeed;
    }

    //Events
    public void onEvent(HighBrakingEvent event) {
        Log.i(TAG, "SpeedEvent");
        //m = event.mSpeed;
    }

    // Update the list of journey events that we have
    public void onEvent(JourneyDataEvent event) {
        Log.i(TAG, "JourneyDataEvent");
    }

    public void onEvent(ActivityStatusEvent event) {
        mActivityStatus = event.mStatus;
        Log.i(TAG, "ActivityStatusEvent");
    }

    public void onEvent(NewLocationEvent event) {
        Log.i(TAG, "NewLocationEvent");
    }

    public void onEvent(JourneyListEvent event){
        Log.i(TAG, "JourneyListEvent");
    }

    
    public void onEvent(JourneyStartEvent event) {
        if(getActivityContext() == null)
        {
            return;
        }
        Log.i(TAG, "JourneyStartEvent");
        if(event.start){
            setStartTime((new Date()).getTime());
            Toast.makeText(SDApplication.getAppContext(), "Journey started", Toast.LENGTH_SHORT).show();
        } else {
            setStartTime(0);
            processCompletedJourney();
        }
        Log.i("JourneyStartEvent", "Event: " + event.toString());

    }

    private void processCompletedJourney()
    {
        ArrayList<JourneyData> allJourneys = getAllJourneys();

        ArrayList<MobileTelemetryRequestModel> telemetryRequestModels = new ArrayList<>();

        for(JourneyData data : allJourneys)
        {
            MobileTelemetryRequestModel model = new MobileTelemetryRequestModel();
            model.populateFromJourneyDate(data);
            telemetryRequestModels.add(model);
        }

        if(telemetryRequestModels.size() > 0)
        {
            ProcessJourneyRequestModel requestModel = new ProcessJourneyRequestModel();
            requestModel.setMinsToTrim((isInAutoMode() ? MINS_TO_TRIM_AUTO : MINS_TO_TRIM_MANUAL));
            requestModel.setMobileTelemetry(telemetryRequestModels);
            requestModel.setUserId(UserModel.getInstance().getUserId());
            addToUploadQueue(requestModel);
            Toast.makeText(SDApplication.getAppContext(), "Journey finished", Toast.LENGTH_SHORT).show();
        }
    }

    private void addToUploadQueue(ProcessJourneyRequestModel requestModel)
    {
        mJourneysToUpload.add(requestModel);
        deleteAllJourneys();
        FileManager.saveObject(mJourneysToUpload, PENDING_JOURNEYS, SDApplication.getAppContext());
        processQueue();
    }

    private void removeFromQueue(ProcessJourneyRequestModel requestModel)
    {
        mJourneysToUpload.remove(requestModel);
        FileManager.saveObject(mJourneysToUpload, PENDING_JOURNEYS, SDApplication.getAppContext());
        processQueue();
    }

    public void processQueue()
    {
        if(mJourneysToUpload == null)
        {
            //nothing to process
            return;
        }

        if(mJourneysToUpload.size() > 0)
        {
            final ProcessJourneyRequestModel requestModel = mJourneysToUpload.get(0);
            ProcessJourneyRequest request = new ProcessJourneyRequest(requestModel);
            request.setUseEncryption(true);
            request.setRequestHandler(new BaseRequest.RequestHandler()
            {
                @Override
                public void requestFinished(BaseRequest request)
                {
                    if(request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                    {
                        removeFromQueue(requestModel);
                    }
                }
            });
            request.executeRequest(SDApplication.getAppContext());
        }
    }

    public void onEvent(HighAccelerationEvent event) {
        Log.i(TAG, "HighAccelerationEvent");
        Toast.makeText(getActivityContext(), "High acceleration detected", Toast.LENGTH_SHORT).show();
        Log.i("HighAccelerationEvent", "Event: " + event.toString());
    }

    /* Getters and Setters */

    /**
     * Getter for property 'activityContext'.
     *
     * @return Value for property 'activityContext'.
     */
    public static Activity getActivityContext()
    {
        return sActivityContext;
    }

    /**
     * Setter for property 'activityContext'.
     *
     * @param activityContext Value to set for property 'activityContext'.
     */
    public static void setActivityContext(Activity activityContext)
    {
        sActivityContext = activityContext;
    }

    public void shutdown()
    {
        stopMonitoring();
        EventBus.getDefault().unregister(this);
        Log.i(TAG, "Shutting down journey manager");
    }

    /**
     * Getter for property 'startTime'.
     *
     * @return Value for property 'startTime'.
     */
    public long getStartTime()
    {
        return mStartTime;
    }

    /**
     * Setter for property 'startTime'.
     *
     * @param startTime Value to set for property 'startTime'.
     */
    public void setStartTime(long startTime)
    {
        mStartTime = startTime;
    }
}
