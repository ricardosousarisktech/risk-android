package com.risktechnology.socialdriver.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.models.requestModels.ForgottenPasswordRequestModel;
import com.risktechnology.socialdriver.models.requestModels.LoginRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.ForgottenPasswordRequest;
import com.risktechnology.socialdriver.requests.LoginRequest;
import com.risktechnology.socialdriver.ui_elements.DimmableButton;
import com.risktechnology.socialdriver.ui_elements.ProgressHUD;
import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dev on 22/07/2015.
 */
public class ForgottenPasswordActivity extends BaseActivity {

    DimmableButton mSubmitButton;
    EditText mEmailEditText;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgotten_password_screen);

        mSubmitButton = (DimmableButton) findViewById(R.id.submitButton);
        mEmailEditText = (EditText) findViewById(R.id.emailTextField);

        mSubmitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                requestPasswordReset();
            }
        });

    }

    private void requestPasswordReset()
    {

        String emailAddress = mEmailEditText.getText().toString();

        if(!Helpers.isEmailValid(emailAddress))
        {
            Helpers.errorAlert(this, "Invalid email", "Please enter a valid email address");
            return;
        }

        ForgottenPasswordRequestModel model = new ForgottenPasswordRequestModel();
        model.setEmail(emailAddress);
        ForgottenPasswordRequest request = new ForgottenPasswordRequest(model);

        final ProgressHUD hud = ProgressHUD.show(this, "Loading..., ", true, false, null);
        request.setRequestHandler(new BaseRequest.RequestHandler()
        {
            @Override
            public void requestFinished(BaseRequest request)
            {
                hud.dismiss();
                if(request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                {
                    Helpers.successAlert(ForgottenPasswordActivity.this, "Reset password", "Please check your email for the password reset email.");
                }
                else
                {
                    Helpers.errorAlert(ForgottenPasswordActivity.this, "Error", "We could not reset your password, please check your email and try again.");
                }
            }
        });

        request.executeRequest(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
