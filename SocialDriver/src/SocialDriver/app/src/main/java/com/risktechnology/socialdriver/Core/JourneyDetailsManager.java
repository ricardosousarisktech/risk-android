package com.risktechnology.socialdriver.core;

import android.content.Context;
import android.support.annotation.Nullable;

import com.risktechnology.socialdriver.application.SDApplication;
import com.risktechnology.socialdriver.events.JourneyDetailsUpdatedEvent;
import com.risktechnology.socialdriver.models.JourneyDetailsModel;
import com.risktechnology.socialdriver.models.JourneyStatisticsModel;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.models.requestModels.MonthlyDetailsRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.MonthlyDetailsRequest;
import com.risktechnology.socialdriver.ui_elements.ProgressHUD;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import de.greenrobot.event.EventBus;

/**
 * Created by Dev on 10/09/2015.
 */
public class JourneyDetailsManager
{
    private static JourneyDetailsManager sInstance;

    private JourneyStatisticsModel mJourneyStatisticsModel;
    private ArrayList<JourneyDetailsModel> mJourneyDetailsModels;

    public static synchronized JourneyDetailsManager sharedManager()
    {
        if (sInstance == null)
        {
            sInstance = new JourneyDetailsManager();
        }
        return sInstance;
    }

    public JourneyDetailsManager()
    {
        mJourneyDetailsModels = new ArrayList<>();
    }

    public void getStatisticsForDate(Date date)
    {
        getStatisticsForDate(date, false, null);
    }

    public void getStatisticsForDate(Date date, Boolean showLoader, @Nullable Context context)
    {
        try
        {
            ProgressHUD hud = null;
            if(showLoader)
            {
                hud = ProgressHUD.show(context, "Getting details...", true, false, null);
            }
            MonthlyDetailsRequestModel model = new MonthlyDetailsRequestModel();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dateString = dateFormat.format(date);
            dateString += "01";
            dateFormat = new SimpleDateFormat("yyyyMMdd");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date startOfMonth = dateFormat.parse(dateString);
            long dateTime = startOfMonth.getTime();

            model.setUserId(UserModel.getInstance().getUserId());
            model.setStartDateOfMonth("/Date(" + dateTime + ")/");

            MonthlyDetailsRequest request = new MonthlyDetailsRequest(model);
            request.setUseEncryption(true);
            final ProgressHUD finalHud = hud;
            request.setRequestHandler(new BaseRequest.RequestHandler()
            {
                @Override
                public void requestFinished(BaseRequest request)
                {
                    if(finalHud != null)
                    {
                        finalHud.dismiss();
                    }
                    if(request.getErrorResponseString() != null)
                    {
                        getJourneyStatisticsModel().setMonthlyScore(0);
                        getJourneyStatisticsModel().setDriverScores(0);
                        getJourneyStatisticsModel().setDistanceTravelled(0);
                        getJourneyStatisticsModel().setDistanceUnits("mi");
                        getJourneyStatisticsModel().setTravelTime("00:00:00");
                        getJourneyStatisticsModel().setWorstJourneyScore(0);
                        getJourneyStatisticsModel().setBestJourneyScore(0);
                    }
                    else
                    {
                        JSONObject responseObject = request.getResponseObject();

                        getJourneyStatisticsModel().setMonthlyScore(responseObject.optInt("MonthlyScore", 0));
                        getJourneyStatisticsModel().setDriverScores(responseObject.optInt("DriverScores", 0));
                        getJourneyStatisticsModel().setDistanceTravelled(responseObject.optInt("DistanceTravelled", 0));
                        getJourneyStatisticsModel().setDistanceUnits(responseObject.optString("DistanceUnits", "mi"));
                        getJourneyStatisticsModel().setTravelTime(responseObject.optString("TravelTime", "00:00:00"));
                        getJourneyStatisticsModel().setWorstJourneyScore(responseObject.optInt("WorstJourneyScore", 0));
                        getJourneyStatisticsModel().setBestJourneyScore(responseObject.optInt("BestJourneyScore", 0));

                        EventBus.getDefault().post(new JourneyDetailsUpdatedEvent());
                    }
                }
            });
            request.executeRequest(SDApplication.getAppContext());
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property 'journeyStatisticsModel'.
     *
     * @return Value for property 'journeyStatisticsModel'.
     */
    public JourneyStatisticsModel getJourneyStatisticsModel()
    {
        if(mJourneyStatisticsModel == null)
        {
            mJourneyStatisticsModel = new JourneyStatisticsModel();
        }
        return mJourneyStatisticsModel;
    }

    /**
     * Setter for property 'journeyStatisticsModel'.
     *
     * @param journeyStatisticsModel Value to set for property 'journeyStatisticsModel'.
     */
    public void setJourneyStatisticsModel(JourneyStatisticsModel journeyStatisticsModel)
    {
        mJourneyStatisticsModel = journeyStatisticsModel;
    }
}
