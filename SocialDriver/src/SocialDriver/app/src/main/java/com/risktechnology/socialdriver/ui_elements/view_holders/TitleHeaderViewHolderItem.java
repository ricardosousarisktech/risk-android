package com.risktechnology.socialdriver.ui_elements.view_holders;

import android.widget.TextView;

public class TitleHeaderViewHolderItem
{
    private TextView mTitleLabel;

    public TextView getTitleLabel()
    {
        return mTitleLabel;
    }

    public void setTitleLabel(TextView titleLabel)
    {
        mTitleLabel = titleLabel;
    }
}
