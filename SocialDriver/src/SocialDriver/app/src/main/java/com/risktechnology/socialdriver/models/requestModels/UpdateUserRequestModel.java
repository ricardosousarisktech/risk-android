package com.risktechnology.socialdriver.models.requestModels;

import android.app.usage.UsageEvents;

import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import de.greenrobot.event.EventBus;


@SuppressWarnings("unused")
public class UpdateUserRequestModel extends BaseRequestModel
{

    private String mUserId;
    private String mFirstname;
    private String mLastname;
    private Date mDOB;
    private String mAddress;
    private String mPostcode;
    private String mCarType;
    private String mNotifyuser;
    private String mGender;

    public UpdateUserRequestModel()
    {
        mUserId = "";
        mFirstname = "";
        mLastname = "";
        mDOB = null;
        mAddress = "";
        mPostcode = "";
        mCarType = "";
        mNotifyuser = "";
        mGender = "";
    }


    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException
    {
        if(!Helpers.isEmpty(mAddress))
        {
            jsonObject.put("Address", mAddress);
        }
        else
        {
            jsonObject.put("Address", "");
        }

        if(!Helpers.isEmpty(mCarType))
        {
            jsonObject.put("CarType", mCarType);
        }

        if(mDOB != null)
        {
            jsonObject.put("DOB", "/Date(" + mDOB.getTime() + ")/");
        }

        if(!Helpers.isEmpty(mFirstname))
        {
            jsonObject.put("Firstname", mFirstname);
        }

        if(!Helpers.isEmpty(mLastname))
        {
            jsonObject.put("Lastname", mLastname);
        }

        if(!Helpers.isEmpty(mPostcode))
        {
            jsonObject.put("Postcode", mPostcode);
        }
        else
        {
            jsonObject.put("Postcode", "");
        }

        if(!Helpers.isEmpty(mGender))
        {
            jsonObject.put("Gender", (mGender.equals("Male") ? "M" : "F"));
        }

        if(!Helpers.isEmpty(mUserId))
        {
            jsonObject.put("UserId", mUserId);
        }
    }

    @Override
    public void describeModel()
    {

    }

    /**
     * Getter for property 'userId'.
     *
     * @return Value for property 'userId'.
     */
    public String getUserId()
    {
        return mUserId;
    }

    /**
     * Setter for property 'userId'.
     *
     * @param userId Value to set for property 'userId'.
     */
    public void setUserId(String userId)
    {
        mUserId = userId;
    }

    /**
     * Getter for property 'firstname'.
     *
     * @return Value for property 'firstname'.
     */
    public String getFirstname()
    {
        return mFirstname;
    }

    /**
     * Setter for property 'firstname'.
     *
     * @param firstname Value to set for property 'firstname'.
     */
    public void setFirstname(String firstname)
    {
        mFirstname = firstname;
    }

    /**
     * Getter for property 'lastname'.
     *
     * @return Value for property 'lastname'.
     */
    public String getLastname()
    {
        return mLastname;
    }

    /**
     * Setter for property 'lastname'.
     *
     * @param lastname Value to set for property 'lastname'.
     */
    public void setLastname(String lastname)
    {
        mLastname = lastname;
    }

    /**
     * Getter for property 'DOB'.
     *
     * @return Value for property 'DOB'.
     */
    public Date getDOB()
    {
        return mDOB;
    }

    /**
     * Setter for property 'DOB'.
     *
     * @param DOB Value to set for property 'DOB'.
     */
    public void setDOB(Date DOB)
    {
        mDOB = DOB;
    }

    /**
     * Getter for property 'address'.
     *
     * @return Value for property 'address'.
     */
    public String getAddress()
    {
        return mAddress;
    }

    /**
     * Setter for property 'address'.
     *
     * @param address Value to set for property 'address'.
     */
    public void setAddress(String address)
    {
        mAddress = address;
    }

    /**
     * Getter for property 'postcode'.
     *
     * @return Value for property 'postcode'.
     */
    public String getPostcode()
    {
        return mPostcode;
    }

    /**
     * Setter for property 'postcode'.
     *
     * @param postcode Value to set for property 'postcode'.
     */
    public void setPostcode(String postcode)
    {
        mPostcode = postcode;
    }

    /**
     * Getter for property 'carType'.
     *
     * @return Value for property 'carType'.
     */
    public String getCarType()
    {
        return mCarType;
    }

    /**
     * Setter for property 'carType'.
     *
     * @param carType Value to set for property 'carType'.
     */
    public void setCarType(String carType)
    {
        mCarType = carType;
    }

    /**
     * Getter for property 'notifyuser'.
     *
     * @return Value for property 'notifyuser'.
     */
    public String getNotifyuser()
    {
        return mNotifyuser;
    }

    /**
     * Setter for property 'notifyuser'.
     *
     * @param notifyuser Value to set for property 'notifyuser'.
     */
    public void setNotifyuser(String notifyuser)
    {
        mNotifyuser = notifyuser;
    }

    /**
     * Getter for property 'gender'.
     *
     * @return Value for property 'gender'.
     */
    public String getGender()
    {
        return mGender;
    }

    /**
     * Setter for property 'gender'.
     *
     * @param gender Value to set for property 'gender'.
     */
    public void setGender(String gender)
    {
        mGender = gender;
    }
}
