package com.risktechnology.socialdriver.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.models.JourneyDetailsModel;
import com.risktechnology.socialdriver.ui_elements.WhiteCircle;
import com.risktechnology.socialdriver.ui_elements.view_holders.JourneyListViewHolderItem;
import com.risktechnology.socialdriver.utilities.UiUtilities;

import java.util.ArrayList;

/**
 * Created by Matthew on 09/01/15.
 */

public class JourneyListAdapter extends ArrayAdapter<JourneyDetailsModel> {
    Context mContext;
    ArrayList<JourneyDetailsModel> mJourneyDetails;

    public JourneyListAdapter(Context context, int resource, int textViewResourceId, ArrayList<JourneyDetailsModel> journeyDetails) {
        super(context, resource, textViewResourceId, journeyDetails);
        mContext = context;
        mJourneyDetails = journeyDetails;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        JourneyListViewHolderItem viewHolder;

        if(convertView==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitem_journey_list_item, parent, false);

            viewHolder = new JourneyListViewHolderItem();

            viewHolder.setStartAddressLabel((TextView) convertView.findViewById(R.id.startAddress));
            viewHolder.setEndAddressLabel((TextView) convertView.findViewById(R.id.endAddress));
            viewHolder.setDistanceLabel((TextView) convertView.findViewById(R.id.distanceLabel));
            viewHolder.setTravelTimeLabel((TextView) convertView.findViewById(R.id.travelTimeLabel));
            viewHolder.setPointsLabel((TextView) convertView.findViewById(R.id.pointsLabel));
            viewHolder.setAccelerationLabel((TextView) convertView.findViewById(R.id.accelerationLabel));
            viewHolder.setDecelerationLabel((TextView) convertView.findViewById(R.id.decelerationLabel));
            viewHolder.setSpeedLabel((TextView) convertView.findViewById(R.id.speedLabel));
            viewHolder.setPointsHolder((WhiteCircle) convertView.findViewById(R.id.pointsHolder));
            viewHolder.setAccelerationHolder((WhiteCircle) convertView.findViewById(R.id.accelerationHolder));
            viewHolder.setDecelerationHolder((WhiteCircle) convertView.findViewById(R.id.decelerationHolder));
            viewHolder.setSpeedHolder((WhiteCircle) convertView.findViewById(R.id.speedHolder));
            viewHolder.setDateLabel((TextView) convertView.findViewById(R.id.journeyDateLabel));
            convertView.setTag(viewHolder);

        }else{
            viewHolder = (JourneyListViewHolderItem) convertView.getTag();
        }

        JourneyDetailsModel model = mJourneyDetails.get(position);

        viewHolder.getStartAddressLabel().setText(model.getFormattedStartAddress());
        viewHolder.getEndAddressLabel().setText(model.getFormattedEndAddress());
        viewHolder.getDistanceLabel().setText(String.format("%s%s",(Math.round(model.getDistanceTravelled() * 100) / 100), model.getDistanceUnits()));
        viewHolder.getTravelTimeLabel().setText(model.getDuration());
        viewHolder.getPointsLabel().setText(String.format("%d", model.getRiskScore()));
        viewHolder.getAccelerationLabel().setText(String.format("%d", model.getAccelScore()));
        viewHolder.getDecelerationLabel().setText(String.format("%d", model.getDecelScore()));
        viewHolder.getSpeedLabel().setText(String.format("%d", model.getSpeedScore()));
        viewHolder.getDateLabel().setText(model.getStyledReadableDate());
        UiUtilities.setMultifontStringOnTextView(viewHolder.getDateLabel(), mContext);

        return convertView;

    }

    public void updateJourneys(ArrayList<JourneyDetailsModel> journeyDetails)
    {
        mJourneyDetails = journeyDetails;
        notifyDataSetChanged();
    }
}

