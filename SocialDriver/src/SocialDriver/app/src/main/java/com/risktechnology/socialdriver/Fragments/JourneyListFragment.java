package com.risktechnology.socialdriver.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.adapters.JourneyListAdapter;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.core.JourneyDetailsManager;
import com.risktechnology.socialdriver.events.JourneyDetailsUpdatedEvent;
import com.risktechnology.socialdriver.models.JourneyDetailsModel;
import com.risktechnology.socialdriver.models.JourneyStatisticsModel;
import com.risktechnology.socialdriver.models.requestModels.JourneysRequestModel;
import com.risktechnology.socialdriver.requests.BaseRequest;
import com.risktechnology.socialdriver.requests.JourneysRequest;
import com.risktechnology.socialdriver.ui_elements.MonthYearPickerDialogFragment;
import com.risktechnology.socialdriver.ui_elements.ProgressHUD;
import com.risktechnology.socialdriver.utilities.UiUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.greenrobot.event.EventBus;

/**
 * Created by Matthew on 23/07/15.
 */
public class JourneyListFragment extends BaseFragment {

    private static final String TAG = "JourneyListFragment";
    private ListView mJourneyListView;
    private TextView mDateLabel;
    private TextView mPointsLabel;
    private TextView mYourScoreLabel;
    private TextView mMinPointsLabel;
    private TextView mMinYourScoreLabel;
    private TextView mMaxPointsLabel;
    private TextView mMaxYourScoreLabel;
    private TextView mDistanceLabel;
    private TextView mDurationLabel;
    private TextView mNoResultsLabel;

    private ArrayList<JourneyDetailsModel> mJourneyDetails;
    private JourneyListAdapter mJourneyListAdapter;

    private Date mCurrentDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setTitle("Journeys");

        View view = inflater.inflate(R.layout.fragment_journey_list, container, false);
        mJourneyListView = (ListView) view.findViewById(R.id.journeyList);

        mJourneyDetails = new ArrayList<>();

        mJourneyListAdapter = new JourneyListAdapter(
                getActivity(),
                R.layout.listitem_navigation_item,
                R.id.menuTitle,
                mJourneyDetails);

        mJourneyListView.setAdapter(mJourneyListAdapter);

        mDateLabel = (TextView) view.findViewById(R.id.dateLabel);
        mPointsLabel = (TextView) view.findViewById(R.id.pointsLabel);
        mYourScoreLabel = (TextView) view.findViewById(R.id.yourScoreLabel);
        mMinPointsLabel = (TextView) view.findViewById(R.id.minPointsLabel);
        mMinYourScoreLabel = (TextView) view.findViewById(R.id.minYourScoreLabel);
        mMaxPointsLabel = (TextView) view.findViewById(R.id.maxPointsLabel);
        mMaxYourScoreLabel = (TextView) view.findViewById(R.id.maxYourScoreLabel);
        mDistanceLabel = (TextView) view.findViewById(R.id.distanceLabel);
        mDurationLabel = (TextView) view.findViewById(R.id.durationLabel);
        mNoResultsLabel = (TextView) view.findViewById(R.id.noResultsLabel);

        setupDateSelector();

        UiUtilities.setMultifontStringOnTextView(mDateLabel, this.getActivity());
        UiUtilities.setMultifontStringOnTextView(mPointsLabel, this.getActivity());
        UiUtilities.setMultifontStringOnTextView(mYourScoreLabel, this.getActivity());
        UiUtilities.setMultifontStringOnTextView(mMinPointsLabel, this.getActivity());
        UiUtilities.setMultifontStringOnTextView(mMinYourScoreLabel, this.getActivity());
        UiUtilities.setMultifontStringOnTextView(mMaxPointsLabel, this.getActivity());
        UiUtilities.setMultifontStringOnTextView(mMaxYourScoreLabel, this.getActivity());
        UiUtilities.setMultifontStringOnTextView(mDistanceLabel, this.getActivity());
        UiUtilities.setMultifontStringOnTextView(mDurationLabel, this.getActivity());

        EventBus.getDefault().register(this, -1);

        UiUtilities.tintCompoundDrawable(mDurationLabel, Color.WHITE);
        UiUtilities.tintCompoundDrawable(mDistanceLabel, Color.WHITE);

        mCurrentDate = new Date();
        getJourneyDetailsForDate(mCurrentDate);

        updateUI();

        return view;
    }

    private void setupDateSelector()
    {
        mDateLabel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                MonthYearPickerDialogFragment fragment = new MonthYearPickerDialogFragment();

                Bundle args = new Bundle();
                args.putString(SpinnerDialogFragment.KEY_TITLE, "Select Date");
                args.putLong(SpinnerDialogFragment.KEY_CURRENT_VALUE, mCurrentDate.getTime());
                fragment.setArguments(args);
                fragment.setAlertResponseHandler(new MonthYearPickerDialogFragment.AlertResponseHandler()
                {
                    @Override
                    public void buttonPressed(MonthYearPickerDialogFragment alert)
                    {
                        if (!alert.wasCancelPressed())
                        {
                            Calendar calendar = alert.getCalendar();
                            mCurrentDate = calendar.getTime();
                            updateUI();
                            JourneyDetailsManager.sharedManager().getStatisticsForDate(mCurrentDate, true, getActivity());
                            getJourneyDetailsForDate(calendar.getTime());
                        }
                    }
                });
                fragment.show(getFragmentManager(), "spinner");
            }
        });
    }

    private void getJourneyDetailsForDate(Date date)
    {
        mNoResultsLabel.setVisibility(View.GONE);
        final ProgressHUD hud = ProgressHUD.show(getActivity(), "Fetching journeys...", true, false, null);
        JourneysRequestModel journeysRequestModel = new JourneysRequestModel(date);
        JourneysRequest journeysRequest = new JourneysRequest(journeysRequestModel);
        journeysRequest.setUseEncryption(true);
        journeysRequest.setRequestHandler(new BaseRequest.RequestHandler()
        {
            @Override
            public void requestFinished(BaseRequest request)
            {
                hud.dismiss();
                if (request.getErrorStatusCode() == Constants.NO_ERROR_CODE)
                {
                    try
                    {
                        mJourneyDetails.clear();
                        JSONArray array = request.getResponseObject().getJSONArray("Journeys");
                        for(int i = 0 ; i < array.length(); i++)
                        {
                            JSONObject object = array.getJSONObject(i);
                            JourneyDetailsModel model = new JourneyDetailsModel();
                            model.populateFromJSON(object);
                            mJourneyDetails.add(model);
                            mJourneyListAdapter.updateJourneys(mJourneyDetails);
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                    if(mJourneyDetails.size() == 0)
                    {
                        mJourneyListView.setVisibility(View.GONE);
                        mNoResultsLabel.setVisibility(View.VISIBLE);
                        mNoResultsLabel.setText(String.format("There are no journeys for %s", mDateLabel.getText()));
                        UiUtilities.setMultifontStringOnTextView(mNoResultsLabel, getActivity());
                    }
                    else
                    {
                        mJourneyListView.setVisibility(View.VISIBLE);
                        mNoResultsLabel.setVisibility(View.GONE);
                    }
                }
                    Log.i(TAG, "Request: " + request);
            }
        });
        journeysRequest.executeRequest(getActivity());
    }

    public void updateUI()
    {
        JourneyStatisticsModel journeyStatisticsModel = JourneyDetailsManager.sharedManager().getJourneyStatisticsModel();
        mPointsLabel.setText(String.format("%d", journeyStatisticsModel.getMonthlyScore()));
        mMinPointsLabel.setText(String.format("%d", journeyStatisticsModel.getWorstJourneyScore()));
        mMaxPointsLabel.setText(String.format("%d", journeyStatisticsModel.getBestJourneyScore()));
        mDistanceLabel.setText(String.format("%.2f %s", Math.round(journeyStatisticsModel.getDistanceTravelled() * 100.0f) / 100.0f, journeyStatisticsModel.getDistanceUnits()));
        mDurationLabel.setText(journeyStatisticsModel.getTravelTime());
        SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
        String dobString = format.format(mCurrentDate.getTime());
        mDateLabel.setText(String.format("<b>%s</b>", dobString));
        UiUtilities.setMultifontStringOnTextView(mDateLabel, getActivity().getApplicationContext());
    }

    @SuppressWarnings("unused")
    public void onEvent(JourneyDetailsUpdatedEvent event)
    {
        updateUI();
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}