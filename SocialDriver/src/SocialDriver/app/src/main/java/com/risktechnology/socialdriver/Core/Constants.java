package com.risktechnology.socialdriver.core;

@SuppressWarnings("UnusedDeclaration")
public class Constants
{

    public static final String API_ENDPOINT = "http://rtl-dev-mobileapi.elasticbeanstalk.com/api/socialdriver/";
    public static final String API_DATA_ENDPOINT = "http://rtl-dev-mobileapi.elasticbeanstalk.com/api/data/";

    public static final String FacebookAppID = "1466974266956359";
    public static final String FacebookAppSecret = "289a93ff3f2147eaf1d73b8cefcf49fa";
    public static final String API_PRIVATE_KEY = "";
    public static final String APP_ID = "2";
    public static final String SENDER_ID = "695512915587";

    public enum PageIndexes
    {
        PAGE_INDEX_HOME,
        PAGE_INDEX_JOURNEYS,
        PAGE_INDEX_SETTINGS,
        PAGE_INDEX_MY_INFORMATION,
        PAGE_INDEX_LOGOUT,
        PAGE_INDEX_COUNT, //anthing below here will be ignored
    }

    public static final String SettingsKeyAutoJourneyDetect = "SettingsKeyAutoJourneyDetect";
    public static final String SettingsKeyLocationServices = "SettingsKeyLocationServices";
    public static final String SettingsKeyNotifications = "SettingsKeyNotifications";


    public static final int NO_ERROR_CODE = -1;

    public class LoginErrorCode
    {
        public static final int INVALID_USERNAME_PASSWORD = 100;     // Invalid username and password combination
        public static final int USERNAME_EMPTY = 200;     // The username field is empty
        public static final int PASSWORD_EMPTY = 300;     // The password field is empty
        public static final int USERNAME_PASSWORD_EMPTY = 400;     // The username and password fields are empty
        public static final int TEMP_PASSWORD = 500;     // The user is using a temporary password and must change it immediately
        public static final int USER_LOCKED_OUT = 600;     // The user is locked out
        public static final int OAUTH_NOT_FOUND = 700;     // OAuthID not found
        public static final int USER_INACTIVE = 800;     // User is not active
        public static final int APP_ID_NOT_FOUND = 900;     // App id not found
        public static final int UNKNOWN_ERROR = 10000;   // Unspecified error
    }

    public enum DashWheelSegments
    {
        SEGMENT_CAR,
        SEGMENT_AGE,
        SEGMENT_LOCATION,
        SEGMENT_GENDER,
        SEGMENT_COUNT,
    }

    public enum SnsDeviceIds
    {
        ANDROID,
        APPLE_SANDBOX,
        APPLE,
        SNS_COUNT,
    }

}
