package com.risktechnology.socialdriver.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.risktechnology.socialdriver.fragments.MyInformationFragment;

/**
 * Created by Dev on 08/09/2015.
 */
public class MyInformationActivity extends BaseActivity
{
    private long mLastSeconds;
    private final int kExitTimeDelay = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fragment newFragment = new MyInformationFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(android.R.id.content, newFragment).commit();
    }

    public void hideKeyboard()
    {
        View view = getCurrentFocus();
        if (view != null)
        {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            view.clearFocus();
        }
    }

    public void onBackPressed()
    {
        hideKeyboard();

        FragmentManager fragmentManager = getFragmentManager();

        if (fragmentManager.getBackStackEntryCount() > 0)
        {
            fragmentManager.popBackStack();
        }

        {
            long seconds = System.currentTimeMillis() / 1000L;

            if (seconds - mLastSeconds <= kExitTimeDelay)
            {
                mLastSeconds = 0;
                finish();
                System.exit(0);
            }
            else
            {
                Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
                mLastSeconds = seconds;
            }
        }
    }
}
