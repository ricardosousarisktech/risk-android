package com.risktechnology.socialdriver.ui_elements.dash_wheel;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.core.JourneyManager;
import com.risktechnology.socialdriver.ui_elements.DimmableImageButton;
import com.risktechnology.socialdriver.utilities.UiUtilities;

import java.util.ArrayList;
import java.util.Date;

public class DashWheel extends RelativeLayout implements ViewTreeObserver.OnGlobalLayoutListener, View.OnTouchListener, View.OnClickListener
{

    private final Context mContext;
    private Point mStartPoint;
    private Point mCurrentPoint;
    private RelativeLayout mSegmentHolder;
    private Point mCenterOfView;
    private double mStartAngle;
    private double mCurrentAngle;
    private float mLastRotation;
    private ArrayList<DashWheelSegment> mSegments;
    private DashWheelSegment mSelectedSegment;
    private ObjectAnimator mRotationAnimator;
    private DimmableImageButton mPlayButton;

    DashWheelCallback mDashWheelCallback;

    public DashWheel(Context context) {
        super(context);
        mContext = context;
        setup();
    }

    public DashWheel(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setup();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DashWheel(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        setup();
    }

    private void setup() {
        //mCenterOfView = new Point(getWidth() / 2, getHeight() / 2);
        setOnTouchListener(this);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    public void updateUI()
    {
        if(mSegments != null)
        {
            for (DashWheelSegment segment : mSegments)
            {
                segment.refreshBitmaps();
            }
        }
        if(mPlayButton != null)
        {
            mPlayButton.setColorFilter(Color.WHITE);
            if (JourneyManager.isRunning() && JourneyManager.sharedManager().getStartTime() != 0)
            {

                mPlayButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.stop_icon));
                mPlayButton.setEnabled(!JourneyManager.isInAutoMode());
            }
            else if(JourneyManager.isInAutoMode())
            {
                mPlayButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.play_icon));
                mPlayButton.setEnabled(!JourneyManager.isInAutoMode());
            }
            else
            {
                if(JourneyManager.isRunning())
                {

                    mPlayButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.stop_icon));
                    mPlayButton.setEnabled(true);
                }
                else
                {

                    mPlayButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.play_icon));
                    mPlayButton.setEnabled(true);
                }
            }
        }
    }

    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void onGlobalLayout() {

        getViewTreeObserver().removeOnGlobalLayoutListener(this);
        int width = getWidth();
        int height = getHeight();

        if(width > height)
        {
            width = height;
        }
        else
        {
            height = width;
        }

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
        params.width = width;
        params.height = height;
        setLayoutParams(params);

        mCenterOfView = new Point(width / 2, height / 2);


        if(mPlayButton == null) {
            mPlayButton = new DimmableImageButton(mContext);
            //noinspection deprecation
            mPlayButton.setBackground(mContext.getResources().getDrawable(R.drawable.play_button));
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 4 + UiUtilities.dpToPixel(mContext, 20), height / 4 + UiUtilities.dpToPixel(mContext, 20));
            layoutParams.addRule(CENTER_IN_PARENT);
            mPlayButton.setLayoutParams(layoutParams);

            mPlayButton.setOnClickListener(this);

            addView(mPlayButton);
        }

        updateUI();
    }

    public DashWheelSegment addSegment(int startAngle, int endAngle, int icon, String segmentName, String infoText, Constants.DashWheelSegments segmentTag)
    {
        if(mSegmentHolder == null)
        {
            mSegmentHolder = new RelativeLayout(mContext);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getMeasuredWidth(), getMeasuredHeight());
            layoutParams.addRule(CENTER_IN_PARENT);
            mSegmentHolder.setLayoutParams(layoutParams);
            mSegmentHolder.setClipToPadding(false);
            this.addView(mSegmentHolder);
        }

        if(mSegments == null)
        {
            mSegments = new ArrayList<>();
        }

        DashWheelSegment dashWheelSegment = new DashWheelSegment(mContext);
        dashWheelSegment.loadSegment(startAngle, endAngle, getWidth(), getHeight(), segmentTag);
        dashWheelSegment.setIcon(icon);
        dashWheelSegment.setInfoText(infoText);
        dashWheelSegment.setName(segmentName);
        mSegmentHolder.addView(dashWheelSegment);
        mSegments.add(dashWheelSegment);

        if(mSelectedSegment == null) {
            setSelectedSegment(dashWheelSegment);
        }

        calculateSegmentForRotation(0);


        return dashWheelSegment;
    }


    public double angleBetween2Points(Point point1, Point point2)
    {
        double deltaY = point2.y - point1.y;
        double deltaX = point2.x - point1.x;

        return (Math.atan2(deltaY, deltaX) * 180 / Math.PI);
    }

    private static boolean sMoving = false;

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        if(mRotationAnimator != null)
        {
            if(mRotationAnimator.isRunning())
            {
                return false;
            }
        }

        mSegmentHolder.setRotation(clampRotation(mSegmentHolder.getRotation()));

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN: {
                    sMoving = false;
                    int x = Math.round(event.getX());
                    int y = Math.round(event.getY());
                    mStartPoint = new Point(x, y);
                    mStartAngle = angleBetween2Points(mStartPoint, mCenterOfView);
                    break;
                }
                case MotionEvent.ACTION_MOVE:
                {
                    int x = Math.round(event.getX());
                    int y = Math.round(event.getY());
                    mCurrentPoint = new Point(x, y);
                    mCurrentAngle = angleBetween2Points(mCurrentPoint, mCenterOfView);

                    if(mCurrentAngle - mStartAngle > 1 || mCurrentAngle - mStartAngle < -1)
                    {
                        sMoving = true;
                    }

                    double nextAngle = mCurrentAngle - mStartAngle + mLastRotation;
                    while(nextAngle >= 360)
                    {
                        nextAngle -= 360;
                    }
                    while(nextAngle <= -360)
                    {
                        nextAngle += 360;
                    }
                    mSegmentHolder.setRotation((float) nextAngle);
                    setSelectedSegment(calculateSegmentForRotation(mSegmentHolder.getRotation()));
                    break;
                }
                case MotionEvent.ACTION_UP:
                {
                    int x = Math.round(event.getX());
                    int y = Math.round(event.getY());
                    mCurrentPoint = new Point(x, y);
                    mCurrentAngle = angleBetween2Points(mCurrentPoint, mCenterOfView);

                    int xDiff = mStartPoint.x - mCurrentPoint.x;
                    int yDiff = mStartPoint.y - mCurrentPoint.y;

                    if(!sMoving)
                    {
                        float lockedRotation = clampRotation(roundToNearest(mSegmentHolder.getRotation()));
                        float currentAngle = clampRotation((float) mCurrentAngle  - 90);

                        DashWheelSegment dashWheelSegment = calculateSegmentForRotation(currentAngle - lockedRotation, false);
                        rotateToSegment(dashWheelSegment);
                        setSelectedSegment(dashWheelSegment);
                        return false;
                    }
                    else {
                        float lockedRotation = roundToNearest(mSegmentHolder.getRotation());
                        ObjectAnimator anim = ObjectAnimator.ofFloat(mSegmentHolder, "rotation", mSegmentHolder.getRotation(), lockedRotation);
                        anim.setDuration(300);
                        anim.start();

                        mLastRotation = clampRotation(lockedRotation);
                    }
                    break;
                }
                default:
                    break;
            }

        return true;
    }

    private void rotateToSegment(DashWheelSegment dashWheelSegment) {

        float fromRotation = mSegmentHolder.getRotation();
        float toRotation = -(dashWheelSegment.getBaseRotation());

        float rotationChange = toRotation - fromRotation;

        while (rotationChange >= 360)
        {
            rotationChange -= 360;
        }
        while (rotationChange <= -360)
        {
            rotationChange += 360;
        }

        if((int)rotationChange < -90)
        {
            rotationChange = 360 + rotationChange;
        }

        mRotationAnimator = ObjectAnimator.ofFloat(mSegmentHolder, "rotation", fromRotation, fromRotation + rotationChange);
        mRotationAnimator.setDuration(600);
        mRotationAnimator.start();
        mRotationAnimator.setInterpolator(new DecelerateInterpolator());
        mLastRotation = toRotation;
    }

    private DashWheelSegment calculateSegmentForRotation(float kRotation) {
        return calculateSegmentForRotation(kRotation, true);
    }

    private DashWheelSegment calculateSegmentForRotation(float kRotation, boolean from360) {
        float currentRotation;
        if(from360)
        {
            currentRotation = roundToNearest(360 - kRotation);
        }
        else
        {
            currentRotation = roundToNearest(kRotation);
        }
        currentRotation = clampRotation(currentRotation);
        for(DashWheelSegment segment : mSegments)
        {
            float segmentRotation = roundToNearest(segment.getBaseRotation());
            segmentRotation = clampRotation(segmentRotation);


            if(segmentRotation == currentRotation)
            {
                if(mSelectedSegment == segment)
                {
                    return mSelectedSegment;
                }
                return segment;
            }
        }
        return null;
    }

    private float clampRotation(float rotation) {
        while(rotation >= 360)
        {
            rotation -= 360;
        }
        while(rotation < 0)
        {
            rotation += 360;
        }
        return rotation;
    }

    public float roundToNearest(float x) {
        float segmentSize = 360.0f / mSegments.size();

        float a = x / segmentSize;
        float b = Math.round(a);
        return b * segmentSize;
    }

    private void setSelectedSegment(DashWheelSegment selectedSegment) {
        if(mSelectedSegment == selectedSegment)
        {
            return;
        }

        for(DashWheelSegment unselectedSegments : mSegments)
        {
            unselectedSegments.deselectSegment();
        }

        mSelectedSegment = selectedSegment;
        mSelectedSegment.selectSegment();
        if(mDashWheelCallback != null)
        {
            mDashWheelCallback.segmentSelected(mSelectedSegment);
        }
    }

    public void setDashWheelCallback(DashWheelCallback dashWheelCallback) {
        mDashWheelCallback = dashWheelCallback;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mSegmentHolder = null;
        mCenterOfView = null;
        if(mSegments != null) {
            mSegments.clear();
        }
        mSegments = null;
        mSelectedSegment = null;
        if(mRotationAnimator != null) {
            mRotationAnimator.cancel();
        }
        mRotationAnimator = null;
        mPlayButton = null;
        mDashWheelCallback = null;
    }

    @Override
    public void onClick(View view)
    {
        if(JourneyManager.isRunning())
        {
            JourneyManager.stopMonitoring();
        }
        else
        {
            JourneyManager.startMonitoring(false);
        }
        updateUI();
    }

    public void clearSegments()
    {
        removeAllViews();
    }
}
