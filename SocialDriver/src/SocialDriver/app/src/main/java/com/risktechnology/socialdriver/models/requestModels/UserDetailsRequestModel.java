package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings("unused")
public class UserDetailsRequestModel extends BaseRequestModel
{
    private String mUserId;

    public UserDetailsRequestModel()
    {
        mUserId = "";
    }

    /**
     * Getter for property 'userId'.
     *
     * @return Value for property 'userId'.
     */
    public String getUserId()
    {
        return mUserId;
    }

    /**
     * Setter for property 'userId'.
     *
     * @param userId Value to set for property 'userId'.
     */
    public void setUserId(String userId)
    {
        mUserId = userId;
    }

    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException
    {
        if(!Helpers.isEmpty(mUserId))
        {
            jsonObject.put("Userid", mUserId);
        }
    }

    @Override
    public void describeModel()
    {

    }
}
