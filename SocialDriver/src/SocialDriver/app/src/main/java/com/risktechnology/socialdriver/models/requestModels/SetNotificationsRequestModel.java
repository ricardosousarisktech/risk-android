package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings("unused")
public class SetNotificationsRequestModel extends BaseRequestModel
{


    private Integer mUserId;
    private Constants.SnsDeviceIds mSnsDeviceId;
    private String mNotifyToken;

    public SetNotificationsRequestModel()
    {
        mUserId = 0;
        mSnsDeviceId = Constants.SnsDeviceIds.ANDROID;
        mNotifyToken = "";
    }


    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException
    {
        if(!Helpers.isEmpty(mNotifyToken))
        {
            jsonObject.put("NotifyToken", mNotifyToken);
        }

        jsonObject.put("SndDeviceId", mSnsDeviceId.ordinal());

        if(mUserId != 0)
        {
            jsonObject.put("UserId", mUserId);
        }
    }

    @Override
    public void describeModel()
    {

    }

    /**
     * Getter for property 'userId'.
     *
     * @return Value for property 'userId'.
     */
    public Integer getUserId()
    {
        return mUserId;
    }

    /**
     * Setter for property 'userId'.
     *
     * @param userId Value to set for property 'userId'.
     */
    public void setUserId(Integer userId)
    {
        mUserId = userId;
    }

    /**
     * Getter for property 'notifyToken'.
     *
     * @return Value for property 'notifyToken'.
     */
    public String getNotifyToken()
    {
        return mNotifyToken;
    }

    /**
     * Setter for property 'notifyToken'.
     *
     * @param notifyToken Value to set for property 'notifyToken'.
     */
    public void setNotifyToken(String notifyToken)
    {
        mNotifyToken = notifyToken;
    }
}
