package com.risktechnology.socialdriver.fragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.activities.MainActivity;
import com.risktechnology.socialdriver.adapters.NavigationDrawerAdapter;
import com.risktechnology.socialdriver.application.SDApplication;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.events.JourneyDetailsUpdatedEvent;
import com.risktechnology.socialdriver.events.UserDetailsUpdatedEvent;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.UiUtilities;
import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private View mFragmentContainerView;
    private ImageView mProfileImageView;

    private int mCurrentSelectedPosition = 0;

    private TextView mNameLabel;
    private TextView mLocationLabel;


    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
        }

        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LinearLayout layout = (LinearLayout) inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        mDrawerListView = (ListView) layout.findViewById(R.id.listView);
        mProfileImageView = (ImageView) layout.findViewById(R.id.profileImage);
        mLocationLabel = (TextView) layout.findViewById(R.id.locationLabel);
        mNameLabel = (TextView) layout.findViewById(R.id.nameLabel);


        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                selectItem(position);
            }
        });

        String[] pages = new String[Constants.PageIndexes.PAGE_INDEX_COUNT.ordinal()];

        addNavigationItemToArray(pages, Constants.PageIndexes.PAGE_INDEX_HOME, R.string.section_title_home);
        addNavigationItemToArray(pages, Constants.PageIndexes.PAGE_INDEX_JOURNEYS, R.string.section_title_journeys);
        addNavigationItemToArray(pages, Constants.PageIndexes.PAGE_INDEX_MY_INFORMATION, R.string.section_title_my_information);
        addNavigationItemToArray(pages, Constants.PageIndexes.PAGE_INDEX_LOGOUT, R.string.section_title_logout);
        addNavigationItemToArray(pages, Constants.PageIndexes.PAGE_INDEX_SETTINGS, R.string.section_title_settings);

        mDrawerListView.setAdapter(new NavigationDrawerAdapter(
                getActivity(),
                R.layout.listitem_navigation_item,
                R.id.menuTitle,
                pages));

        mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);


        populateUserDetails();

        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this, -1);
        }

        return layout;
    }


    private void populateUserDetails()
    {
        String username = UserModel.getInstance().getFullName();

        if(username.length() == 0)
        {
            mNameLabel.setVisibility(View.GONE);
        }
        else {
            mNameLabel.setVisibility(View.VISIBLE);
            UiUtilities.setMultifontStringOnTextView(mNameLabel, "<b>" + username + "</b>", getActivity());
        }
        String locationString = UserModel.getInstance().getAddress() + "\n" + UserModel.getInstance().getPostcode();
        locationString = locationString.replace("\n", ", ");

        if(locationString.length() == 0)
        {
            mLocationLabel.setVisibility(View.GONE);
        }
        else {
            mLocationLabel.setVisibility(View.VISIBLE);
            UiUtilities.setMultifontStringOnTextView(mLocationLabel, locationString, getActivity());
        }

        if(UserModel.getInstance().getImageUrl() != null) {
            Picasso.with(getActivity()).load(UserModel.getInstance().getImageUrl()).placeholder(R.drawable.my_information).into(mProfileImageView);
        }
    }

    private void addNavigationItemToArray(String[] array, Constants.PageIndexes index, int titleResource) {
        if (index.ordinal() < Constants.PageIndexes.PAGE_INDEX_COUNT.ordinal()) {
            array[index.ordinal()] = getString(titleResource);
        }
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerLayout.setScrimColor(Color.argb(0,0,0,0));
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    public void openDraw()
    {
        hideKeyboard();
        if(!isDrawerOpen())
        {

            if(MainActivity.sharedInstance().isOpenAllowed())
            {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        }
    }


    public void closeDraw()
    {
        hideKeyboard();
        if(isDrawerOpen())
        {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
    }

    public void hideKeyboard()
    {
        View view = this.getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this);
        }
        mCallbacks = null;
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }


    public void onEvent(UserDetailsUpdatedEvent event)
    {
        populateUserDetails();
    }
}
