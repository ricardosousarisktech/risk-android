package com.risktechnology.socialdriver.utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.risktechnology.socialdriver.ui_elements.DimmableButton;

@SuppressWarnings("UnusedDeclaration")
public class UiUtilities {

    private static Point sScreenSize;
    private static Typeface sTypefaceBold;
    private static Typeface sTypefaceLight;

    public static int dpToPixel(Context context, int dp)
    {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float fPixels = metrics.density * dp;
        return (int) (fPixels + 0.5f);
    }

    public static Point getScreenDimensions(Activity activity)
    {
        if(sScreenSize == null) {
            Display display = activity.getWindowManager().getDefaultDisplay();
            sScreenSize = new Point();
            display.getSize(sScreenSize);
        }
        return sScreenSize;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if (listItem != null) {
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void prepareFonts(Context context)
    {
        if(sTypefaceBold == null) {
            sTypefaceBold = Typeface.create("HelveticaNeue", Typeface.BOLD);
        }

        if(sTypefaceLight == null) {
            sTypefaceLight = Typeface.createFromAsset(context.getAssets(), "Fonts/HelveticaNeueUL.otf");
        }
    }

    public static void setMultifontStringOnTextView(TextView textView, Context context) {
        setMultifontStringOnTextView(textView, textView.getText().toString(), context);
    }

    public static void setMultifontStringOnTextView(TextView textView, String content, Context context) {

        prepareFonts(context);

        textView.setTypeface(sTypefaceBold, Typeface.BOLD);
        textView.setTypeface(sTypefaceLight, Typeface.NORMAL);

        textView.setText(Html.fromHtml(content), TextView.BufferType.SPANNABLE);
    }

    public static void setMultifontStringOnButton(Button button, Context context) {
        setMultifontStringOnButton(button, button.getText().toString(), context);
    }

    public static void setMultifontStringOnButton(Button button, String content, Context context) {
        prepareFonts(context);

        button.setTypeface(sTypefaceBold, Typeface.BOLD);
        button.setTypeface(sTypefaceLight, Typeface.NORMAL);

        button.setText(Html.fromHtml(button.getText().toString()), TextView.BufferType.SPANNABLE);
    }

    public static void setDefaultFontStringOnTextView(TextView textView, Context context) {

        prepareFonts(context);

        textView.setTypeface(sTypefaceBold);
    }

    public static void tintCompoundDrawable(TextView textview, Drawable d, int colour)
    {
        if (d != null)
        {
            d = DrawableCompat.wrap(d);
            DrawableCompat.setTint(d, colour);
            d = DrawableCompat.unwrap(d);
            textview.setCompoundDrawablesWithIntrinsicBounds(d, null, null, null);
        }
    }

    public static void tintCompoundDrawable(TextView textview, Drawable d1, Drawable d2, Drawable d3, Drawable d4, int colour)
    {
        if (d1 != null)
        {
            d1 = DrawableCompat.wrap(d1);
            DrawableCompat.setTint(d1, colour);
            d1 = DrawableCompat.unwrap(d1);
        }
        if (d2 != null)
        {
            d2 = DrawableCompat.wrap(d2);
            DrawableCompat.setTint(d2, colour);
            d2 = DrawableCompat.unwrap(d2);
        }
        if (d3 != null)
        {
            d3 = DrawableCompat.wrap(d3);
            DrawableCompat.setTint(d3, colour);
            d3 = DrawableCompat.unwrap(d3);
        }
        if (d4 != null)
        {
            d4 = DrawableCompat.wrap(d4);
            DrawableCompat.setTint(d4, colour);
            d4 = DrawableCompat.unwrap(d4);
        }

        textview.setCompoundDrawablesWithIntrinsicBounds(d1, d2, d3, d4);
    }

    public static void tintCompoundDrawable(TextView durationLabel, int colour)
    {
        Drawable[] compoundDrawables = durationLabel.getCompoundDrawables();
        tintCompoundDrawable(durationLabel, compoundDrawables[0], compoundDrawables[1], compoundDrawables[2], compoundDrawables[3], colour);
    }
}
