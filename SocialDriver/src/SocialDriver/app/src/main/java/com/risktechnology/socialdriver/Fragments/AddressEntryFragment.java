package com.risktechnology.socialdriver.fragments;

import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.risktechnology.socialdriver.R;
import com.risktechnology.socialdriver.events.UserDetailsUpdatedEvent;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.ui_elements.DimmableButton;
import com.risktechnology.socialdriver.ui_elements.DimmableImageButton;

import de.greenrobot.event.EventBus;

/**
 * Created by Dev on 09/09/2015.
 */
public class AddressEntryFragment extends BaseFragment implements View.OnClickListener
{
    private DimmableButton mSaveButton;
    private EditText mAddressField;
    private EditText mPostcodeField;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        setTitle("Address");

        View view = inflater.inflate(R.layout.fragment_address_entry, container, false);

        mSaveButton = (DimmableButton) view.findViewById(R.id.saveButton);

        mAddressField = (EditText) view.findViewById(R.id.addressEditText);
        mPostcodeField = (EditText) view.findViewById(R.id.postcodeEditText);

        mAddressField.setText(UserModel.getInstance().getAddress());
        mPostcodeField.setText(UserModel.getInstance().getPostcode());

        mSaveButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v)
    {
        if (v == mSaveButton)
        {
            UserModel.getInstance().setAddress(mAddressField.getText().toString());
            UserModel.getInstance().setPostcode(mPostcodeField.getText().toString());
            UserModel.getInstance().save();

            EventBus.getDefault().post(new UserDetailsUpdatedEvent());
            FragmentManager fragmentManager = getActivity().getFragmentManager();
            fragmentManager.popBackStack();
        }
    }

    @Override
    public void onPause()
    {
        hideKeyboard();
        super.onPause();
    }
}
