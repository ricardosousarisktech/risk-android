package com.risktechnology.socialdriver.models.requestModels;

import com.risktechnology.socialdriver.models.JourneyDetailsModel;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.utilities.Helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Dev on 16/09/2015.
 */
public class JourneysRequestModel extends BaseRequestModel
{
    private Integer mUserId = 0;
    private String mStartDate;
    private String mEndDate;
    private Boolean mLoadTelemetry = false;
    private Boolean mOffsetTime = false;

    public JourneysRequestModel()
    {
        populateDetailsWithDate(new Date());

    }
    public JourneysRequestModel(Date monthDate)
    {
        populateDetailsWithDate(monthDate);
    }

    private void populateDetailsWithDate(Date date)
    {
        mUserId = UserModel.getInstance().getUserId();

        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        Date firstDayOfMonth = cal.getTime();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date lastDayOfMonth = cal.getTime();

        mStartDate = "/Date(" + firstDayOfMonth.getTime() + ")/";
        mEndDate = "/Date(" + lastDayOfMonth.getTime() + ")/";
    }



    @Override
    public void addToJSONObject(JSONObject jsonObject) throws JSONException
    {
        if(!Helpers.isEmpty(mEndDate))
        {
            jsonObject.put("EndDate", mEndDate);
        }

        jsonObject.put("LoadTelemetry", mLoadTelemetry);
        jsonObject.put("OffsetTime", mOffsetTime);

        if(!Helpers.isEmpty(mStartDate))
        {
            jsonObject.put("StartDate", mStartDate);
        }

        if(mUserId > 0)
        {
            jsonObject.put("UserId", mUserId);
        }
    }

    @Override
    public void describeModel()
    {

    }
}
