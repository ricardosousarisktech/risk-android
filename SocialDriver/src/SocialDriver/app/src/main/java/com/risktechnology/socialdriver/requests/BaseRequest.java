package com.risktechnology.socialdriver.requests;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.risktechnology.socialdriver.core.Constants;
import com.risktechnology.socialdriver.models.UserModel;
import com.risktechnology.socialdriver.models.requestModels.BaseRequestModel;
import com.risktechnology.socialdriver.models.requestModels.ProcessJourneyRequestModel;
import com.risktechnology.socialdriver.utilities.Encryption;
import com.risktechnology.socialdriver.utilities.Helpers;
import com.risktechnology.socialdriver.utilities.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

@SuppressWarnings("unused")
public class BaseRequest {


    private static final String TAG = "BaseRequest";

    public static final int REQUEST_METHOD_GET = 1;
    public static final int REQUEST_METHOD_POST = 2;

    //FIXME!!! This is too high, the dev server needs looking at for this
    private static int TIMEOUT = 45000; //45 seconds

    private final BaseRequest mRequest;
    AsyncHttpClient mClient;
    AsyncHttpResponseHandler mResponseHandler;
    RequestHandler mRequestHandler;
    private int mHTTPStatusCode;
    private String mHTTPStatusMessage;
    private int mResponseStatusCode;
    private String mResponseStatusMessage;
    private Header[] mHeaders;
    private String mResponseString;
    private String mErrorResponseString;
    private JSONObject mResponseObject;
    private JSONObject mErrorResponseObject;
    private BaseRequestModel mModel;
    public static boolean mNoConnection = false;
    private String mUrl;
    private int mErrorStatusCode;
    private String mErrorStatusMessage;
    private boolean mUseEncryption;
    private int mMethod;

    public BaseRequest(BaseRequestModel model) {
        Logger.LogMessage(TAG, "init");
        mRequest = this;
        mModel = model;
        mMethod = REQUEST_METHOD_POST;
        setupResponseHandler();
    }

    public JSONObject getResponseObject() {
        return mResponseObject;
    }

    public JSONObject getMethodResponseObject() {
        //TODO: process response object
        return null;
    }

    public int getHTTPStatusCode() {
        return mHTTPStatusCode;
    }

    public String getHTTPStatusMessage() {
        return mHTTPStatusMessage;
    }

    public int getResponseStatusCode() {
        return mResponseStatusCode;
    }

    public String getResponseStatusMessage() {
        if(mHTTPStatusCode >= 500)
        {
            mResponseStatusMessage = "We are currently trying to reconnect to our secure server for you. Press OK to continue.";
        }
        return mResponseStatusMessage;
    }

    public Header[] getHeaders() {
        return mHeaders;
    }

    public String getResponseString() {
        return mResponseString;
    }

    public String getErrorResponseString() {
        return mErrorResponseString;
    }

    public void setRequestHandler(RequestHandler requestHandler) {
        mRequestHandler = requestHandler;
    }

    public void executeRequest(Context context) {
        Logger.LogDebugMessage("Request", "executeRequest");
        mClient = new AsyncHttpClient();

        mClient.setTimeout(TIMEOUT);

        if (mUrl == null) {
            Logger.LogErrorMessage(TAG, "URL for requests must be set", new Throwable("URL for requests must be set"));
            return;
        }

        JSONObject dataJSON = new JSONObject();
        try {
            dataJSON = processDataModel();

            if(!Helpers.isEmpty(UserModel.getInstance().getSecretKey()) && mUseEncryption)
            {

                if(this instanceof ProcessJourneyRequest)
                {
                    ProcessJourneyRequestModel processJourneyRequestModel = (ProcessJourneyRequestModel) mModel;
                    String encryptString = "";
                    encryptString += processJourneyRequestModel.getMinsToTrim() + "";
                    encryptString += processJourneyRequestModel.getMobileTelemetry().size() + "";
                    encryptString += UserModel.getInstance().getUserId() + "";
                    encryptString += UserModel.getInstance().getUserId() + ""; //add twice to override internal value
                    String hmac = Encryption.stringToHmacSHA1(encryptString, UserModel.getInstance().getSecretKey());
                    mUrl += "?userid=" + UserModel.getInstance().getUserId() + "&signature=" + hmac;
                }
                else
                {
                    String encryptString = "";
                    Iterator<String> keys = dataJSON.keys();
                    ArrayList<String> sortedKeys = new ArrayList<>();

                    while (keys.hasNext())
                    {
                        String key = keys.next();
                        sortedKeys.add(key);
                    }
                    Collections.sort(sortedKeys);
                    for (String key : sortedKeys)
                    {
                        Object value = dataJSON.get(key);
                        if (value instanceof String)
                        {
                            if (((String) value).contains("/Date("))
                            {
                                value = ((String) value).substring(6, ((String) value).length() - 2);
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
                                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                                Date parsedDate = new Date(Long.parseLong((String) value));
                                value = sdf.format(parsedDate);
                            }
                        }
                        else if(value instanceof Boolean)
                        {
                            value = ((Boolean)value ? "True" : "False");
                        }
                        encryptString += value;
                    }
                    encryptString += UserModel.getInstance().getUserId();

                    String hmac = Encryption.stringToHmacSHA1(encryptString, UserModel.getInstance().getSecretKey());
                    mUrl += "?userid=" + UserModel.getInstance().getUserId() + "&signature=" + hmac;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Logger.LogDebugMessage("Request " + this.getClass().getName(), mUrl + ": " + dataJSON.toString());
        StringEntity se;
        try {
            se = new StringEntity(dataJSON.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        if(mMethod == REQUEST_METHOD_POST)
        {
            mClient.post(context, mUrl, se, "application/json", mResponseHandler);
        }
        else
        {
            mClient.get(context, mUrl, se, "application/json", mResponseHandler);
        }


    }

    public JSONObject processDataModel() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        mModel.addToJSONObject(jsonObject);
        return jsonObject;
    }

    private void setupResponseHandler() {
        mResponseHandler = new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody)
            {
                mRequest.onSuccess(statusCode, headers, responseBody);
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error)
            {
                mRequest.onFailure(statusCode, headers, responseBody, error);
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                mRequest.onRetry(retryNo);
            }
        };
    }

    public void onRetry(int retryNo) {
        this.mHTTPStatusCode = Constants.NO_ERROR_CODE;

    }

    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        this.mHTTPStatusCode = statusCode;
        this.mHeaders = headers;
        if (responseBody != null) {
            try {
                this.mResponseString = new String(responseBody, "UTF-8");
                Logger.LogDebugMessage("Request " + this.getClass().getName(), this.mResponseString);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (this.mResponseString != null) {
            try {
                this.mResponseObject = new JSONObject(this.mResponseString);
                loadDetailsFromJSONResponse();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
        this.mHTTPStatusCode = statusCode;
        this.mHeaders = headers;

        if (errorResponse != null) {
            try {
                this.mErrorResponseString = new String(errorResponse, "UTF-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }


        if (this.mErrorResponseString != null) {
            try {
                this.mErrorResponseObject = new JSONObject(this.mErrorResponseString);
            } catch (JSONException exception) {
                exception.printStackTrace();
            }
        }

        if (e != null) {
            Logger.LogMessage(TAG, "Error with request", e);
        }
    }

    public void fireRequestFinished() {

        if (mRequestHandler != null) {
            mRequestHandler.requestFinished(this);
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    public void cancelRequest() {
        mRequest.cancelRequest();
    }

    public void loadDetailsFromJSONResponse() {
        if (this.mResponseObject != null) {
            setErrorStatusCode(this.getResponseObject().optInt("ErrorCode", Constants.NO_ERROR_CODE));
            setErrorStatusMessage(this.mErrorStatusMessage = this.getResponseObject().optString("ErrorDescription", ""));
        }
    }

    public void setUrl(String url)
    {
        mUrl = url;
    }

    public void setMethod(int method)
    {
        mMethod = method;
    }

    public int getMethod()
    {
        return mMethod;
    }

    public interface RequestHandler {
        public void requestFinished(BaseRequest request);
    }

    /**
     * Setter for property 'HTTPStatusCode'.
     *
     * @param HTTPStatusCode Value to set for property 'HTTPStatusCode'.
     */
    public void setHTTPStatusCode(int HTTPStatusCode)
    {
        mHTTPStatusCode = HTTPStatusCode;
    }

    /**
     * Setter for property 'HTTPStatusMessage'.
     *
     * @param HTTPStatusMessage Value to set for property 'HTTPStatusMessage'.
     */
    public void setHTTPStatusMessage(String HTTPStatusMessage)
    {
        mHTTPStatusMessage = HTTPStatusMessage;
    }

    /**
     * Getter for property 'errorStatusCode'.
     *
     * @return Value for property 'errorStatusCode'.
     */
    public int getErrorStatusCode()
    {
        return mErrorStatusCode;
    }

    /**
     * Setter for property 'errorStatusCode'.
     *
     * @param errorStatusCode Value to set for property 'errorStatusCode'.
     */
    public void setErrorStatusCode(int errorStatusCode)
    {
        mErrorStatusCode = errorStatusCode;
    }

    /**
     * Getter for property 'errorStatusMessage'.
     *
     * @return Value for property 'errorStatusMessage'.
     */
    public String getErrorStatusMessage()
    {
        return mErrorStatusMessage;
    }

    /**
     * Setter for property 'errorStatusMessage'.
     *
     * @param errorStatusMessage Value to set for property 'errorStatusMessage'.
     */
    public void setErrorStatusMessage(String errorStatusMessage)
    {
        mErrorStatusMessage = errorStatusMessage;
    }

    /**
     * Setter for property 'responseStatusMessage'.
     *
     * @param responseStatusMessage Value to set for property 'responseStatusMessage'.
     */
    public void setResponseStatusMessage(String responseStatusMessage)
    {
        mResponseStatusMessage = responseStatusMessage;
    }

    /**
     * Setter for property 'responseStatusCode'.
     *
     * @param responseStatusCode Value to set for property 'responseStatusCode'.
     */
    public void setResponseStatusCode(int responseStatusCode)
    {
        mResponseStatusCode = responseStatusCode;
    }

    /**
     * Getter for property 'useEncryption'.
     *
     * @return Value for property 'useEncryption'.
     */
    public boolean isUseEncryption()
    {
        return mUseEncryption;
    }

    /**
     * Setter for property 'useEncryption'.
     *
     * @param useEncryption Value to set for property 'useEncryption'.
     */
    public void setUseEncryption(boolean useEncryption)
    {
        mUseEncryption = useEncryption;
    }
}

