package com.risktechnology.socialdriver.models;

/**
 * Created by Dev on 10/09/2015.
 */
public class JourneyStatisticsModel
{
    private int mMonthlyScore;
    private int mDriverScores;
    private float mDistanceTravelled;
    private String mDistanceUnits;
    private String mTravelTime;
    private int mWorstJourneyScore;
    private int mBestJourneyScore;

    public JourneyStatisticsModel()
    {
        mMonthlyScore = 0;
        mDriverScores = 0;
        mDistanceTravelled = 0.0f;
        mDistanceUnits = "mi";
        mTravelTime = "00:00:00";
        mWorstJourneyScore = 0;
        mBestJourneyScore = 0;
    }

    /**
     * Getter for property 'monthlyScore'.
     *
     * @return Value for property 'monthlyScore'.
     */
    public int getMonthlyScore()
    {
        return mMonthlyScore;
    }

    /**
     * Setter for property 'monthlyScore'.
     *
     * @param monthlyScore Value to set for property 'monthlyScore'.
     */
    public void setMonthlyScore(int monthlyScore)
    {
        mMonthlyScore = monthlyScore;
    }

    /**
     * Getter for property 'driverScores'.
     *
     * @return Value for property 'driverScores'.
     */
    public int getDriverScores()
    {
        return mDriverScores;
    }

    /**
     * Setter for property 'driverScores'.
     *
     * @param driverScores Value to set for property 'driverScores'.
     */
    public void setDriverScores(int driverScores)
    {
        mDriverScores = driverScores;
    }

    /**
     * Getter for property 'distanceTravelled'.
     *
     * @return Value for property 'distanceTravelled'.
     */
    public float getDistanceTravelled()
    {
        return mDistanceTravelled;
    }

    /**
     * Setter for property 'distanceTravelled'.
     *
     * @param distanceTravelled Value to set for property 'distanceTravelled'.
     */
    public void setDistanceTravelled(float distanceTravelled)
    {
        mDistanceTravelled = distanceTravelled;
    }

    /**
     * Getter for property 'distanceUnits'.
     *
     * @return Value for property 'distanceUnits'.
     */
    public String getDistanceUnits()
    {
        return mDistanceUnits;
    }

    /**
     * Setter for property 'distanceUnits'.
     *
     * @param distanceUnits Value to set for property 'distanceUnits'.
     */
    public void setDistanceUnits(String distanceUnits)
    {
        mDistanceUnits = distanceUnits;
    }

    /**
     * Getter for property 'travelTime'.
     *
     * @return Value for property 'travelTime'.
     */
    public String getTravelTime()
    {
        return mTravelTime;
    }

    /**
     * Setter for property 'travelTime'.
     *
     * @param travelTime Value to set for property 'travelTime'.
     */
    public void setTravelTime(String travelTime)
    {
        mTravelTime = travelTime;
    }

    /**
     * Getter for property 'worstJourneyScore'.
     *
     * @return Value for property 'worstJourneyScore'.
     */
    public int getWorstJourneyScore()
    {
        return mWorstJourneyScore;
    }

    /**
     * Setter for property 'worstJourneyScore'.
     *
     * @param worstJourneyScore Value to set for property 'worstJourneyScore'.
     */
    public void setWorstJourneyScore(int worstJourneyScore)
    {
        mWorstJourneyScore = worstJourneyScore;
    }

    /**
     * Getter for property 'bestJourneyScore'.
     *
     * @return Value for property 'bestJourneyScore'.
     */
    public int getBestJourneyScore()
    {
        return mBestJourneyScore;
    }

    /**
     * Setter for property 'bestJourneyScore'.
     *
     * @param bestJourneyScore Value to set for property 'bestJourneyScore'.
     */
    public void setBestJourneyScore(int bestJourneyScore)
    {
        mBestJourneyScore = bestJourneyScore;
    }
}
