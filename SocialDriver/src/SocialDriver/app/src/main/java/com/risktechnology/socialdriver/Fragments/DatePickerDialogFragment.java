package com.risktechnology.socialdriver.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.risktechnology.socialdriver.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Matthew on 11/11/14.
 */
public class DatePickerDialogFragment extends DialogFragment implements DatePicker.OnDateChangedListener {
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_MESSAGE = "KEY_MESSAGE";
    public static final String KEY_CURRENT_VALUE = "KEY_CURRENT_VALUE";
    public static final String KEY_POSITIVE_TEXT = "KEY_POSITIVE_TEXT";
    public static final String KEY_NEGATIVE_TEXT = "KEY_NEGATIVE_TEXT";
    private AlertResponseHandler mAlertResponseHandler;

    private TextView mTitleView;
    private TextView mMessageView;
    private Button mPositiveButton;
    private Button mNegativeButton;
    private DatePicker mDatePicker;
    private boolean mCancelPressed = false;
    private Calendar mCalendar;

    public Calendar getCalendar() {
        return mCalendar;
    }

    public boolean wasCancelPressed()
    {
        return mCancelPressed;
    }

    public DatePickerDialogFragment() {
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dialog.setContentView(R.layout.fragment_alert_date_picker);

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        mTitleView = (TextView) dialog.findViewById(R.id.titleTextView);
        mMessageView = (TextView) dialog.findViewById(R.id.messageTextView);
        mPositiveButton = (Button) dialog.findViewById(R.id.positiveButton);
        mNegativeButton = (Button) dialog.findViewById(R.id.negativeButton);
        mDatePicker = (DatePicker) dialog.findViewById(R.id.datePicker);

        mCalendar = Calendar.getInstance();

        Bundle args = getArguments();
        String title = args.getString(KEY_TITLE, "");
        String message = args.getString(KEY_MESSAGE, "");
        String positiveMessage = args.getString(KEY_POSITIVE_TEXT, "Set");
        String negativeMessage = args.getString(KEY_NEGATIVE_TEXT, "Cancel");

        mPositiveButton.setText(positiveMessage);
        mNegativeButton.setText(negativeMessage);

        if(message.length() == 0)
        {
            mMessageView.setVisibility(View.GONE);
        }
        else
        {
            mMessageView.setText(message);
        }
        long defaultValue = args.getLong(KEY_CURRENT_VALUE, new Date().getTime());

        mCalendar.setTimeInMillis(defaultValue);
        mDatePicker.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), this);

        mDatePicker.setMaxDate(new Date().getTime());

        mTitleView.setText(title);

        mPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                mCalendar.set(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
                dismiss();
                Fragment targetFragment = getTargetFragment();
                if(targetFragment != null) {
                    targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                }
                if(mAlertResponseHandler != null) {
                    mAlertResponseHandler.buttonPressed(DatePickerDialogFragment.this);
                }
            }
        });

        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                dismiss();
                Fragment targetFragment = getTargetFragment();
                if (targetFragment != null) {
                    targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                }
                if (mAlertResponseHandler != null) {
                    mCancelPressed = true;
                    mAlertResponseHandler.buttonPressed(DatePickerDialogFragment.this);
                }
            }
        });

        dialog.show();
        return dialog;
    }


    public void setAlertResponseHandler(AlertResponseHandler alertResponseHandler) {
        mAlertResponseHandler = alertResponseHandler;
    }

    @Override
    public void onDateChanged(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        mCalendar.set(year, monthOfYear, dayOfMonth);
    }

    public interface AlertResponseHandler {
        public void buttonPressed(DatePickerDialogFragment alert);
    }
}
