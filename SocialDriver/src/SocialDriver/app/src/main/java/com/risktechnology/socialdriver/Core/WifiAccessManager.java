package com.risktechnology.socialdriver.core;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;

import com.risktechnology.socialdriver.application.SDApplication;
import com.risktechnology.socialdriver.events.WifiConnectionChangedEvent;
import com.risktechnology.socialdriver.utilities.UserPreferences;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Dev on 25/08/2015.
 */
public class WifiAccessManager
{
    private static final String ALLOWED_WIFI_DEVICE_KEY = "allowed_wifi_devices";
    private static final String BLOCKED_WIFI_DEVICE_KEY = "blocked_wifi_devices";
    private static final String TAG = "WifiAccessManager";
    private static WifiAccessManager sInstance;

    private ArrayList<String> mAllowedWifiDevices;
    private ArrayList<String> mBlockedWifiDevices;
    private WifiManager mWifiManager;

    private BroadcastReceiver mRegisteredReceiver;

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            if (this != mRegisteredReceiver)
            {
                //catch old receivers
                SDApplication.getAppContext().unregisterReceiver(this);
                return;
            }

            final String action = intent.getAction();
            NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            String ssid = wifiInfo.getSSID();
            if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION))
            {
                    if (info.isConnected())
                    {
                        Log.i(TAG, "A wifi network has been connected: " + ssid);
                        if (mAllowedWifiDevices.contains(ssid))
                        {
                            JourneyManager.stopMonitoring();
                        }
                    }
                    else
                    {
                        Log.i(TAG, "A wifi network has been disconnected: " + ssid);
                        boolean autoJourneyEnabled = UserPreferences.getSharedInstance().getOptBoolForKey(Constants.SettingsKeyAutoJourneyDetect, true);
                        if(autoJourneyEnabled)
                        {
                            JourneyManager.startMonitoring(true);
                        }
                    }

                EventBus.getDefault().post(new WifiConnectionChangedEvent());

            }
        }
    };

    public static synchronized WifiAccessManager sharedManager()
    {
        if (sInstance == null)
        {
            sInstance = new WifiAccessManager();
        }
        return sInstance;
    }

    public WifiAccessManager()
    {
        loadDevices();
        findDevices();
        setupListeners();
    }


    private void setupListeners()
    {
        // Create a BroadcastReceiver for ACTION_FOUND
        Log.i(TAG, "Adding intent filters for detecting wifi networks");
        IntentFilter connectedFilter = new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        SDApplication.getAppContext().registerReceiver(mBroadcastReceiver, connectedFilter);
        mRegisteredReceiver = mBroadcastReceiver;
    }

    @SuppressWarnings("unchecked")
    private void loadDevices()
    {
        mAllowedWifiDevices = (ArrayList<String>) UserPreferences.getSharedInstance().getArrayForKey(ALLOWED_WIFI_DEVICE_KEY, String[].class);
        if (mAllowedWifiDevices == null)
        {
            mAllowedWifiDevices = new ArrayList<>();
        }
        mBlockedWifiDevices = (ArrayList<String>) UserPreferences.getSharedInstance().getArrayForKey(BLOCKED_WIFI_DEVICE_KEY, String[].class);
        if (mBlockedWifiDevices == null)
        {
            mBlockedWifiDevices = new ArrayList<>();
        }
    }

    private void saveDevices()
    {
        UserPreferences.getSharedInstance().putArrayList(mAllowedWifiDevices, ALLOWED_WIFI_DEVICE_KEY);
        UserPreferences.getSharedInstance().putArrayList(mBlockedWifiDevices, BLOCKED_WIFI_DEVICE_KEY);
    }

    private void findDevices()
    {
        if (mWifiManager == null)
        {
            mWifiManager = (WifiManager) SDApplication.getAppContext().getSystemService(Context.WIFI_SERVICE);
        }

        List<WifiConfiguration> configuredNetworks = mWifiManager.getConfiguredNetworks();

        if (configuredNetworks != null)
        {
            for (WifiConfiguration wifiConfiguration : configuredNetworks)
            {
                if (!mAllowedWifiDevices.contains(wifiConfiguration.SSID) && !mBlockedWifiDevices.contains(wifiConfiguration.SSID))
                {
                    mBlockedWifiDevices.add(wifiConfiguration.SSID);
                }
            }
        }

        saveDevices();
    }


    public void swapDevice(String wifiSSID)
    {
        if (mAllowedWifiDevices.contains(wifiSSID))
        {
            mAllowedWifiDevices.remove(wifiSSID);
            mBlockedWifiDevices.add(wifiSSID);
        }
        else if (mBlockedWifiDevices.contains(wifiSSID))
        {
            mBlockedWifiDevices.remove(wifiSSID);
            mAllowedWifiDevices.add(wifiSSID);
        }
        saveDevices();
    }

    /**
     * Getter for property 'allowedWifiDevices'.
     *
     * @return Value for property 'allowedWifiDevices'.
     */
    public ArrayList<String> getAllowedWifiDevices()
    {
        return mAllowedWifiDevices;
    }

    /**
     * Getter for property 'blockedWifiDevices'.
     *
     * @return Value for property 'blockedWifiDevices'.
     */
    public ArrayList<String> getBlockedWifiDevices()
    {
        return mBlockedWifiDevices;
    }

    public void refreshDevices()
    {
        findDevices();
    }

    public void shutdown()
    {
        Log.i(TAG, "Shutting down wifi manager");
        if (mRegisteredReceiver != null)
        {
            SDApplication.getAppContext().unregisterReceiver(mRegisteredReceiver);
        }
    }
}
