package com.risktechnology.utils;

/**
 * Created by rsousa on 19/03/15.
 */
public final class APIUrls {
    // Dev Api Root Url - http://rtl-dev-mobileapi.elasticbeanstalk.com
    // Rtl Api Root Url - https://mobile-api-eu.rtltelematics.com
    // Rtl US Api Root Url - http://us-mobileapi.rtltelematics.com
    public static String APIRootUrl = "https://mobile-api-eu.rtltelematics.com";
    public static String UserLogin = "/api/account/loginv2/";
    public static String GetPermissions = "/api/account/getpermissions?";
    public static String GetSummaryStatistics = "/api/data/getsummarystatistics?";
    public static String GetWeeklyStatistics = "/api/data/getweeklystatistics?";
    public static String GetMonthlyStatistics = "/api/data/getmonthlystatistics?";
    public static String GetMpg = "/api/vehicle/getmpg?";
    public static String SetMpg = "/api/vehicle/setmpg?";
    public static String GetCurrentLocation = "/api/data/getcurrentlocation?";
    public static String GetJourneys = "/api/data/getjourneys?";
    public static String GetStatistics = "/api/data/getstatistics?";
    public static String GetFuelCosts = "/api/data/getfuelcosts?";
}
