package com.risktechnology.utils;

/**
 * Created by ricardo on 20/05/2015.
 */
public class GlobalValues {
    public static int GoodScore = 75;
    public static int MediumScoreMax = 74;
    public static int MediumScoreMin = 55;
    public static int BadScore = 54;

    public static int MilesFirstSpeedLimit = 30;
    public static int MilesSecondSpeedLimit = 40;
    public static int MilesThirdSpeedLimit = 50;
    public static int MilesFourthSpeedLimit = 60;
    public static int MilesFifthSpeedLimit = 70;
    public static int MilesSixthSpeedLimit = 80;
    public static int MilesSeventhSpeedLimit = 90;
    public static int MilesEighthSpeedLimit = 100;

    public static int KilometersFirstSpeedLimit = 50;
    public static int KilometersSecondSpeedLimit = 60;
    public static int KilometersThirdSpeedLimit = 70;
    public static int KilometersFourthSpeedLimit = 80;
    public static int KilometersFifthSpeedLimit = 90;
    public static int KilometersSixthSpeedLimit = 100;
    public static int KilometersSeventhSpeedLimit = 110;
    public static int KilometersEighthSpeedLimit = 120;
}
