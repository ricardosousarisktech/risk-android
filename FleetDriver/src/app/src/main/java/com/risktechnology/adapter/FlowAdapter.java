package com.risktechnology.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.risktechnology.model.Statistics;
import com.risktechnology.riskmobileapp.HomeActivity;
import com.risktechnology.riskmobileapp.R;
import com.risktechnology.utils.GlobalValues;
import com.risktechnology.widget.Coverflow;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by ricardo on 24/03/2015.
 */
public class FlowAdapter extends CoverFlowAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Integer> mDriverScores;
    private Typeface mFont;
    private boolean mClickableItems;
    HomeActivity.HomeFragment.DataType mDataType;

    public FlowAdapter (Context context, List<Integer> driverScores, boolean clickableItems, HomeActivity.HomeFragment.DataType dataType) {
        mContext = context;
        mDriverScores = driverScores;
        mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mFont = Typeface.createFromAsset(mContext.getAssets(), context.getResources().getString(R.string.selected_font));
        mClickableItems = clickableItems;
        mDataType = dataType;
    }

    @Override
    public int getCount() {
        return mDriverScores.size();
    }

    @Override
    public Integer getItem(int i) {
        return mDriverScores.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getCoverFlowItem(int i, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;

        if (convertView != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            holder = new ViewHolder();
            view = mLayoutInflater.inflate(R.layout.daily_score_flow_item, null);

            holder.score = (TextView)view.findViewById(R.id.dailyDriverScore);
            holder.day = (TextView)view.findViewById(R.id.dailyScoreDayOfWeek);

            holder.score.setTypeface(mFont);
            holder.day.setTypeface(mFont);

            // Change font colour
            holder.score.setTextColor(mContext.getResources().getColor(R.color.grey_good_color));
            holder.day.setTextColor(mContext.getResources().getColor(R.color.grey_good_color));

            view.setLayoutParams(new Coverflow.LayoutParams((int)mContext.getResources().getDimension(R.dimen.coverflow_item_size),
                    (int)mContext.getResources().getDimension(R.dimen.coverflow_item_height)));
            view.setTag(holder);
        }

        if (mDriverScores.get(i) == -1) {
            holder.score.setText("NA");
            holder.score.setBackgroundResource(R.drawable.na_score_ring);
            holder.score.setAlpha(0.5f);
            holder.day.setAlpha(0.5f);
        } else {
            holder.score.setText(String.valueOf(mDriverScores.get(i)));
            if (mDriverScores.get(i) >= GlobalValues.GoodScore) {
                if (mClickableItems)
                    holder.score.setBackgroundResource(R.drawable.good_score_ring_button);
                else
                    holder.score.setBackgroundResource(R.drawable.good_score_ring);
            }
            if (mDriverScores.get(i) >= GlobalValues.MediumScoreMin
                    && mDriverScores.get(i) <= GlobalValues.MediumScoreMax) {
                if (mClickableItems)
                    holder.score.setBackgroundResource(R.drawable.medium_score_ring_button);
                else
                    holder.score.setBackgroundResource(R.drawable.medium_score_ring);
            }
            if (mDriverScores.get(i) <= GlobalValues.BadScore) {
                if (mClickableItems)
                    holder.score.setBackgroundResource(R.drawable.bad_score_ring_button);
                else
                    holder.score.setBackgroundResource(R.drawable.bad_score_ring);
            }
        }
        if (mDataType == HomeActivity.HomeFragment.DataType.WEEK) {
            switch (i) {
                case 0:
                    holder.day.setText(R.string.sunday);
                    break;
                case 1:
                    holder.day.setText(R.string.monday);
                    break;
                case 2:
                    holder.day.setText(R.string.tuesday);
                    break;
                case 3:
                    holder.day.setText(R.string.wednesday);
                    break;
                case 4:
                    holder.day.setText(R.string.thursday);
                    break;
                case 5:
                    holder.day.setText(R.string.friday);
                    break;
                case 6:
                    holder.day.setText(R.string.saturday);
                    break;
            }
        } else if (mDataType == HomeActivity.HomeFragment.DataType.MONTH) {
//            if (String.valueOf(i + 1).endsWith("1"))
//                holder.day.setText((i + 1) + "st");
//            else if (String.valueOf(i + 1).endsWith("2"))
//                holder.day.setText((i + 1) + "nd");
//            else if (String.valueOf(i + 1).endsWith("3"))
//                holder.day.setText((i + 1) + "rd");
//            else
//                holder.day.setText((i + 1) + "th");
            DecimalFormat format = new DecimalFormat("00");
            holder.day.setText(format.format(i + 1));
        }

        return view;
    }

    private static class ViewHolder {
        TextView score, day;
    }
}
