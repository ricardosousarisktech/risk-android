package com.risktechnology.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rsousa on 20/03/15.
 */
public class Statistics implements Parcelable {

    public enum DistanceUnits {
        mi,
        km
    }
    private int mOverallScore;
    private double mDistanceTravelled;
    private DistanceUnits mDistanceUnits;
    private String mTravelTime;
    private Date mStartDate;
    private Date mEndDate;
    private List<Integer> mDriverScores;
    private int mBestScore;
    private int mWorstScore;

    public Statistics () {}

    public Statistics(int mOverallScore) {
        this.mOverallScore = mOverallScore;
    }

    public Statistics(int mOverallScore, double mDistanceTravelled, DistanceUnits mDistanceUnits,
                      String mTravelTime, Date mStartDate, Date mEndDate, List<Integer> mDriverScores) {
        this.mOverallScore = mOverallScore;
        this.mDistanceTravelled = mDistanceTravelled;
        this.mDistanceUnits = mDistanceUnits;
        this.mTravelTime = mTravelTime;
        this.mStartDate = mStartDate;
        this.mEndDate = mEndDate;
        this.mDriverScores = mDriverScores;
        this.mBestScore = mBestScore;
        this.mWorstScore = mWorstScore;
    }

    public int getOverallScore() {
        return mOverallScore;
    }

    public void setOverallScore(int mOverallScore) {
        this.mOverallScore = mOverallScore;
    }

    public double getDistanceTravelled() {
        return mDistanceTravelled;
    }

    public void setDistanceTravelled(double mDistanceTravelled) {
        this.mDistanceTravelled = mDistanceTravelled;
    }

    public DistanceUnits getDistanceUnits() {
        return mDistanceUnits;
    }

    public void setDistanceUnits(String units) {
        switch (units) {
            case "mi":
                mDistanceUnits = DistanceUnits.mi;
                break;
            case "km":
                mDistanceUnits = DistanceUnits.km;
                break;
            default:
                mDistanceUnits = DistanceUnits.mi;
                break;
        }
    }

    public String getTravelTime() {
        return mTravelTime;
    }

    public void setTravelTime(String mTravelTime) {
        this.mTravelTime = mTravelTime;
    }

    public Date getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Date mStartDate) {
        this.mStartDate = mStartDate;
    }

    public Date getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Date mEndDate) {
        this.mEndDate = mEndDate;
    }

    public List<Integer> getDriverScores () {
        return mDriverScores;
    }

    public void setDriverScores (List<Integer> mDriverScores) {
        this.mDriverScores = mDriverScores;
    }

    public int getBestScore() {
        return mBestScore;
    }

    public void setBestScore(int mBestScore) {
        this.mBestScore = mBestScore;
    }

    public int getWorstScore() {
        return mWorstScore;
    }

    public void setWorstScore(int mWorstScore) {
        this.mWorstScore = mWorstScore;
    }

    public boolean isObjectBuilt () {
        try {
            return (!mTravelTime.equals("") && mDriverScores.size() > 0);
        } catch (Exception ex) {
            return false;
        }
    }

    protected Statistics(Parcel in) {
        mOverallScore = in.readInt();
        mDistanceTravelled = in.readDouble();
        mDistanceUnits = (DistanceUnits) in.readValue(DistanceUnits.class.getClassLoader());
        mTravelTime = in.readString();
        long tmpMStartDate = in.readLong();
        mStartDate = tmpMStartDate != -1 ? new Date(tmpMStartDate) : null;
        long tmpMEndDate = in.readLong();
        mEndDate = tmpMEndDate != -1 ? new Date(tmpMEndDate) : null;
        if (in.readByte() == 0x01) {
            mDriverScores = new ArrayList<>();
            in.readList(mDriverScores, Integer.class.getClassLoader());
        } else {
            mDriverScores = null;
        }
        mBestScore = in.readInt();
        mWorstScore = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mOverallScore);
        dest.writeDouble(mDistanceTravelled);
        dest.writeValue(mDistanceUnits);
        dest.writeString(mTravelTime);
        dest.writeLong(mStartDate != null ? mStartDate.getTime() : -1L);
        dest.writeLong(mEndDate != null ? mEndDate.getTime() : -1L);
        if (mDriverScores == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mDriverScores);
        }
        dest.writeInt(mBestScore);
        dest.writeInt(mWorstScore);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Statistics> CREATOR = new Parcelable.Creator<Statistics>() {
        @Override
        public Statistics createFromParcel(Parcel in) {
            return new Statistics(in);
        }

        @Override
        public Statistics[] newArray(int size) {
            return new Statistics[size];
        }
    };
}