package com.risktechnology.model;

/**
 * Created by ricardo on 14/04/2015.
 */
public class FuelCost {
    private double mDistanceTravelled;
    private Statistics.DistanceUnits mDistanceUnits;
    private int mTotalFuelSpend;
    private int mCurrentFuelScore;
    private int mPossibleSavings;

    public FuelCost(double mDistanceTravelled, Statistics.DistanceUnits mDistanceUnits, int mTotalFuelSpend, int mCurrentFuelScore, int mPossibleSavings) {
        this.mDistanceTravelled = mDistanceTravelled;
        this.mDistanceUnits = mDistanceUnits;
        this.mTotalFuelSpend = mTotalFuelSpend;
        this.mCurrentFuelScore = mCurrentFuelScore;
        this.mPossibleSavings = mPossibleSavings;
    }

    public double getDistanceTravelled() {
        return mDistanceTravelled;
    }

    public void setDistanceTravelled(double mDistanceTravelled) {
        this.mDistanceTravelled = mDistanceTravelled;
    }

    public Statistics.DistanceUnits getDistanceUnits() {
        return mDistanceUnits;
    }

    public void setDistanceUnits(Statistics.DistanceUnits mDistanceUnits) {
        this.mDistanceUnits = mDistanceUnits;
    }

    public int getTotalFuelSpend() {
        return mTotalFuelSpend;
    }

    public void setTotalFuelSpend(int mTotalFuelSpend) {
        this.mTotalFuelSpend = mTotalFuelSpend;
    }

    public int getCurrentFuelScore() {
        return mCurrentFuelScore;
    }

    public void setCurrentFuelScore(int mCurrentFuelScore) {
        this.mCurrentFuelScore = mCurrentFuelScore;
    }

    public int getPossibleSavings() {
        return mPossibleSavings;
    }

    public void setPossibleSavings(int mPossibleSavings) {
        this.mPossibleSavings = mPossibleSavings;
    }
}
