package com.risktechnology.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rsousa on 20/03/15.
 */
public class User implements Parcelable {
    public enum Currency implements Serializable {
        GBP,
        EUR,
        USD
    }
    private int mUserID;
    private int mPermissions;
    private Currency mCurrency;
    private List<IMEI> mIMEIList;
    private String mSelectedVehicleReg;

    public User(int mUserID, int mPermissions, String mCurrency, List<IMEI> mIMEIList) {
        this.mUserID = mUserID;
        this.mPermissions = mPermissions;
        setCurrency(mCurrency);
        this.mIMEIList = mIMEIList;
    }

    public int getUserID() {
        return mUserID;
    }

    public void setUserID(int mUserID) {
        this.mUserID = mUserID;
    }

    public int getPermissions() {
        return mPermissions;
    }

    public void setPermissions(int mPermissions) {
        this.mPermissions = mPermissions;
    }

    public Currency getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String value) {
        switch (value) {
            case "GBP":
                mCurrency = Currency.GBP;
                break;
            case "EUR":
                mCurrency = Currency.EUR;
                break;
            case "USD":
                mCurrency = Currency.USD;
                break;
        }
    }

    public List<IMEI> getIMEIList() {
        return mIMEIList;
    }

    public void setIMEIList(List<IMEI> mIMEIList) {
        this.mIMEIList = mIMEIList;
    }

    public String getSelectedVehicleReg() {
        return mSelectedVehicleReg;
    }

    public void setSelectedVehicleReg(String mSelectedVehicleReg) {
        this.mSelectedVehicleReg = mSelectedVehicleReg;
    }

    public String getImeiByVehicleReg (String vehicleReg) {
        String imei = "";
        for (int i = 0; i < mIMEIList.size(); i++) {
            if (vehicleReg.equals(mIMEIList.get(i).getVehicleReg())) {
                imei = mIMEIList.get(i).getIMEI();
                break;
            }
        }

        return imei;
    }

    public String getVehicleRegByIMEI (String imei) {
        String vehicleReg = "";
        for (int i = 0; i < mIMEIList.size(); i++) {
            if (imei.equals(mIMEIList.get(i).getIMEI())) {
                vehicleReg = mIMEIList.get(i).getVehicleReg();
                break;
            }
        }
        return  vehicleReg;
    }

    protected User(Parcel in) {
        mUserID = in.readInt();
        mPermissions = in.readInt();
        mCurrency = (Currency) in.readValue(Currency.class.getClassLoader());
        if (in.readByte() == 0x01) {
            mIMEIList = new ArrayList<IMEI>();
            in.readList(mIMEIList, IMEI.class.getClassLoader());
        } else {
            mIMEIList = null;
        }
        mSelectedVehicleReg = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mUserID);
        dest.writeInt(mPermissions);
        dest.writeValue(mCurrency);
        if (mIMEIList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mIMEIList);
        }
        dest.writeString(mSelectedVehicleReg);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
