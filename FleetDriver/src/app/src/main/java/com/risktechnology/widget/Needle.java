package com.risktechnology.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import com.risktechnology.riskmobileapp.R;

/**
 * Created by ricardo on 27/03/2015.
 */
public class Needle extends View {
    private Paint linePaint;
    private Path linePath;
    private Paint needleScrewPaint;
    private float x = 270.0f, y = 240.0f, x2 = 450.0f, y2 = 230.0f, y3 = 250.0f;

    private Matrix matrix;
    private int framePerSeconds = 500;
    private long animationDuration = 100000;
    private long startTime;

    private float mMaxRotationDegrees;
    private float mRotatedDegrees;

    private float mStrokeWidth, mCircleRadius, mInnerCircleRadius, mShadowLayerRadius;

    private boolean mResetNeedle;

    public Needle(Context context) {
        super(context);
        initSizes ();
        matrix = new Matrix();
        matrix.postRotate(90.0f, x, y);
        mRotatedDegrees = 0.0f;
        this.startTime = System.currentTimeMillis();
        this.postInvalidate();
        init();
    }

    public Needle(Context context, AttributeSet attrs) {
        super(context, attrs);
        initSizes ();
        matrix = new Matrix();
        matrix.postRotate(90.0f, x, y);
        mRotatedDegrees = 0.0f;
        this.startTime = System.currentTimeMillis();
        this.postInvalidate();
        init();
    }

    public Needle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initSizes ();
        matrix = new Matrix();
        matrix.postRotate(90.0f, x, y);
        mRotatedDegrees = 0.0f;
        this.startTime = System.currentTimeMillis();
        this.postInvalidate();
        init();
    }

    public void init() {
//        int screenSize = getResources().getConfiguration().screenLayout &
//                Configuration.SCREENLAYOUT_SIZE_MASK;
//        switch (screenSize) {
//            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
//                // If less then XHDPI:
//                if (getResources().getDisplayMetrics().density < 2.0) {
//                    mStrokeWidth = 3.0f;
//                    mCircleRadius = 10.0f;
//                    mInnerCircleRadius = 6.0f;
//                    mShadowLayerRadius = 5.0f;
//                } else {
//                    mStrokeWidth = 3.0f;
//                    mCircleRadius = 16.0f;
//                    mInnerCircleRadius = 12.0f;
//                }
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_SMALL:
//                mStrokeWidth = 2.0f;
//                mCircleRadius = 14.0f;
//                mInnerCircleRadius = 10.0f;
//                break;
//        }
        linePaint = new Paint();
        linePaint.setColor(Color.WHITE); // Set the color
        linePaint.setStyle(Paint.Style.FILL_AND_STROKE); // set the border and fills the inside of needle
        linePaint.setAntiAlias(true);
        linePaint.setStrokeWidth(mStrokeWidth); // width of the border
        linePaint.setShadowLayer(mShadowLayerRadius, 0.1f, 0.1f, Color.GRAY); // Shadow of the needle

        linePath = new Path();
        linePath.moveTo(y, y);
        linePath.lineTo(x, y2);
        linePath.lineTo(x2, y);
        linePath.lineTo(x, y3);
        linePath.lineTo(y, y);
        linePath.addCircle(x, y, mCircleRadius, Path.Direction.CW);
        linePath.close();

        needleScrewPaint = new Paint();
        needleScrewPaint.setColor(Color.BLACK);
        needleScrewPaint.setAntiAlias(true);
        needleScrewPaint.setShader(new RadialGradient(x, y, 10.0f,
                Color.DKGRAY, Color.BLACK, Shader.TileMode.CLAMP));
    }

    private void initSizes () {
        x = getResources().getDimension(R.dimen.needle_x);
        x2 = getResources().getDimension(R.dimen.needle_x2);
        y = getResources().getDimension(R.dimen.needle_y);
        y2 = getResources().getDimension(R.dimen.needle_y2);
        y3 = getResources().getDimension(R.dimen.needle_y3);
        mStrokeWidth = getResources().getDimension(R.dimen.needle_stroke_width);
        mCircleRadius = getResources().getDimension(R.dimen.needle_circle_radius);
        mInnerCircleRadius = getResources().getDimension(R.dimen.needle_inner_circle);
        mShadowLayerRadius = getResources().getDimension(R.dimen.needle_shadow_layer);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mResetNeedle) {
            matrix = new Matrix();
            matrix.postRotate(90.0f, x, y);
            mRotatedDegrees = 0.0f;
            this.startTime = System.currentTimeMillis();
            this.postInvalidate();
            init();
            mResetNeedle = false;
        }

        if (mRotatedDegrees < mMaxRotationDegrees) {
            float degreesToRotate = mMaxRotationDegrees / 50;
            if (mRotatedDegrees + degreesToRotate > mMaxRotationDegrees) {
                degreesToRotate = mMaxRotationDegrees - mRotatedDegrees;
                mRotatedDegrees = mMaxRotationDegrees;
            } else
                mRotatedDegrees += mMaxRotationDegrees / 50;
            long elapsedTime = System.currentTimeMillis() - startTime;

            matrix.postRotate(degreesToRotate, x, y); // rotate every second
            canvas.concat(matrix);

            canvas.drawPath(linePath, linePaint);

            canvas.drawCircle(x, y, mInnerCircleRadius, needleScrewPaint);

            if (elapsedTime < animationDuration) {
                this.postInvalidateDelayed(10000 / framePerSeconds);
            }

//            this.postInvalidateOnAnimation();
            invalidate();
        } else {
            canvas.concat(matrix);
            canvas.drawPath(linePath, linePaint);
            canvas.drawCircle(x, y, mInnerCircleRadius, needleScrewPaint);
        }
    }

    public void setRotatedDegrees (float rotation) {
        mRotatedDegrees = rotation;
    }

    public void setmMaxRotationDegrees (float maxRotationDegrees) {
        this.mMaxRotationDegrees = maxRotationDegrees;
    }

    public void setResetNeedle (boolean value) {
        mResetNeedle = value;
    }
}
