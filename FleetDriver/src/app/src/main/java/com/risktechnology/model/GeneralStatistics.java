package com.risktechnology.model;

import java.util.List;

/**
 * Created by ricardo on 14/04/2015.
 */
public class GeneralStatistics {
    private int mAccelEventCount;
    private int mDecelEventCount;
    private int mSpeedingEventCount;
    private List<SpeedingEvent> mSpeedingEvents;

    public GeneralStatistics () {}

    public GeneralStatistics(int mAccelEventCount, int mDecelEventCount, int mSpeedingEventCount) {
        this.mAccelEventCount = mAccelEventCount;
        this.mDecelEventCount = mDecelEventCount;
        this.mSpeedingEventCount = mSpeedingEventCount;
    }

    public int getAccelEventCount() {
        return mAccelEventCount;
    }

    public void setAccelEventCount(int mAccelEventCount) {
        this.mAccelEventCount = mAccelEventCount;
    }

    public int getDecelEventCount() {
        return mDecelEventCount;
    }

    public void setDecelEventCount(int mDecelEventCount) {
        this.mDecelEventCount = mDecelEventCount;
    }

    public int getSpeedingEventCount() {
        return mSpeedingEventCount;
    }

    public void setSpeedingEventCount(int mSpeedingEventCount) {
        this.mSpeedingEventCount = mSpeedingEventCount;
    }

    public List<SpeedingEvent> getSpeedingEvents() {
        return mSpeedingEvents;
    }

    public void setSpeedingEvents(List<SpeedingEvent> mSpeedingEvents) {
        this.mSpeedingEvents = mSpeedingEvents;
    }

    public class SpeedingEvent {
        private int mSpeedLimit;
        private int mEventCount;

        public SpeedingEvent(int mSpeedLimit, int mEventCount) {
            this.mSpeedLimit = mSpeedLimit;
            this.mEventCount = mEventCount;
        }

        public int getSpeedLimit() {
            return mSpeedLimit;
        }

        public void setSpeedLimit(int mSpeedLimit) {
            this.mSpeedLimit = mSpeedLimit;
        }

        public int getEventCount() {
            return mEventCount;
        }

        public void setEventCount(int mEventCount) {
            this.mEventCount = mEventCount;
        }
    }
}
