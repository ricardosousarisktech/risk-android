package com.risktechnology.riskmobileapp;

import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.risktechnology.model.ApiResponse;
import com.risktechnology.model.FuelCost;
import com.risktechnology.model.Statistics;
import com.risktechnology.model.User;
import com.risktechnology.utils.APIUrls;
import com.risktechnology.utils.Functions;
import com.risktechnology.utils.GlobalValues;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ricardo on 14/04/2015.
 */
public class FuelSavingsFragment extends Fragment {
    private enum CountingType {
        Normal,
        CurrencyPound,
        CurrencyEuro,
        CurrencyDollar,
        DistanceKms,
        DistanceMi
    }
    private Typeface mFont;
    private User mActiveUser;
    private SharedPreferences mUserPreferences;
    private GetFuelCost mGetFuelCost;

    private List<Statistics> mDriverStatistics;
    private int mCurrentDisplayedChild;
    private HomeActivity.HomeFragment.DataType mCurrentSelectedType;

    private TextView mDistanceTraveledValue, mDistanceTraveledDesc, mFuelCostValue, mFuelCostDesc,
            mFuelScore, mFuelScoreDesc, mPossibleSavings, mPossibleSavingsDesc, mLeftArrow, mRightArrow;
    private ViewFlipper mStatisticsViewFlipper;
    private RelativeLayout mProgressView;
    private GestureDetector mGestureDetector;

    public FuelSavingsFragment () {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_fuel_savings, null);
        mFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.selected_font));
        mActiveUser = ((HomeActivity)getActivity()).getActiveUser();
        mUserPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        mDriverStatistics = getArguments().getParcelableArrayList("Statistics");
        mCurrentDisplayedChild = getArguments().getInt("DisplayedChild");
        mCurrentSelectedType = HomeActivity.HomeFragment.DataType.valueOf(getArguments().getString("DataType"));

        mProgressView = (RelativeLayout)fragmentView.findViewById(R.id.fuelSavingsProgressBackground);
        mStatisticsViewFlipper = (ViewFlipper)fragmentView.findViewById(R.id.savingsViewFlipper);
        mLeftArrow = (TextView) fragmentView.findViewById(R.id.leftArrow);
        mRightArrow = (TextView) fragmentView.findViewById(R.id.rightArrow);
        mDistanceTraveledValue = (TextView)fragmentView.findViewById(R.id.fuelSavingsDistance);
        mDistanceTraveledDesc = (TextView)fragmentView.findViewById(R.id.fuelSavingsDistanceDesc);
        mFuelCostValue = (TextView)fragmentView.findViewById(R.id.estimatedCost);
        mFuelCostDesc = (TextView)fragmentView.findViewById(R.id.estimatedCostDesc);
        mFuelScore = (TextView)fragmentView.findViewById(R.id.fuelScore);
        mFuelScoreDesc = (TextView)fragmentView.findViewById(R.id.fuelScoreDesc);
        mPossibleSavings = (TextView)fragmentView.findViewById(R.id.possibleSavings);
        mPossibleSavingsDesc = (TextView)fragmentView.findViewById(R.id.possibleSavingsDesc);

        mLeftArrow.setText("<");
        mRightArrow.setText(">");

        mGestureDetector = new GestureDetector (getActivity(), new CustomGestureDetector());
        mStatisticsViewFlipper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mGestureDetector.onTouchEvent(event);
                return true;
            }
        });
        //Simulate left to right gesture on view flipper
        mLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayNextWeek();
            }
        });
        mLeftArrow.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    mLeftArrow.setAlpha(0.6f);
                if (event.getAction() == MotionEvent.ACTION_UP)
                    mLeftArrow.setAlpha(1f);
                return false;
            }
        });

        //Simulate right to left gesture on view flipper
        mRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayPreviousWeek();
            }
        });
        mRightArrow.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    mRightArrow.setAlpha(0.6f);
                if (event.getAction() == MotionEvent.ACTION_UP)
                    mRightArrow.setAlpha(1f);
                return false;
            }
        });

        setFonts();
        setViewFlipperWithValues ();

        mGetFuelCost = new GetFuelCost ();
        mGetFuelCost.execute (new DateTime(getArguments().getLong("StartDate")),
                new DateTime(getArguments().getLong("EndDate")));

        return fragmentView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGetFuelCost.getStatus() == AsyncTask.Status.RUNNING)
            mGetFuelCost.cancel(true);
    }

    private class GetFuelCost extends AsyncTask<DateTime, Void, ApiResponse> {
        private FuelCost mFuelCost;

        public GetFuelCost () {
            setDefaultValues();
        }

        @Override
        protected ApiResponse doInBackground(DateTime... params) {
            ApiResponse apiResponse = null;

            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetFuelCosts");
            try {
                URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetFuelCosts);
                HttpHost httpHost = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());

                // Dates format for signature
                SimpleDateFormat signatureDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

                int startDateMillisecondsOffset = Functions.getGMTOffset(params[0]) * 3600000;
                int endDateMillisecondsOffset = Functions.getGMTOffset(params[1]) * 3600000;

                // Post body
                JSONObject jsonBody = new JSONObject();
                jsonBody.put("UserID", mActiveUser.getUserID());
                jsonBody.put("IMEI", mActiveUser.getImeiByVehicleReg(mActiveUser.getSelectedVehicleReg()));
                jsonBody.put("StartDate", "/Date(" + params[0].plusMillis(startDateMillisecondsOffset).getMillis() + ")/");

                String signatureStartDate = signatureDateFormat.format(new Date(params[0].getMillis()));

                // endDateTime + 1 day at midnight
//                params[1] = new DateTime (params[1].plusDays(1).getMillis()).withTimeAtStartOfDay();

                jsonBody.put("EndDate", "/Date(" + params[1].plusMillis(endDateMillisecondsOffset).getMillis() + ")/");
                String signatureEndDate = signatureDateFormat.format(new Date (params[1].getMillis()));

                // Parameters message to encrypt (EndDate, IMEI, StartDate, userid, UserID)
                String message = signatureEndDate +
                        mActiveUser.getImeiByVehicleReg(mActiveUser.getSelectedVehicleReg()) +
                        signatureStartDate + String.valueOf(mActiveUser.getUserID()) +
                        String.valueOf(mActiveUser.getUserID());

                // Get the signature value
                String signature = Functions.getHMACKey(message, mUserPreferences.getString("secretkey", ""));

                HttpPost httpPost = new HttpPost(APIUrls.APIRootUrl + APIUrls.GetFuelCosts + "userid="
                        + mActiveUser.getUserID() + "&signature=" + signature);

                StringEntity se = new StringEntity(jsonBody.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String responseString = EntityUtils.toString(httpEntity);
                JSONObject responseJSON = new JSONObject(responseString);

                if (responseJSON.getString("Success").equals("true")) {
                    JSONObject fuelCostJSONObj = responseJSON.getJSONObject("FuelCost");
                    mFuelCost = new FuelCost(fuelCostJSONObj.getDouble("DistanceTravelled"),
                            Statistics.DistanceUnits.valueOf(fuelCostJSONObj.getString("DistanceUnits")),
                            (int)Math.round(fuelCostJSONObj.getDouble("TotalFuelSpend")),
                            fuelCostJSONObj.getInt("CurrentFuelScore"),
                            (int)Math.round(fuelCostJSONObj.getDouble("PossibleSaving")));
                    httpClient.close();
                    apiResponse = new ApiResponse(true, 0, "Success");
                    return apiResponse;
                } else {
                    apiResponse = new ApiResponse(false, responseJSON.getInt("ErrorCode"), responseJSON.getString("ErrorDescription"));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                httpClient.close();
                return apiResponse;
            }
            httpClient.close();
            return apiResponse;
        }

        @Override
        protected void onPostExecute(ApiResponse response) {
            super.onPostExecute(response);
            mProgressView.setVisibility(View.GONE);
            if (response != null) {
                if (response.getSuccess()) {
                    switch (mFuelCost.getDistanceUnits()) {
                        case km:
                            animateCounting(mDistanceTraveledValue, 0, (int) Math.round(mFuelCost.getDistanceTravelled()), CountingType.DistanceKms);
                            break;
                        case mi:
                            animateCounting(mDistanceTraveledValue, 0, (int) Math.round(mFuelCost.getDistanceTravelled()), CountingType.DistanceMi);
                            break;
                    }
                    switch (mActiveUser.getCurrency()) {
                        case EUR:
                            animateCounting(mFuelCostValue, 0, mFuelCost.getTotalFuelSpend(), CountingType.CurrencyEuro);
                            animateCounting(mPossibleSavings, 0, mFuelCost.getPossibleSavings(), CountingType.CurrencyEuro);
                            break;
                        case USD:
                            animateCounting(mFuelCostValue, 0, mFuelCost.getTotalFuelSpend(), CountingType.CurrencyDollar);
                            animateCounting(mPossibleSavings, 0, mFuelCost.getPossibleSavings(), CountingType.CurrencyDollar);
                            break;
                        case GBP:
                            animateCounting(mFuelCostValue, 0, mFuelCost.getTotalFuelSpend(), CountingType.CurrencyPound);
                            animateCounting(mPossibleSavings, 0, mFuelCost.getPossibleSavings(), CountingType.CurrencyPound);
                            break;
                    }
                    if (mFuelCost.getCurrentFuelScore() >= GlobalValues.GoodScore) {
                        mFuelScore.setBackgroundResource(R.drawable.good_score_ring);
                    } else if (mFuelCost.getCurrentFuelScore() <= GlobalValues.MediumScoreMax
                            && mFuelCost.getCurrentFuelScore() >= GlobalValues.MediumScoreMin) {
                        mFuelScore.setBackgroundResource(R.drawable.medium_score_ring);
                    } else if (mFuelCost.getCurrentFuelScore() <= GlobalValues.BadScore) {
                        mFuelScore.setBackgroundResource(R.drawable.bad_score_ring);
                    }
                    animateCounting(mFuelScore, 0, mFuelCost.getCurrentFuelScore(), CountingType.Normal);
                } else {
                    setDefaultValues ();
                    switch (response.getErrorCode()) {
                        case 100:
                            Toast.makeText(getActivity(), getResources().getString(R.string.api_error_100), Toast.LENGTH_SHORT).show();
                            break;
                        case 200:
                            Toast.makeText(getActivity(), getResources().getString(R.string.api_error_200), Toast.LENGTH_SHORT).show();
                            break;
                        case 1000:
                            Toast.makeText(getActivity(), getResources().getString(R.string.api_error_1000), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            } else
                setDefaultValues ();
        }
    }

    private class CustomGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private final int SWIPE_MIN_DISTANCE = 120;
        private final int SWIPE_THRESHOLD_VELOCITY = 200;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                // Swipe right to left
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    displayPreviousWeek ();
                } else
                    // Swipe left to right
                    if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                        displayNextWeek();
                    }
            } catch (Exception e) {
                System.out.println (e.getMessage());
            }

            return false;
        }
    }

    public void displayNextWeek () {
        if (mStatisticsViewFlipper.getDisplayedChild() < mStatisticsViewFlipper.getChildCount() - 1) {
            mStatisticsViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_right_in));
            mStatisticsViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_right_out));
            mStatisticsViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_right_out));
            mStatisticsViewFlipper.showNext();
            if (mGetFuelCost.getStatus() == AsyncTask.Status.RUNNING)
                mGetFuelCost.cancel(true);
            mProgressView.setVisibility(View.VISIBLE);
            setDefaultValues ();
            mGetFuelCost = new GetFuelCost();
            mGetFuelCost.execute(new DateTime(mDriverStatistics.get(mStatisticsViewFlipper.getDisplayedChild()).getStartDate().getTime()),
                    new DateTime(mDriverStatistics.get(mStatisticsViewFlipper.getDisplayedChild()).getEndDate().getTime()));
        }
    }

    public void displayPreviousWeek () {
        if (mStatisticsViewFlipper.getDisplayedChild() > 0) {
            mStatisticsViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_left_in));
            mStatisticsViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_left_out));
            mStatisticsViewFlipper.showPrevious();
            if (mGetFuelCost.getStatus() == AsyncTask.Status.RUNNING)
                mGetFuelCost.cancel(true);
            mProgressView.setVisibility(View.VISIBLE);
            setDefaultValues ();
            mGetFuelCost = new GetFuelCost();
            mGetFuelCost.execute(new DateTime(mDriverStatistics.get(mStatisticsViewFlipper.getDisplayedChild()).getStartDate().getTime()),
                    new DateTime(mDriverStatistics.get(mStatisticsViewFlipper.getDisplayedChild()).getEndDate().getTime()));
        }
    }

    private void setFonts () {
        mLeftArrow.setTypeface(mFont);
        mRightArrow.setTypeface(mFont);
        mDistanceTraveledValue.setTypeface(mFont);
        mDistanceTraveledDesc.setTypeface(mFont);
        mFuelCostValue.setTypeface(mFont);
        mFuelCostDesc.setTypeface(mFont);
        mFuelScore.setTypeface(mFont);
        mFuelScoreDesc.setTypeface(mFont);
        mPossibleSavings.setTypeface(mFont);
        mPossibleSavingsDesc.setTypeface(mFont);
    }

    /**
     * @param view view to animate
     * @param from minimum value
     * @param to maximum value
     * @param type type of counting (place the right symbols after or before text)
     */
    public void animateCounting (final TextView view, int from, int to, final CountingType type) {
        ValueAnimator animator = new ValueAnimator();
        animator.setIntValues(from, to);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                try {
                    switch (type) {
                        case Normal:
                            view.setText(String.valueOf(animation.getAnimatedValue()));
                            break;
                        case CurrencyEuro:
                            view.setText(getResources().getString(R.string.currency_eur) + animation.getAnimatedValue());
                            break;
                        case CurrencyDollar:
                            view.setText(getResources().getString(R.string.currency_dollar) + animation.getAnimatedValue());
                            break;
                        case CurrencyPound:
                            view.setText(getResources().getString(R.string.currency_pound) + animation.getAnimatedValue());
                            break;
                        case DistanceKms:
                            view.setText(animation.getAnimatedValue() + " " + getResources().getString(R.string.kms));
                            break;
                        case DistanceMi:
                            view.setText(animation.getAnimatedValue() + " " + getResources().getString(R.string.miles));
                            break;
                    }
                } catch (IllegalStateException ex) {
                    ex.printStackTrace();
                }
            }
        });
        animator.setEvaluator(new TypeEvaluator<Integer>() {
            @Override
            public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
                return Math.round((endValue - startValue) * fraction);
            }
        });
        animator.setDuration(1000);
        animator.start();
    }

    private void setDefaultValues () {
        mDistanceTraveledValue.setText("");
        mFuelCostValue.setText("");
        mFuelScore.setText(getResources().getString(R.string.default_counting_value));
        mFuelScore.setBackgroundResource(R.drawable.bad_score_ring);
        mPossibleSavings.setText("");
    }

    private void setViewFlipperWithValues () {
        DateTime now = new DateTime();
        for (int i = 0; i < mDriverStatistics.size(); i++) {
            View flipperView = getActivity().getLayoutInflater().inflate(R.layout.view_flipper_view_layout, null);
            TextView flipperText = (TextView) flipperView.findViewById(R.id.viewFlipperTextView);
            flipperText.setTypeface(mFont);
            switch (mCurrentSelectedType) {
                case WEEK:
                    if (i == 0)
                        flipperText.setText(getResources().getString(R.string.current_week));
                    else if (i == 1)
                        flipperText.setText(getResources().getString(R.string.last_week));
                    else {
                        DateTime startDate = new DateTime(mDriverStatistics.get(i).getStartDate().getTime());
                        DateTime endDate = new DateTime(mDriverStatistics.get(i).getEndDate().getTime());
                        flipperText.setText(String.format("%d/%d - %d/%d", startDate.getDayOfMonth(), startDate.getMonthOfYear(), endDate.getDayOfMonth(), endDate.getMonthOfYear()));
                    }
                    break;
                case MONTH:
                    DateTime.Property monthOfYear = now.monthOfYear();
                    flipperText.setText(monthOfYear.getAsText(Locale.getDefault()).toUpperCase() + " " + now.getYear());
                    now = now.minusMonths(1);
                    break;
            }
            mStatisticsViewFlipper.addView(flipperView);
        }
        mStatisticsViewFlipper.setDisplayedChild(mCurrentDisplayedChild);
    }

    public void reloadValues () {
        if (mGetFuelCost.getStatus() == AsyncTask.Status.RUNNING)
            mGetFuelCost.cancel(true);
        mGetFuelCost = new GetFuelCost();
        mGetFuelCost.execute(new DateTime(mDriverStatistics.get(mStatisticsViewFlipper.getDisplayedChild()).getStartDate().getTime()),
                new DateTime(mDriverStatistics.get(mStatisticsViewFlipper.getDisplayedChild()).getEndDate().getTime()));
    }
}
