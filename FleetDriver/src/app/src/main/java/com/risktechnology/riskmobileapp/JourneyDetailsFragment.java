package com.risktechnology.riskmobileapp;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.risktechnology.adapter.MarkerTitleAdapter;
import com.risktechnology.model.IMEI;
import com.risktechnology.model.Journey;
import com.risktechnology.model.JourneyEvent;
import com.risktechnology.model.Location;
import com.risktechnology.model.Statistics;
import com.risktechnology.model.User;
import com.risktechnology.utils.APIUrls;
import com.risktechnology.utils.Functions;
import com.risktechnology.utils.GlobalValues;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class JourneyDetailsFragment extends Fragment {
    private enum MarkerType {
        ALL,
        BREAKING,
        ACCELERATION,
        SPEED
    }

    Typeface mFont;

    // Earth's offset and radius
    private int offset = 268435456;
    private double radius = 85445659.4471;

    private User mActiveUser;
    private GoogleMap mMap;
    private Journey mJourney;
    private List<JourneyEvent> mJourneyEvents;
    private LatLngBounds mJourneyBounds = null;

    private GetDirections mGetDirections;
    private GetJourneyEvents mGetJourneyEvents;

    private TextView mDriverScore, mJourneyDate, mJourneyStartTime, mJourneyEndTime, mJourneyStartAddressLine,
            mJourneyStartPostCode, mJourneyDistanceTravelled, mJourneyEndAddressLine, mJourneyEndPostCode,
            mJourneyTravelTime, mJourneyBrakingScore, mJourneyAccelerationScore, mJourneySpeedScore;

    private List<Marker> mAllMarkers = new ArrayList<>();
    private List<Marker> mBreakingMarkers = new ArrayList<>();
    private List<Marker> mAccelerationMarkers = new ArrayList<>();
    private List<Marker> mSpeedMarkers = new ArrayList<>();
    private List<Marker> mHighlightedMarkers = new ArrayList<>();

    Bitmap mIconSmallEvent, mIconBigEvent, mIconStart, mIconEnd;

    private MarkerType mMarkersToShow;

    private static int mJourneyStartEndIconSize;
    private static int mEventIconBigSize;
    private static int mEventIconSize;

    private String mIMEI;

    private float mCurrentBearing = 0.0f;

    public JourneyDetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_journey_detail, container, false);

        mFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.selected_font));

        mJourneyStartEndIconSize = Math.round(getActivity().getResources().getDimension(R.dimen.journey_start_end_icon_size));
        mEventIconBigSize = Math.round(getActivity().getResources().getDimension(R.dimen.event_big_size));
        mEventIconSize = Math.round(getActivity().getResources().getDimension(R.dimen.event_small_size));

        switch (getArguments().getInt("MarkerType")) {
            case 0:
                mMarkersToShow = MarkerType.ALL;
                break;
            case 1:
                mMarkersToShow = MarkerType.BREAKING;
                break;
            case 2:
                mMarkersToShow = MarkerType.ACCELERATION;
                break;
            case 3:
                mMarkersToShow = MarkerType.SPEED;
                break;
            default:
                mMarkersToShow = MarkerType.ALL;
        }

        mActiveUser = ((HomeActivity)getActivity()).getActiveUser();
        mJourney = getArguments().getParcelable("Journey");
        mJourneyEvents = mJourney.getJourneyEvents();
        mDriverScore = (TextView)view.findViewById(R.id.journeyItemScore);
        mJourneyDate = (TextView)view.findViewById(R.id.journeyDate);
        mJourneyStartTime = (TextView)view.findViewById(R.id.journeyStartTime);
        mJourneyEndTime = (TextView)view.findViewById(R.id.journeyEndTime);
        mJourneyStartAddressLine = (TextView)view.findViewById(R.id.journeyStartAddressLine);
        mJourneyStartPostCode = (TextView)view.findViewById(R.id.journeyStartPostCode);
        mJourneyDistanceTravelled = (TextView)view.findViewById(R.id.journeyDistanceTravelled);
        mJourneyEndAddressLine = (TextView)view.findViewById(R.id.journeyEndAddressLine);
        mJourneyEndPostCode = (TextView)view.findViewById(R.id.journeyEndPostCode);
        mJourneyTravelTime = (TextView)view.findViewById(R.id.journeyTravelTime);
        mJourneyBrakingScore = (TextView)view.findViewById(R.id.journeyBrakingScore);
        mJourneyAccelerationScore = (TextView)view.findViewById(R.id.journeyAccelerationScore);
        mJourneySpeedScore = (TextView)view.findViewById(R.id.journeySpeedScore);

        setTextViewsTypefaces();
        setJourneyValues();

        ((HomeActivity)getActivity()).setScreenTitle(mActiveUser.getSelectedVehicleReg());

        mIconSmallEvent = BitmapFactory.decodeResource(getResources(), R.drawable.event_bearing_arrow);
        mIconBigEvent = BitmapFactory.decodeResource(getResources(), R.drawable.event_bearing_arrow_red);
        mIconStart = BitmapFactory.decodeResource(getResources(), R.drawable.journey_start);
        mIconEnd = BitmapFactory.decodeResource(getResources(), R.drawable.journey_end);

        mIMEI = mActiveUser.getImeiByVehicleReg(mActiveUser.getSelectedVehicleReg());

        mGetJourneyEvents = new GetJourneyEvents();
        mGetJourneyEvents.execute(mJourney.getStartTime(), mJourney.getFinishTime());

        mJourneyBrakingScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBreakingMarkers(true);
            }
        });

        mJourneyAccelerationScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAccelerationMarkers(true);
            }
        });

        mJourneySpeedScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSpeedMarkers(true);
            }
        });

        mDriverScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearMarkersFromMap(true);
                clearMarkers();
                addAllMarkers();
            }
        });

        return view;
    }

    private void addBreakingMarkers (boolean animate) {
        clearMarkersFromMap(false);

        for (int i = 0; i < mBreakingMarkers.size(); i++) {
            mBreakingMarkers.get(i).remove();
            Marker m = mMap.addMarker(new MarkerOptions().position(mBreakingMarkers.get(i).getPosition())
                    .title(mBreakingMarkers.get(i).getTitle())
                    .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconBigEvent, mEventIconBigSize, mEventIconBigSize, false)))
                    .rotation(mBreakingMarkers.get(i).getRotation()));
            if (animate)
                animateMarker(m, m.getPosition());
            mHighlightedMarkers.add(m);
        }
    }

    private void addAccelerationMarkers (boolean animate) {
        clearMarkersFromMap(false);

        for (int i = 0; i < mAccelerationMarkers.size(); i++) {
            mAccelerationMarkers.get(i).remove();
            Marker m = mMap.addMarker(new MarkerOptions().position(mAccelerationMarkers.get(i).getPosition())
                    .title(mAccelerationMarkers.get(i).getTitle())
                    .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconBigEvent, mEventIconBigSize, mEventIconBigSize, false)))
                    .rotation(mAccelerationMarkers.get(i).getRotation()));
            if (animate)
                animateMarker(m, m.getPosition());
            mHighlightedMarkers.add(m);
        }
    }

    private void addSpeedMarkers (boolean animate) {
        clearMarkersFromMap(false);

        for (int i = 0; i < mSpeedMarkers.size(); i++) {
            mSpeedMarkers.get(i).remove();
            Marker m = mMap.addMarker(new MarkerOptions().position(mSpeedMarkers.get(i).getPosition())
                    .title(mSpeedMarkers.get(i).getTitle())
                    .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconBigEvent, mEventIconBigSize, mEventIconBigSize, false)))
                    .rotation(mSpeedMarkers.get(i).getRotation()));
            if (animate)
                animateMarker(m, m.getPosition());
            mHighlightedMarkers.add(m);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager manager = getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();

        final com.google.android.gms.maps.MapFragment mapFragment = new com.google.android.gms.maps.MapFragment() {
            @Override
            public void onActivityCreated(Bundle savedInstanceState) {
                super.onActivityCreated(savedInstanceState);
                mMap = getMap();
                if (mMap != null) {
                    UiSettings settings = mMap.getUiSettings();
                    settings.setRotateGesturesEnabled(false);
                    settings.setTiltGesturesEnabled(false);
                }
            }

            // Exits the fragment if the user touches the back button
            @Override
            public void onDestroy() {
                super.onDestroy();
                if (mGetDirections != null) {
                    if (mGetDirections.getStatus() == AsyncTask.Status.RUNNING)
                        mGetDirections.cancel(true);
                }
            }
        };
        fragmentTransaction.replace(R.id.map_container, mapFragment, "JourneyMap");
        fragmentTransaction.commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGetJourneyEvents != null) {
            if (mGetJourneyEvents.getStatus() == AsyncTask.Status.RUNNING)
                mGetJourneyEvents.cancel(true);
        }
        if (mGetDirections != null) {
            if (mGetDirections.getStatus() == AsyncTask.Status.RUNNING)
                mGetDirections.cancel(true);
        }
    }

    private class GetJourneyEvents extends AsyncTask<DateTime, Void, Boolean> {
        @Override
        protected Boolean doInBackground(DateTime... params) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetJourneys");

            try {
                URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetJourneys);
                HttpHost httpHost = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());

                // Dates format for signature
                SimpleDateFormat signatureDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

                int startDateMillisecondsOffset = Functions.getGMTOffset(params[0]) * 3600000;

                // Post JSON body
                JSONObject getJourneysJSONObj = new JSONObject();
                getJourneysJSONObj.put("UserID", mActiveUser.getUserID());
                getJourneysJSONObj.put("IMEI", mIMEI);
                getJourneysJSONObj.put("StartDate", "/Date(" + params[0].getMillis() + ")/");
                String signatureStartDate = signatureDateFormat.format(new Date(params[0].getMillis() - startDateMillisecondsOffset));

                int endDateMillisecondsOffset = Functions.getGMTOffset(params[1]) * 3600000;

                getJourneysJSONObj.put("EndDate", "/Date(" + params[1].getMillis() + ")/");
                String signatureEndDate = signatureDateFormat.format(new Date (params[1].getMillis() - endDateMillisecondsOffset));

                getJourneysJSONObj.put("LoadTelemetry", true);
                getJourneysJSONObj.put("OffsetTime", true);

                // Parameters message to encrypt (EndDate, IMEI, LoadTelemetry, OffsetTime, StartDate, userid, UserID)
                String message = signatureEndDate + mIMEI + "True" + "True" +
                        signatureStartDate + String.valueOf(mActiveUser.getUserID()) +
                        String.valueOf(mActiveUser.getUserID());
                // Get the signature value
                String signature = Functions.getHMACKey(message, preferences.getString("secretkey", ""));

                HttpPost httpPost = new HttpPost (APIUrls.APIRootUrl + APIUrls.GetJourneys + "userid="
                        + mActiveUser.getUserID() + "&signature=" + signature);
                StringEntity se = new StringEntity(getJourneysJSONObj.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String responseString = EntityUtils.toString(httpEntity);
                JSONObject responseJSON = new JSONObject(responseString);

                if (responseJSON.getString("Success").equals("true")) {
                    JSONArray journeysJSONArray = responseJSON.getJSONArray("Journeys");
                    if (journeysJSONArray.length() > 0) {
                        JSONObject journey = journeysJSONArray.getJSONObject(0);
                        JSONArray journeyEventsJSONArray = journey.getJSONArray("JourneyEvents");
                        for (int j = 0; j < journeyEventsJSONArray.length(); j++) {
                            try {
                                JSONObject journeyEvent = journeyEventsJSONArray.getJSONObject(j);
                                DateTime eventTime = new DateTime(journeyEvent.getString("EventTime"));
                                JSONObject locationJSON = journeyEvent.getJSONObject("Location");
                                JourneyEvent event = new JourneyEvent(JourneyEvent.EventType.valueOf(journeyEvent.getString("EventType")),
                                        new Date(eventTime.getMillis()),
                                        new LatLng(locationJSON.getDouble("Latitude"), locationJSON.getDouble("Longitude")),
                                        journeyEvent.getDouble("EventSpeed"),
                                        (((int)Math.round(journeyEvent.getDouble("SpeedLimit")) + 5) / 10) * 10,
                                        Statistics.DistanceUnits.valueOf(journeyEvent.getString("SpeedUnits")),
                                        journeyEvent.getString("StreetAddress"),
                                        journeyEvent.getInt("Bearing"));
                                mJourneyEvents.add(event);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    } else {
                        httpClient.close();
                        return false;
                    }
                } else {
                    httpClient.close();
                    return false;
                }
            } catch (MalformedURLException | UnsupportedEncodingException | JSONException | NullPointerException ex) {
                httpClient.close();
                return false;
            } catch (IOException ex1) {
                httpClient.close();
                return false;
            }
            httpClient.close();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (mMap != null && success) {
                mMap.setMyLocationEnabled(true);
                UiSettings settings = mMap.getUiSettings();
                settings.setZoomControlsEnabled(true);
//                    settings.setAllGesturesEnabled(false);
                mGetDirections = new GetDirections();
                mGetDirections.execute();

                LatLng southwestPoint = null, northeastPoint = null;

                for (int i = 0; i < mJourneyEvents.size(); i++) {
                    if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.JOURNEY_START_EVENT) {
                        mAllMarkers.add (mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                                .title(getResources().getString(R.string.journey_start_event))
                                .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconStart, mJourneyStartEndIconSize, mJourneyStartEndIconSize, false)))));
                        southwestPoint = mJourneyEvents.get(i).getLatLng();
                        continue;
                    } else if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.JOURNEY_END_EVENT) {
                        mAllMarkers.add(mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                                .title(getResources().getString(R.string.journey_end_event))
                                .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconEnd, mJourneyStartEndIconSize, mJourneyStartEndIconSize, false)))));
                        northeastPoint = mJourneyEvents.get(i).getLatLng();
                        continue;
                    }
                    mAllMarkers.add (mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                            .title(getResources().getString(R.string.timed_event))
                            .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                            .rotation(mJourneyEvents.get(i).getBearing())));
                    if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.DECEL_THRESHOLD_EXCEEDED ||
                            mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.MAX_DECELL_EXCEEDED ||
                            mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.MID_DECELL_EXCEEDED) {
                        Marker brakingMarker = mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                                .title(getResources().getString(R.string.deccel_event))
                                .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                                .rotation(mJourneyEvents.get(i).getBearing()));
                        mBreakingMarkers.add(brakingMarker);
                        mAllMarkers.add(brakingMarker);
                    }
                    if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.ACCEL_THRESHOLD_EXCEEDED ||
                            mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.MAX_ACCEL_EXCEEDED ||
                            mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.MID_ACCEL_EXCEEDED) {
                        Marker accelMarker = mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                                .title(getResources().getString(R.string.accel_event))
                                .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                                .rotation(mJourneyEvents.get(i).getBearing()));
                        mAccelerationMarkers.add(accelMarker);
                        mAllMarkers.add(accelMarker);
                    }
                    if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.OVERSPEED_EVENT) {
                        Marker speedMarker = mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                                .title(getResources().getString(R.string.speeding_event)
                                        + "\n" + getResources().getString(R.string.speed_limit) + " " + mJourneyEvents.get(i).getSpeedLimit()
                                        + "\n" + getResources().getString(R.string.actual_speed) + " " + Math.round(mJourneyEvents.get(i).getEventSpeed()))
                                .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                                .rotation(mJourneyEvents.get(i).getBearing()));
                        mSpeedMarkers.add(speedMarker);
                        mAllMarkers.add(speedMarker);
                    }
                }
                switch (mMarkersToShow) {
                    case BREAKING:
                        addBreakingMarkers(false);
                        break;
                    case ACCELERATION:
                        addAccelerationMarkers(false);
                        break;
                    case SPEED:
                        addSpeedMarkers(false);
                        break;
                }

                double southestLat = 0.0, westestLng = 0.0, northestLat = 0.0, eastestLng = 0.0;
                // This means that either journey doesn't have the JOURNEY_START_EVENT or JOURNEY_END_EVENT
                if (southwestPoint == null) {
                    JourneyEvent startEvent = Functions.getEarliestEvent(mJourneyEvents);
                    southwestPoint = startEvent.getLatLng();
                }
                if (northeastPoint == null) {
                    JourneyEvent endEvent = Functions.getLatestEvent(mJourneyEvents);
                    northeastPoint = endEvent.getLatLng();
                }
                if (southwestPoint.latitude > northeastPoint.latitude) {
                    southestLat = northeastPoint.latitude;
                    northestLat = southwestPoint.latitude;
                } else if (southwestPoint.latitude <= northeastPoint.latitude) {
                    southestLat = southwestPoint.latitude;
                    northestLat = northeastPoint.latitude;
                }
                if (southwestPoint.longitude > northeastPoint.longitude) {
                    westestLng = northeastPoint.longitude;
                    eastestLng = southwestPoint.longitude;
                } else if (southwestPoint.longitude <= northeastPoint.longitude) {
                    westestLng = southwestPoint.longitude;
                    eastestLng = northeastPoint.longitude;
                }
                southwestPoint = new LatLng(southestLat, westestLng);
                northeastPoint = new LatLng(northestLat, eastestLng);
                mJourneyBounds = new LatLngBounds(southwestPoint, northeastPoint);
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(mJourneyBounds, 500, 500, 100));
                mMap.setInfoWindowAdapter(new MarkerTitleAdapter(getActivity().getLayoutInflater(), getActivity()));
            } else if (!success) {
                Toast.makeText(getActivity(), getResources().getString(R.string.api_no_journey_events_found), Toast.LENGTH_LONG).show();
            }
        }
    }

    private class GetDirections extends AsyncTask<Void, PolylineOptions, Boolean> {
        private List<PolylineOptions> mPaths = new ArrayList<>();
        private List<Polyline> mPolyLines = new ArrayList<>();

        @Override
        protected Boolean doInBackground(Void... params) {
            LatLng startPoint = new LatLng(mJourneyEvents.get(0).getLatLng().latitude, mJourneyEvents.get(0).getLatLng().longitude),
                    endPoint = new LatLng(mJourneyEvents.get(1).getLatLng().latitude, mJourneyEvents.get(1).getLatLng().longitude);
            for (int i = 1; i < mJourneyEvents.size() - 1; i++) {
                double distance = pixelDistance(startPoint.latitude, startPoint.longitude, endPoint.latitude, endPoint.longitude, 2);
                // If next one is the last event - force it to draw the line
                if (i + 2 == mJourneyEvents.size()) {
                    distance = 20.0;
                    endPoint = new LatLng(mJourneyEvents.get(i + 1).getLatLng().latitude, mJourneyEvents.get(i + 1).getLatLng().longitude);
                }
                if (distance > 0.1) {
                    String url = "http://maps.googleapis.com/maps/api/directions/xml?"
                            + "origin=" + startPoint.latitude + "," + startPoint.longitude
                            + "&destination=" + endPoint.latitude + "," + endPoint.longitude
                            + "&sensor=false&units=metric&mode=driving";
                    try {
                        HttpClient httpClient = new DefaultHttpClient();
                        HttpContext localContext = new BasicHttpContext();
                        HttpPost httpPost = new HttpPost(url);
                        HttpResponse response = httpClient.execute(httpPost, localContext);
                        InputStream in = response.getEntity().getContent();
                        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        Document doc = builder.parse(in);
                        PolylineOptions path = getPolylineOptions(doc);
                        if (path != null) {
                            publishProgress(path);
                            Thread.sleep(100);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                    startPoint = new LatLng(mJourneyEvents.get(i).getLatLng().latitude, mJourneyEvents.get(i).getLatLng().longitude);
                    endPoint = new LatLng(mJourneyEvents.get(i + 1).getLatLng().latitude, mJourneyEvents.get(i + 1).getLatLng().longitude);
                } else
                    endPoint = new LatLng(mJourneyEvents.get(i + 1).getLatLng().latitude, mJourneyEvents.get(i + 1).getLatLng().longitude);
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(PolylineOptions... values) {
            super.onProgressUpdate(values);
            mPolyLines.add(mMap.addPolyline(values[0]));
        }

        private PolylineOptions getPolylineOptions (Document doc) {
            if (doc != null) {
                NodeList nl1, nl2, nl3;
                ArrayList<LatLng> listGeopoints = new ArrayList<>();
                nl1 = doc.getElementsByTagName("step");
                if (nl1.getLength() > 0) {
                    for (int i = 0; i < nl1.getLength(); i++) {
                        Node node1 = nl1.item(i);
                        nl2 = node1.getChildNodes();

                        Node locationNode = nl2.item(getNodeIndex(nl2, "start_location"));
                        nl3 = locationNode.getChildNodes();
                        Node latNode = nl3.item(getNodeIndex(nl3, "lat"));
                        double lat = Double.parseDouble(latNode.getTextContent());
                        Node lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                        double lng = Double.parseDouble(lngNode.getTextContent());
                        listGeopoints.add(new LatLng(lat, lng));

                        locationNode = nl2.item(getNodeIndex(nl2, "polyline"));
                        nl3 = locationNode.getChildNodes();
                        latNode = nl3.item(getNodeIndex(nl3, "points"));
                        ArrayList<LatLng> arr = decodePoly(latNode.getTextContent());
                        for (int j = 0; j < arr.size(); j++) {
                            listGeopoints.add(new LatLng(arr.get(j).latitude, arr.get(j).longitude));
                        }

                        locationNode = nl2.item(getNodeIndex(nl2, "end_location"));
                        nl3 = locationNode.getChildNodes();
                        latNode = nl3.item(getNodeIndex(nl3, "lat"));
                        lat = Double.parseDouble(latNode.getTextContent());
                        lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                        lng = Double.parseDouble(lngNode.getTextContent());
                        listGeopoints.add(new LatLng(lat, lng));
                    }
                }

                PolylineOptions path = new PolylineOptions().width(getResources().getDimension(R.dimen.map_stroke_width)).color(getResources().getColor(R.color.gmaps_line));
                for (int i = 0; i < listGeopoints.size(); i++) {
                    path.add(listGeopoints.get(i));
                }

                return path;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (success) {

            }
        }

        private int getNodeIndex(NodeList nl, String nodename) {
            for(int i = 0 ; i < nl.getLength() ; i++) {
                if(nl.item(i).getNodeName().equals(nodename))
                    return i;
            }
            return -1;
        }

        private ArrayList<LatLng> decodePoly(String encoded) {
            ArrayList<LatLng> poly = new ArrayList<>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;
            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;
                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
                poly.add(position);
            }
            return poly;
        }

        public double lonToX(double longitude) {
            return Math.round(offset + radius * (longitude) * (Math.PI / 180));
        }

        public double latToY(double latitude) {
            return Math.round(offset
                    - radius
                    * (Math.log(1 + Math.sin(latitude * Math.PI / 180)) / 1 - Math
                    .sin(latitude * Math.PI / 180)) / 2);
        }

        //Distance between two markers (on pixels)
        public double pixelDistance(double latitude1, double longitude1,
                                    double latitude2, double longitude2, int zoom) {
            double x1 = lonToX(longitude1);
            double y1 = latToY(latitude1);

            double x2 = lonToX(longitude2);
            double y2 = latToY(latitude2);

            double pow1 = Math.pow((x1 - x2), 2);
            double pow2 = Math.pow((y1 - y2), 2);

            double x = Math.sqrt(pow1 + pow2);
            double y = 20 - zoom;
            return (x / (Math.pow(2, y)));
        }
    }

    private void setTextViewsTypefaces () {
        mDriverScore.setTypeface(mFont);
        mJourneyDate.setTypeface(mFont);
        mJourneyStartTime.setTypeface(mFont);
        mJourneyEndTime.setTypeface(mFont);
        mJourneyStartAddressLine.setTypeface(mFont);
        mJourneyStartPostCode.setTypeface(mFont);
        mJourneyDistanceTravelled.setTypeface(mFont);
        mJourneyEndAddressLine.setTypeface(mFont);
        mJourneyEndPostCode.setTypeface(mFont);
        mJourneyTravelTime.setTypeface(mFont);
        mJourneyBrakingScore.setTypeface(mFont);
        mJourneyAccelerationScore.setTypeface(mFont);
        mJourneySpeedScore.setTypeface(mFont);
    }

    private void setJourneyValues () {
        DateTime.Property dayOfWeek = mJourney.getStartTime().dayOfWeek();
        DecimalFormat format = new DecimalFormat("00");
        DateTime startTime = new DateTime(mJourney.getStartTime().getMillis()
                - (Functions.getGMTOffset(mJourney.getStartTime()) * 3600000));
        DateTime endTime = new DateTime(mJourney.getFinishTime().getMillis()
                - (Functions.getGMTOffset(mJourney.getFinishTime()) * 3600000));

        mDriverScore.setText(String.valueOf(mJourney.getRiskScore()));
        mJourneyDate.setText(dayOfWeek.getAsText(Locale.getDefault()).toUpperCase() + " " +
                format.format(mJourney.getStartTime().getDayOfMonth()) + "/" +
                format.format(mJourney.getStartTime().getMonthOfYear()));
        mJourneyStartTime.setText(format.format(startTime.getHourOfDay()) + ":" +
                format.format(startTime.getMinuteOfHour()));
        mJourneyEndTime.setText(format.format(endTime.getHourOfDay()) + ":" +
                format.format(endTime.getMinuteOfHour()));
        mJourneyStartAddressLine.setText(mJourney.getStartAddress().split(",")[0]
                + "\n" + mJourney.getStartAddress().split(",")[1]);
        mJourneyStartPostCode.setText(mJourney.getStartAddress().split(",")[2]);
        mJourneyEndAddressLine.setText(mJourney.getEndAddress().split(",")[0]
                + "\n" + mJourney.getEndAddress().split(",")[1]);
        mJourneyEndPostCode.setText(mJourney.getEndAddress().split(",")[2]);
        mJourneyDistanceTravelled.setText(String.valueOf((double) Math.round(mJourney.getDistanceTravelled() * 10) / 10)
                + mJourney.getDistanceUnits().toString().toUpperCase());
        // Get minutes of the hour:
        long minutes = mJourney.getJourneyDuration().getStandardMinutes() - (60 * mJourney.getJourneyDuration().getStandardHours());
        mJourneyTravelTime.setText(format.format(mJourney.getJourneyDuration().getStandardHours())
                + ":" + format.format(minutes));
        mJourneyBrakingScore.setText(String.valueOf(mJourney.getDecelScore()));
        mJourneyAccelerationScore.setText(String.valueOf(mJourney.getAccelScore()));
        mJourneySpeedScore.setText(String.valueOf(mJourney.getSpeedScore()));
        if (mJourney.getRiskScore() >= GlobalValues.GoodScore) {
            mDriverScore.setBackgroundResource(R.drawable.good_score_ring_button);
        } else if (mJourney.getRiskScore() <= GlobalValues.MediumScoreMax
                && mJourney.getRiskScore() >= GlobalValues.MediumScoreMin) {
            mDriverScore.setBackgroundResource(R.drawable.medium_score_ring_button);
        } else if (mJourney.getRiskScore() <= GlobalValues.BadScore) {
            mDriverScore.setBackgroundResource(R.drawable.bad_score_ring_button);
        }
        if (mJourney.getDecelScore() >= GlobalValues.GoodScore) {
            mJourneyBrakingScore.setBackgroundResource(R.drawable.good_score_small_ring);
        } else if (mJourney.getDecelScore() <= GlobalValues.MediumScoreMax
                && mJourney.getDecelScore() >= GlobalValues.MediumScoreMin) {
            mJourneyBrakingScore.setBackgroundResource(R.drawable.medium_score_small_ring);
        } else if (mJourney.getDecelScore() <= GlobalValues.BadScore) {
            mJourneyBrakingScore.setBackgroundResource(R.drawable.bad_score_small_ring);
        }
        if (mJourney.getAccelScore() >= GlobalValues.GoodScore) {
            mJourneyAccelerationScore.setBackgroundResource(R.drawable.good_score_small_ring);
        } else if (mJourney.getAccelScore() <= GlobalValues.MediumScoreMax
                && mJourney.getAccelScore() >= GlobalValues.MediumScoreMin) {
            mJourneyAccelerationScore.setBackgroundResource(R.drawable.medium_score_small_ring);
        } else if (mJourney.getAccelScore() <= GlobalValues.BadScore) {
            mJourneyAccelerationScore.setBackgroundResource(R.drawable.bad_score_small_ring);
        }
        if (mJourney.getSpeedScore() >= GlobalValues.GoodScore) {
            mJourneySpeedScore.setBackgroundResource(R.drawable.good_score_small_ring);
        } else if (mJourney.getSpeedScore() <= GlobalValues.MediumScoreMax
                && mJourney.getSpeedScore() >= GlobalValues.MediumScoreMin) {
            mJourneySpeedScore.setBackgroundResource(R.drawable.medium_score_small_ring);
        } else if (mJourney.getSpeedScore() <= GlobalValues.BadScore) {
            mJourneySpeedScore.setBackgroundResource(R.drawable.bad_score_small_ring);
        }
    }

    public void animateMarker(Marker marker, LatLng point){
        final Marker mMarker = marker;
        final LatLng mPoint = point;
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(point);
        startPoint.offset(0, -150);
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1500;
        final Interpolator interpolator = new BounceInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * mPoint.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * mPoint.latitude + (1 - t) * startLatLng.latitude;
                mMarker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    private void clearMarkersFromMap (boolean includeStartAndFinish) {
        if (includeStartAndFinish)
            for (int i = 0; i < mAllMarkers.size(); i++)
                mAllMarkers.get(i).remove();
        else
            for (int i = 1; i < mAllMarkers.size() - 1; i++)
                mAllMarkers.get(i).remove();
        for (int i = 0; i < mBreakingMarkers.size(); i++)
            mBreakingMarkers.get(i).remove();

        for (int i = 0; i < mAccelerationMarkers.size(); i++)
            mAccelerationMarkers.get(i).remove();

        for (int i = 0; i < mSpeedMarkers.size(); i++)
            mSpeedMarkers.get(i).remove();

        for (int i = 0; i < mHighlightedMarkers.size(); i++)
            mHighlightedMarkers.get(i).remove();

        mHighlightedMarkers.clear();
    }

    private void clearMarkers () {
        mAllMarkers.clear();
        mBreakingMarkers.clear();
        mAccelerationMarkers.clear();
        mSpeedMarkers.clear();
    }

    private void addAllMarkers () {
        for (int i = 0; i < mJourneyEvents.size(); i++) {
            if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.JOURNEY_START_EVENT) {
                mAllMarkers.add(mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                        .title(getResources().getString(R.string.journey_start_event))
                        .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconStart, mJourneyStartEndIconSize, mJourneyStartEndIconSize, false)))));
            } else if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.JOURNEY_END_EVENT) {
                mAllMarkers.add(mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                        .title(getResources().getString(R.string.journey_end_event))
                        .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconEnd, mJourneyStartEndIconSize, mJourneyStartEndIconSize, false)))));
            } else if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.DECEL_THRESHOLD_EXCEEDED ||
                    mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.MAX_DECELL_EXCEEDED ||
                    mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.MID_DECELL_EXCEEDED) {
                Marker brakingMarker = mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                        .title(getResources().getString(R.string.deccel_event))
                        .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                        .rotation(mJourneyEvents.get(i).getBearing()));
                mBreakingMarkers.add(brakingMarker);
                mAllMarkers.add(brakingMarker);
            } else if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.ACCEL_THRESHOLD_EXCEEDED ||
                    mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.MAX_ACCEL_EXCEEDED ||
                    mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.MID_ACCEL_EXCEEDED) {
                Marker accelMarker = mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                        .title(getResources().getString(R.string.accel_event))
                        .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                        .rotation(mJourneyEvents.get(i).getBearing()));
                mAccelerationMarkers.add(accelMarker);
                mAllMarkers.add(accelMarker);
            } else if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.OVERSPEED_EVENT) {
                Marker speedMarker = mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                        .title(getResources().getString(R.string.speeding_event)
                                + "\n" + getResources().getString(R.string.speed_limit) + " " + mJourneyEvents.get(i).getSpeedLimit()
                                + "\n" + getResources().getString(R.string.actual_speed) + " " + Math.round(mJourneyEvents.get(i).getEventSpeed()))
                        .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                        .rotation(mJourneyEvents.get(i).getBearing()));
                mSpeedMarkers.add(speedMarker);
                mAllMarkers.add(speedMarker);
            } else if (mJourneyEvents.get(i).getEventType() == JourneyEvent.EventType.TIMED_EVENT) {
                mAllMarkers.add (mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                        .title(getResources().getString(R.string.timed_event))
                        .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                        .rotation(mJourneyEvents.get(i).getBearing())));
            } else {
                mAllMarkers.add(mMap.addMarker(new MarkerOptions().position(mJourneyEvents.get(i).getLatLng())
                        .title(getResources().getString(R.string.default_event))
                        .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(mIconSmallEvent, mEventIconSize, mEventIconSize, false)))
                        .rotation(mJourneyEvents.get(i).getBearing())));
            }
        }
    }
}
