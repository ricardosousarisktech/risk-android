package com.risktechnology.riskmobileapp;

import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.risktechnology.model.ApiResponse;
import com.risktechnology.model.GeneralStatistics;
import com.risktechnology.model.Statistics;
import com.risktechnology.model.User;
import com.risktechnology.utils.APIUrls;
import com.risktechnology.utils.Functions;
import com.risktechnology.utils.GlobalValues;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by ricardo on 14/04/2015.
 */
public class StatisticsFragmentActivity extends FragmentActivity {

    ActionBar mActionBar;
    View mActionBarView, mVehiclePickerDialogView;
    private Button mLogout, mAuxButton, mPickerConfirmButton;
    private TextView mScreenTitle;
    AlertDialog mVehiclePickerAlertDialog;
    private NumberPicker mVehicleRegPicker;

    private Typeface mFont, mLightFont, mMediumFont;
//    private GetStatistics mGetStatisticsTask;
    private User mActiveUser;
    private SharedPreferences mUserPreferences;

    private List<Statistics> mDriverStatistics;
    private int mCurrentDisplayedChild;
    private HomeActivity.HomeFragment.DataType mCurrentSelectedType;

    private TextView mOverallScore, mOverallScoreDesc, mSpeedScore, mSpeedScoreDesc, mAccelScore, mAccelScoreDesc, mDecelScore, mDecelScoreDesc;
    private ViewFlipper mStatisticsViewFlipper;
    private RelativeLayout mOverallScoreButton, mSpeedScoreButton, mAccelScoreButton, mDecelScoreButton;
    private ProgressBar mStatisticsProgressBar;

    private GestureDetector mGestureDetector;

    private List<GeneralStatistics> mGeneralStatisticsList;
    private int mStatisticsListSize = 0;

    private Thread mLoadingNextViewThread, mLoadingPreviousViewThread;
    private boolean mStopAnimation = false;

    public StatisticsFragmentActivity(){}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_statistics);

        mActionBar = getActionBar();
        mActionBarView = getLayoutInflater().inflate(R.layout.action_bar_layout, null);
        mLogout = (Button)mActionBarView.findViewById(R.id.logout_button);
        mAuxButton = (Button)mActionBarView.findViewById(R.id.aux_button);
        mScreenTitle = (TextView)mActionBarView.findViewById(R.id.screen_title);

        mFont = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.selected_font));
        mLightFont = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.selected_font_extra_light));
        mMediumFont = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.selected_font_medium));

        TabHost tabs=(TabHost)findViewById(R.id.tabHost);
        tabs.setup();
        TabHost.TabSpec spec=tabs.newTabSpec("tag1");

        View tabHostView1 = getLayoutInflater().inflate(R.layout.tab_host_indicator_view, null);
        TextView title1 = (TextView)tabHostView1.findViewById(R.id.tabNameText);
        title1.setTypeface(mMediumFont);
        title1.setText("DAY");
        spec.setContent(R.id.tab1);
        spec.setIndicator("DAY");
        tabs.addTab(spec);

        spec=tabs.newTabSpec("tag2");
        spec.setContent(R.id.tab2);
        View tabHostView2 = getLayoutInflater().inflate(R.layout.tab_host_indicator_view, null);
        TextView title2 = (TextView)tabHostView2.findViewById(R.id.tabNameText);
        title2.setTypeface(mMediumFont);
        title2.setText("WEEK");
        spec.setIndicator("WEEK");
        tabs.addTab(spec);

        spec=tabs.newTabSpec("tag3");
        spec.setContent(R.id.tab3);
        View tabHostView3 = getLayoutInflater().inflate(R.layout.tab_host_indicator_view, null);
        TextView title3 = (TextView)tabHostView3.findViewById(R.id.tabNameText);
        title3.setTypeface(mMediumFont);
        title3.setText("MONTH");
        spec.setIndicator("MONTH");
        tabs.addTab(spec);

        for (int i=0; i<tabs.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView)tabs.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.font_white_color));
            tv.setTypeface(mLightFont);
            tv.setTextSize(getResources().getDimension(R.dimen.small_text_size));
        }

        mOverallScore = (TextView)findViewById(R.id.statisticsOverallScore);
        mOverallScoreDesc = (TextView)findViewById(R.id.statisticsOverallScoreDesc);
        mSpeedScore = (TextView)findViewById(R.id.statisticsSpeedScore);
        mSpeedScoreDesc = (TextView)findViewById(R.id.statisticsSpeedScoreDesc);
        mAccelScore = (TextView)findViewById(R.id.statisticsAccelScore);
        mAccelScoreDesc = (TextView)findViewById(R.id.statisticsAccelScoreDesc);
        mDecelScore = (TextView)findViewById(R.id.statisticsDecelScore);
        mDecelScoreDesc = (TextView)findViewById(R.id.statisticsDecelScoreDesc);
        mOverallScoreButton = (RelativeLayout)findViewById(R.id.statisticsOverallScoreButton);
        mSpeedScoreButton = (RelativeLayout)findViewById(R.id.statisticsSpeedScoreButton);
        mAccelScoreButton = (RelativeLayout)findViewById(R.id.statisticsAccelScoreButton);
        mDecelScoreButton = (RelativeLayout)findViewById(R.id.statisticsDecelScoreButton);

        mVehiclePickerDialogView = getLayoutInflater().inflate(R.layout.vehicle_picker_dialog, null);
        mPickerConfirmButton = (Button) mVehiclePickerDialogView.findViewById(R.id.licence_plate_confirm_button);
        mVehicleRegPicker = (NumberPicker) mVehiclePickerDialogView.findViewById(R.id.licence_plate_picker);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        mVehiclePickerAlertDialog = builder.create();
        mVehiclePickerAlertDialog.setCancelable(true);
        mVehiclePickerAlertDialog.setView(mVehiclePickerDialogView);

        mPickerConfirmButton.setTypeface(mFont);
        mScreenTitle.setTypeface(mFont);
        mOverallScore.setTypeface(mMediumFont);
        mOverallScoreDesc.setTypeface(mLightFont);
        mSpeedScore.setTypeface(mMediumFont);
        mSpeedScoreDesc.setTypeface(mLightFont);
        mAccelScore.setTypeface(mMediumFont);
        mAccelScoreDesc.setTypeface(mLightFont);
        mDecelScore.setTypeface(mMediumFont);
        mDecelScoreDesc.setTypeface(mLightFont);

        mOverallScoreButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mOverallScore.setTextColor(getResources().getColor(R.color.font_white_color));
                mOverallScoreDesc.setTextColor(getResources().getColor(R.color.font_white_color));
                setGreyFonts(0);
                mOverallScoreButton.setPressed(true);
                mSpeedScoreButton.setPressed(false);
                mAccelScoreButton.setPressed(false);
                mDecelScoreButton.setPressed(false);
                return true;
            }
        });
        mSpeedScoreButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mSpeedScore.setTextColor(getResources().getColor(R.color.font_white_color));
                mSpeedScoreDesc.setTextColor(getResources().getColor(R.color.font_white_color));
                setGreyFonts(1);
                mOverallScoreButton.setPressed(false);
                mSpeedScoreButton.setPressed(true);
                mAccelScoreButton.setPressed(false);
                mDecelScoreButton.setPressed(false);
                return true;
            }
        });
        mAccelScoreButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mAccelScore.setTextColor(getResources().getColor(R.color.font_white_color));
                mAccelScoreDesc.setTextColor(getResources().getColor(R.color.font_white_color));
                setGreyFonts(2);
                mOverallScoreButton.setPressed(false);
                mSpeedScoreButton.setPressed(false);
                mAccelScoreButton.setPressed(true);
                mDecelScoreButton.setPressed(false);
                return true;
            }
        });
        mDecelScoreButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mDecelScore.setTextColor(getResources().getColor(R.color.font_white_color));
                mDecelScoreDesc.setTextColor(getResources().getColor(R.color.font_white_color));
                setGreyFonts(3);
                mOverallScoreButton.setPressed(false);
                mSpeedScoreButton.setPressed(false);
                mAccelScoreButton.setPressed(false);
                mDecelScoreButton.setPressed(true);
                return true;
            }
        });

        mScreenTitle.setText(((User)getIntent().getParcelableExtra("ActiveUser")).getSelectedVehicleReg());
        mActionBar.setCustomView(mActionBarView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setGreyFonts (int except) {
        switch (except) {
            case 0:
                mSpeedScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mSpeedScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mAccelScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mAccelScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mDecelScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mDecelScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                break;
            case 1:
                mOverallScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mOverallScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mAccelScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mAccelScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mDecelScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mDecelScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                break;
            case 2:
                mOverallScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mOverallScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mSpeedScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mSpeedScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mDecelScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mDecelScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                break;
            case 3:
                mOverallScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mOverallScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mSpeedScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mSpeedScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mAccelScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mAccelScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                break;
            default:
                mOverallScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mOverallScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mSpeedScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mSpeedScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mAccelScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mAccelScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mDecelScore.setTextColor(getResources().getColor(R.color.font_greyish_color));
                mDecelScoreDesc.setTextColor(getResources().getColor(R.color.font_greyish_color));
                break;
        }

    }
}
