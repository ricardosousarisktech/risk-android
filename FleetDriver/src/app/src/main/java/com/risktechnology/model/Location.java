package com.risktechnology.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ricardo on 01/04/2015.
 */
public class Location implements Parcelable {
    private String mIMEI;
    private LatLng mLatLng;
    private boolean mStationary;
    private int mBearing;

    public Location() {}

    public Location(String mIMEI, LatLng mLatLng, boolean mStationary, int mBearing) {
        this.mIMEI = mIMEI;
        this.mLatLng = mLatLng;
        this.mStationary = mStationary;
        this.mBearing = mBearing;
    }

    public String getIMEI() {
        return mIMEI;
    }

    public void setIMEI(String mIMEI) {
        this.mIMEI = mIMEI;
    }

    public LatLng getLatLng() {
        return mLatLng;
    }

    public void setLatLng(LatLng mLatLng) {
        this.mLatLng = mLatLng;
    }

    public boolean isStationary () {
        return mStationary;
    }

    public void setStationary (boolean mStationary) {
        this.mStationary = mStationary;
    }

    public int getBearing () {
        return mBearing;
    }

    public void setBearing (int mBearing) {
        this.mBearing = mBearing;
    }

    protected Location(Parcel in) {
        mIMEI = in.readString();
        mLatLng = (LatLng) in.readValue(LatLng.class.getClassLoader());
        mStationary = in.readByte() != 0x00;
        mBearing = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mIMEI);
        dest.writeValue(mLatLng);
        dest.writeByte((byte) (mStationary ? 0x01 : 0x00));
        dest.writeInt(mBearing);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };
}
