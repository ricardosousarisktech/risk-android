package com.risktechnology.utils;

import com.google.android.gms.maps.model.LatLng;
import com.risktechnology.model.JourneyEvent;
import com.risktechnology.model.Location;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by rsousa on 16/03/15.
 */
public final class Functions {
    private static String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    private Functions () {}

    public static String getHMACKey (String data, String key) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec (key.getBytes(), HMAC_SHA1_ALGORITHM);
            Mac macKey = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            macKey.init(secretKey);

            byte[] finalBytes = macKey.doFinal(data.getBytes());
            return bytesToHex(finalBytes);
        } catch (NoSuchAlgorithmException e2) {
            return "";
        } catch (InvalidKeyException e3) {
            return "";
        }
    }

    private final static char[] hexArray = "0123456789abcdef".toCharArray();

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static void disableCertificateValidation () {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                }};

        // Ignore differences between given hostname and certificate hostname
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) { return true; }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
        } catch (Exception e) {}
    }

    public static int getGMTOffset (DateTime date) {
        DateTimeZone dateTimeZone = date.getZone();
        int gmtOffset = dateTimeZone.getOffset(date.getMillis());
        return gmtOffset / (60 * 60 * 1000);
    }

    public static JourneyEvent getEarliestEvent (List<JourneyEvent> events) {
        JourneyEvent earliestEvent = events.get(0);
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getEventTime().getTime() < earliestEvent.getEventTime().getTime())
                earliestEvent = events.get(i);
        }
        return earliestEvent;
    }

    public static JourneyEvent getLatestEvent (List<JourneyEvent> events) {
        JourneyEvent latestEvent = events.get(0);
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getEventTime().getTime() > latestEvent.getEventTime().getTime())
                latestEvent = events.get(i);
        }
        return latestEvent;
    }

    public static LatLng findSouthwestPoint (List<Location> locations) {
        LatLng southwestPoint = locations.get(0).getLatLng();
        double southLat = southwestPoint.latitude, westLng = southwestPoint.longitude;
        for (int i = 1; i < locations.size(); i++) {
            if (southwestPoint.latitude > locations.get(i).getLatLng().latitude)
                southLat = locations.get(i).getLatLng().latitude;
            if (southwestPoint.longitude > locations.get(i).getLatLng().longitude)
                westLng = locations.get(i).getLatLng().longitude;
        }
        southwestPoint = new LatLng(southLat, westLng);
        return southwestPoint;
    }
    public static LatLng findNortheastPoint (List<Location> locations) {
        LatLng northeastPoint = locations.get(0).getLatLng();
        double northLat = northeastPoint.latitude, eastLng = northeastPoint.longitude;
        for (int i = 1; i < locations.size(); i++) {
            if (northeastPoint.latitude < locations.get(i).getLatLng().latitude)
                northLat = locations.get(i).getLatLng().latitude;
            if (northeastPoint.longitude < locations.get(i).getLatLng().longitude)
                eastLng = locations.get(i).getLatLng().longitude;
        }
        northeastPoint = new LatLng(northLat, eastLng);
        return northeastPoint;
    }
}
