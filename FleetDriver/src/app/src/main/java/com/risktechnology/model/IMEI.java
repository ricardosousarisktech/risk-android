package com.risktechnology.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by rsousa on 20/03/15.
 */
public class IMEI implements Parcelable {
    private String mIMEI;
    private Date mFirstPolicyStartDate;
    private String mVehicleReg;
    private double mLatitude;
    private double mLongitude;

    public IMEI(String mIMEI, Date mFirstPolicyStartDate, String mVehicleReg) {
        this.mIMEI = mIMEI;
        this.mFirstPolicyStartDate = mFirstPolicyStartDate;
        this.mVehicleReg = mVehicleReg;
    }

    public String getIMEI() {
        return mIMEI;
    }

    public void setIMEI(String mIMEI) {
        this.mIMEI = mIMEI;
    }

    public Date getFirstPolicyStartDate() {
        return mFirstPolicyStartDate;
    }

    public void setFirstPolicyStartDate(Date mFirstPolicyStartDate) {
        this.mFirstPolicyStartDate = mFirstPolicyStartDate;
    }

    public String getVehicleReg() {
        return mVehicleReg;
    }

    public void setVehicleReg(String mVehicleReg) {
        this.mVehicleReg = mVehicleReg;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public Date getFirstPolicyByVehicleReg (String mVehicleReg) {
        if (mVehicleReg.equals(this.mVehicleReg)) {
            return mFirstPolicyStartDate;
        }
        return null;
    }

    protected IMEI(Parcel in) {
        mIMEI = in.readString();
        long tmpMFirstPolicyStartDate = in.readLong();
        mFirstPolicyStartDate = tmpMFirstPolicyStartDate != -1 ? new Date(tmpMFirstPolicyStartDate) : null;
        mVehicleReg = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mIMEI);
        dest.writeLong(mFirstPolicyStartDate != null ? mFirstPolicyStartDate.getTime() : -1L);
        dest.writeString(mVehicleReg);
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<IMEI> CREATOR = new Parcelable.Creator<IMEI>() {
        @Override
        public IMEI createFromParcel(Parcel in) {
            return new IMEI(in);
        }

        @Override
        public IMEI[] newArray(int size) {
            return new IMEI[size];
        }
    };
}
