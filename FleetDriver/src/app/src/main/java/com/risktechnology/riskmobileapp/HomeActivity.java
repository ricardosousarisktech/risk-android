package com.risktechnology.riskmobileapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.app.Fragment;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.model.LatLng;
import com.risktechnology.adapter.FlowAdapter;
import com.risktechnology.model.ApiResponse;
import com.risktechnology.model.IMEI;
import com.risktechnology.model.Journey;
import com.risktechnology.model.JourneyEvent;
import com.risktechnology.model.Statistics;
import com.risktechnology.model.User;
import com.risktechnology.utils.APIUrls;
import com.risktechnology.utils.ArcUtils;
import com.risktechnology.utils.Functions;
import com.risktechnology.utils.GlobalValues;
import com.risktechnology.widget.Coverflow;
import com.risktechnology.widget.Needle;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Months;
import org.joda.time.Weeks;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class HomeActivity extends Activity {
    Typeface mFont;

    private Button mLogout, mAuxButton;
    private TextView mScreenTitle;
    AlertDialog mVehiclePickerAlertDialog;
    private NumberPicker mVehicleRegPicker;
    Button mPickerConfirmButton;
    SharedPreferences mUserPreferences;
    User mActiveUser;
    Activity mThisActivity;
    HomeFragment mHomeFragment;

    public static int mDisplayedViewOnViewFlipper;

    ActionBar mActionBar;
    View mActionBarView, mVehiclePickerDialogView;

    RelativeLayout mProgressActivity;

    String [] mVehicleRegArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mFont = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.selected_font));
        mThisActivity = this;
        if (savedInstanceState == null) {
            mUserPreferences = PreferenceManager.getDefaultSharedPreferences(this);

            mActionBar = getActionBar();
            mActionBarView = getLayoutInflater().inflate(R.layout.action_bar_layout, null);
            mLogout = (Button)mActionBarView.findViewById(R.id.logout_button);
            mAuxButton = (Button)mActionBarView.findViewById(R.id.aux_button);
            mScreenTitle = (TextView)mActionBarView.findViewById(R.id.screen_title);
            mProgressActivity = (RelativeLayout)findViewById(R.id.home_activity_progress);

            mVehiclePickerDialogView = getLayoutInflater().inflate(R.layout.vehicle_picker_dialog, null);
            mPickerConfirmButton = (Button) mVehiclePickerDialogView.findViewById(R.id.licence_plate_confirm_button);
            mVehicleRegPicker = (NumberPicker) mVehiclePickerDialogView.findViewById(R.id.licence_plate_picker);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            mVehiclePickerAlertDialog = builder.create();
            mVehiclePickerAlertDialog.setCancelable(true);
            mVehiclePickerAlertDialog.setView(mVehiclePickerDialogView);

            mPickerConfirmButton.setTypeface(mFont);

            mScreenTitle.setTypeface(mFont);

            Bundle bundle = this.getIntent().getExtras();
            if (bundle != null) {
                mActiveUser = bundle.getParcelable("activeuser");
                setActivityConfigurations();
                launchHomeFragment (false);
                mProgressActivity.setVisibility(View.GONE);
            } else {
                GetActiveUserInfo getActiveUserInfo = new GetActiveUserInfo();
                getActiveUserInfo.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.logging_out), Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
                // Clears this user's preferences
                mUserPreferences.edit().clear().commit();
            }
        });

        Fragment currentFragment = getFragmentManager().findFragmentByTag("location");
        if (currentFragment != null)
            refreshLocationButton();
        else
            setLocationButtonProperties();

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment currFragment = getFragmentManager().findFragmentByTag("home");
                if (currFragment != null) {
                    if (currFragment.isResumed() && currFragment.isVisible())
                        mLogout.setVisibility(View.VISIBLE);
                    else
                        mLogout.setVisibility(View.GONE);
                } else
                    mLogout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getFragmentManager().findFragmentByTag("home");
        if (currentFragment != null)
            if (!currentFragment.isVisible() && !currentFragment.isResumed()) {
                // If the current fragment isn't the HomeScreen - standard back button action
                super.onBackPressed();
            } else if (currentFragment.isVisible() && currentFragment.isResumed()) {
                // else close the app
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        currentFragment = getFragmentManager().findFragmentByTag("location");
        if (currentFragment != null)
            if (currentFragment.isVisible() && currentFragment.isResumed()) {
                getFragmentManager().popBackStackImmediate();
                mAuxButton.setVisibility(View.VISIBLE);
                return;
            }
        currentFragment = getFragmentManager().findFragmentByTag("journeymap");
        if (currentFragment != null)
            if (currentFragment.isVisible() && currentFragment.isResumed()) {
                getFragmentManager().popBackStackImmediate();
            }
    }

    public User getActiveUser () {
        return mActiveUser;
    }

    private void setVehicleRegPickerTextStyle () {
        for (int i = 0; i < mVehicleRegPicker.getChildCount(); i++) {
            View child = mVehicleRegPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = mVehicleRegPicker.getClass().getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((EditText)child).setTypeface(mFont);
                    ((EditText)child).setTextColor(getResources().getColor(android.R.color.white));
                    ((EditText)child).setTextSize(getResources().getDimension(R.dimen.small_text_size));

                    ((Paint)selectorWheelPaintField.get(mVehicleRegPicker)).setTypeface(mFont);
                    ((Paint)selectorWheelPaintField.get(mVehicleRegPicker)).setColor(getResources().getColor(android.R.color.white));
                    ((Paint)selectorWheelPaintField.get(mVehicleRegPicker)).setTextSize(getResources().getDimension(R.dimen.regular_text_size));
                    mVehicleRegPicker.invalidate();
                    return;
                } catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException ex) {
                    System.out.println (ex.getMessage());
                }
            }
        }
    }
    public void addVehiclesToPickerOption () {
        mVehicleRegArray = new String [mActiveUser.getIMEIList().size()];
        for (int i = 0; i < mActiveUser.getIMEIList().size(); i++) {
            mVehicleRegArray[i] = mActiveUser.getIMEIList().get(i).getVehicleReg();
        }
        mVehicleRegPicker.setMinValue(0);
        mVehicleRegPicker.setMaxValue(mVehicleRegArray.length - 1);
        mVehicleRegPicker.setDisplayedValues(mVehicleRegArray);
        mVehicleRegPicker.setValue(0);
        setVehicleRegPickerTextStyle();
        mVehiclePickerAlertDialog.setView(mVehiclePickerDialogView);
    }
    public int getVehicleRegPickerValue () {
        return mVehicleRegPicker.getValue();
    }

    public void setScreenTitle (String title) {
        mScreenTitle.setText(title);
    }

    public String getScreenTitle () {
        return mScreenTitle.getText().toString();
    }

    public void setLocationButtonProperties () {
        mAuxButton.setBackgroundResource(R.drawable.ic_action_location_button);
        mAuxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment currFragment = getFragmentManager().findFragmentByTag("location");
                if (mActiveUser.getIMEIList().size() > 0 && currFragment == null) {
                    WheresMyCarFragment wmcf = new WheresMyCarFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, wmcf, "location");
                    transaction.addToBackStack("location");
                    transaction.commit();
                }
            }
        });
    }

    public void refreshLocationButton () {
        mAuxButton.setBackgroundResource(R.drawable.ic_action_refresh_button);
        mAuxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuxButton.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotating_button));
                // If on the where's my car screen delegate the click listener
                Fragment currentFragment = getFragmentManager().findFragmentByTag("location");
                if (currentFragment != null)
                    if (currentFragment.isResumed() && currentFragment.isVisible()) {
                        ((WheresMyCarFragment)currentFragment).refreshLocation(false);
                    }
            }
        });
    }

    private void setActivityConfigurations () {
        setScreenTitle(mActiveUser.getIMEIList().get(0).getVehicleReg());
        mActiveUser.setSelectedVehicleReg(mActiveUser.getIMEIList().get(0).getVehicleReg());

        mScreenTitle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // If the current fragment is neither Journeys or JourneyDetail don't do anything:
                Fragment currentFragment = getFragmentManager().findFragmentByTag("journeys");
                if (currentFragment != null)
                    if (currentFragment.isResumed() && currentFragment.isVisible())
                        return false;
                currentFragment = getFragmentManager().findFragmentByTag("journeymap");
                if (currentFragment != null)
                    if (currentFragment.isResumed() && currentFragment.isVisible())
                        return false;

                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    mScreenTitle.setAlpha(0.6f);
                else if (event.getAction() == MotionEvent.ACTION_UP)
                    mScreenTitle.setAlpha(1f);

                return false;
            }
        });

        mScreenTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // If the current fragment is neither Journeys or JourneyDetail don't do anything:
                Fragment currentFragment = getFragmentManager().findFragmentByTag("journeys");
                if (currentFragment != null)
                    if (currentFragment.isResumed() && currentFragment.isVisible())
                        return;
                currentFragment = getFragmentManager().findFragmentByTag("journeymap");
                if (currentFragment != null)
                    if (currentFragment.isResumed() && currentFragment.isVisible())
                        return;
                // If on the where's my car screen delegate the click listener
                currentFragment = getFragmentManager().findFragmentByTag("location");
                if (currentFragment != null)
                    if (currentFragment.isResumed() && currentFragment.isVisible()) {
                        ((WheresMyCarFragment)currentFragment).screenTitleClick();
                        return;
                    }

                addVehiclesToPickerOption();

                mVehiclePickerAlertDialog.show();

                mPickerConfirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Only change if a different vehicle was selected:
                        if (!mActiveUser.getSelectedVehicleReg().equals(mVehicleRegArray[mVehicleRegPicker.getValue()])) {
                            mScreenTitle.setText(mVehicleRegArray[mVehicleRegPicker.getValue()]);
                            mActiveUser.setSelectedVehicleReg(mVehicleRegArray[mVehicleRegPicker.getValue()]);
                            mHomeFragment.setSelectedIMEI(mActiveUser.getImeiByVehicleReg(mVehicleRegArray[mVehicleRegPicker.getValue()]));
                            mHomeFragment.generateCalendarData (HomeFragment.DataType.WEEK);
                            mHomeFragment.generateCalendarData (HomeFragment.DataType.MONTH);
                            mHomeFragment.clearSpeedometer();
                            switch (mHomeFragment.getCurrentDataTypeSelected ()) {
                                case WEEK:
                                    mHomeFragment.addViewFlipperViews(HomeFragment.DataType.WEEK);
                                    mHomeFragment.getWeeklyScoreByVehicle(mVehicleRegArray[mVehicleRegPicker.getValue()]);
                                    break;
                                case MONTH:
                                    mHomeFragment.addViewFlipperViews(HomeFragment.DataType.MONTH);
                                    mHomeFragment.getMonthlyScoreByVehicle(mVehicleRegArray[mVehicleRegPicker.getValue()]);
                                    break;
                            }
//                            mHomeFragment.getMPG(mVehicleRegArray[mVehicleRegPicker.getValue()]);

                            try {
                                // Take action if user is using Statistics, Fuel Savings or Where's my car
                                // TODO: See if needs to be changed for Statistics
                                Fragment currentFragment = getFragmentManager().findFragmentByTag("savings");
                                if (currentFragment != null)
                                    if (currentFragment.isResumed() && currentFragment.isVisible())
                                        ((FuelSavingsFragment) currentFragment).reloadValues();
                                currentFragment = getFragmentManager().findFragmentByTag("location");
                                if (currentFragment != null)
                                    if (currentFragment.isResumed() && currentFragment.isVisible())
                                        ((WheresMyCarFragment) currentFragment).refreshLocation(true);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        mVehiclePickerAlertDialog.dismiss();
                    }
                });
            }
        });

        mActionBar.setCustomView(mActionBarView);
        mActionBar.setDisplayShowCustomEnabled(true);

        mDisplayedViewOnViewFlipper = 0;
    }

    private void launchHomeFragment (boolean putExtras) {
        mHomeFragment = new HomeFragment();
        if (putExtras) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("activeuser", mActiveUser);
            mHomeFragment.setArguments(bundle);
        }
        getFragmentManager().beginTransaction()
                .add(R.id.container, mHomeFragment, "home")
                .addToBackStack("home")
                .commit();
    }

    private class GetActiveUserInfo extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected Boolean doInBackground(Void... params) {
            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Login");

            try {
                URL loginURL = new URL(APIUrls.APIRootUrl + APIUrls.UserLogin);
                HttpHost host = new HttpHost (loginURL.getHost(), loginURL.getPort(), loginURL.getProtocol());

                List<NameValuePair> parameters = new ArrayList<>(3);
                parameters.add(new BasicNameValuePair("Username", mUserPreferences.getString("email", "")));
                parameters.add(new BasicNameValuePair("Password", mUserPreferences.getString("password", "")));
                parameters.add(new BasicNameValuePair("AppID", getResources().getString(R.string.app_id)));

                HttpPost post = new HttpPost (APIUrls.APIRootUrl + APIUrls.UserLogin);
                post.setEntity(new UrlEncodedFormEntity(parameters));

                InetAddress ipAddress = InetAddress.getByName(host.getHostName());
                // Debug problems:
//                try {
//                    File debugs = new File(Environment.getExternalStorageDirectory(), "Debugs");
//                    if (!debugs.exists()) {
//                        debugs.mkdirs();
//                    }
//                    File debug = new File(debugs, "Debug.txt");
//                    FileWriter writer = new FileWriter(debug, true);
//                    writer.append("\n" + ipAddress.toString());
//                    writer.flush();
//                    writer.close();
//                } catch (IOException exception) {
//                    exception.printStackTrace();
//                }

                HttpResponse response = httpClient.execute(host, post);

                HttpEntity entity = response.getEntity();
                String stringResponse = EntityUtils.toString(entity);

                try {
                    JSONObject json = new JSONObject (stringResponse);
                    if (json.getString("Success").equals("true")) {
                        List<IMEI> imeiList = new ArrayList<>();
                        JSONArray imeiArray = json.getJSONArray("IMEIList");
                        for (int i = 0; i < imeiArray.length(); i++) {
                            JSONObject imeiObj = (JSONObject)imeiArray.get(i);
                            DateTime dateTime = new DateTime (imeiObj.getString("FirstPolicyStartDate"));
                            imeiList.add (new IMEI (imeiObj.getString("IMEI"), dateTime.toDate(),
                                    imeiObj.getString("VehicleReg")));
                        }
                        JSONObject userObject = json.getJSONObject("User");
                        mActiveUser = new User (Integer.parseInt(userObject.getString("UserID")),
                                Integer.parseInt(userObject.getString("Permissions")),
                                userObject.getString("Currency"), imeiList);
                    } else {
                        return false;
                    }
                } catch (JSONException ex) {
                    return false;
                }
                httpClient.close();
            } catch (MalformedURLException | UnsupportedEncodingException e) {
                httpClient.close();
                return false;
            } catch (IOException e1) {
                httpClient.close();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            mProgressActivity.setVisibility(View.GONE);

            if (success) {
                setActivityConfigurations ();
                // Add options to the vehicle reg picker
                addVehiclesToPickerOption();
                launchHomeFragment (true);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(mThisActivity);
                final AlertDialog alertDialog = builder.create();
                View dialogView = getLayoutInflater().inflate(R.layout.connection_retry_dialog, null);
                TextView text = (TextView) dialogView.findViewById(R.id.connection_text);
                Button reloadButton = (Button)dialogView.findViewById(R.id.connection_reload_button);
                Button logoutButton = (Button) dialogView.findViewById(R.id.connection_logout_button);

                text.setTypeface(mFont);
                reloadButton.setTypeface(mFont);
                logoutButton.setTypeface(mFont);

                reloadButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        mProgressActivity.setVisibility(View.VISIBLE);
                        GetActiveUserInfo getActiveUserInfo = new GetActiveUserInfo();
                        getActiveUserInfo.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                });
                logoutButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.logging_out), Toast.LENGTH_SHORT).show();
                        setResult(Activity.RESULT_OK);
                        finish();
                        // Clears this user's preferences
                        mUserPreferences.edit().clear().commit();
                    }
                });
                alertDialog.setView(dialogView);
                alertDialog.setCancelable(false);

                alertDialog.show();
            }
        }
    }

    public static class HomeFragment extends Fragment {
        public enum DataType {
            WEEK,
            MONTH
        }
        Typeface mFont, mLightFont;

        View mRootView;
        ViewFlipper mViewFlipper;
        TextView mDriverScoreTextView, mDistanceTraveledText, mTravelTimeText, mMPGTextView,
                mLastJourneyDesc, mStatisticsDesc, mSavingsDesc, mDistanceDesc, mTravelTimeDesc;
        ImageView mSpeedoImage, mSpeedoGraphics, mWeekSelectorLine, mMonthSelectorLine;
        RelativeLayout mLastJourneyButton, mStatisticsButton, mSavingsButton, mHomeProgressBackground, mWeekButton, mMonthButton;
        Needle mSpeedoNeedleImage;
        Coverflow mDriverScoresCoverflow;

        SharedPreferences mUserPreferences;
        User mActiveUser;
        String mSelectedIMEI;

        LayoutInflater mFragmentLayoutInflater;

        GestureDetector mGestureDetector;

        GetMPGTask mMPGTask;
        GetWeeklyStatistics mWeeklyStatisticsTask;
        GetMonthlyStatistics mMonthlyStatisticsTask;

        List<Statistics> mWeeklyStatistics = new ArrayList<>();
        List<Statistics> mMonthlyStatistics = new ArrayList<>();
        Journey mLastJourney;

        private Statistics mSelectedStatistics;

        private Fragment mHomeFragment;

        private int mCurrentSelectedCoverflowItem = -1;

        private boolean mWeeklyDataAvailable = false;
        private boolean mMonthlyDataAvailable = false;
        private DataType mCurrentDataTypeSelected = DataType.WEEK;

        public HomeFragment () {}

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            mHomeFragment = this;
            mFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.selected_font));
            mLightFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.selected_font_extra_light));
            mFragmentLayoutInflater = inflater;
            mRootView = mFragmentLayoutInflater.inflate(R.layout.fragment_home, null);
            mHomeProgressBackground = (RelativeLayout)mRootView.findViewById(R.id.home_progress_background);
            final ProgressBar homeProgressBar = (ProgressBar)mRootView.findViewById(R.id.home_progress);
            TextView tabWeekText = (TextView)mRootView.findViewById(R.id.tabWeekText);
            TextView tabMonthText = (TextView)mRootView.findViewById(R.id.tabMonthText);
            mWeekButton = (RelativeLayout)mRootView.findViewById(R.id.weekTabButton);
            mMonthButton = (RelativeLayout)mRootView.findViewById(R.id.monthTabButton);
            mWeekSelectorLine = (ImageView)mRootView.findViewById(R.id.weekTabSelectorLine);
            mMonthSelectorLine = (ImageView)mRootView.findViewById(R.id.monthTabSelectorLine);
            mViewFlipper = (ViewFlipper) mRootView.findViewById(R.id.homeViewFlipper);
            mDriverScoreTextView = (TextView) mRootView.findViewById(R.id.driverScoreTextView);
            mMPGTextView = (TextView) mRootView.findViewById(R.id.mpgTextView);
            mDistanceTraveledText = (TextView)mRootView.findViewById(R.id.distanceTraveledText);
            mTravelTimeText = (TextView)mRootView.findViewById(R.id.travelTimeText);
            mLastJourneyButton = (RelativeLayout)mRootView.findViewById(R.id.lastJourneyButton);
            mLastJourneyDesc = (TextView)mRootView.findViewById(R.id.lastJourneyText);
            mSpeedoImage = (ImageView)mRootView.findViewById(R.id.speedoImageView);
            mSpeedoGraphics = (ImageView)mRootView.findViewById(R.id.speedoGraphics);
            mSpeedoNeedleImage = (Needle)mRootView.findViewById(R.id.speedoNeedleImage);
            mDriverScoresCoverflow = (Coverflow)mRootView.findViewById(R.id.journeysScrollView);
            final TextView leftArrow = (TextView) mRootView.findViewById(R.id.leftArrow);
            final TextView rightArrow = (TextView) mRootView.findViewById(R.id.rightArrow);
            mDistanceDesc = (TextView)mRootView.findViewById(R.id.distanceTraveledUnitsText);
            mTravelTimeDesc = (TextView)mRootView.findViewById(R.id.travelTimeDescText);
            mStatisticsButton = (RelativeLayout)mRootView.findViewById(R.id.statisticsButton);
            mSavingsButton = (RelativeLayout)mRootView.findViewById(R.id.savingsButton);
            ImageView savingsImage = (ImageView)mRootView.findViewById(R.id.savingsImage);
            mStatisticsDesc = (TextView)mRootView.findViewById(R.id.statisticsTextView);
            mSavingsDesc = (TextView)mRootView.findViewById(R.id.savingsTextView);

            if (Build.VERSION.SDK_INT >= 20) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                homeProgressBar.animate().setDuration(shortAnimTime).alpha(1.0f).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        homeProgressBar.setVisibility(View.VISIBLE);
                    }
                });
                homeProgressBar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {}
                });
            }

            mHomeProgressBackground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {}
            });

            leftArrow.setText("<");
            rightArrow.setText(">");

            // Define Typefaces
            mDriverScoreTextView.setTypeface(mFont);
            mDistanceTraveledText.setTypeface(mFont);
            mTravelTimeText.setTypeface(mFont);
            mLastJourneyDesc.setTypeface(mFont);
            mStatisticsDesc.setTypeface(mLightFont);
            mSavingsDesc.setTypeface(mLightFont);
            leftArrow.setTypeface(mFont);
            rightArrow.setTypeface(mFont);
            mDistanceDesc.setTypeface(mFont);
            mTravelTimeDesc.setTypeface(mFont);
            tabWeekText.setTypeface(mFont);
            tabMonthText.setTypeface(mFont);

            mUserPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            Bundle activityBundle = getActivity().getIntent().getExtras();
            if (activityBundle != null)
                mActiveUser = activityBundle.getParcelable("activeuser");
            else if (getArguments() != null)
                mActiveUser = getArguments().getParcelable("activeuser");

            // Define week values
            if (mWeeklyStatistics.isEmpty())
                generateCalendarData(DataType.WEEK);
            // Define month values
            if (mMonthlyStatistics.isEmpty())
                generateCalendarData(DataType.MONTH);

            mGestureDetector = new GestureDetector (getActivity(), new CustomGestureDetector());
            mViewFlipper.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    mGestureDetector.onTouchEvent(event);
                    return true;
                }
            });

            //Simulate left to right gesture on view flipper
            leftArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayNextView();
                }
            });
            leftArrow.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        leftArrow.setAlpha(0.6f);
                    if (event.getAction() == MotionEvent.ACTION_UP)
                        leftArrow.setAlpha(1f);
                    return false;
                }
            });

            //Simulate right to left gesture on view flipper
            rightArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayPreviousView ();
                }
            });
            rightArrow.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        rightArrow.setAlpha(0.6f);
                    if (event.getAction() == MotionEvent.ACTION_UP)
                        rightArrow.setAlpha(1f);
                    return false;
                }
            });

            mMPGTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    final AlertDialog alertDialog = builder.create();
                    View dialogView = getActivity().getLayoutInflater().inflate(R.layout.simple_input_dialog, null);
                    final EditText urbanMpgEditText = (EditText) dialogView.findViewById(R.id.simple_input_edit_text);
                    final EditText extraUrbanMpgEditText = (EditText) dialogView.findViewById(R.id.simple_input_edit_text2);
                    final EditText combinedMpgEditText = (EditText) dialogView.findViewById(R.id.simple_input_edit_text3);
                    Button confirmButton = (Button) dialogView.findViewById(R.id.simple_input_confirm_button);

                    confirmButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SetMPGTask mpgTask = new SetMPGTask();
                            String selectedIMEI = mActiveUser.getImeiByVehicleReg(mActiveUser.getSelectedVehicleReg());
                            mpgTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, String.valueOf(selectedIMEI), urbanMpgEditText.getText().toString(),
                                    extraUrbanMpgEditText.getText().toString(), combinedMpgEditText.getText().toString());
                            mMPGTextView.setText(urbanMpgEditText.getText());
                            alertDialog.dismiss();
                        }
                    });

                    alertDialog.setView(dialogView);
                    alertDialog.show();
                }
            });

            mLastJourneyButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        mLastJourneyDesc.setAlpha(0.6f);
                    if (event.getAction() == MotionEvent.ACTION_UP)
                        mLastJourneyDesc.setAlpha(1f);
                    return false;
                }
            });

            mLastJourneyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLastJourney != null) {
                        JourneyDetailsFragment rmf = new JourneyDetailsFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("Journey", mLastJourney);
                        bundle.putInt("MarkerType", 0);
                        rmf.setArguments(bundle);

                        FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
                        transaction.replace(R.id.container, rmf, "journeymap");
                        transaction.addToBackStack("journeymap");
                        transaction.commit();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.last_journey_not_available), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mStatisticsButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        mStatisticsDesc.setAlpha(0.6f);
                    if (event.getAction() == MotionEvent.ACTION_UP)
                        mStatisticsDesc.setAlpha(1f);
                    return false;
                }
            });
            mSavingsButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        mSavingsDesc.setAlpha(0.6f);
                    if (event.getAction() == MotionEvent.ACTION_UP)
                        mSavingsDesc.setAlpha(1f);
                    return false;
                }
            });

            mStatisticsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((mCurrentDataTypeSelected == DataType.MONTH && mMonthlyDataAvailable) ||
                            (mCurrentDataTypeSelected == DataType.WEEK && mWeeklyDataAvailable)) {
                        Bundle argsBundle = new Bundle();
                        StatisticsFragmentActivity statisticsFragment = new StatisticsFragmentActivity();
                        ArrayList<Statistics> arrayList = new ArrayList<>();
                        if (mCurrentDataTypeSelected == DataType.WEEK)
                            arrayList.addAll(mWeeklyStatistics);
                        else if (mCurrentDataTypeSelected == DataType.MONTH)
                            arrayList.addAll(mMonthlyStatistics);
                        argsBundle.putParcelableArrayList("Statistics", arrayList);
                        argsBundle.putString("DataType", String.valueOf(mCurrentDataTypeSelected));
                        argsBundle.putInt("DisplayedChild", mViewFlipper.getDisplayedChild());
                        argsBundle.putLong("StartDate", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getStartDate().getTime());
                        argsBundle.putLong("EndDate", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getEndDate().getTime());
                        Intent intent = new Intent(getActivity(), StatisticsFragmentActivity.class);
                        intent.putExtra("ActiveUser", mActiveUser);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.no_statistics_available), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mSavingsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((mCurrentDataTypeSelected == DataType.MONTH && mMonthlyDataAvailable) ||
                            (mCurrentDataTypeSelected == DataType.WEEK && mWeeklyDataAvailable)) {
                        Bundle argsBundle = new Bundle();
                        FuelSavingsFragment savingsFragment = new FuelSavingsFragment();
                        ArrayList<Statistics> arrayList = new ArrayList<>();
                        if (mCurrentDataTypeSelected == DataType.WEEK)
                            arrayList.addAll(mWeeklyStatistics);
                        else if (mCurrentDataTypeSelected == DataType.MONTH)
                            arrayList.addAll(mMonthlyStatistics);
                        argsBundle.putParcelableArrayList("Statistics", arrayList);
                        argsBundle.putString("DataType", String.valueOf(mCurrentDataTypeSelected));
                        argsBundle.putInt("DisplayedChild", mViewFlipper.getDisplayedChild());
                        if (mCurrentDataTypeSelected == DataType.WEEK) {
                            argsBundle.putLong("StartDate", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getStartDate().getTime());
                            argsBundle.putLong("EndDate", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getEndDate().getTime());
                        } else if (mCurrentDataTypeSelected == DataType.MONTH) {
                            argsBundle.putLong("StartDate", mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getStartDate().getTime());
                            argsBundle.putLong("EndDate", mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getEndDate().getTime());
                        }
                        savingsFragment.setArguments(argsBundle);
                        FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
                        transaction.replace(R.id.container, savingsFragment, "savings");
                        transaction.addToBackStack("savings");
                        transaction.commit();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.no_savings_available), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            mWeekButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCurrentDataTypeSelected != DataType.WEEK) {
                        mMonthSelectorLine.setVisibility(View.GONE);
                        mMonthButton.setAlpha(0.5f);
                        mWeekSelectorLine.setVisibility(View.VISIBLE);
                        mWeekButton.setAlpha(1.0f);

                        mCurrentDataTypeSelected = DataType.WEEK;

                        showAllFragment(true);

                        mDisplayedViewOnViewFlipper = 0;
                        addViewFlipperViews(DataType.WEEK);
                        int resultfullIndex = 0;
                        for (int i = 0; i < mWeeklyStatistics.size(); i++) {
                            if (!mWeeklyStatistics.get(i).isObjectBuilt())
                                resultfullIndex = - 1;
                            else {
                                resultfullIndex = i;
                                break;
                            }
                        }
                        if (resultfullIndex == -1) {
                            if (mWeeklyStatisticsTask != null)
                                if (mWeeklyStatisticsTask.getStatus() == AsyncTask.Status.RUNNING)
                                    mWeeklyStatisticsTask.cancel(true);

                            mHomeProgressBackground.setVisibility(View.VISIBLE);
                            mWeeklyStatisticsTask = new GetWeeklyStatistics();
                            mWeeklyStatisticsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0, 4);
                        } else {
                            // Driver's week general score - counting animation
                            animateScoreCounting(0, mWeeklyStatistics.get(resultfullIndex).getOverallScore(), false);
                            // Set driver's distance traveled + travel time texts
                            displayStatisticsResults(mWeeklyStatistics.get(resultfullIndex));
                            // SpeedNeedle + Speedometer
                            animateSpeedNeedle(mWeeklyStatistics.get(resultfullIndex));
                            drawSpeedometer(mWeeklyStatistics.get(resultfullIndex));
                            // CoverFlow
                            createCoverFlow(mWeeklyStatistics.get(resultfullIndex), true);
                            // Go to the latest week with results
                            mViewFlipper.setDisplayedChild(resultfullIndex);
                        }
                    }
                }
            });
            mWeekButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (mCurrentDataTypeSelected != DataType.WEEK) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN)
                            mWeekButton.setAlpha(1.0f);
                        if (event.getAction() == MotionEvent.ACTION_UP)
                            mWeekButton.setAlpha(0.5f);
                    }
                    return false;
                }
            });
            mMonthButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCurrentDataTypeSelected != DataType.MONTH) {
                        mWeekSelectorLine.setVisibility(View.GONE);
                        mWeekButton.setAlpha(0.5f);
                        mMonthSelectorLine.setVisibility(View.VISIBLE);
                        mMonthButton.setAlpha(1.0f);


                        mCurrentDataTypeSelected = DataType.MONTH;

                        showAllFragment(true);

                        mDisplayedViewOnViewFlipper = 0;
                        addViewFlipperViews(DataType.MONTH);
                        int resultfullIndex = 0;
                        for (int i = 0; i < mMonthlyStatistics.size(); i++) {
                            if (!mMonthlyStatistics.get(i).isObjectBuilt())
                                resultfullIndex = - 1;
                            else {
                                resultfullIndex = i;
                                break;
                            }
                        }
                        if (resultfullIndex == -1) {
                            if (mMonthlyStatisticsTask != null)
                                if (mMonthlyStatisticsTask.getStatus() == AsyncTask.Status.RUNNING)
                                    mMonthlyStatisticsTask.cancel(true);

                            mHomeProgressBackground.setVisibility(View.VISIBLE);
                            mMonthlyStatisticsTask = new GetMonthlyStatistics();
                            mMonthlyStatisticsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0, 4);
                        } else {
                            // Driver's week general score - counting animation
                            animateScoreCounting(0, mMonthlyStatistics.get(resultfullIndex).getOverallScore(), false);
                            // Set driver's distance traveled + travel time texts
                            displayStatisticsResults(mMonthlyStatistics.get(resultfullIndex));
                            // SpeedNeedle + Speedometer
                            animateSpeedNeedle(mMonthlyStatistics.get(resultfullIndex));
                            drawSpeedometer(mMonthlyStatistics.get(resultfullIndex));
                            // CoverFlow
                            createCoverFlow(mMonthlyStatistics.get(resultfullIndex), true);
                            // Go to the latest month with results
                            mViewFlipper.setDisplayedChild(resultfullIndex);
                        }

                    }
                }
            });
            mMonthButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (mCurrentDataTypeSelected != DataType.MONTH) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN)
                            mMonthButton.setAlpha(1.0f);
                        if (event.getAction() == MotionEvent.ACTION_UP)
                            mMonthButton.setAlpha(0.5f);
                    }
                    return false;
                }
            });

            // Set savings button to correct currency:
            switch (mActiveUser.getCurrency()) {
                case EUR:
                    savingsImage.setImageResource(R.drawable.ic_savings_euro_button);
                    break;
                case GBP:
                    savingsImage.setImageResource(R.drawable.ic_savings_pound_button);
                    break;
                case USD:
                    savingsImage.setImageResource(R.drawable.ic_savings_dollar_button);
                    break;
            }

            return mRootView;
        }

        @Override
        public void onResume() {
            super.onResume();
            mActiveUser = ((HomeActivity) getActivity()).getActiveUser();
            ((HomeActivity)getActivity()).setScreenTitle(mActiveUser.getSelectedVehicleReg());
            mSelectedIMEI = mActiveUser.getImeiByVehicleReg(mActiveUser.getSelectedVehicleReg());
//            mMPGTask = new GetMPGTask();
//            mMPGTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, String.valueOf(mSelectedIMEI));
            switch (mCurrentDataTypeSelected) {
                case WEEK:
                    mMonthSelectorLine.setVisibility(View.GONE);
                    mMonthButton.setAlpha(0.5f);
                    mWeekSelectorLine.setVisibility(View.VISIBLE);
                    mWeekButton.setAlpha(1.0f);
                    if (mWeeklyDataAvailable) {
                        loadWeeklyStatistics();
                    } else {
                        addViewFlipperViews (DataType.WEEK);
                        getWeeklyScoreByVehicle(mActiveUser.getSelectedVehicleReg());
                    }
                    break;
                case MONTH:
                    mWeekSelectorLine.setVisibility(View.GONE);
                    mWeekButton.setAlpha(0.5f);
                    mMonthSelectorLine.setVisibility(View.VISIBLE);
                    mMonthButton.setAlpha(1.0f);
                    if (mMonthlyDataAvailable) {
                        loadMonthlyStatistics();
                    } else {
                        addViewFlipperViews (DataType.MONTH);
                        getMonthlyScoreByVehicle(mActiveUser.getSelectedVehicleReg());
                    }
                    break;
            }
        }

        @Override
        public void onPause() {
            super.onPause();
            clearSpeedometer();
//            if (mMPGTask.getStatus() == AsyncTask.Status.RUNNING)
//                mMPGTask.cancel(true);
            if (mWeeklyStatistics != null)
                if (mWeeklyStatisticsTask.getStatus() == AsyncTask.Status.RUNNING)
                    mWeeklyStatisticsTask.cancel(true);

            if (mMonthlyStatisticsTask != null)
                if (mMonthlyStatisticsTask.getStatus() == AsyncTask.Status.RUNNING)
                    mMonthlyStatisticsTask.cancel(true);

            // Keep track of the currently selected week day:
            mCurrentSelectedCoverflowItem = mDriverScoresCoverflow.getSelectedItemPosition();
            // Keep track of the currently selected week:
            mDisplayedViewOnViewFlipper = mViewFlipper.getDisplayedChild();
        }

        private void generateCalendarData (DataType type) {
            Date firstPolicyStartDate = new Date();
            DateTime startDate = new DateTime();
            for (int i = 0; i < mActiveUser.getIMEIList().size(); i++) {
                if (mActiveUser.getSelectedVehicleReg() == null)
                    firstPolicyStartDate = mActiveUser.getIMEIList().get(i).getFirstPolicyByVehicleReg(mActiveUser.getIMEIList().get(i).getVehicleReg());
                else
                    firstPolicyStartDate = mActiveUser.getIMEIList().get(i).getFirstPolicyByVehicleReg(mActiveUser.getSelectedVehicleReg());

                if (firstPolicyStartDate != null)
                    break;
            }
            switch (type) {
                case WEEK:
                    // Cancel task because we are clearing a List used on it:
                    if (mWeeklyStatisticsTask != null)
                        if (mWeeklyStatisticsTask.getStatus() == AsyncTask.Status.RUNNING)
                            mWeeklyStatisticsTask.cancel(true);
                    mWeeklyStatistics.clear();
                    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    calendar.setFirstDayOfWeek(Calendar.SUNDAY);
                    // Get first week's end and start date
                    Statistics currentWeekStatistics = new Statistics ();
                    int thisWeekEndOffset = Functions.getGMTOffset(new DateTime(calendar.getTimeInMillis()));
                    currentWeekStatistics.setEndDate(new Date(calendar.getTimeInMillis() - (thisWeekEndOffset * 3600000)));
                    calendar.add(Calendar.DATE, -(calendar.get(calendar.DAY_OF_WEEK) - 1));
                    calendar = setCalendarToMidNight(calendar);
                    int thisWeekStartOffset = Functions.getGMTOffset(new DateTime(calendar.getTimeInMillis()));
                    currentWeekStatistics.setStartDate(new Date(calendar.getTimeInMillis() - (thisWeekStartOffset * 3600000)));
                    mWeeklyStatistics.add (currentWeekStatistics);

                    // Get last week's end and start date
                    Statistics lastWeekStatistics = new Statistics();
                    calendar.add(Calendar.DATE, -1);
                    calendar = setCalendarToBeforeMidNight(calendar);
                    int lastWeekEndOffset = Functions.getGMTOffset(new DateTime(calendar.getTimeInMillis()));
                    lastWeekStatistics.setEndDate(new Date(calendar.getTimeInMillis() - (lastWeekEndOffset * 3600000)));
                    calendar.add(Calendar.DATE, -6);
                    calendar = setCalendarToMidNight(calendar);
                    int lastWeekStartOffset = Functions.getGMTOffset(new DateTime(calendar.getTimeInMillis()));
                    lastWeekStatistics.setStartDate(new Date(calendar.getTimeInMillis() - (lastWeekStartOffset * 3600000)));
                    calendar.add(Calendar.DATE, -1);
                    mWeeklyStatistics.add (lastWeekStatistics);

                    // Go back 5 weeks if null...
                    if (firstPolicyStartDate == null) {
                        startDate = DateTime.now();
                        startDate = startDate.minusWeeks(5);
                    } else {
                        startDate = new DateTime(firstPolicyStartDate.getTime());
                    }

                    // Get number of weeks between this and last week's Sunday
                    Weeks weeksObj = Weeks.weeksBetween(startDate.minusDays(startDate.getDayOfWeek()), DateTime.now().plusDays(7 - DateTime.now().getDayOfWeek()));
                    int numberOfWeeks = weeksObj.getWeeks();

                    // Set views for current, last and until last week on the policy
                    for (int i = 0; i < numberOfWeeks; i++) {
                        View weekView = mFragmentLayoutInflater.inflate(R.layout.view_flipper_view_layout, null);
                        TextView weeksText = (TextView) weekView.findViewById(R.id.viewFlipperTextView);
                        weeksText.setTypeface(mFont);
                        if (i == 0)
                            weeksText.setText(getResources().getString(R.string.current_week));
                        else if (i == 1)
                            weeksText.setText(getResources().getString(R.string.last_week));
                        else {
                            Statistics weeklyStatistic = new Statistics();
                            calendar = setCalendarToBeforeMidNight (calendar);
                            Date endDate = new Date (calendar.getTimeInMillis() -
                                    (Functions.getGMTOffset(new DateTime(calendar.getTimeInMillis())) * 3600000));
                            DateTime endDateTime = new DateTime(endDate.getTime());
                            int lastDay = endDateTime.getDayOfMonth();
                            int lastDaysMonth = endDateTime.getMonthOfYear();
                            int lastDaysYear = endDateTime.getYear();
                            weeklyStatistic.setEndDate(endDate);
                            calendar.add (Calendar.DATE, -6);

                            calendar = setCalendarToMidNight (calendar);
                            Date weekStartDate = new Date (calendar.getTimeInMillis() -
                                    (Functions.getGMTOffset(new DateTime(calendar.getTimeInMillis())) * 3600000));
                            DateTime startDateTime = new DateTime(weekStartDate.getTime());
                            int firstDay = startDateTime.getDayOfMonth();
                            int firstDaysMonth = startDateTime.getMonthOfYear();
                            int firstDaysYear = startDateTime.getYear();
                            weeklyStatistic.setStartDate(weekStartDate);
                            mWeeklyStatistics.add (weeklyStatistic);
                            DecimalFormat format = new DecimalFormat("00");
                            // Month + 1 because months on Calendar object run from 0 to 11
                            weeksText.setText(String.format("%s/%s/%d - %s/%s/%d", format.format(firstDay), format.format(firstDaysMonth + 1), firstDaysYear
                                    , format.format(lastDay), format.format(lastDaysMonth + 1), lastDaysYear));

                            // Go back one day to previous Saturday
                            calendar.add (Calendar.DATE, -1);
                        }

                        mViewFlipper.addView(weekView);
                    }
                    break;
                case MONTH:
                    // Cancel task because we are clearing a List used on it:
                    if (mMonthlyStatisticsTask != null)
                        if (mMonthlyStatisticsTask.getStatus() == AsyncTask.Status.RUNNING)
                            mMonthlyStatisticsTask.cancel(true);
                    mMonthlyStatistics.clear();
                    // Get current month's stats
                    Calendar monthCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    Statistics currentMonthStatistics = new Statistics();
                    currentMonthStatistics.setEndDate(new Date(monthCalendar.getTimeInMillis()));
                    monthCalendar.add(Calendar.DATE, -(monthCalendar.get(monthCalendar.DAY_OF_MONTH) - 1));
                    monthCalendar = setCalendarToMidNight(monthCalendar);
                    int thisMonthStartOffset = Functions.getGMTOffset(new DateTime(monthCalendar.getTimeInMillis()));
                    currentMonthStatistics.setStartDate(new Date(monthCalendar.getTimeInMillis() - (thisMonthStartOffset * 3600000)));
                    mMonthlyStatistics.add(currentMonthStatistics);

                    // Get last month's stats
                    Statistics lastMonthsStatistics = new Statistics();
                    monthCalendar.add(Calendar.DATE, -1);
                    monthCalendar = setCalendarToBeforeMidNight (monthCalendar);
                    int lastMonthEndOffset = Functions.getGMTOffset(new DateTime(monthCalendar.getTimeInMillis()));
                    lastMonthsStatistics.setEndDate(new Date(monthCalendar.getTimeInMillis() - (lastMonthEndOffset * 3600000)));
                    monthCalendar.add(Calendar.DATE, -(monthCalendar.get(monthCalendar.DAY_OF_MONTH) - 1));
                    monthCalendar = setCalendarToMidNight(monthCalendar);
                    int lastMonthStartOffset = Functions.getGMTOffset(new DateTime(monthCalendar.getTimeInMillis()));
                    lastMonthsStatistics.setStartDate(new Date(monthCalendar.getTimeInMillis() - (lastMonthStartOffset * 3600000)));
                    monthCalendar.add(Calendar.DATE, -1);
                    mMonthlyStatistics.add (lastMonthsStatistics);

                    // Go back 3 months if null...
                    if (firstPolicyStartDate == null) {
                        startDate = DateTime.now();
                        startDate = startDate.minusMonths(3);
                    } else {
                        startDate = new DateTime(firstPolicyStartDate.getTime());
                    }

                    // Get number of months between this and last week's Sunday
                    Months monthsObj = Months.monthsBetween(startDate.minusDays(startDate.getDayOfWeek()), DateTime.now().plusDays(7 - DateTime.now().getDayOfWeek()));
                    int numberOfMonths = monthsObj.getMonths();

                    // Set views for current, last and until last month on the policy
                    for (int i = 0; i < numberOfMonths - 1; i++) {
                        Statistics monthlyStatistic = new Statistics();
                        monthCalendar = setCalendarToBeforeMidNight (monthCalendar);
                        Date endDate = new Date (monthCalendar.getTimeInMillis() -
                                (Functions.getGMTOffset(new DateTime(monthCalendar.getTimeInMillis())) * 3600000));
                        monthlyStatistic.setEndDate(endDate);
                        monthCalendar.add (Calendar.DATE, -(monthCalendar.get(monthCalendar.DAY_OF_MONTH) - 1));
                        monthCalendar = setCalendarToMidNight (monthCalendar);
                        Date monthStartDate = new Date (monthCalendar.getTimeInMillis() -
                                (Functions.getGMTOffset(new DateTime(monthCalendar.getTimeInMillis())) * 3600000));
                        monthlyStatistic.setStartDate(monthStartDate);
                        mMonthlyStatistics.add (monthlyStatistic);

                        // Go back one day to last day of the month before
                        monthCalendar.add (Calendar.DATE, -1);
                    }
                    break;
            }
        }

        private Calendar setCalendarToMidNight (Calendar calendar) {
            calendar.set(Calendar.HOUR_OF_DAY, calendar.getMinimum(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, calendar.getMinimum(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, calendar.getMinimum(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND, calendar.getMinimum(Calendar.MILLISECOND));
            return calendar;
        }
        private Calendar setCalendarToBeforeMidNight (Calendar calendar) {
            calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, calendar.getMaximum(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, calendar.getMaximum(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
            return calendar;
        }

        private void loadWeeklyStatistics () {
            boolean reloadWeeklyStatistics = true;
            for (int i = 0; i < mWeeklyStatistics.size(); i++) {
                if (mWeeklyStatistics.get(i).isObjectBuilt ()) {
                    reloadWeeklyStatistics = false;
                    break;
                }
            }
            // If no weekly statistics were found
            if (reloadWeeklyStatistics) {
                mHomeProgressBackground.setVisibility(View.VISIBLE);
                mWeeklyStatisticsTask = new GetWeeklyStatistics();
                mWeeklyStatisticsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0, 4);
            }

            // If the weekly statistics objected was already created, animate the panels
            if (mWeeklyStatisticsTask.getStatus() != AsyncTask.Status.RUNNING) {
                mHomeProgressBackground.setVisibility(View.GONE);
                showAllFragment (true);
                addViewFlipperViews (DataType.WEEK);
                try {
                    mViewFlipper.setDisplayedChild(mDisplayedViewOnViewFlipper);
                    displayStatisticsResults(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()));
                    createCoverFlow(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()), false);
                    animateSpeedNeedle(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()));
                    animateScoreCounting(0, mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getOverallScore(), true);
                    drawSpeedometer(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (mDriverScoresCoverflow != null) {
                mDriverScoresCoverflow.setSelection(mCurrentSelectedCoverflowItem, true);
            }
        }

        private void loadMonthlyStatistics () {
            boolean reloadMonthlyStatistics = true;
            for (int i = 0; i < mMonthlyStatistics.size(); i++) {
                if (mMonthlyStatistics.get(i).isObjectBuilt ()) {
                    reloadMonthlyStatistics = false;
                    break;
                }
            }
            // If no monthly statistics were found
            if (reloadMonthlyStatistics) {
                mHomeProgressBackground.setVisibility(View.VISIBLE);
                mMonthlyStatisticsTask = new GetMonthlyStatistics();
                mMonthlyStatisticsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0, 4);
            }

            // If the monthly statistics objected was already created, animate the panels
            if (mMonthlyStatisticsTask.getStatus() != AsyncTask.Status.RUNNING) {
                mHomeProgressBackground.setVisibility(View.GONE);
                showAllFragment (true);
                addViewFlipperViews (DataType.MONTH);
                try {
                    mViewFlipper.setDisplayedChild(mDisplayedViewOnViewFlipper);
                    displayStatisticsResults(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()));
                    createCoverFlow(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()), false);
                    animateSpeedNeedle(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()));
                    animateScoreCounting(0, mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getOverallScore(), true);
                    drawSpeedometer(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (mDriverScoresCoverflow != null) {
                mDriverScoresCoverflow.setSelection(mCurrentSelectedCoverflowItem, true);
            }
        }

        private void addViewFlipperViews (DataType type) {
            mViewFlipper.removeAllViews();
            switch (type) {
                case WEEK:
                    for (int i = 0; i < mWeeklyStatistics.size(); i++) {
                        View weekView = mFragmentLayoutInflater.inflate(R.layout.view_flipper_view_layout, null);
                        TextView weeksText = (TextView) weekView.findViewById(R.id.viewFlipperTextView);
                        weeksText.setTypeface(mFont);
                        if (i == 0)
                            weeksText.setText(getResources().getString(R.string.current_week));
                        else if (i == 1)
                            weeksText.setText(getResources().getString(R.string.last_week));
                        else {
                            DateTime startDate = new DateTime(mWeeklyStatistics.get(i).getStartDate().getTime());
                            DateTime endDate = new DateTime(mWeeklyStatistics.get(i).getEndDate().getTime());
                            DecimalFormat format = new DecimalFormat("00");
                            // Month + 1 because months on Calendar object run from 0 to 11
                            weeksText.setText(String.format("%s/%s/%d - %s/%s/%d", format.format(startDate.getDayOfMonth()), format.format(startDate.getMonthOfYear()), startDate.getYear()
                                    , format.format(endDate.getDayOfMonth()), format.format(endDate.getMonthOfYear()), endDate.getYear()));
                        }
                        mViewFlipper.addView(weekView);
                    }
//                    mViewFlipper.setDisplayedChild(mDisplayedViewOnViewFlipper);
                    break;
                case MONTH:
                    DateTime now = new DateTime();
                    // Display 5 months worth of data:
                    for (int i = 0; i < mMonthlyStatistics.size(); i++) {
                        View monthView = mFragmentLayoutInflater.inflate(R.layout.view_flipper_view_layout, null);
                        TextView monthText = (TextView) monthView.findViewById(R.id.viewFlipperTextView);
                        monthText.setTypeface(mFont);
                        DateTime.Property monthOfYear = now.monthOfYear();
                        monthText.setText(monthOfYear.getAsText(Locale.getDefault()).toUpperCase() + " " + now.getYear());
                        now = now.minusMonths(1);
                        mViewFlipper.addView(monthView);
                    }
//                    mViewFlipper.setDisplayedChild(mDisplayedViewOnViewFlipper);
                    break;
            }
        }

        private void createCoverFlow (final Statistics selectedStatistics, boolean resetSelection) {
            if (resetSelection) {
                // Reset coverflow selection
                mCurrentSelectedCoverflowItem = -1;
            }
            mDriverScoresCoverflow.setAdapter(new FlowAdapter(getActivity(), selectedStatistics.getDriverScores(), true, mCurrentDataTypeSelected));
            mDriverScoresCoverflow.setUnselectedAlpha(0.7f);
            mDriverScoresCoverflow.setUnselectedSaturation(0.0f);
            mDriverScoresCoverflow.setUnselectedScale(0.005f);
            mDriverScoresCoverflow.setSpacing(-15);
            mDriverScoresCoverflow.setMaxRotation(0);
            mDriverScoresCoverflow.setScaleDownGravity(0.2f);
            mDriverScoresCoverflow.setActionDistance(Coverflow.ACTION_DISTANCE_AUTO);
            // If the the current day hasn't been selected by the user yet,
            // go to the first day of week with good score...
            if (mCurrentSelectedCoverflowItem == -1) {
                for (int i = 0; i < selectedStatistics.getDriverScores().size(); i++) {
                    if (selectedStatistics.getDriverScores().get(i) > -1) {
                        mDriverScoresCoverflow.setSelection(i, true);
                        mCurrentSelectedCoverflowItem = i;
                        break;
                    }
                }
            }
            mDriverScoresCoverflow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mDriverScoresCoverflow.setSelection(position);
                    if (mCurrentDataTypeSelected == DataType.WEEK) {
                        if (mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getDriverScores().get(position) != -1) {
                            startJourneysFragment();
                        }
                    } else if (mCurrentDataTypeSelected == DataType.MONTH)
                        if (mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getDriverScores().get(position) != -1)
                            startJourneysFragment ();
                }
            });
            mDriverScoresCoverflow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if (mCurrentDataTypeSelected == DataType.WEEK) {
                            Statistics selectedStats = mWeeklyStatistics.get(mViewFlipper.getDisplayedChild());
                            if (selectedStats != null)
                                if (selectedStats.getDriverScores() != null)
                                    if (selectedStats.getDriverScores().size() > 0)
                                        if (selectedStats.getDriverScores().get(position) == -1)
                                            view.setAlpha(0.6f);
                        } else if (mCurrentDataTypeSelected == DataType.MONTH) {
                            Statistics selectedStats = mMonthlyStatistics.get(mViewFlipper.getDisplayedChild());
                            if (selectedStats != null)
                                if (selectedStats.getDriverScores() != null)
                                    if (selectedStats.getDriverScores().size() > 0)
                                        if (selectedStats.getDriverScores().get(position) == -1)
                                            view.setAlpha(0.6f);
                        }
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        private void drawSpeedometer (Statistics selectedStatistics) {
            int innerSpeedometerSize = (int)getResources().getDimension(R.dimen.inner_speedometer_size);
            float halfInnerSpeedometerRealSize = getResources().getDimension(R.dimen.inner_speedometer_size) / 2;
            Bitmap bitmap = Bitmap.createBitmap(innerSpeedometerSize, innerSpeedometerSize, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            mSpeedoGraphics.setImageBitmap(bitmap);
            float minimumScoreDegrees = (selectedStatistics.getWorstScore() * 270.0f) / 100;
            float maximumScoreDegrees = (selectedStatistics.getBestScore() * 270.0f) / 100;

            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setAntiAlias(true);
            paint.setDither(true);
            paint.setFilterBitmap(true);
            if(selectedStatistics.getOverallScore() >= GlobalValues.GoodScore)
                paint.setColor(Color.GREEN);
            else if (selectedStatistics.getOverallScore() <= GlobalValues.MediumScoreMax
                    && selectedStatistics.getOverallScore() >= GlobalValues.MediumScoreMin)
                paint.setColor(Color.YELLOW);
            else if (selectedStatistics.getOverallScore() <= GlobalValues.BadScore)
                paint.setColor(Color.RED);
            paint.setStrokeWidth(getResources().getDimension(R.dimen.speedometer_score_stroke_width));
            ArcUtils.drawArc(canvas, new PointF(halfInnerSpeedometerRealSize, halfInnerSpeedometerRealSize),
                    getResources().getDimension(R.dimen.inner_speedometer_radius), 90f, ((selectedStatistics.getOverallScore() * 270.0f) / 100), paint);
            if (maximumScoreDegrees != minimumScoreDegrees) {
                Paint rangePaint = new Paint();
                rangePaint.setStyle(Paint.Style.STROKE);
                rangePaint.setStrokeWidth(getResources().getDimension(R.dimen.score_range_stroke_width));
                rangePaint.setAntiAlias(true);
                rangePaint.setShader(new RadialGradient(halfInnerSpeedometerRealSize,
                        halfInnerSpeedometerRealSize,
                        halfInnerSpeedometerRealSize,
                        getResources().getColor(android.R.color.transparent),
                        getResources().getColor(android.R.color.white), Shader.TileMode.CLAMP));
                ArcUtils.drawArc(canvas, new PointF(halfInnerSpeedometerRealSize,
                                halfInnerSpeedometerRealSize), getResources().getDimension(R.dimen.score_range_gradient), minimumScoreDegrees + 90.0f,
                        maximumScoreDegrees - minimumScoreDegrees, rangePaint);
            }
            canvas.save();
            canvas.rotate(minimumScoreDegrees, halfInnerSpeedometerRealSize,
                    halfInnerSpeedometerRealSize);
            Paint minimumScorePaint = new Paint ();
            minimumScorePaint.setStyle(Paint.Style.STROKE);
            minimumScorePaint.setStrokeWidth(getResources().getDimension(R.dimen.speedometer_score_stroke_width));
            minimumScorePaint.setAntiAlias(true);
            minimumScorePaint.setColor(Color.RED);
            canvas.drawLine(halfInnerSpeedometerRealSize,halfInnerSpeedometerRealSize,
                    halfInnerSpeedometerRealSize, getResources().getDimension(R.dimen.speedometer_lines_size), minimumScorePaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(maximumScoreDegrees, halfInnerSpeedometerRealSize,
                    halfInnerSpeedometerRealSize);
            Paint maxScorePaint = new Paint ();
            maxScorePaint.setStyle(Paint.Style.STROKE);
            maxScorePaint.setStrokeWidth(getResources().getDimension(R.dimen.speedometer_score_stroke_width));
            maxScorePaint.setAntiAlias(true);
            maxScorePaint.setColor(Color.GREEN);
            canvas.drawLine(halfInnerSpeedometerRealSize, halfInnerSpeedometerRealSize,
                    halfInnerSpeedometerRealSize, getResources().getDimension(R.dimen.speedometer_lines_size), maxScorePaint);
            canvas.restore();
        }

        private void animateSpeedNeedle (Statistics selectedStatistics) {
            float rotationDegrees = (selectedStatistics.getOverallScore() * 270.0f) / 100;
            mSpeedoNeedleImage.setResetNeedle(true);
            mSpeedoNeedleImage.setmMaxRotationDegrees(rotationDegrees);
            mSpeedoNeedleImage.invalidate();
        }

        public void getWeeklyScoreByVehicle (String vehicleRegPlate) {
            // Reset current selected item on coverflow
            mCurrentSelectedCoverflowItem = -1;

            if (mWeeklyStatisticsTask != null)
                if (mWeeklyStatisticsTask.getStatus() == AsyncTask.Status.RUNNING)
                    mWeeklyStatisticsTask.cancel(true);

            mWeeklyStatisticsTask = new GetWeeklyStatistics();
            mHomeProgressBackground.setVisibility(View.VISIBLE);
            setSelectedIMEI(String.valueOf(mActiveUser.getImeiByVehicleReg(vehicleRegPlate)));
            mWeeklyStatisticsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0, 4);
        }

        public void getMonthlyScoreByVehicle (String vehicleRegPlate) {
            // Reset current selected item on coverflow
            mCurrentSelectedCoverflowItem = -1;

            if (mMonthlyStatisticsTask != null)
                if (mMonthlyStatisticsTask.getStatus() == AsyncTask.Status.RUNNING)
                    mMonthlyStatisticsTask.cancel(true);

            mMonthlyStatisticsTask = new GetMonthlyStatistics();
            mHomeProgressBackground.setVisibility(View.VISIBLE);
            setSelectedIMEI(String.valueOf(mActiveUser.getImeiByVehicleReg(vehicleRegPlate)));
            mMonthlyStatisticsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0, 4);
        }

        public void displayStatisticsResults (Statistics driverStatistics) {
            // Round to two decimal places
            // double roundedDistance = Math.round(driverStatistics.getDistanceTravelled() * 100) / 100;

            switch (driverStatistics.getDistanceUnits()) {
                case km:
                    mDistanceDesc.setText(getResources().getString(R.string.kms));
                    break;
                case mi:
                    mDistanceDesc.setText(getResources().getString(R.string.miles));
                    break;
            }
            mTravelTimeDesc.setText(getResources().getString(R.string.travel_time));

            // Round with no decimal places
            if (driverStatistics.getDistanceTravelled() > 0)
                mDistanceTraveledText.setText(String.valueOf(Math.round(driverStatistics.getDistanceTravelled())));
            else
                mDistanceTraveledText.setText("");
            mTravelTimeText.setText(driverStatistics.getTravelTime().substring(0, driverStatistics.getTravelTime().length() - 3));
        }

        public void getMPG (String vehicleRegPlate) {
            GetMPGTask mpgTask = new GetMPGTask();
            mpgTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, String.valueOf(mActiveUser.getImeiByVehicleReg(vehicleRegPlate)));
        }

        public void displayPreviousView () {
//            if ((mCurrentDataTypeSelected == DataType.MONTH && mMonthlyDataAvailable) ||
//                    (mCurrentDataTypeSelected == DataType.WEEK && mWeeklyDataAvailable)) {
            if (mViewFlipper.getDisplayedChild() > 0) {
                mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_left_in));
                mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_left_out));
                mViewFlipper.showPrevious();
                mDisplayedViewOnViewFlipper = mViewFlipper.getDisplayedChild();
                animateFragment();
            }
//            }
        }

        public void displayNextView () {
//            if ((mCurrentDataTypeSelected == DataType.MONTH && mMonthlyDataAvailable) ||
//                    (mCurrentDataTypeSelected == DataType.WEEK && mWeeklyDataAvailable)) {
            if (mViewFlipper.getDisplayedChild() < mViewFlipper.getChildCount() - 1) {
                mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_right_in));
                mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_right_out));
                mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fling_anim_right_out));
                mViewFlipper.showNext();
                mDisplayedViewOnViewFlipper = mViewFlipper.getDisplayedChild();
                // if multiple of four and not zero
                if (mDisplayedViewOnViewFlipper % 4 == 0) {
                    switch (getCurrentDataTypeSelected()) {
                        case WEEK:
                            if (!mWeeklyStatistics.get(mDisplayedViewOnViewFlipper).isObjectBuilt()) {
                                mHomeProgressBackground.setVisibility(View.VISIBLE);
                                mWeeklyStatisticsTask = new GetWeeklyStatistics();
                                mWeeklyStatisticsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mDisplayedViewOnViewFlipper,
                                        mDisplayedViewOnViewFlipper + 4);
                            } else {
                                animateFragment();
                            }
                            break;
                        case MONTH:
                            if (!mMonthlyStatistics.get(mDisplayedViewOnViewFlipper).isObjectBuilt()) {
                                mHomeProgressBackground.setVisibility(View.VISIBLE);
                                mMonthlyStatisticsTask = new GetMonthlyStatistics();
                                mMonthlyStatisticsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mDisplayedViewOnViewFlipper,
                                        mDisplayedViewOnViewFlipper + 4);
                            } else {
                                animateFragment();
                            }
                            break;
                    }
                } else {
                    switch (getCurrentDataTypeSelected()) {
                        case WEEK:
                            if (mWeeklyStatistics.get(mDisplayedViewOnViewFlipper).getDriverScores() != null) {
                                animateFragment();
                                return;
                            }
                            break;
                        case MONTH:
                            if (mMonthlyStatistics.get(mDisplayedViewOnViewFlipper).getDriverScores() != null) {
                                animateFragment();
                                return;
                            }
                            break;
                    }
                    mHomeProgressBackground.setVisibility(View.VISIBLE);
                    Thread waitForResultsThread = new Thread() {
                        public void run() {
                            try {
                                sleep(1500);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mHomeProgressBackground.setVisibility(View.GONE);
                                        animateFragment();
                                    }
                                });
                            } catch (NullPointerException | InterruptedException ex) {
                                ex.printStackTrace();
                            }
                        }
                    };
                    waitForResultsThread.start();
                }
            }
//            }
        }

        private void animateFragment() {
            switch (mCurrentDataTypeSelected) {
                case WEEK:
                    try {
                        animateScoreCounting(0, mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getOverallScore(), false);
                        displayStatisticsResults(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()));
                        createCoverFlow(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()), true);
                        animateSpeedNeedle(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()));
                        drawSpeedometer(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()));
                        showAllFragment(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        clearAllFragment();
                    }
                    break;
                case MONTH:
                    try {
                        animateScoreCounting(0, mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getOverallScore(), false);
                        displayStatisticsResults(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()));
                        createCoverFlow(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()), true);
                        animateSpeedNeedle(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()));
                        drawSpeedometer(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()));
                        showAllFragment(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        clearAllFragment();
                    }
                    break;
            }
        }

        private class CustomGestureDetector extends GestureDetector.SimpleOnGestureListener {
            private final int SWIPE_MIN_DISTANCE = 120;
            private final int SWIPE_THRESHOLD_VELOCITY = 200;

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                try {
                    // Swipe right to left
                    if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                        displayPreviousView();
                    } else
                        // Swipe left to right
                        if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                            displayNextView();
                        }
                } catch (Exception e) {
                    System.out.println (e.getMessage());
                }

                return false;
            }
        }

        /**
         *
         *  Integer params - first and last month to load
         */
        private class GetWeeklyStatistics extends AsyncTask<Integer, Statistics, ApiResponse> {

            int mStartIndex, mEndIndex;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                clearAllFragment();
                mLastJourneyButton.setAlpha(0.6f);
                mLastJourneyButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });
            }

            @Override
            protected ApiResponse doInBackground(Integer... params) {
                mStartIndex = params[0];
                mEndIndex = params[1];
                ApiResponse apiResponse = null;
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetWeeklyStatistics");

                try {
                    URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetWeeklyStatistics);
                    HttpHost httpHost = new HttpHost (url.getHost(), url.getPort(), url.getProtocol());

                    for (int i = mStartIndex; i < mWeeklyStatistics.size(); i++) {
                        if (isCancelled() || i == mEndIndex)
                            break;
                        // Post JSON body
                        JSONObject getWeeklyStatsJSONObj = new JSONObject();
                        getWeeklyStatsJSONObj.put("IMEI", getSelectedIMEI());
                        getWeeklyStatsJSONObj.put("StartofWeek", "/Date(" + (mWeeklyStatistics.get(i).getStartDate().getTime()
                                + (Functions.getGMTOffset(new DateTime(mWeeklyStatistics.get(i).getStartDate().getTime())) * 3600000)) + ")/");
                        getWeeklyStatsJSONObj.put("UserID", mActiveUser.getUserID());

                        // Format dates
                        SimpleDateFormat signatureDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

                        // Parameters message to encrypt (IMEI, StartDate, userid, UserID)
                        String message = getSelectedIMEI() + signatureDateFormat.format(mWeeklyStatistics.get(i).getStartDate()) + String.valueOf(mActiveUser.getUserID())
                                + String.valueOf(mActiveUser.getUserID());
                        // Get the signature value
                        String signature = Functions.getHMACKey(message, preferences.getString("secretkey", ""));

                        HttpPost httpPost = new HttpPost (APIUrls.APIRootUrl + APIUrls.GetWeeklyStatistics + "userid="
                                + mActiveUser.getUserID() + "&signature=" + signature);

                        StringEntity se = new StringEntity(getWeeklyStatsJSONObj.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        httpPost.setEntity(se);

                        HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        String responseString = EntityUtils.toString(httpEntity);
                        JSONObject responseJSON = new JSONObject(responseString);
                        if (responseJSON.getString("Success").equals("true")) {
                            JSONArray driverScoresArray = responseJSON.getJSONArray("DriverScores");
                            // Get 7 days' driver scores
                            List<Integer> driverScores = new ArrayList<>();
                            for (int j = 0; j < driverScoresArray.length(); j++) {
                                driverScores.add(driverScoresArray.getInt(j));
                            }
                            mWeeklyStatistics.get(i).setDriverScores(driverScores);
                            mWeeklyStatistics.get(i).setOverallScore(responseJSON.getInt("WeeklyScore"));
                            mWeeklyStatistics.get(i).setDistanceTravelled(responseJSON.getDouble("DistanceTravelled"));
                            mWeeklyStatistics.get(i).setDistanceUnits(responseJSON.getString("DistanceUnits"));
                            mWeeklyStatistics.get(i).setTravelTime(responseJSON.getString("TravelTime"));
                            mWeeklyStatistics.get(i).setBestScore(responseJSON.getInt("BestJourneyScore"));
                            mWeeklyStatistics.get(i).setWorstScore(responseJSON.getInt("WorstJourneyScore"));

                            apiResponse = new ApiResponse(true, 0, "Success");
                            publishProgress(mWeeklyStatistics.get(i));
                        } else {
                            // If no matching data for this criteria - clear the data previously used
                            mWeeklyStatistics.get(i).setDriverScores(new ArrayList<Integer>());
                            mWeeklyStatistics.get(i).setOverallScore(0);
                            mWeeklyStatistics.get(i).setDistanceTravelled(0.0);
                            mWeeklyStatistics.get(i).setDistanceUnits("");
                            mWeeklyStatistics.get(i).setTravelTime("");
                            mWeeklyStatistics.get(i).setBestScore(0);
                            mWeeklyStatistics.get(i).setWorstScore(0);
                            // If the response has already been successful before - don't change it because there's data to show...
                            if (apiResponse != null) {
                                if (!apiResponse.getSuccess())
                                    apiResponse = new ApiResponse(false, responseJSON.getInt("ErrorCode"), responseJSON.getString("ErrorDescription"));
                            } else
                                apiResponse = new ApiResponse(false, responseJSON.getInt("ErrorCode"), responseJSON.getString("ErrorDescription"));
                        }
                    }
                    httpClient.close();
                    return apiResponse;
                } catch (MalformedURLException | UnsupportedEncodingException | JSONException ex) {
                    httpClient.close();
                    apiResponse = new ApiResponse(false, 1, ex.getMessage());
                    return apiResponse;
                } catch (IOException ex1) {
                    httpClient.close();
                    apiResponse = new ApiResponse(false, 1, ex1.getMessage());
                    return apiResponse;
                }
            }

            @Override
            protected void onProgressUpdate(Statistics... values) {
                super.onProgressUpdate(values);
                if (mHomeProgressBackground.getVisibility() == View.VISIBLE) {
                    if (mCurrentDataTypeSelected == DataType.WEEK) {
                        mHomeProgressBackground.setVisibility(View.GONE);
                        mWeeklyDataAvailable = true;
                        // If this statistics doesn't have proper results
                        // go to the first week with some
                        for (int i = 0; i < mWeeklyStatistics.size(); i++) {
                            if (mWeeklyStatistics.get(i).equals(values[0]) && i != mStartIndex)
                                if (mWeeklyStatistics.get(i).getDriverScores().size() > 0) {
                                    mViewFlipper.setDisplayedChild(i);

                                    // Display distance and time travelled
                                    mTravelTimeDesc.setText(getResources().getString(R.string.travel_time));
                                    if (mWeeklyStatistics.get(i).getDistanceUnits() == Statistics.DistanceUnits.km)
                                        mDistanceDesc.setText(R.string.kms);
                                    else if (mWeeklyStatistics.get(i).getDistanceUnits() == Statistics.DistanceUnits.mi)
                                        mDistanceDesc.setText(R.string.miles);

                                    values[0] = mWeeklyStatistics.get(i);
                                    break;
                                }
                        }

                        if (values[0].getDriverScores().size() > 0)
                            showAllFragment(true);
                        else
                            showAllFragment(false);
                        try {
                            // Driver's week general score - counting animation
                            animateScoreCounting(0, values[0].getOverallScore(), false);
                            // Set driver's distance traveled + travel time texts
                            displayStatisticsResults(values[0]);
                            // SpeedNeedle + Speedometer
                            animateSpeedNeedle(values[0]);
                            drawSpeedometer(values[0]);
                            // CoverFlow
                            createCoverFlow(values[0], true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            protected void onPostExecute(ApiResponse apiResponse) {
                super.onPostExecute(apiResponse);
                if (apiResponse != null) {
                    if (apiResponse.getSuccess()) {
                        mSelectedStatistics = mWeeklyStatistics.get(mViewFlipper.getDisplayedChild());

                        // if the statistics fragment is open, update the driver score from here:
                        // TODO: See if needs to be changed for Statistics and Last Journey

                        // Get last week with results && Last Journey
//                        for (int i = 0; i < mWeeklyStatistics.size(); i++) {
//                            boolean gotResults = false;
//                            Statistics currWeek = mWeeklyStatistics.get(i);
//                            int lastDayOfWeekWithResults = 0;
//                            try {
//                                for (int j = currWeek.getDriverScores().size() - 1; j >= 0; j--) {
//                                    if (currWeek.getDriverScores().get(j) > -1) {
//                                        lastDayOfWeekWithResults = j;
//                                        gotResults = true;
//                                        break;
//                                    }
//                                }
//                            } catch (NullPointerException ex) {
//                                ex.printStackTrace();
//                                continue;
//                            }
//                            if (gotResults) {
//                                GetLastJourney lastJourney = new GetLastJourney();
//                                lastJourney.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
//                                        new Date(currWeek.getStartDate().getTime() + (lastDayOfWeekWithResults * 86400000)), currWeek.getEndDate());
//                                break;
//                            }
//                        }
                    } else if (apiResponse.getErrorCode() != 1) {
                        // If the start index isn't 0 (first request)
//                        if (mStartIndex == 0) {
                        mWeeklyDataAvailable = false;
                        mMonthlyDataAvailable = false;
//                        }
                        // If the statistics fragment is open update the driver score from here:
                        // TODO: See if needs to be changed for Statistics
                        clearAllFragment();
                        mHomeProgressBackground.setVisibility(View.GONE);
                        if (mHomeFragment.isResumed() && mHomeFragment.isVisible())
                            Toast.makeText(getActivity(), apiResponse.getErrorDescription(), Toast.LENGTH_SHORT).show();
                    } else {
                        // If the start index isn't 0 (first request)
//                        if (mStartIndex == 0) {
                        mWeeklyDataAvailable = false;
                        mMonthlyDataAvailable = false;
//                        }

                        // if the statistics fragment is open update the driver score from here:
                        // TODO: See if needs to be changed for Statistics
                        clearAllFragment();
                        mHomeProgressBackground.setVisibility(View.GONE);
                        if (mHomeFragment.isResumed() && mHomeFragment.isVisible())
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_network_connection), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mWeeklyDataAvailable = false;
                    mMonthlyDataAvailable = false;
                }
            }
        }

        /**
         *
         *  Integer params - first and last month to load
         */
        private class GetMonthlyStatistics extends AsyncTask<Integer, Statistics, ApiResponse> {

            boolean wasAlreadyAnimated = false;
            int mStartIndex, mEndIndex;

            @Override
            protected ApiResponse doInBackground(Integer... params) {
                mStartIndex = params[0];
                mEndIndex = params[1];
                ApiResponse apiResponse = null;
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetMonthlyStatistics");

                try {
                    URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetMonthlyStatistics);
                    HttpHost httpHost = new HttpHost (url.getHost(), url.getPort(), url.getProtocol());

                    for (int i = mStartIndex; i < mMonthlyStatistics.size(); i++) {
                        if (isCancelled() || i == mEndIndex)
                            break;
                        // Post JSON body
                        JSONObject getMonthlyStatsJSONObj = new JSONObject();
                        getMonthlyStatsJSONObj.put("IMEI", getSelectedIMEI());
                        getMonthlyStatsJSONObj.put("StartofMonth", "/Date(" + (mMonthlyStatistics.get(i).getStartDate().getTime()
                                + (Functions.getGMTOffset(new DateTime(mMonthlyStatistics.get(i).getStartDate().getTime())) * 3600000)) + ")/");
                        getMonthlyStatsJSONObj.put("UserID", mActiveUser.getUserID());

                        // Format dates
                        SimpleDateFormat signatureDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

                        // Parameters message to encrypt (IMEI, StartDate, userid, UserID)
                        String message = getSelectedIMEI() + signatureDateFormat.format(mMonthlyStatistics.get(i).getStartDate())
                                + String.valueOf(mActiveUser.getUserID())
                                + String.valueOf(mActiveUser.getUserID());
                        // Get the signature value
                        String signature = Functions.getHMACKey(message, preferences.getString("secretkey", ""));

                        HttpPost httpPost = new HttpPost (APIUrls.APIRootUrl + APIUrls.GetMonthlyStatistics + "userid="
                                + mActiveUser.getUserID() + "&signature=" + signature);

                        StringEntity se = new StringEntity(getMonthlyStatsJSONObj.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        httpPost.setEntity(se);

                        HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        String responseString = EntityUtils.toString(httpEntity);
                        JSONObject responseJSON = new JSONObject(responseString);
                        if (responseJSON.getString("Success").equals("true")) {
                            JSONArray driverScoresArray = responseJSON.getJSONArray("DriverScores");
                            // Get daily driver scores for the month
                            List<Integer> driverScores = new ArrayList<>();
                            for (int j = 0; j < driverScoresArray.length(); j++) {
                                driverScores.add(driverScoresArray.getInt(j));
                            }
                            mMonthlyStatistics.get(i).setDriverScores(driverScores);
                            mMonthlyStatistics.get(i).setOverallScore(responseJSON.getInt("MonthlyScore"));
                            mMonthlyStatistics.get(i).setDistanceTravelled(responseJSON.getDouble("DistanceTravelled"));
                            mMonthlyStatistics.get(i).setDistanceUnits(responseJSON.getString("DistanceUnits"));
                            mMonthlyStatistics.get(i).setTravelTime(responseJSON.getString("TravelTime"));
                            mMonthlyStatistics.get(i).setBestScore(responseJSON.getInt("BestJourneyScore"));
                            mMonthlyStatistics.get(i).setWorstScore(responseJSON.getInt("WorstJourneyScore"));

                            apiResponse = new ApiResponse(true, 0, "Success");
                            publishProgress(mMonthlyStatistics.get(i));
                        } else {
                            // If no matching data for this criteria - clear the data previously used
                            mMonthlyStatistics.get(i).setDriverScores(new ArrayList<Integer>());
                            mMonthlyStatistics.get(i).setOverallScore(0);
                            mMonthlyStatistics.get(i).setDistanceTravelled(0.0);
                            mMonthlyStatistics.get(i).setDistanceUnits("");
                            mMonthlyStatistics.get(i).setTravelTime("");
                            mMonthlyStatistics.get(i).setBestScore(0);
                            mMonthlyStatistics.get(i).setWorstScore(0);
                            // If the response has already been successful before - don't change it because there's data to show...
                            if (apiResponse != null) {
                                if (!apiResponse.getSuccess())
                                    apiResponse = new ApiResponse(false, responseJSON.getInt("ErrorCode"), responseJSON.getString("ErrorDescription"));
                            } else
                                apiResponse = new ApiResponse(false, responseJSON.getInt("ErrorCode"), responseJSON.getString("ErrorDescription"));
                        }
                    }
                    httpClient.close();
                    return apiResponse;
                } catch (MalformedURLException | UnsupportedEncodingException | JSONException ex) {
                    httpClient.close();
                    apiResponse = new ApiResponse(false, 1, ex.getMessage());
                    return apiResponse;
                } catch (IOException ex1) {
                    httpClient.close();
                    apiResponse = new ApiResponse(false, 1, ex1.getMessage());
                    return apiResponse;
                }
            }

            @Override
            protected void onProgressUpdate(Statistics... values) {
                super.onProgressUpdate(values);
                mMonthlyDataAvailable = true;
                if (mCurrentDataTypeSelected == DataType.MONTH && !wasAlreadyAnimated) {
                    if (mHomeProgressBackground.getVisibility() == View.VISIBLE)
                        mHomeProgressBackground.setVisibility(View.GONE);
                    wasAlreadyAnimated = true;

                    // If this statistics doesn't have proper results
                    // go to the first month with some
                    for (int i = 0; i < mMonthlyStatistics.size(); i++) {
                        if (mMonthlyStatistics.get(i).equals(values[0]) && i != mStartIndex)
                            if (mMonthlyStatistics.get(i).getDriverScores().size() > 0) {
                                mViewFlipper.setDisplayedChild(i);

                                // Display distance and time travelled
                                mTravelTimeDesc.setText(getResources().getString(R.string.travel_time));
                                if (mMonthlyStatistics.get(i).getDistanceUnits() == Statistics.DistanceUnits.km)
                                    mDistanceDesc.setText(R.string.kms);
                                else if (mMonthlyStatistics.get(i).getDistanceUnits() == Statistics.DistanceUnits.mi)
                                    mDistanceDesc.setText(R.string.miles);

                                values[0] = mMonthlyStatistics.get(i);

                                break;
                            }
                    }
                    if (values[0].getDriverScores().size() > 0)
                        showAllFragment(true);
                    else
                        showAllFragment(false);
                    try {
                        // Driver's week general score - counting animation
                        animateScoreCounting(0, values[0].getOverallScore(), false);
                        // Set driver's distance traveled + travel time texts
                        displayStatisticsResults(values[0]);
                        // SpeedNeedle + Speedometer
                        animateSpeedNeedle(values[0]);
                        drawSpeedometer(values[0]);
                        // CoverFlow
                        createCoverFlow(values[0], true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            protected void onPostExecute(ApiResponse apiResponse) {
                super.onPostExecute(apiResponse);
                if (mHomeProgressBackground.getVisibility() == View.VISIBLE)
                    mHomeProgressBackground.setVisibility(View.GONE);
                if (apiResponse != null) {
                    if (apiResponse.getSuccess()) {
                        mSelectedStatistics = mMonthlyStatistics.get(mViewFlipper.getDisplayedChild());
                    } else if (apiResponse.getErrorCode() != 1) {
                        mMonthlyDataAvailable = false;
                        mWeeklyDataAvailable = false;

                        // if the statistics fragment is open update the driver score from here:
                        // TODO: See if needs to be changed for Statistics
                        clearAllFragment();
                        if (mHomeFragment.isResumed() && mHomeFragment.isVisible())
                            Toast.makeText(getActivity(), apiResponse.getErrorDescription(), Toast.LENGTH_SHORT).show();
                    } else {
                        mMonthlyDataAvailable = false;
                        mWeeklyDataAvailable = false;

                        clearAllFragment();
                        if (mHomeFragment.isResumed() && mHomeFragment.isVisible())
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_network_connection), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mMonthlyDataAvailable = false;
                    mWeeklyDataAvailable = false;
                }
            }
        }

        private class GetMPGTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetMPG");

                try {
                    URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetMpg);
                    HttpHost httpHost = new HttpHost (url.getHost(), url.getPort(), url.getProtocol());

                    // Post parameters
                    List<NameValuePair> bodyParameters = new ArrayList<>(2);
                    bodyParameters.add(new BasicNameValuePair("UserID", String.valueOf(mActiveUser.getUserID())));
                    bodyParameters.add(new BasicNameValuePair("IMEI", params[0]));

                    // Parameters message to encrypt (IMEI, UserID)
                    String message = params[0] + String.valueOf(mActiveUser.getUserID());
                    // Get the signature value
                    String signature = Functions.getHMACKey(message, preferences.getString("secretkey", ""));

                    HttpPost httpPost = new HttpPost (APIUrls.APIRootUrl + APIUrls.GetMpg + "userid="
                            + mActiveUser.getUserID() + "&signature=" + signature);
                    httpPost.setEntity(new UrlEncodedFormEntity(bodyParameters));

                    HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    String responseString = EntityUtils.toString(httpEntity);
                    JSONObject jsonObject = new JSONObject(responseString);
                    JSONObject mpgObject = jsonObject.getJSONObject("MPG");

                    // return the mpg urban value
                    httpClient.close();
                    return String.valueOf(mpgObject.getDouble("UrbanMPG"));
                } catch (MalformedURLException | UnsupportedEncodingException | JSONException ex) {
                    httpClient.close();
                    return "";
                } catch (IOException ex1) {
                    httpClient.close();
                    return ex1.getMessage();
                }
            }

            @Override
            protected void onPostExecute(String mpg) {
                super.onPostExecute(mpg);
                if (!mpg.equals("")) {
                    mMPGTextView.setText (mpg);
                }
            }
        }

        private class SetMPGTask extends AsyncTask<String, Void, Boolean> {
            @Override
            protected Boolean doInBackground(String... params) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                AndroidHttpClient httpClient = AndroidHttpClient.newInstance("SetMPG");

                try {
                    URL url = new URL(APIUrls.APIRootUrl + APIUrls.SetMpg);
                    HttpHost httpHost = new HttpHost (url.getHost(), url.getPort(), url.getProtocol());

                    // Post JSON body
                    JSONObject setMpgJSONObject = new JSONObject();
                    setMpgJSONObject.put("IMEI", params[0]);
                    JSONObject mpgObject = new JSONObject();
                    mpgObject.put("UrbanMPG", params[1]);
                    mpgObject.put("ExtraUrbanMPG", params[2]);
                    mpgObject.put("CombinedMPG", params[3]);
                    setMpgJSONObject.put("MPG", mpgObject);

                    // Parameters message to encrypt (CombinedMPG, ExtraUrbanMPG, IMEI, UrbanMPG, UserID)
                    String message = params[3] + params[2] + params[0] + params[1] + String.valueOf(mActiveUser.getUserID());
                    // Get the signature value
                    String signature = Functions.getHMACKey(message, preferences.getString("secretkey", ""));

                    HttpPost httpPost = new HttpPost (APIUrls.APIRootUrl + APIUrls.SetMpg + "userid="
                            + mActiveUser.getUserID() + "&signature=" + signature);
                    StringEntity se = new StringEntity(setMpgJSONObject.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httpPost.setEntity(se);

                    HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);

                    HttpEntity httpEntity = httpResponse.getEntity();
                    String responseString = EntityUtils.toString(httpEntity);
                    JSONObject responseJSON = new JSONObject(responseString);

                    httpClient.close();

                    if (responseJSON.getString("Success").equals("true"))
                        return true;
                    else
                        return false;
                } catch (MalformedURLException | UnsupportedEncodingException | JSONException ex) {
                    httpClient.close();
                    return false;
                } catch (IOException ex1) {
                    httpClient.close();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean success) {
                super.onPostExecute(success);
                if (success)
                    Toast.makeText(getActivity(), "MPG values have been changed...", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getActivity(), "Something went wrong setting the MPG values...", Toast.LENGTH_SHORT).show();
            }
        }

        private class GetLastJourney extends AsyncTask<Date, Void, Boolean> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Boolean doInBackground(Date... params) {
                AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetJourneys");

                try {
                    URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetJourneys);
                    HttpHost httpHost = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());

                    // Dates format for signature
                    SimpleDateFormat signatureDateFormat = new SimpleDateFormat("yyyyMMdd");

                    DateTime endDateTime = new DateTime(params[1].getTime());
                    // One day's offset (to be sure we get some data)
                    endDateTime = endDateTime.plusDays(1);

                    // Post JSON body
                    JSONObject getJourneysJSONObj = new JSONObject();
                    getJourneysJSONObj.put("UserID", mActiveUser.getUserID());
                    getJourneysJSONObj.put("IMEI", mSelectedIMEI);
                    getJourneysJSONObj.put("StartDate", "/Date(" + params[0].getTime() + ")/");

                    int startDateMillisecondsOffset = Functions.getGMTOffset(new DateTime(params[0])) * 3600000;

                    String signatureStartDate = signatureDateFormat.format(new Date (params[0].getTime() - startDateMillisecondsOffset));

                    // endDateTime at midnight + offset
                    endDateTime = new DateTime (endDateTime.getMillis()).withTimeAtStartOfDay();
                    int endDateMillisecondsOffset = Functions.getGMTOffset(endDateTime) * 3600000;

                    getJourneysJSONObj.put("EndDate", "/Date(" + endDateTime.plusMillis(endDateMillisecondsOffset).getMillis() + ")/");
                    String signatureEndDate = signatureDateFormat.format(new Date (endDateTime.getMillis()));

                    // Parameters message to encrypt (EndDate, IMEI, StartDate, userid, UserID)
                    String message = signatureEndDate + "000000" + mSelectedIMEI +
                            signatureStartDate + "000000" + String.valueOf(mActiveUser.getUserID()) +
                            String.valueOf(mActiveUser.getUserID());
                    // Get the signature value
                    String signature = Functions.getHMACKey(message, mUserPreferences.getString("secretkey", ""));

                    HttpPost httpPost = new HttpPost(APIUrls.APIRootUrl + APIUrls.GetJourneys + "userid="
                            + mActiveUser.getUserID() + "&signature=" + signature);
                    StringEntity se = new StringEntity(getJourneysJSONObj.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httpPost.setEntity(se);

                    HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    String responseString = EntityUtils.toString(httpEntity);
                    JSONObject responseJSON = new JSONObject(responseString);

                    if (responseJSON.getString("Success").equals("true")) {
                        if (responseJSON.getJSONArray("Journeys").length() > 0) {
                            JSONArray journeysArray = responseJSON.getJSONArray("Journeys");
                            JSONObject journey = journeysArray.getJSONObject(journeysArray.length() - 1);
                            List<JourneyEvent> journeyEvents = new ArrayList<>();
                            JSONArray journeyEventsJSONArray = journey.getJSONArray("JourneyEvents");
                            DateTime journeyStart = null, journeyEnd = null;
                            Duration journeyDuration = null;
                            String startAddress = "", endAddress = "";
                            for (int j = 0; j < journeyEventsJSONArray.length(); j++) {
                                try {
                                    JSONObject journeyEvent = journeyEventsJSONArray.getJSONObject(j);
                                    DateTime eventTime = new DateTime(journeyEvent.getString("EventTime"));
                                    JSONObject locationJSON = journeyEvent.getJSONObject("Location");
                                    JourneyEvent event = new JourneyEvent(JourneyEvent.EventType.valueOf(journeyEvent.getString("EventType")),
                                            new Date(eventTime.getMillis()),
                                            new LatLng(locationJSON.getDouble("Latitude"), locationJSON.getDouble("Longitude")),
                                            journeyEvent.getDouble("EventSpeed"),
                                            journeyEvent.getInt("SpeedLimit"),
                                            Statistics.DistanceUnits.valueOf(journeyEvent.getString("SpeedUnits")),
                                            journeyEvent.getString("StreetAddress"),
                                            journeyEvent.getInt("Bearing"));
                                    if (event.getEventType() == JourneyEvent.EventType.JOURNEY_START_EVENT) {
                                        journeyStart = new DateTime(event.getEventTime().getTime());
                                        startAddress = event.getStreetAddress();
                                    }
                                    if (event.getEventType() == JourneyEvent.EventType.JOURNEY_END_EVENT) {
                                        journeyEnd = new DateTime(event.getEventTime().getTime());
                                        endAddress = event.getStreetAddress();
                                    }
                                    journeyEvents.add(event);
                                } catch (Exception ex) {
                                    ex.printStackTrace ();
                                }
                            }
                            if (journeyStart != null && journeyEnd != null)
                                journeyDuration = new Duration(journeyStart.getMillis(), journeyEnd.getMillis());
                                // If the API didn't return any JOURNEY_START_EVENT or JOURNEY_END_EVENT - Go for the
                                // earliest and the latest event on the array of events
                            else {
                                if (journeyStart == null) {
                                    JourneyEvent event = Functions.getEarliestEvent(journeyEvents);
                                    journeyStart = new DateTime(event.getEventTime().getTime());
                                    startAddress = event.getStreetAddress();
                                }
                                if (journeyEnd == null) {
                                    JourneyEvent event = Functions.getLatestEvent(journeyEvents);
                                    journeyEnd = new DateTime(event.getEventTime().getTime());
                                    endAddress = event.getStreetAddress();
                                }
                                journeyDuration = new Duration(journeyStart.getMillis(), journeyEnd.getMillis());
                            }
                            mLastJourney = new Journey(journeyEvents, journey.getDouble("DistanceTravelled"),
                                    Statistics.DistanceUnits.valueOf(journey.getString("DistanceUnits")),
                                    journey.getInt("RiskScore"),
                                    journey.getInt("AccelScore"),
                                    journey.getInt("DecelScore"),
                                    journey.getInt("SpeedScore"),
                                    journey.getString("IdlingTime"),
                                    journeyStart, journeyEnd, journeyDuration, startAddress, endAddress);
                            httpClient.close();
                            return true;
                        }
                    }
                    httpClient.close();
                    return false;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    httpClient.close();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean success) {
                super.onPostExecute(success);
                mHomeProgressBackground.setVisibility(View.GONE);
                if (success) {
                    mLastJourneyButton.setAlpha(1.0f);
                    mLastJourneyButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JourneyDetailsFragment rmf = new JourneyDetailsFragment();
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("Journey", mLastJourney);
                            bundle.putInt("MarkerType", 0);
                            rmf.setArguments(bundle);

                            FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
                            transaction.replace(R.id.container, rmf, "journeymap");
                            transaction.addToBackStack("journeymap");
                            transaction.commit();
                        }
                    });
                } else {
                    mLastJourneyButton.setAlpha(0.6f);
                    mLastJourneyButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.last_journey_not_available), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }

        /**
         *
         * @param from minimum value
         * @param to maximum value
         * @param wait make animation wait (half a second) for the Fragment to load after
         *             user unlocked the screen
         */
        public void animateScoreCounting (int from, int to, boolean wait) {
            ValueAnimator animator = new ValueAnimator();
            animator.setIntValues(from, to);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    mDriverScoreTextView.setText(String.valueOf(animation.getAnimatedValue()));
                }
            });
            animator.setEvaluator(new TypeEvaluator<Integer>() {
                @Override
                public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
                    return Math.round((endValue - startValue) * fraction);
                }
            });
            animator.setDuration(1000);
            try {
                if (wait) {
                    synchronized (animator) {
                        animator.wait(500);
                        animator.start();
                    }
                }
                else
                    animator.start();
            } catch (InterruptedException ex) {
                System.out.println(ex.getMessage());
            }
        }

        public void clearSpeedometer () {
            mSpeedoGraphics.setVisibility(View.GONE);
            mDriverScoreTextView.setVisibility(View.GONE);
            mSpeedoNeedleImage.setVisibility(View.GONE);
        }

        public void showSpeedometer (boolean changeVisibility) {
            if (changeVisibility) {
                mSpeedoGraphics.setVisibility(View.VISIBLE);
                mDriverScoreTextView.setVisibility(View.VISIBLE);
                mSpeedoNeedleImage.setVisibility(View.VISIBLE);
            } else {
                drawSpeedometer (mSelectedStatistics);
            }
        }

        public void clearAllFragment () {
            mWeeklyDataAvailable = false;
            mMonthlyDataAvailable = false;
            mDriverScoresCoverflow.setVisibility(View.GONE);
            mDriverScoreTextView.setText("");
            mDistanceTraveledText.setText("");
            mTravelTimeText.setText("");
            clearSpeedometer();
        }

        public void showAllFragment (boolean changeVisibility) {
            mWeeklyDataAvailable = true;
            mMonthlyDataAvailable = true;
            if (changeVisibility) {
                mDriverScoresCoverflow.setVisibility(View.VISIBLE);
            } else {
                if (mSelectedStatistics.getDriverScores() == null)
                    mSelectedStatistics.setDriverScores(new ArrayList<Integer>());
                createCoverFlow(mSelectedStatistics, true);
            }
            showSpeedometer(changeVisibility);
        }

        private void startJourneysFragment () {
            JourneysFragment journeysFragment = new JourneysFragment();

            Bundle argsBundle = new Bundle ();
            if (mCurrentDataTypeSelected == DataType.WEEK) {
                ArrayList<Integer> driverScores = new ArrayList<>
                        (mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getDriverScores().size());
                driverScores.addAll(mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getDriverScores());
                argsBundle.putIntegerArrayList("DriverScores", driverScores);
                argsBundle.putString("DataType", String.valueOf(mCurrentDataTypeSelected));
                argsBundle.putLong("StartDate", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getStartDate().getTime());
                argsBundle.putInt("BestScore", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getBestScore());
                argsBundle.putInt("WorstScore", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getWorstScore());
                argsBundle.putDouble("TravelledDistance", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getDistanceTravelled());
                argsBundle.putString("TravelUnits", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getDistanceUnits().toString());
                argsBundle.putString("TimeTravelled", mWeeklyStatistics.get(mViewFlipper.getDisplayedChild()).getTravelTime());
            } else if (mCurrentDataTypeSelected == DataType.MONTH) {
                ArrayList<Integer> driverScores = new ArrayList<>
                        (mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getDriverScores().size());
                driverScores.addAll(mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getDriverScores());
                argsBundle.putIntegerArrayList("DriverScores", driverScores);
                argsBundle.putString("DataType", String.valueOf(mCurrentDataTypeSelected));
                argsBundle.putLong("StartDate", mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getStartDate().getTime());
                argsBundle.putInt("BestScore", mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getBestScore());
                argsBundle.putInt("WorstScore", mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getWorstScore());
                argsBundle.putDouble("TravelledDistance", mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getDistanceTravelled());
                argsBundle.putString("TravelUnits", mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getDistanceUnits().toString());
                argsBundle.putString("TimeTravelled", mMonthlyStatistics.get(mViewFlipper.getDisplayedChild()).getTravelTime());
            }

            argsBundle.putInt("SelectedDay", mDriverScoresCoverflow.getSelectedItemPosition());
            journeysFragment.setArguments(argsBundle);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.container, journeysFragment, "journeys");
            transaction.addToBackStack("journeys");
            transaction.commit();
        }

        public void setSelectedIMEI (String imei) {
            mSelectedIMEI = imei;
        }
        public String getSelectedIMEI () {
            return mSelectedIMEI;
        }
        public DataType getCurrentDataTypeSelected () {
            return mCurrentDataTypeSelected;
        }
    }}