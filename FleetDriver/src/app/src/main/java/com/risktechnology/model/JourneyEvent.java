package com.risktechnology.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

/**
 * Created by ricardo on 01/04/2015.
 */

public class JourneyEvent implements Parcelable {
    public enum EventType {
        NO_EVENT,
        TIMED_EVENT,
        DISTANCE_EVENT,
        JOURNEY_START_EVENT,
        JOURNEY_END_EVENT,
        TAMPER_EVENT,
        THEFT_ATTEMPT_EVENT,
        ALARM_ACTIVATED_EVENT,
        ALARM_DEACTIVATED_EVENT,
        SHIELD_BREACH_EVENT,
        PANIC_EVENT,
        IDLE_START_EVENT,
        EXCESSIVE_IDLE_START_EVENT,
        OVERSPEED_EVENT,
        UNDERSPEED_EVENT,
        ABOVE_LOG_THRESHOLD_EVENT,
        GEOFENCE_IN_EVENT,
        GEOFENCE_OUT_EVENT,
        POWER_LOSS_EVENT,
        POWER_RESTORE_EVENT,
        ALARM_CLEAR_EVENT,
        SOC_TAG_ENTRY_EVENT,
        SOC_TAG_EXIT_EVENT,
        TAG_ENTRY_EVENT,
        TAG_EXIT_EVENT,
        HEARTBEAT_EVENT,
        ABOVE_DATA_THRESHOLD_EVENT,
        IDLE_END_EVENT,
        ON_SITE_EVENT,
        IMMOB_CONFIRM_EVENT,
        DISARM_SOC_EVENT,
        REARM_SOC_EVENT,
        T4_INPUT0_GOES_HIGH,
        T4_INPUT0_GOES_LOW,
        T4_INPUT1_GOES_HIGH,
        T4_INPUT1_GOES_LOW,
        T4_INPUT2_GOES_HIGH,
        T4_INPUT2_GOES_LOW,
        T4_INPUT3_GOES_HIGH,
        T4_INPUT3_GOES_LOW,
        IGNITION_ON,
        IGNITION_OFF,
        IO_EVENT,
        PASSTHROUGH_MESSAGE,
        CAN_PTO_ON,
        CAN_PTO_OFF,
        CAN_IDLE_END,
        CAN_IDLE_START,
        XDTS4_EV_ACCEL,
        HEADING_CHANGE,
        RPM_THRESHOLD_EXCEEDED,
        ACCEL_THRESHOLD_EXCEEDED,
        DECEL_THRESHOLD_EXCEEDED,
        GSM_POSITION,
        RF_BEACON,
        FREE_WHEELING,
        TRIP_ANALYSIS_RPM,
        TRIP_ANALYSIS_SPEED,
        TRIP_ANALYSIS_SUM,
        DEVICE_PLUG_IN,
        DEVICE_REMOVAL,
        GPS_POSITION,
        ACCIDENT,
        RECKLESS_DRIVING,
        PASSENGER_STOP,
        CRUISE_START,
        CRUISE_STOP,
        COASTING_START,
        COASTING_STOP,
        EXCESS_TORQUE_START,
        EXCESS_TORQUE_STOP,
        JOURNEY_START_WEIGHT,
        JOURNEY_END_WEIGHT,
        WEIGHT_STATUS_ALERT,
        STABILITY_ERROR,
        JOURNEY_WEIGHT,
        ODB_PROTOCOL_FOUND,
        ODB_PROTOCOL_NOT_FOUND,
        MIL_FAULT,
        MIL_FAULT_CLEARED,
        ACCEL_CALIBRATION_COMPLETE,
        TOW_START,
        TOW_END,
        TOW_TIMED,
        MID_ACCEL_EXCEEDED,
        MID_DECELL_EXCEEDED,
        MAX_ACCEL_EXCEEDED,
        MAX_DECELL_EXCEEDED,
        VALET_MODE_DIST,
        VALET_MODE_SPEED,
        POLY_TEST_IN,
        POLY_TEST_OUT,
        BATTERY_LOW,
        CRASH_EVENT,
        CRASH_DATA,
        VERSION_DATA,
        SUPPORTED_PIDS,
        DEVICE_REBOOT,
        PASSENGER_JOURNEY_END,
        LEFT_CORNERING_EVENT,
        RIGHT_CORNERING_EVENT,
        NOVATEL_HARSH_CORNERING_EVENT,
        TEMPERATURE_VALUES,
        LOW_TEMPERATURE_ALERT,
        HIGH_TEMPERATURE_ALERT,
        TEMPERATURE_NORMAL_ALERT,
        RESCHEDULED_KEYFOB_MSG,
        DONE_JOB_KEYFOB_MSG,
        CANCELLED_KEYFOB_MSG,
        PULL_TO_WORKSHOP_KEYFOB_MSG,
        REQUIRES_SPARE_PARTS_KEYFOB_MSG,
        EXCESSIVE_THROTTLE,
        ONGOING_SPEED_EVENT,
        LOC_BEFORE_ACCELEROMETER_EVENT,
        STOP_START_JOURNEY_END,
        BUSINESS_EVENT,
        STOP_START_IDLE_END,
        JNY_HEARTBEAT,
        OBD_DISCOVERY_TIMEOUT,
        FOTA_COMPLETE,
        OBD_SERVICE_DATA,
        CRASH_TRANSFER_COMPLETE,
        POWER_UP
    }

    private EventType mEventType;
    private Date mEventTime;
    private LatLng mLocation;
    private double mEventSpeed;
    private int mSpeedLimit;
    private Statistics.DistanceUnits mSpeedUnits;
    private String mStreetAddress;
    private int mBearing;

    public JourneyEvent() {}

    public JourneyEvent(EventType mEventType, Date mEventTime, LatLng mLocation, double mEventSpeed, int mSpeedLimit, Statistics.DistanceUnits mSpeedUnits, String mStreetAddress, int mBearing) {
        this.mEventType = mEventType;
        this.mEventTime = mEventTime;
        this.mLocation = mLocation;
        this.mEventSpeed = mEventSpeed;
        this.mSpeedLimit = mSpeedLimit;
        this.mSpeedUnits = mSpeedUnits;
        this.mStreetAddress = mStreetAddress;
        this.mBearing = mBearing;
    }

    public EventType getEventType() {
        return mEventType;
    }

    public void setEventType(EventType mEventType) {
        this.mEventType = mEventType;
    }

    public Date getEventTime() {
        return mEventTime;
    }

    public void setEventTime(Date mEventTime) {
        this.mEventTime = mEventTime;
    }

    public LatLng getLatLng() {
        return mLocation;
    }

    public void setLatLng(LatLng mLocation) {
        this.mLocation = mLocation;
    }

    public double getEventSpeed() {
        return mEventSpeed;
    }

    public void setEventSpeed(double mEventSpeed) {
        this.mEventSpeed = mEventSpeed;
    }

    public int getSpeedLimit() {
        return mSpeedLimit;
    }

    public void setSpeedLimit(int mSpeedLimit) {
        this.mSpeedLimit = mSpeedLimit;
    }

    public Statistics.DistanceUnits getSpeedUnits() {
        return mSpeedUnits;
    }

    public void setSpeedUnits(Statistics.DistanceUnits mSpeedUnits) {
        this.mSpeedUnits = mSpeedUnits;
    }

    public String getStreetAddress() {
        return mStreetAddress;
    }

    public void setStreetAddress(String mStreetAddress) {
        this.mStreetAddress = mStreetAddress;
    }

    public int getBearing () {
        return mBearing;
    }

    public void setBearing (int mBearing) {
        this.mBearing = mBearing;
    }

    protected JourneyEvent(Parcel in) {
        mEventType = (EventType) in.readValue(EventType.class.getClassLoader());
        long tmpMEventTime = in.readLong();
        mEventTime = tmpMEventTime != -1 ? new Date(tmpMEventTime) : null;
        mLocation = (LatLng) in.readValue(LatLng.class.getClassLoader());
        mEventSpeed = in.readDouble();
        mSpeedLimit = in.readInt();
        mSpeedUnits = (Statistics.DistanceUnits) in.readValue(Statistics.DistanceUnits.class.getClassLoader());
        mStreetAddress = in.readString();
        mBearing = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mEventType);
        dest.writeLong(mEventTime != null ? mEventTime.getTime() : -1L);
        dest.writeValue(mLocation);
        dest.writeDouble(mEventSpeed);
        dest.writeInt(mSpeedLimit);
        dest.writeValue(mSpeedUnits);
        dest.writeString(mStreetAddress);
        dest.writeInt(mBearing);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<JourneyEvent> CREATOR = new Parcelable.Creator<JourneyEvent>() {
        @Override
        public JourneyEvent createFromParcel(Parcel in) {
            return new JourneyEvent(in);
        }

        @Override
        public JourneyEvent[] newArray(int size) {
            return new JourneyEvent[size];
        }
    };
}