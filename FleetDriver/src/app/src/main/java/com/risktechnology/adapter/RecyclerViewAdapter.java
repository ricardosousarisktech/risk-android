package com.risktechnology.adapter;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.risktechnology.model.Journey;
import com.risktechnology.riskmobileapp.JourneyDetailsFragment;
import com.risktechnology.riskmobileapp.R;
import com.risktechnology.utils.Functions;
import com.risktechnology.utils.GlobalValues;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by ricardo on 31/03/2015.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private List<Journey> mJourneys;

    public RecyclerViewAdapter(Context context, List<Journey> journeys){
        mContext = context;
        mJourneys = journeys;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.journeys_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(mContext, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (mJourneys.get(position).getRiskScore() == 0) {
            holder.progressBar.setVisibility(View.VISIBLE);
            clearCard (holder);
        } else {
            holder.progressBar.setVisibility(View.GONE);
            try {
                holder.journeyStartAddress.setText(mJourneys.get(position).getStartAddress().split(",")[0]
                        + "\n" + mJourneys.get(position).getStartAddress().split(",")[1]);
                holder.journeyStartPostCode.setText(mJourneys.get(position).getStartAddress().split(",")[2]);
                holder.journeyEndAddress.setText(mJourneys.get(position).getEndAddress().split(",")[0]
                        + "\n" + mJourneys.get(position).getEndAddress().split(",")[1]);
                holder.journeyEndPostCode.setText(mJourneys.get(position).getEndAddress().split(",")[2]);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            DateTime startTime = new DateTime(mJourneys.get(position).getStartTime().getMillis() -
                    (Functions.getGMTOffset(mJourneys.get(position).getStartTime()) * 3600000));
            DateTime.Property dayOfWeek = startTime.dayOfWeek();
            DecimalFormat format = new DecimalFormat("00");
            holder.date.setText(dayOfWeek.getAsText(Locale.getDefault()).toUpperCase() + " " +
                    format.format(mJourneys.get(position).getStartTime().getDayOfMonth()) + "/" +
                    format.format(mJourneys.get(position).getStartTime().getMonthOfYear()));
            holder.startTime.setText(format.format(startTime.getHourOfDay()) + ":" +
                    format.format(startTime.getMinuteOfHour()));
            DateTime endTime = new DateTime(mJourneys.get(position).getFinishTime().getMillis() -
                    (Functions.getGMTOffset(mJourneys.get(position).getFinishTime()) * 3600000));
            holder.endTime.setText(format.format(endTime.getHourOfDay()) + ":" +
                    format.format(endTime.getMinuteOfHour()));
            holder.driverScore.setText(String.valueOf(mJourneys.get(position).getRiskScore()));
            holder.brakingScore.setText(String.valueOf(mJourneys.get(position).getDecelScore()));
            holder.accelerationScore.setText(String.valueOf(mJourneys.get(position).getAccelScore()));
            holder.speedScore.setText(String.valueOf(mJourneys.get(position).getSpeedScore()));
            holder.journeyDistanceTravelled.setText(String.valueOf((double) Math.round(mJourneys.get(position).getDistanceTravelled() * 10) / 10)
                    + mJourneys.get(position).getDistanceUnits().toString().toUpperCase());
            // Get minutes of the hour:
            long minutes = mJourneys.get(position).getJourneyDuration().getStandardMinutes() -
                    (60 * mJourneys.get(position).getJourneyDuration().getStandardHours());
            holder.journeyTravelTime.setText(format.format(mJourneys.get(position).getJourneyDuration().getStandardHours())
                    + ":" + format.format(minutes));

            if (mJourneys.get(position).getRiskScore() >= GlobalValues.GoodScore)
                holder.driverScore.setBackgroundResource(R.drawable.good_score_ring_button);
            else if (mJourneys.get(position).getRiskScore() <= GlobalValues.MediumScoreMax
                    && mJourneys.get(position).getRiskScore() >= GlobalValues.MediumScoreMin)
                holder.driverScore.setBackgroundResource(R.drawable.medium_score_ring_button);
            else if (mJourneys.get(position).getRiskScore() <= GlobalValues.BadScore)
                holder.driverScore.setBackgroundResource(R.drawable.bad_score_ring_button);

            if (mJourneys.get(position).getDecelScore() >= GlobalValues.GoodScore)
                holder.brakingScore.setBackgroundResource(R.drawable.good_score_small_ring);
            else if (mJourneys.get(position).getDecelScore() <= GlobalValues.MediumScoreMax
                    && mJourneys.get(position).getDecelScore() >= GlobalValues.MediumScoreMin)
                holder.brakingScore.setBackgroundResource(R.drawable.medium_score_small_ring);
            else if (mJourneys.get(position).getDecelScore() <= GlobalValues.BadScore)
                holder.brakingScore.setBackgroundResource(R.drawable.bad_score_small_ring);

            if (mJourneys.get(position).getAccelScore() >= GlobalValues.GoodScore)
                holder.accelerationScore.setBackgroundResource(R.drawable.good_score_small_ring);
            else if (mJourneys.get(position).getAccelScore() <= GlobalValues.MediumScoreMax
                    && mJourneys.get(position).getAccelScore() >= GlobalValues.MediumScoreMin)
                holder.accelerationScore.setBackgroundResource(R.drawable.medium_score_small_ring);
            else if (mJourneys.get(position).getAccelScore() <= GlobalValues.BadScore)
                holder.accelerationScore.setBackgroundResource(R.drawable.bad_score_small_ring);

            if (mJourneys.get(position).getSpeedScore() >= GlobalValues.GoodScore)
                holder.speedScore.setBackgroundResource(R.drawable.good_score_small_ring);
            else if (mJourneys.get(position).getSpeedScore() <= GlobalValues.MediumScoreMax
                    && mJourneys.get(position).getSpeedScore() >= GlobalValues.MediumScoreMin)
                holder.speedScore.setBackgroundResource(R.drawable.medium_score_small_ring);
            else if (mJourneys.get(position).getSpeedScore() <= GlobalValues.BadScore)
                holder.speedScore.setBackgroundResource(R.drawable.bad_score_small_ring);

            holder.driverScore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initJourneyDetailsFragment (position, 0);
                }
            });
            holder.brakingScore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initJourneyDetailsFragment (position, 1);
                }
            });
            holder.accelerationScore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initJourneyDetailsFragment (position, 2);
                }
            });
            holder.speedScore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initJourneyDetailsFragment (position, 3);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mJourneys.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        protected RelativeLayout progressBar;
        protected TextView date, startTime, endTime, driverScore, journeyStartAddress,
                journeyStartPostCode, journeyEndAddress, journeyEndPostCode, journeyDistanceTravelled,
                journeyTravelTime, brakingDesc, brakingScore, accelerationDesc,
                accelerationScore, speedDesc, speedScore;
        Typeface mFont, mLightFont;

        public ViewHolder(final Context context, View itemView) {
            super(itemView);
            mFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.selected_font));
            mLightFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.selected_font_extra_light));
            progressBar = (RelativeLayout) itemView.findViewById(R.id.journeyItemProgressBar);
            date = (TextView) itemView.findViewById(R.id.journeyDate);
            startTime = (TextView) itemView.findViewById(R.id.journeyStartTime);
            endTime = (TextView) itemView.findViewById(R.id.journeyEndTime);
            driverScore = (TextView) itemView.findViewById(R.id.journeyItemScore);
            journeyStartAddress = (TextView) itemView.findViewById(R.id.journeyStartAddressLine);
            journeyStartPostCode = (TextView) itemView.findViewById(R.id.journeyStartPostCode);
            journeyEndAddress = (TextView) itemView.findViewById(R.id.journeyEndAddressLine);
            journeyEndPostCode = (TextView) itemView.findViewById(R.id.journeyEndPostCode);
            journeyDistanceTravelled = (TextView) itemView.findViewById(R.id.journeyDistanceTravelled);
            journeyTravelTime = (TextView) itemView.findViewById(R.id.journeyTravelTime);
            brakingDesc = (TextView) itemView.findViewById(R.id.journeyBrakingScoreDesc);
            brakingScore = (TextView) itemView.findViewById(R.id.journeyBrakingScore);
            accelerationDesc = (TextView) itemView.findViewById(R.id.journeyAccelerationScoreDesc);
            accelerationScore = (TextView)itemView.findViewById(R.id.journeyAccelerationScore);
            speedDesc = (TextView) itemView.findViewById(R.id.journeySpeedScoreDesc);
            speedScore = (TextView) itemView.findViewById(R.id.journeySpeedScore);

            driverScore.setTypeface(mFont);
            date.setTypeface(mFont);
            startTime.setTypeface(mFont);
            endTime.setTypeface(mFont);
            journeyStartAddress.setTypeface(mLightFont);
            journeyStartPostCode.setTypeface(mLightFont);
            journeyEndAddress.setTypeface(mLightFont);
            journeyEndPostCode.setTypeface(mLightFont);
            journeyDistanceTravelled.setTypeface(mLightFont);
            journeyTravelTime.setTypeface(mLightFont);
            brakingDesc.setTypeface(mFont);
            brakingScore.setTypeface(mFont);
            accelerationDesc.setTypeface(mFont);
            accelerationScore.setTypeface(mFont);
            speedDesc.setTypeface(mFont);
            speedScore.setTypeface(mFont);
        }
    }

    private void clearCard (ViewHolder holder) {
        holder.driverScore.setText("");
        holder.date.setText("");
        holder.startTime.setText("");
        holder.endTime.setText("");
        holder.journeyStartAddress.setText("");
        holder.journeyStartPostCode.setText("");
        holder.journeyEndAddress.setText("");
        holder.journeyEndPostCode.setText("");
        holder.journeyDistanceTravelled.setText("");
        holder.journeyTravelTime.setText("");
        holder.brakingScore.setText("");
        holder.accelerationScore.setText("");
        holder.speedScore.setText("");

        // Set neutral colors to scores
        holder.driverScore.setBackgroundResource(R.drawable.good_score_ring);
        holder.brakingScore.setBackgroundResource(R.drawable.good_score_small_ring);
        holder.accelerationScore.setBackgroundResource(R.drawable.good_score_small_ring);
        holder.speedScore.setBackgroundResource(R.drawable.good_score_small_ring);
    }

    private void initJourneyDetailsFragment (int journeyPosition, int mapType) {
        JourneyDetailsFragment rmf = new JourneyDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("Journey", mJourneys.get(journeyPosition));
        bundle.putInt("MarkerType", mapType);
        rmf.setArguments(bundle);

        FragmentTransaction transaction = ((Activity)mContext).getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, rmf, "journeymap");
        transaction.addToBackStack("journeymap");
        transaction.commit();
    }
}
