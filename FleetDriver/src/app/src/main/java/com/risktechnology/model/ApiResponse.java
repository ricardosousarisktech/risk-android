package com.risktechnology.model;

/**
 * Created by rsousa on 20/03/15.
 */
public class ApiResponse {
    private boolean mSuccess;
    private int mErrorCode;
    private String mErrorDescription;

    public ApiResponse(boolean mSuccess, int mErrorCode, String mErrorDescription) {
        this.mSuccess = mSuccess;
        this.mErrorCode = mErrorCode;
        this.mErrorDescription = mErrorDescription;
    }

    public boolean getSuccess() {
        return mSuccess;
    }

    public void setSuccess(boolean mSuccess) {
        this.mSuccess = mSuccess;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(int mErrorCode) {
        this.mErrorCode = mErrorCode;
    }

    public String getErrorDescription() {
        return mErrorDescription;
    }

    public void setErrorDescription(String mErrorDescription) {
        this.mErrorDescription = mErrorDescription;
    }
}
