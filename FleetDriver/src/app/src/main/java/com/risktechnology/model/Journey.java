package com.risktechnology.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricardo on 01/04/2015.
 */
public class Journey implements Parcelable {
    private List<JourneyEvent> mJourneyEvents;
    private double mDistanceTravelled;
    private Statistics.DistanceUnits mDistanceUnits;
    private int mRiskScore;
    private int mAccelScore;
    private int mDecelScore;
    private int mSpeedScore;
    private String mIdlingTime;
    private DateTime mStartTime;
    private DateTime mFinishTime;
    private Duration mJourneyDuration;
    private String mStartAddress;
    private String mEndAddress;

    public Journey() {}

    public Journey(List<JourneyEvent> mJourneyEvents, double mDistanceTravelled, Statistics.DistanceUnits mDistanceUnits,
                   int mRiskScore, int mAccelScore, int mDecelScore, int mSpeedScore, String mIdlingTime) {
        this.mJourneyEvents = mJourneyEvents;
        this.mDistanceTravelled = mDistanceTravelled;
        this.mDistanceUnits = mDistanceUnits;
        this.mRiskScore = mRiskScore;
        this.mAccelScore = mAccelScore;
        this.mDecelScore = mDecelScore;
        this.mSpeedScore = mSpeedScore;
        this.mIdlingTime = mIdlingTime;
    }

    public Journey(List<JourneyEvent> mJourneyEvents, double mDistanceTravelled, Statistics.DistanceUnits mDistanceUnits,
                   int mRiskScore, int mAccelScore, int mDecelScore, int mSpeedScore, String mIdlingTime, DateTime mStartTime,
                   DateTime mFinishTime, Duration mJourneyDuration, String mStartAddress, String mEndAddress) {
        this.mJourneyEvents = mJourneyEvents;
        this.mDistanceTravelled = mDistanceTravelled;
        this.mDistanceUnits = mDistanceUnits;
        this.mRiskScore = mRiskScore;
        this.mAccelScore = mAccelScore;
        this.mDecelScore = mDecelScore;
        this.mSpeedScore = mSpeedScore;
        this.mIdlingTime = mIdlingTime;
        this.mStartTime = mStartTime;
        this.mFinishTime = mFinishTime;
        this.mJourneyDuration = mJourneyDuration;
        this.mStartAddress = mStartAddress;
        this.mEndAddress = mEndAddress;
    }

    public List<JourneyEvent> getJourneyEvents() {
        return mJourneyEvents;
    }

    public void setJourneyEvents(List<JourneyEvent> mJourneyEvents) {
        this.mJourneyEvents = mJourneyEvents;
    }

    public double getDistanceTravelled() {
        return mDistanceTravelled;
    }

    public void setDistanceTravelled(double mDistanceTravelled) {
        this.mDistanceTravelled = mDistanceTravelled;
    }

    public Statistics.DistanceUnits getDistanceUnits() {
        return mDistanceUnits;
    }

    public void setDistanceUnits(Statistics.DistanceUnits mDistanceUnits) {
        this.mDistanceUnits = mDistanceUnits;
    }

    public int getRiskScore() {
        return mRiskScore;
    }

    public void setRiskScore(int mRiskScore) {
        this.mRiskScore = mRiskScore;
    }

    public int getAccelScore() {
        return mAccelScore;
    }

    public void setAccelScore(int mAccelScore) {
        this.mAccelScore = mAccelScore;
    }

    public int getDecelScore() {
        return mDecelScore;
    }

    public void setDecelScore(int mDecelScore) {
        this.mDecelScore = mDecelScore;
    }

    public int getSpeedScore() {
        return mSpeedScore;
    }

    public void setSpeedScore(int mSpeedScore) {
        this.mSpeedScore = mSpeedScore;
    }

    public String getIdlingTime() {
        return mIdlingTime;
    }

    public void setIdlingTime(String mIdlingTime) {
        this.mIdlingTime = mIdlingTime;
    }

    public DateTime getStartTime() {
        return mStartTime;
    }

    public void setStartTime(DateTime mStartTime) {
        this.mStartTime = mStartTime;
    }

    public DateTime getFinishTime() {
        return mFinishTime;
    }

    public void setFinishTime(DateTime mFinishTime) {
        this.mFinishTime = mFinishTime;
    }

    public Duration getJourneyDuration() {
        return mJourneyDuration;
    }

    public void setJourneyDuration(Duration mJourneyDuration) {
        this.mJourneyDuration = mJourneyDuration;
    }

    public String getStartAddress() {
        return mStartAddress;
    }

    public void setStartAddress(String mStartAddress) {
        this.mStartAddress = mStartAddress;
    }

    public String getEndAddress() {
        return mEndAddress;
    }

    public void setEndAddress(String mEndAddress) {
        this.mEndAddress = mEndAddress;
    }

    protected Journey(Parcel in) {
        if (in.readByte() == 0x01) {
            mJourneyEvents = new ArrayList<>();
            in.readList(mJourneyEvents, JourneyEvent.class.getClassLoader());
        } else {
            mJourneyEvents = null;
        }
        mDistanceTravelled = in.readDouble();
        mDistanceUnits = (Statistics.DistanceUnits) in.readValue(Statistics.DistanceUnits.class.getClassLoader());
        mRiskScore = in.readInt();
        mAccelScore = in.readInt();
        mDecelScore = in.readInt();
        mSpeedScore = in.readInt();
        mIdlingTime = in.readString();
        mStartTime = (DateTime) in.readValue(DateTime.class.getClassLoader());
        mFinishTime = (DateTime) in.readValue(DateTime.class.getClassLoader());
        mJourneyDuration = (Duration) in.readValue(Duration.class.getClassLoader());
        mStartAddress = in.readString();
        mEndAddress = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mJourneyEvents == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mJourneyEvents);
        }
        dest.writeDouble(mDistanceTravelled);
        dest.writeValue(mDistanceUnits);
        dest.writeInt(mRiskScore);
        dest.writeInt(mAccelScore);
        dest.writeInt(mDecelScore);
        dest.writeInt(mSpeedScore);
        dest.writeString(mIdlingTime);
        dest.writeValue(mStartTime);
        dest.writeValue(mFinishTime);
        dest.writeValue(mJourneyDuration);
        dest.writeString(mStartAddress);
        dest.writeString(mEndAddress);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Journey> CREATOR = new Parcelable.Creator<Journey>() {
        @Override
        public Journey createFromParcel(Parcel in) {
            return new Journey(in);
        }

        @Override
        public Journey[] newArray(int size) {
            return new Journey[size];
        }
    };
}
