package com.risktechnology.riskmobileapp;


import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.risktechnology.adapter.FlowAdapter;
import com.risktechnology.adapter.RecyclerViewAdapter;
import com.risktechnology.model.Journey;
import com.risktechnology.model.JourneyEvent;
import com.risktechnology.model.Location;
import com.risktechnology.model.Statistics;
import com.risktechnology.model.User;
import com.risktechnology.utils.APIUrls;
import com.risktechnology.utils.Functions;
import com.risktechnology.utils.GlobalValues;
import com.risktechnology.widget.Coverflow;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


/**
 * A simple {@link Fragment} subclass.
 */
public class JourneysFragment extends Fragment {
    Typeface mFont;

    User mActiveUser;
    String mIMEI;
    long mStartDate;
    ArrayList<Integer> mDriverScores;

    List<Journey> mWeekJourneys;

    Coverflow mDriverScoresCoverflow;
    RecyclerView mJourneysRecyclerView;
    RecyclerView.Adapter mJourneysAdapter;
    RecyclerView.LayoutManager mJourneysLayoutManager;

    RelativeLayout mJourneysProgressBackground;

    TextView mBestScoreValue, mBestScoreDesc, mDistanceValue,
            mDistanceDesc, mTimeValue, mTimeDesc, mWorstScoreValue, mWorstScoreDesc;

    GetJourneysTask mJourneysTask;
//    GetAllJourneysTask mAllJourneysTask;

    private int mDriversBestScore, mDriversWorstScore, mBestScoreIndex, mWorstScoreIndex;
    private HomeActivity.HomeFragment.DataType mCurrentSelectedType;
    private double mTravelledDistance;
    private String mTimeTravelled;
    private Statistics.DistanceUnits mDistanceUnits;
    // Keeps track of the first index (journey), each day
    private int[] mDaysIndexes;

    private boolean mUserTouchedScreen = false;
    private int mScrollYAxisValue = 0;

    public JourneysFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_journeys, container, false);

        // Get reference of active user form HomeActivity
        mActiveUser = ((HomeActivity)getActivity()).getActiveUser();
        mIMEI = mActiveUser.getImeiByVehicleReg(mActiveUser.getSelectedVehicleReg());
        mDriverScores = getArguments().getIntegerArrayList("DriverScores");
        mCurrentSelectedType = HomeActivity.HomeFragment.DataType.valueOf(getArguments().getString("DataType"));
        mStartDate = getArguments().getLong("StartDate");
        mDriversBestScore = getArguments().getInt("BestScore");
        mDriversWorstScore = getArguments().getInt("WorstScore");
        mTravelledDistance = getArguments().getDouble("TravelledDistance");
        mTimeTravelled = getArguments().getString("TimeTravelled");
        mDistanceUnits = Statistics.DistanceUnits.valueOf(getArguments().getString("TravelUnits"));

        if (mDaysIndexes == null)
            mDaysIndexes = new int[mDriverScores.size()];

        DateTime journeyStartDate = new DateTime(mStartDate);
        DateTime journeyEndDate = journeyStartDate.plusDays(mDriverScores.size() - 1);
        DecimalFormat format = new DecimalFormat("00");
        ((HomeActivity) getActivity()).setScreenTitle(format.format(journeyStartDate.getDayOfMonth()) + "/"
                + format.format(journeyStartDate.getMonthOfYear())
                + " - " + format.format(journeyEndDate.getDayOfMonth()) + "/"
                + format.format(journeyEndDate.getMonthOfYear()));

        mJourneysProgressBackground = (RelativeLayout)view.findViewById(R.id.journeysProgressBackground);
        mDriverScoresCoverflow = (Coverflow)view.findViewById(R.id.journeysListCoverflow);
        mJourneysRecyclerView = (RecyclerView)view.findViewById(R.id.journeysList);
        mJourneysRecyclerView.setHasFixedSize(true);
        mJourneysLayoutManager = new LinearLayoutManager(getActivity());
        mJourneysRecyclerView.setLayoutManager(mJourneysLayoutManager);

        mBestScoreValue = (TextView)view.findViewById(R.id.journeyBestScoreValue);
        mBestScoreDesc = (TextView)view.findViewById(R.id.journeyBestScoreDesc);
        mDistanceValue = (TextView)view.findViewById(R.id.journeyKmsDialValue);
        mDistanceDesc = (TextView)view.findViewById(R.id.journeyKmsDialDesc);
        mTimeValue = (TextView)view.findViewById(R.id.journeyTimeDialValue);
        mTimeDesc = (TextView)view.findViewById(R.id.journeyTimeDialDesc);
        mWorstScoreValue = (TextView)view.findViewById(R.id.journeyMinScoreValue);
        mWorstScoreDesc = (TextView)view.findViewById(R.id.journeyMinScoreDesc);

        mFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.selected_font));
        mBestScoreValue.setTypeface(mFont);
        mBestScoreDesc.setTypeface(mFont);
        mDistanceValue.setTypeface(mFont);
        mDistanceDesc.setTypeface(mFont);
        mTimeValue.setTypeface(mFont);
        mTimeDesc.setTypeface(mFont);
        mWorstScoreValue.setTypeface(mFont);
        mWorstScoreDesc.setTypeface(mFont);

        mBestScoreValue.setText(String.valueOf(mDriversBestScore));
        mWorstScoreValue.setText(String.valueOf(mDriversWorstScore));
        if (mDriversBestScore >= GlobalValues.GoodScore)
            mBestScoreValue.setBackgroundResource(R.drawable.good_score_ring_button);
        else if (mDriversBestScore <= GlobalValues.MediumScoreMax
                && mDriversBestScore >= GlobalValues.MediumScoreMin)
            mBestScoreValue.setBackgroundResource(R.drawable.medium_score_ring_button);
        else if (mDriversBestScore <= GlobalValues.BadScore)
            mBestScoreValue.setBackgroundResource(R.drawable.bad_score_ring_button);
        if (mDriversWorstScore >= GlobalValues.GoodScore)
            mWorstScoreValue.setBackgroundResource(R.drawable.good_score_ring_button);
        else if (mDriversWorstScore <= GlobalValues.MediumScoreMax
                && mDriversWorstScore >= GlobalValues.MediumScoreMin)
            mWorstScoreValue.setBackgroundResource(R.drawable.medium_score_ring_button);
        else if (mDriversWorstScore <= GlobalValues.BadScore)
            mWorstScoreValue.setBackgroundResource(R.drawable.bad_score_ring_button);
        mDistanceValue.setText(String.valueOf(Math.round(mTravelledDistance)));
        switch (mDistanceUnits) {
            case km:
                mDistanceDesc.setText(getResources().getString(R.string.kms));
                break;
            case mi:
                mDistanceDesc.setText(getResources().getString(R.string.miles));
                break;
        }
        mTimeValue.setText(mTimeTravelled.substring(0, mTimeTravelled.length() - 3));
        mTimeDesc.setText(getResources().getString(R.string.travel_time));

        createWeeklyCoverflow();

        if (mJourneysAdapter == null) {
            mJourneysTask = new GetJourneysTask();
            mJourneysTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            mAllJourneysTask = new GetAllJourneysTask();
//            mAllJourneysTask.execute(journeyStartDate, journeyEndDate);
        } else {
            mJourneysRecyclerView.setAdapter(mJourneysAdapter);
        }

        // Know if the user has touched anywhere on the screen:
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserTouchedScreen = true;
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mJourneysProgressBackground.getVisibility() == View.VISIBLE
                && mJourneysTask.getStatus() != AsyncTask.Status.RUNNING)
            mJourneysProgressBackground.setVisibility(View.GONE);
        mBestScoreValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserTouchedScreen = true;
                LinearLayoutManager layoutManager = ((LinearLayoutManager) mJourneysRecyclerView.getLayoutManager());
                int scroll = mBestScoreIndex * (Math.round(getResources().getDimension(R.dimen.journey_card_height))
                        + (Math.round(getResources().getDimension(R.dimen.journey_card_margin)) * 2));
                if (mBestScoreIndex > layoutManager.findLastVisibleItemPosition()) {
                    mJourneysRecyclerView.smoothScrollBy(0, scroll - mScrollYAxisValue);
                } else
                    mJourneysRecyclerView.smoothScrollToPosition(mBestScoreIndex);
            }
        });
        mWorstScoreValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserTouchedScreen = true;
                LinearLayoutManager layoutManager = ((LinearLayoutManager) mJourneysRecyclerView.getLayoutManager());
                int scroll = mWorstScoreIndex * (Math.round(getResources().getDimension(R.dimen.journey_card_height))
                        + (Math.round(getResources().getDimension(R.dimen.journey_card_margin)) * 2));
                if (mWorstScoreIndex > layoutManager.findLastVisibleItemPosition()) {
                    mJourneysRecyclerView.smoothScrollBy(0, scroll - mScrollYAxisValue);
                } else
                    mJourneysRecyclerView.smoothScrollToPosition(mWorstScoreIndex);
            }
        });
        mJourneysRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScrollYAxisValue += dy;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mJourneysTask.getStatus() == AsyncTask.Status.RUNNING)
            mJourneysTask.cancel(true);
//        if (mAllJourneysTask.getStatus() == AsyncTask.Status.RUNNING)
//            mAllJourneysTask.cancel(true);
    }

    private void createWeeklyCoverflow () {
        mDriverScoresCoverflow.setAdapter(new FlowAdapter(getActivity(), mDriverScores, false, mCurrentSelectedType));
        mDriverScoresCoverflow.setUnselectedAlpha(0.7f);
        mDriverScoresCoverflow.setUnselectedSaturation(0.0f);
        mDriverScoresCoverflow.setUnselectedScale(0.005f);
        mDriverScoresCoverflow.setSpacing(-15);
        mDriverScoresCoverflow.setMaxRotation(0);
        mDriverScoresCoverflow.setScaleDownGravity(0.2f);
        mDriverScoresCoverflow.setActionDistance(Coverflow.ACTION_DISTANCE_AUTO);
        if (getArguments().getInt("SelectedDay") >= 0)
            mDriverScoresCoverflow.setSelection(getArguments().getInt("SelectedDay"), true);
        else
            mDriverScoresCoverflow.setSelection(3, true);
        mDriverScoresCoverflow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mUserTouchedScreen = true;
                if (mDriverScores.get(position) == -1)
                    view.setAlpha(0.6f);
                else {
                    LinearLayoutManager layoutManager = ((LinearLayoutManager) mJourneysRecyclerView.getLayoutManager());
                    int scroll = mDaysIndexes[position] * (Math.round(getResources().getDimension(R.dimen.journey_card_height))
                            + (Math.round(getResources().getDimension(R.dimen.journey_card_margin)) * 2));
                    if (mDaysIndexes[position] > layoutManager.findLastVisibleItemPosition()) {
                        mJourneysRecyclerView.smoothScrollBy(0, scroll - mScrollYAxisValue);
                    } else
                        mJourneysRecyclerView.smoothScrollToPosition(mDaysIndexes[position]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private class GetAllJourneysTask extends AsyncTask<DateTime, Void, Boolean> {
        int mDays = 0;

        @Override
        protected Boolean doInBackground(DateTime... params) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetJourneys");

            try {
                URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetJourneys);
                HttpHost httpHost = new HttpHost (url.getHost(), url.getPort(), url.getProtocol());
                int journeysCount = 0, plusDays = 0;
                mWeekJourneys = new ArrayList<>();

                // Dates format for signature
                SimpleDateFormat signatureDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

                while (plusDays < (mDriverScores.size() - 1)) {
                    // Post JSON body
                    JSONObject getJourneysJSONObj = new JSONObject();
                    getJourneysJSONObj.put("UserID", mActiveUser.getUserID());
                    getJourneysJSONObj.put("IMEI", mIMEI);
                    int startDateMillisecondsOffset = Functions.getGMTOffset(params[0].plusDays(plusDays)) * 3600000;
                    getJourneysJSONObj.put("StartDate", "/Date(" + params[0].plusDays(plusDays).getMillis() + ")/");
                    String signatureStartDate = signatureDateFormat.format(new Date(params[0].plusDays(plusDays).getMillis() - startDateMillisecondsOffset));

                    if ((plusDays + 7) > mDriverScores.size()) {
                        plusDays += mDriverScores.size() - plusDays;
                    } else {
                        plusDays += 7;
                    }
                    int endDateMillisecondsOffset = Functions.getGMTOffset(params[0].plusDays(plusDays)) * 3600000;

                    getJourneysJSONObj.put("EndDate", "/Date(" + params[0].plusDays(plusDays).getMillis() + ")/");
                    String signatureEndDate = signatureDateFormat.format(new Date(params[0].plusDays(plusDays).getMillis() - endDateMillisecondsOffset));

                    getJourneysJSONObj.put("LoadTelemetry", false);

                    // Parameters message to encrypt (EndDate, IMEI, LoadTelemetry, StartDate, userid, UserID)
                    String message = signatureEndDate + mIMEI + "False" +
                            signatureStartDate + String.valueOf(mActiveUser.getUserID()) +
                            String.valueOf(mActiveUser.getUserID());
                    // Get the signature value
                    String signature = Functions.getHMACKey(message, preferences.getString("secretkey", ""));

                    HttpPost httpPost = new HttpPost(APIUrls.APIRootUrl + APIUrls.GetJourneys + "userid="
                            + mActiveUser.getUserID() + "&signature=" + signature);
                    StringEntity se = new StringEntity(getJourneysJSONObj.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httpPost.setEntity(se);

                    HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    String responseString = EntityUtils.toString(httpEntity);
                    JSONObject responseJSON = new JSONObject(responseString);

                    if (responseJSON.getString("Success").equals("true")) {
                        if (responseJSON.getJSONArray("Journeys").length() > 0) {
                            // Add reference to the first journey for the current day
                            mDaysIndexes[mDays] = journeysCount;
                            JSONArray journeysJSONArray = responseJSON.getJSONArray("Journeys");
                            DateTime journeyStart = null, journeyEnd = null;
                            Duration journeyDuration = null;
                            String startAddress = "", endAddress = "";
                            for (int i = 0; i < journeysJSONArray.length(); i++) {
                                try {
                                    JSONObject journey = journeysJSONArray.getJSONObject(i);
                                    List<JourneyEvent> journeyEvents = new ArrayList<>();
                                    JSONArray journeyEventsJSONArray = journey.getJSONArray("JourneyEvents");
                                    for (int j = 0; j < journeyEventsJSONArray.length(); j++) {
                                        try {
                                            JSONObject journeyEvent = journeyEventsJSONArray.getJSONObject(j);
                                            DateTime eventTime = new DateTime(journeyEvent.getString("EventTime"));
                                            JSONObject locationJSON = journeyEvent.getJSONObject("Location");
                                            JourneyEvent event = new JourneyEvent(JourneyEvent.EventType.valueOf(journeyEvent.getString("EventType")),
                                                    new Date(eventTime.getMillis()),
                                                    new LatLng(locationJSON.getDouble("Latitude"), locationJSON.getDouble("Longitude")),
                                                    journeyEvent.getDouble("EventSpeed"),
                                                    journeyEvent.getInt("SpeedLimit"),
                                                    Statistics.DistanceUnits.valueOf(journeyEvent.getString("SpeedUnits")),
                                                    journeyEvent.getString("StreetAddress"),
                                                    journeyEvent.getInt("Bearing"));
                                            if (event.getEventType() == JourneyEvent.EventType.JOURNEY_START_EVENT) {
                                                journeyStart = new DateTime(event.getEventTime().getTime());
                                                startAddress = event.getStreetAddress();
                                            }
                                            if (event.getEventType() == JourneyEvent.EventType.JOURNEY_END_EVENT) {
                                                journeyEnd = new DateTime(event.getEventTime().getTime());
                                                endAddress = event.getStreetAddress();
                                            }
                                            journeyEvents.add(event);
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                    }

                                    if (journey.getInt("RiskScore") == mDriversBestScore)
                                        mBestScoreIndex = journeysCount;
                                    if (journey.getInt("RiskScore") == mDriversWorstScore)
                                        mWorstScoreIndex = journeysCount;

                                    if (journeyStart != null && journeyEnd != null)
                                        journeyDuration = new Duration(journeyStart.getMillis(), journeyEnd.getMillis());
                                        // If the API didn't return any JOURNEY_START_EVENT or JOURNEY_END_EVENT - Go for the
                                        // earliest and the latest event on the array of events
                                    else if (journeyEvents.size() > 0) {
                                        if (journeyStart == null) {
                                            JourneyEvent event = Functions.getEarliestEvent(journeyEvents);
                                            journeyStart = new DateTime(event.getEventTime().getTime());
                                            startAddress = event.getStreetAddress();
                                        }
                                        if (journeyEnd == null) {
                                            JourneyEvent event = Functions.getLatestEvent(journeyEvents);
                                            journeyEnd = new DateTime(event.getEventTime().getTime());
                                            endAddress = event.getStreetAddress();
                                        }
                                        journeyDuration = new Duration(journeyStart.getMillis(), journeyEnd.getMillis());
                                    } else {
                                        journeyStart = new DateTime(journey.getString("StartTime"));
                                        journeyEnd = new DateTime(journey.getString("EndTime"));
                                        startAddress = journey.getString("StartAddress");
                                        endAddress = journey.getString("EndAddress");
                                        journeyDuration = new Duration(journeyStart.getMillis(), journeyEnd.getMillis());
                                    }
                                    if (journeysCount < mWeekJourneys.size()) {
                                        Journey updateJourney = mWeekJourneys.get(journeysCount);
                                        updateJourney.setJourneyEvents(journeyEvents);
                                        updateJourney.setDistanceTravelled(journey.getDouble("DistanceTravelled"));
                                        updateJourney.setDistanceUnits(Statistics.DistanceUnits.valueOf(journey.getString("DistanceUnits")));
                                        updateJourney.setRiskScore(journey.getInt("RiskScore"));
                                        updateJourney.setAccelScore(journey.getInt("AccelScore"));
                                        updateJourney.setDecelScore(journey.getInt("DecelScore"));
                                        updateJourney.setSpeedScore(journey.getInt("SpeedScore"));
                                        updateJourney.setIdlingTime(journey.getString("IdlingTime"));
                                        updateJourney.setStartTime(journeyStart);
                                        updateJourney.setFinishTime(journeyEnd);
                                        updateJourney.setJourneyDuration(journeyDuration);
                                        updateJourney.setStartAddress(startAddress);
                                        updateJourney.setEndAddress(endAddress);
                                    } else
                                        mWeekJourneys.add(new Journey(journeyEvents, journey.getDouble("DistanceTravelled"),
                                                Statistics.DistanceUnits.valueOf(journey.getString("DistanceUnits")),
                                                journey.getInt("RiskScore"),
                                                journey.getInt("AccelScore"),
                                                journey.getInt("DecelScore"),
                                                journey.getInt("SpeedScore"),
                                                journey.getString("IdlingTime"),
                                                journeyStart, journeyEnd, journeyDuration, startAddress, endAddress));

                                    publishProgress();
                                    journeysCount++;
                                } catch (Exception ex) {
                                    journeysCount++;
                                    ex.printStackTrace();
                                }
                                // Reset temporary objects for next journey:
                                journeyStart = null;
                                journeyEnd = null;
                                startAddress = "";
                                endAddress = "";
                            }
                        }
                    }
                }
                httpClient.close();
            } catch (MalformedURLException | UnsupportedEncodingException | JSONException | NullPointerException ex) {
                httpClient.close();
                return false;
            } catch (IOException ex1) {
                httpClient.close();
                return false;
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            if (mJourneysAdapter == null) {
                mJourneysAdapter = new RecyclerViewAdapter(getActivity(), mWeekJourneys);
                mJourneysRecyclerView.setAdapter(mJourneysAdapter);
            }

            mJourneysRecyclerView.getAdapter().notifyDataSetChanged();
            if (!mUserTouchedScreen && (getArguments().getInt("SelectedDay") + 1) == mDays) {
                LinearLayoutManager layoutManager = ((LinearLayoutManager) mJourneysRecyclerView.getLayoutManager());
                int scroll = mDaysIndexes[getArguments().getInt("SelectedDay")] * (Math.round(getResources().getDimension(R.dimen.journey_card_height))
                        + (Math.round(getResources().getDimension(R.dimen.journey_card_margin)) * 2));
                if (mDaysIndexes[getArguments().getInt("SelectedDay")] > layoutManager.findLastVisibleItemPosition()) {
                    mJourneysRecyclerView.smoothScrollBy(0, scroll - mScrollYAxisValue);
                } else
                    mJourneysRecyclerView.smoothScrollToPosition(mDaysIndexes[getArguments().getInt("SelectedDay")]);
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (success) {

            }
            // If was successful but no journeys were found - clear the loading card
            if (success && mWeekJourneys.get(0).getRiskScore() == 0) {
                mWeekJourneys.clear();
                mJourneysRecyclerView.getAdapter().notifyDataSetChanged();
                Toast.makeText(getActivity(), getResources().getString(R.string.api_no_journeys_found), Toast.LENGTH_LONG).show();
            } else if (mWeekJourneys.get(0).getRiskScore() == 0) {
                mWeekJourneys.clear();
                mJourneysRecyclerView.getAdapter().notifyDataSetChanged();
                Toast.makeText(getActivity(), getResources().getString(R.string.api_no_journeys_found), Toast.LENGTH_LONG).show();
            }
        }
    }

    private class GetJourneysTask extends AsyncTask<Void, Void, Boolean> {
        int mDays = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mJourneysProgressBackground.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetJourneys");

            try {
                URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetJourneys);
                HttpHost httpHost = new HttpHost (url.getHost(), url.getPort(), url.getProtocol());

                int journeysCount = 0, usefulDays = 0;

                // Don't count Sunday because it will be the first loading card
                for (int i = 1; i < mDriverScores.size(); i++) {
                    if (mDriverScores.get(i) > -1)
                        usefulDays++;
                }
                // Add empty Journey (Sunday) to show first card (loading)
                mWeekJourneys = new ArrayList<>();
                mWeekJourneys.add(new Journey());
                if (mJourneysProgressBackground.getVisibility() == View.GONE)
                    publishProgress();

                DateTime dateTime = new DateTime (mStartDate);

                // Dates format for signature
                SimpleDateFormat signatureDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

                do {
                    if (isCancelled())
                        break;
                    // Post JSON body
                    JSONObject getJourneysJSONObj = new JSONObject();
                    getJourneysJSONObj.put("UserID", mActiveUser.getUserID());
                    getJourneysJSONObj.put("IMEI", mIMEI);
                    int startDateMillisecondsOffset = Functions.getGMTOffset(dateTime) * 3600000;
                    getJourneysJSONObj.put("StartDate", "/Date(" + dateTime.plusMillis(startDateMillisecondsOffset).getMillis() + ")/");
                    String signatureStartDate = signatureDateFormat.format(new Date (dateTime.getMillis()));

                    // Next day
                    dateTime = dateTime.plusMillis(86400000);
                    int endDateMillisecondsOffset = Functions.getGMTOffset(dateTime) * 3600000;

                    getJourneysJSONObj.put("EndDate", "/Date(" + dateTime.plusMillis(endDateMillisecondsOffset).getMillis() + ")/");
                    String signatureEndDate = signatureDateFormat.format(new Date (dateTime.getMillis()));

                    getJourneysJSONObj.put("LoadTelemetry", false);

                    // Parameters message to encrypt (EndDate, IMEI, LoadTelemetry, StartDate, userid, UserID)
                    String message = signatureEndDate + mIMEI + "False" +
                            signatureStartDate + String.valueOf(mActiveUser.getUserID()) +
                            String.valueOf(mActiveUser.getUserID());
                    // Get the signature value
                    String signature = Functions.getHMACKey(message, preferences.getString("secretkey", ""));

                    HttpPost httpPost = new HttpPost (APIUrls.APIRootUrl + APIUrls.GetJourneys + "userid="
                            + mActiveUser.getUserID() + "&signature=" + signature);
                    StringEntity se = new StringEntity(getJourneysJSONObj.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httpPost.setEntity(se);

                    HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    String responseString = EntityUtils.toString(httpEntity);
                    JSONObject responseJSON = new JSONObject(responseString);

                    if (responseJSON.getString("Success").equals("true")) {
                        if (responseJSON.getJSONArray("Journeys").length() > 0) {
                            // Add reference to the first journey for the current day
                            mDaysIndexes[mDays] = journeysCount;
                            JSONArray journeysJSONArray = responseJSON.getJSONArray("Journeys");
                            DateTime journeyStart = null, journeyEnd = null;
                            Duration journeyDuration = null;
                            String startAddress = "", endAddress = "";
                            for (int i = 0; i < journeysJSONArray.length(); i++) {
                                try {
                                    JSONObject journey = journeysJSONArray.getJSONObject(i);
                                    List<JourneyEvent> journeyEvents = new ArrayList<>();
                                    JSONArray journeyEventsJSONArray = journey.getJSONArray("JourneyEvents");
                                    for (int j = 0; j < journeyEventsJSONArray.length(); j++) {
                                        try {
                                            JSONObject journeyEvent = journeyEventsJSONArray.getJSONObject(j);
                                            DateTime eventTime = new DateTime(journeyEvent.getString("EventTime"));
                                            JSONObject locationJSON = journeyEvent.getJSONObject("Location");
                                            JourneyEvent event = new JourneyEvent(JourneyEvent.EventType.valueOf(journeyEvent.getString("EventType")),
                                                    new Date(eventTime.getMillis()),
                                                    new LatLng(locationJSON.getDouble("Latitude"), locationJSON.getDouble("Longitude")),
                                                    journeyEvent.getDouble("EventSpeed"),
                                                    journeyEvent.getInt("SpeedLimit"),
                                                    Statistics.DistanceUnits.valueOf(journeyEvent.getString("SpeedUnits")),
                                                    journeyEvent.getString("StreetAddress"),
                                                    journeyEvent.getInt("Bearing"));
                                            if (event.getEventType() == JourneyEvent.EventType.JOURNEY_START_EVENT) {
                                                journeyStart = new DateTime(event.getEventTime().getTime());
                                                startAddress = event.getStreetAddress();
                                            }
                                            if (event.getEventType() == JourneyEvent.EventType.JOURNEY_END_EVENT) {
                                                journeyEnd = new DateTime(event.getEventTime().getTime());
                                                endAddress = event.getStreetAddress();
                                            }
                                            journeyEvents.add(event);
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                    }

                                    if (journey.getInt("RiskScore") == mDriversBestScore)
                                        mBestScoreIndex = journeysCount;
                                    if (journey.getInt("RiskScore") == mDriversWorstScore)
                                        mWorstScoreIndex = journeysCount;

                                    if (journeyStart != null && journeyEnd != null)
                                        journeyDuration = new Duration(journeyStart.getMillis(), journeyEnd.getMillis());
                                        // If the API didn't return any JOURNEY_START_EVENT or JOURNEY_END_EVENT - Go for the
                                        // earliest and the latest event on the array of events
                                    else if (journeyEvents.size() > 0) {
                                        if (journeyStart == null) {
                                            JourneyEvent event = Functions.getEarliestEvent(journeyEvents);
                                            journeyStart = new DateTime(event.getEventTime().getTime());
                                            startAddress = event.getStreetAddress();
                                        }
                                        if (journeyEnd == null) {
                                            JourneyEvent event = Functions.getLatestEvent(journeyEvents);
                                            journeyEnd = new DateTime(event.getEventTime().getTime());
                                            endAddress = event.getStreetAddress();
                                        }
                                        journeyDuration = new Duration(journeyStart.getMillis(), journeyEnd.getMillis());
                                    } else {
                                        journeyStart = new DateTime (journey.getString("StartTime"));
                                        journeyEnd = new DateTime (journey.getString("EndTime"));
                                        startAddress = journey.getString("StartAddress");
                                        endAddress = journey.getString("EndAddress");
                                        journeyDuration = new Duration(journeyStart.getMillis(), journeyEnd.getMillis());
                                    }
                                    if (journeysCount < mWeekJourneys.size()) {
                                        Journey updateJourney = mWeekJourneys.get(journeysCount);
                                        updateJourney.setJourneyEvents(journeyEvents);
                                        updateJourney.setDistanceTravelled(journey.getDouble("DistanceTravelled"));
                                        updateJourney.setDistanceUnits(Statistics.DistanceUnits.valueOf(journey.getString("DistanceUnits")));
                                        updateJourney.setRiskScore(journey.getInt("RiskScore"));
                                        updateJourney.setAccelScore(journey.getInt("AccelScore"));
                                        updateJourney.setDecelScore(journey.getInt("DecelScore"));
                                        updateJourney.setSpeedScore(journey.getInt("SpeedScore"));
                                        updateJourney.setIdlingTime(journey.getString("IdlingTime"));
                                        updateJourney.setStartTime(journeyStart);
                                        updateJourney.setFinishTime(journeyEnd);
                                        updateJourney.setJourneyDuration(journeyDuration);
                                        updateJourney.setStartAddress(startAddress);
                                        updateJourney.setEndAddress(endAddress);
                                    } else
                                        mWeekJourneys.add(new Journey(journeyEvents, journey.getDouble("DistanceTravelled"),
                                                Statistics.DistanceUnits.valueOf(journey.getString("DistanceUnits")),
                                                journey.getInt("RiskScore"),
                                                journey.getInt("AccelScore"),
                                                journey.getInt("DecelScore"),
                                                journey.getInt("SpeedScore"),
                                                journey.getString("IdlingTime"),
                                                journeyStart, journeyEnd, journeyDuration, startAddress, endAddress));

                                    publishProgress();
                                    journeysCount++;
                                } catch (Exception ex) {
                                    journeysCount++;
                                    ex.printStackTrace();
                                }
                                // Reset temporary objects for next journey:
                                journeyStart = null;
                                journeyEnd = null;
                                startAddress = "";
                                endAddress = "";
                            }
                            // If it's before the last useful day, keep adding the waiting card
                            if (mDays < usefulDays) {
                                mWeekJourneys.add(new Journey());
                                publishProgress();
                            }
                        }
                    }
                    mDays++;
                } while (mDays < mDriverScores.size());
                httpClient.close();
            } catch (MalformedURLException | UnsupportedEncodingException | JSONException | NullPointerException ex) {
                httpClient.close();
                return false;
            } catch (IOException ex1) {
                httpClient.close();
                return false;
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            if (mJourneysAdapter == null) {
                mJourneysAdapter = new RecyclerViewAdapter(getActivity(), mWeekJourneys);
                mJourneysRecyclerView.setAdapter(mJourneysAdapter);
            }

            mJourneysRecyclerView.getAdapter().notifyDataSetChanged();
            if (!mUserTouchedScreen && (getArguments().getInt("SelectedDay") + 1) == mDays) {
                mJourneysProgressBackground.setVisibility(View.GONE);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) mJourneysRecyclerView.getLayoutManager());
                int scroll = mDaysIndexes[getArguments().getInt("SelectedDay")] * (Math.round(getResources().getDimension(R.dimen.journey_card_height))
                        + (Math.round(getResources().getDimension(R.dimen.journey_card_margin)) * 2));
                if (mDaysIndexes[getArguments().getInt("SelectedDay")] > layoutManager.findLastVisibleItemPosition()) {
                    mJourneysRecyclerView.scrollBy(0, scroll - mScrollYAxisValue);
                } else
                    mJourneysRecyclerView.scrollToPosition(mDaysIndexes[getArguments().getInt("SelectedDay")]);
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            mJourneysProgressBackground.setVisibility(View.GONE);
            // If was successful but no journeys were found - clear the loading card
            if (success && mWeekJourneys.get(0).getRiskScore() == 0) {
                mWeekJourneys.clear();
                mJourneysRecyclerView.getAdapter().notifyDataSetChanged();
                Toast.makeText(getActivity(), getResources().getString(R.string.api_no_journeys_found), Toast.LENGTH_LONG).show();
            } else if (mWeekJourneys.get(0).getRiskScore() == 0) {
                mWeekJourneys.clear();
                mJourneysRecyclerView.getAdapter().notifyDataSetChanged();
                Toast.makeText(getActivity(), getResources().getString(R.string.api_no_journeys_found), Toast.LENGTH_LONG).show();
            }
        }
    }
}
