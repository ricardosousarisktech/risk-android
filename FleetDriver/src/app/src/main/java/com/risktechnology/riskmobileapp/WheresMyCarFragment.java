package com.risktechnology.riskmobileapp;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.risktechnology.adapter.MarkerTitleAdapter;
import com.risktechnology.model.IMEI;
import com.risktechnology.model.Location;
import com.risktechnology.model.User;
import com.risktechnology.utils.APIUrls;
import com.risktechnology.utils.Functions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ricardo on 10/04/2015.
 */
public class WheresMyCarFragment extends Fragment {
    Typeface mFont;

    private User mActiveUser;
    GetCurrentLocationTask mCurrentLocationTask;
    private List<Location> mIMEILocations;
    private LatLngBounds WORLD = new LatLngBounds(new LatLng(-28.231810, -72.773438), new LatLng(61.349346, 20.390625));
    private GoogleMap mMap;

    View mVehiclePickerDialogView;
    AlertDialog mVehiclePickerAlertDialog;
    Button mPickerConfirmButton;
    NumberPicker mVehicleRegPicker;

    String [] mVehicleRegArray;

    public WheresMyCarFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wheres_my_car, container, false);

        mFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.selected_font));

        mVehiclePickerDialogView = getActivity().getLayoutInflater().inflate(R.layout.vehicle_picker_dialog, null);
        mPickerConfirmButton = (Button) mVehiclePickerDialogView.findViewById(R.id.licence_plate_confirm_button);
        mVehicleRegPicker = (NumberPicker) mVehiclePickerDialogView.findViewById(R.id.licence_plate_picker);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        mVehiclePickerAlertDialog = builder.create();
        mVehiclePickerAlertDialog.setCancelable(true);
        mVehiclePickerAlertDialog.setView(mVehiclePickerDialogView);

        mPickerConfirmButton.setTypeface(mFont);

        mActiveUser = ((HomeActivity)getActivity()).getActiveUser();

        ((HomeActivity)getActivity()).setScreenTitle(mActiveUser.getSelectedVehicleReg());

        addAllVehiclesSelection ();

        mCurrentLocationTask = new GetCurrentLocationTask();
        mCurrentLocationTask.execute(true);

        ((HomeActivity)getActivity()).refreshLocationButton();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager manager = getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();

        final com.google.android.gms.maps.MapFragment mapFragment = new com.google.android.gms.maps.MapFragment(){
            @Override
            public void onActivityCreated(Bundle savedInstanceState){
                super.onActivityCreated(savedInstanceState);
                mMap = getMap();
                if (mMap != null){
                    mMap.setMyLocationEnabled(true);
                    UiSettings settings = mMap.getUiSettings();
                    settings.setRotateGesturesEnabled(false);
                    settings.setTiltGesturesEnabled(false);
                    settings.setZoomControlsEnabled(true);
                    settings.setMyLocationButtonEnabled(false);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(WORLD, 200, 200, 0));
                }
            }

            // Exits the fragment if the user touches the back button
            @Override
            public void onDestroy() {
                super.onDestroy();
                if (mCurrentLocationTask.getStatus() == AsyncTask.Status.RUNNING)
                    mCurrentLocationTask.cancel(true);
            }
        };
        fragmentTransaction.add(R.id.map_container, mapFragment, "MyCarMap");
        fragmentTransaction.addToBackStack("MyCarMap");
        fragmentTransaction.commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((HomeActivity)getActivity()).setLocationButtonProperties();
    }

    public void screenTitleClick () {
        mVehiclePickerAlertDialog.show();

        mPickerConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Only change if a different vehicle was selected:
                String title = ((HomeActivity)getActivity()).getScreenTitle();
                ((HomeActivity)getActivity()).setScreenTitle(mVehicleRegArray[mVehicleRegPicker.getValue()]);
                String selected = mVehicleRegArray[mVehicleRegPicker.getValue()];
                if (!title.equals(selected))
                    refreshLocation(true);
                mVehiclePickerAlertDialog.dismiss();
            }
        });
    }

    private class GetCurrentLocationTask extends AsyncTask<Boolean, Void, Boolean> {
        boolean mUpdateCamera;

        @Override
        protected Boolean doInBackground(Boolean... params) {
            mUpdateCamera = params[0];
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("GetCurrentLocation");

            try {
                URL url = new URL(APIUrls.APIRootUrl + APIUrls.GetCurrentLocation);
                HttpHost httpHost = new HttpHost (url.getHost(), url.getPort(), url.getProtocol());

                String messageSignature = "";

                // Post JSON body
                JSONObject imeiJSONObj = new JSONObject();
                JSONArray imeiJSONArray = new JSONArray ();
                String selectedImei = mActiveUser.getImeiByVehicleReg(((HomeActivity)getActivity()).getScreenTitle());
                // If selected imei == "" - All vehicles have been selected
                if (selectedImei.equals("")) {
                    for (Iterator<IMEI> iterator = mActiveUser.getIMEIList().iterator(); iterator.hasNext(); ) {
                        IMEI imei = iterator.next();
                        imeiJSONArray.put(imei.getIMEI());
                        messageSignature += imei.getIMEI();
                    }
                } else {
                    imeiJSONArray.put(selectedImei);
                    messageSignature = selectedImei;
                }

                imeiJSONObj.put("IMEIList", imeiJSONArray);

                String message = messageSignature + String.valueOf(mActiveUser.getUserID());
                // Get the signature value
                String signature = Functions.getHMACKey(message, preferences.getString("secretkey", ""));

                HttpPost httpPost = new HttpPost(APIUrls.APIRootUrl + APIUrls.GetCurrentLocation + "userid="
                        + mActiveUser.getUserID() + "&signature=" + signature);
                StringEntity se = new StringEntity(imeiJSONObj.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpHost, httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String responseString = EntityUtils.toString(httpEntity);

                httpClient.close();

                JSONObject responseJSON = new JSONObject(responseString);
                if (responseJSON.getString("Success").equals("true")) {
                    mIMEILocations = new ArrayList<>();
                    JSONArray locationsJSONArray = responseJSON.getJSONArray("Locations");
                    for (int i = 0; i < locationsJSONArray.length(); i++) {
                        JSONObject locationJSON = locationsJSONArray.getJSONObject(i);
                        Location location = new Location(locationJSON.getString("IMEI"),
                                new LatLng(locationJSON.getDouble("Latitude"), locationJSON.getDouble("Longitude")),
                                locationJSON.getBoolean("Stationary"),
                                locationJSON.getInt("Bearing"));
                        mIMEILocations.add(location);
                    }
                    return true;
                } else
                    return false;
            } catch (MalformedURLException | UnsupportedEncodingException | JSONException ex) {
                httpClient.close();
                return false;
            } catch (IOException ex1) {
                httpClient.close();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            mMap.clear();
            Bitmap movingCar = BitmapFactory.decodeResource(getResources(), R.drawable.moving_car);
            Bitmap stationaryCar = BitmapFactory.decodeResource(getResources(), R.drawable.stationary_car);

            if (success && mIMEILocations.size() > 1) {
                for (Location location : mIMEILocations) {
                    String vehicleReg = mActiveUser.getVehicleRegByIMEI (location.getIMEI());
                    if (location.isStationary())
                        mMap.addMarker(new MarkerOptions().position(location.getLatLng()).title(vehicleReg)
                                .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(stationaryCar,
                                        Math.round(getResources().getDimension(R.dimen.map_car_size)),
                                        Math.round(getResources().getDimension(R.dimen.map_car_size)), false))));
                    else
                        mMap.addMarker(new MarkerOptions().position(location.getLatLng()).title(vehicleReg)
                                .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(movingCar,
                                        Math.round(getResources().getDimension(R.dimen.map_car_size)),
                                        Math.round(getResources().getDimension(R.dimen.map_car_size)), false)))
                                .rotation(location.getBearing()));
                }
                LatLng southwestPoint = Functions.findSouthwestPoint (mIMEILocations);
                LatLng northeastPoint = Functions.findNortheastPoint (mIMEILocations);
                if (mUpdateCamera)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(new LatLngBounds(southwestPoint, northeastPoint), 400, 400, 100));
                mMap.setInfoWindowAdapter(new MarkerTitleAdapter(getActivity().getLayoutInflater(), getActivity()));
            } else if (success && mIMEILocations.size() == 1) {
                String vehicleReg = mActiveUser.getVehicleRegByIMEI(mIMEILocations.get(0).getIMEI());
                if (mIMEILocations.get(0).isStationary())
                    mMap.addMarker(new MarkerOptions().position(mIMEILocations.get(0).getLatLng()).title(vehicleReg)
                            .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(stationaryCar,
                                    Math.round(getResources().getDimension(R.dimen.map_car_size)),
                                    Math.round(getResources().getDimension(R.dimen.map_car_size)), false))));
                else
                    mMap.addMarker(new MarkerOptions().position(mIMEILocations.get(0).getLatLng()).title(vehicleReg)
                            .icon(BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(movingCar,
                                    Math.round(getResources().getDimension(R.dimen.map_car_size)),
                                    Math.round(getResources().getDimension(R.dimen.map_car_size)), false)))
                            .rotation(mIMEILocations.get(0).getBearing()));
                if (mUpdateCamera)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mIMEILocations.get(0).getLatLng(), 13f));
                mMap.setInfoWindowAdapter(new MarkerTitleAdapter(getActivity().getLayoutInflater(), getActivity()));
            } else {
                if (mUpdateCamera)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(WORLD, 200, 200, 0));
                Toast.makeText(getActivity(), getResources().getString(R.string.no_location_found), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void addAllVehiclesSelection () {
        mVehicleRegArray = new String [mActiveUser.getIMEIList().size() + 1];
        mVehicleRegArray[0] = "All vehicles";
        for (int i = 0; i < mActiveUser.getIMEIList().size(); i++) {
            mVehicleRegArray[i+1] = mActiveUser.getIMEIList().get(i).getVehicleReg();
        }
        mVehicleRegPicker.setDisplayedValues(mVehicleRegArray);
        mVehicleRegPicker.setMinValue(0);
        mVehicleRegPicker.setMaxValue(mVehicleRegArray.length - 1);
        mVehicleRegPicker.setValue(0);
        setVehicleRegPickerTextStyle();
        mVehiclePickerAlertDialog.setView(mVehiclePickerDialogView);
    }
    private void setVehicleRegPickerTextStyle () {
        for (int i = 0; i < mVehicleRegPicker.getChildCount(); i++) {
            View child = mVehicleRegPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = mVehicleRegPicker.getClass().getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((EditText)child).setTypeface(mFont);
                    ((EditText)child).setTextColor(getResources().getColor(android.R.color.white));
                    ((EditText)child).setTextSize(getResources().getDimension(R.dimen.small_text_size));

                    ((Paint)selectorWheelPaintField.get(mVehicleRegPicker)).setTypeface(mFont);
                    ((Paint)selectorWheelPaintField.get(mVehicleRegPicker)).setColor(getResources().getColor(android.R.color.white));
                    ((Paint)selectorWheelPaintField.get(mVehicleRegPicker)).setTextSize(getResources().getDimension(R.dimen.regular_text_size));
                    mVehicleRegPicker.invalidate();
                    return;
                } catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException ex) {
                    System.out.println (ex.getMessage());
                }
            }
        }
    }

    public void refreshLocation (boolean updateCamera) {
        mActiveUser = ((HomeActivity)getActivity()).getActiveUser();
        if (mCurrentLocationTask.getStatus() == AsyncTask.Status.RUNNING) {
            mCurrentLocationTask.cancel(true);
        }
        mCurrentLocationTask = new GetCurrentLocationTask ();
        mCurrentLocationTask.execute(updateCamera);
    }
}
