package com.risktechnology.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.risktechnology.riskmobileapp.R;

/**
 * Created by ricardo on 30/04/2015.
 */
public class MarkerTitleAdapter implements GoogleMap.InfoWindowAdapter {
    Typeface mFont;
    private View popup=null;
    private LayoutInflater inflater=null;

    public MarkerTitleAdapter(LayoutInflater inflater, Context context) {
        this.inflater=inflater;
        mFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.selected_font));
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return(null);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        if (popup == null) {
            popup=inflater.inflate(R.layout.marker_title_layout, null);
        }

        TextView tv=(TextView)popup.findViewById(R.id.snippet);

        tv.setText(marker.getTitle());
        tv.setTypeface(mFont);

        return(popup);
    }
}
