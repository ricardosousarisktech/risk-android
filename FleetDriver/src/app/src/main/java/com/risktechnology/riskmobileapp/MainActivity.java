package com.risktechnology.riskmobileapp;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.risktechnology.model.*;
import com.risktechnology.utils.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.JodaTimePermission;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;


/**
 * A login screen that offers login via email/password.
 */
public class MainActivity extends Activity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);

    Typeface mLightFont;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private ProgressBar mProgressOldView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // If we're using HTTPS requests disable the certificate validation:
//        Functions.disableCertificateValidation ();

        float den = getResources().getDisplayMetrics().density;
//        switch (getResources().getConfiguration().screenLayout &
//                Configuration.SCREENLAYOUT_SIZE_MASK) {
//            case Configuration.SCREENLAYOUT_SIZE_LARGE:
//                Toast.makeText(this, "LARGE", Toast.LENGTH_SHORT).show();
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
//                Toast.makeText(this, "NORMAL", Toast.LENGTH_SHORT).show();
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
//                Toast.makeText(this, "X LARGE", Toast.LENGTH_SHORT).show();
//                break;
//            default:
//                Toast.makeText(this, "" + getResources().getConfiguration().screenLayout, Toast.LENGTH_SHORT).show();
//                break;
//        }
        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
        }

        SharedPreferences userPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            if (!userPrefs.getString("secretkey", "").equals("")) {
                Intent i = new Intent();
                i.setClass(getApplicationContext(), HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivityForResult(i, 100);
            }
        } catch (NullPointerException ex) {}

        mLightFont = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.selected_font_extra_light));

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mEmailView.setTypeface(mLightFont);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setTypeface(mLightFont);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setTypeface(mLightFont);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mProgressOldView = (ProgressBar)findViewById(R.id.login_progress_old);
//        mEmailView.setText("mark.packman.md@googlemail.com");
//        mPasswordView.setText("Chicago1961");
//        US User:
//        mEmailView.setText("stephen.plant@risktechnology.co.uk");
//        mPasswordView.setText("I8pkVVIqj6");
//        mEmailView.setText("david.mason@risktechnology.com");
//        mPasswordView.setText("Password02");
//        mEmailView.setText("risktest");
//        mPasswordView.setText("test");
    }

    private void populateAutoComplete() {
        Account[] accounts = AccountManager.get(this).getAccounts();
        Set<String> emailSet = new HashSet<>();
        for (Account account : accounts) {
            if (EMAIL_PATTERN.matcher(account.name).matches())
                emailSet.add(account.name);
        }
        mEmailView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, new ArrayList<>(emailSet)));
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check if password isn't empty.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check if email address isn't empty.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= 20) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(View.GONE);
            mProgressOldView.setVisibility(View.VISIBLE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login task used to authenticate user
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, ApiResponse> {

        private final String mEmail;
        private final String mPassword;

        private final String mAppId = getResources().getString(R.string.app_id);

        private String mSecretKey = "";
        private User mActiveUser;

        private ApiResponse mApiResponse;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected ApiResponse doInBackground(Void... params) {
            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Login/Register");

            try {
                // Access the API
                publishProgress();
                URL loginURL = new URL(APIUrls.APIRootUrl + APIUrls.UserLogin);
                HttpHost host = new HttpHost (loginURL.getHost(), loginURL.getPort(), loginURL.getProtocol());

                List<NameValuePair> parameters = new ArrayList<>(3);
                parameters.add(new BasicNameValuePair("Username", mEmail));
                parameters.add(new BasicNameValuePair("Password", mPassword));
                parameters.add(new BasicNameValuePair("AppID", mAppId));

                publishProgress();

                HttpPost post = new HttpPost (APIUrls.APIRootUrl + APIUrls.UserLogin);
                post.setEntity(new UrlEncodedFormEntity(parameters));

                // Login user with username and password
                HttpResponse response = httpClient.execute(host, post);

                publishProgress();

                HttpEntity entity = response.getEntity();

                String stringResponse = EntityUtils.toString(entity);

                // LOG REQUEST:
                publishProgress();

                try {
                    JSONObject json = new JSONObject (stringResponse);
                    if (json.getString("Success").equals("true")) {
                        List<IMEI> imeiList = new ArrayList<>();
                        JSONArray imeiArray = json.getJSONArray("IMEIList");
                        for (int i = 0; i < imeiArray.length(); i++) {
                            JSONObject imeiObj = (JSONObject)imeiArray.get(i);
                            DateTime dateTime = new DateTime (imeiObj.getString("FirstPolicyStartDate"));
                            imeiList.add (new IMEI (imeiObj.getString("IMEI"), dateTime.toDate(),
                                    imeiObj.getString("VehicleReg")));
                        }
                        JSONObject userObject = json.getJSONObject("User");
                        mActiveUser = new User (Integer.parseInt(userObject.getString("UserID")),
                                Integer.parseInt(userObject.getString("Permissions")),
                                userObject.getString("Currency"), imeiList);
                        mSecretKey = userObject.getString ("Secret");
                        mApiResponse = new ApiResponse(true, 0, "");
                    } else {
                return new ApiResponse(false, Integer.parseInt(json.getString("ErrorCode")),
                                json.getString("ErrorDescription"));
                    }
                } catch (JSONException ex) {
                    return new ApiResponse(false, 1, "JSON Exception");
                }

                publishProgress();

                httpClient.close();

                httpClient = AndroidHttpClient.newInstance("Permissions");

                // Get signature encrypted key
                String encryptedKey = Functions.getHMACKey(String.valueOf (mActiveUser.getUserID())
                        + String.valueOf(mActiveUser.getUserID()), mSecretKey);

                publishProgress();

                URL permissionsUrl = new URL(APIUrls.APIRootUrl + APIUrls.GetPermissions);
                HttpHost permissionsHost = new HttpHost (permissionsUrl.getHost(), permissionsUrl.getPort(), permissionsUrl.getProtocol());

                List<NameValuePair> permissionsParameters = new ArrayList<>(1);
                permissionsParameters.add(new BasicNameValuePair("UserID", String.valueOf(mActiveUser.getUserID())));

                HttpPost permissionsPost = new HttpPost (APIUrls.APIRootUrl + APIUrls.GetPermissions + "userid="
                        + mActiveUser.getUserID() + "&signature=" + encryptedKey);
                permissionsPost.setEntity(new UrlEncodedFormEntity(permissionsParameters));

                HttpResponse permissionsResponse = httpClient.execute(permissionsHost, permissionsPost);

                publishProgress();

                HttpEntity permissionsEntity = permissionsResponse.getEntity();

                String stringPermissionsResponse = EntityUtils.toString(permissionsEntity);

                publishProgress();

                try {
                    JSONObject permissionsJSON = new JSONObject(stringPermissionsResponse);
                    if (permissionsJSON.getString("Success").equals("true")) {
                        mActiveUser.setPermissions(Integer.parseInt(permissionsJSON.getString("Permissions")));
                        SharedPreferences userPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = userPreferences.edit();
                        // Clear previous preferences
                        editor.clear();
                        editor.putString("secretkey", mSecretKey);
                        editor.putInt("userid", mActiveUser.getUserID());
                        editor.putInt("permissions", mActiveUser.getPermissions());
                        editor.putString("email", mEmail);
                        editor.putString("password", mPassword);
                        editor.apply();
                        mApiResponse = new ApiResponse(true, 0, "");
                    } else {
                        return new ApiResponse(false, Integer.parseInt(permissionsJSON.getString("ErrorCode")),
                                permissionsJSON.getString("ErrorDescription"));
                    }
                } catch (JSONException permissionsEx) {
                    return new ApiResponse(false, 1, "JSON Exception " + permissionsEx.getMessage ());
                }

                publishProgress();

                httpClient.close();
            } catch (MalformedURLException | UnsupportedEncodingException e) {
                httpClient.close();
                return new ApiResponse(false, 1, "Exception " + e.getMessage());
            } catch (IOException e1) {
                httpClient.close();
                return new ApiResponse(false, 1, "IOException " + e1.getMessage());
            }

            return mApiResponse;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            mProgressOldView.setProgress(mProgressOldView.getProgress() + 9);
        }

        @Override
        protected void onPostExecute(ApiResponse success) {
            mAuthTask = null;
            showProgress(false);

            if (success.getSuccess()) {
                Toast.makeText(getApplicationContext(), getString (R.string.successful_authentication), Toast.LENGTH_SHORT).show ();
                mPasswordView.clearFocus();
                mEmailView.clearFocus();
                mPasswordView.setText("");
                mEmailView.setText("");
                Intent i = new Intent ();
                Bundle bundle = new Bundle();
                bundle.putParcelable("activeuser", mActiveUser);
                i.putExtras(bundle);
                i.setClass(getApplicationContext (), HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivityForResult(i, 100);
            } else if (success.getErrorCode() != 1) {
                mPasswordView.setText("");
                switch (success.getErrorCode()) {
                    case 100:
                        mEmailView.setError(getResources().getString(R.string.api_error_login_100));
                        break;
                    case 200:
                        mEmailView.setError(getResources().getString(R.string.api_error_login_200));
                        break;
                    case 300:
                        mEmailView.setError(getResources().getString(R.string.api_error_login_300));
                        break;
                    case 400:
                        mEmailView.setError(getResources().getString(R.string.api_error_login_400));
                        break;
                    case 500:
                        mEmailView.setError(getResources().getString(R.string.api_error_login_500));
                        break;
                    case 600:
                        mEmailView.setError(getResources().getString(R.string.api_error_login_600));
                        break;
                    case 1000:
                        mEmailView.setError(getResources().getString(R.string.api_error_1000));
                        break;
                }
                mEmailView.requestFocus();
            } else {
                Toast.makeText(getApplicationContext(), getString (R.string.error_network_connection), Toast.LENGTH_SHORT).show ();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Closing the app...
        if (resultCode == Activity.RESULT_CANCELED) {
            finish();
        }
    }
}



