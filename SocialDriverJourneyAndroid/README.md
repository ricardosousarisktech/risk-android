SocialDriverJourneyAndroid 
==========================

This code is the Journey module of the Risk SoialDriver mobile application.


Introduction
============

The expected functionality:
- Detect Journey Start and Stop conditions
- Within Journey collect GPS track
- Within Journey monitor acceleration and report hars acceleration/deceleration

Code Base
=========

The code at the beginning is a clone of [ActivityRecognitionApi][1] sample code from Google.
[1]: https://github.com/googlesamples/android-play-location/tree/master/ActivityRecognition



Releases
========

v0.3  2015.07.02
------------------
**Changes**
- pure GPS based operation
- KML log generated

**Functionality**

This version changes the functionality to GPS based only.
Once the user starts monitoring a background service is started which then starts to collect GPS locations every 30sec. This is done by Location API and Android can give more frequent updates or no updates if location not available, or less frequent if takes more time to obtain. 
The Journey detection checks GPS speed, if it is above 7m/s (25kmp) then it interpret is as START. If speed is below 2.5m/s (9kph) for more than 3min then it is a STOP.
GPS points saves a track into a KML file each by each. New file created with every Journey, file name correlates to the date/time.
All files saved into the root user accessible folder.


**Operation**
- MainActivity controlls the Journy service by the user pushable button. 
- JourneyService runs at the background, and starts GpsManager to get updates.
- Updates are put on EventBus and the service catch it, then passes it to the JourneyDetection module.
- JourneyDetection decides if Start-Stop detected and maintains inJourney state. On changes EventBus notification sent
- JourneyService logs the data to file
- JourneyService also can monitor Activity and pass it to the detection algorithm. Both can be enabled at the same time, but in this version only GPS used.

**TODO**
- make logic a bit more clever. Use lower power Activity monitoring only and if no updates received then use GPS. Can fall back to Activity only once OS gives updates.
- tweek thresholds to prevent false Journeys and optimize detection time.
- Possibly add events to log to notify server that it is likely Journey stopped, but keep getting GPS locations in case it starts again. This is useful if you leave the car and run, Journey will end later but hars acceleration could occure.
- Improve KML file
- Clean up notifications (some uses EventBus, some just function calls)
- add email out log
- tweek UI


v0.2  2015.06.30
------------------

**Changes**
- App restructured and a service created to run at background and collect activity updates
- Journey detection changed: Start is when at least two Vehicle detected within 20sec with minimum 35%, STOP when no vehicle detected above 20% for longer than 2min
- basics of GPS handling implemented, but not active yet
- app package name changed, old one has to be uninstalled before this is installed, otherwise two app will exist under same name


v0.1.2  2015.06.26
------------------

**Changes**
- When monitoring started Partial Wakelock applied to prevent system going to sleep Neds testing to check effect on battery life


v0.1.1  2015.06.24
------------------

**Changes**
- removed Timeout related log
- added full activity detected og (not only highest confidence
- persistent settings removed from log
- UI changed, one button for activity start/stop monitoring, and dedicated buttons to mark Journey start and stop


v0.1  2015.06.23
----------------
**Functionality:**

App monitors activity events at the bakground once the "Start Monitoring" button pressed, till "Stop Monitoring button pressed".
The current algorithm checks if there is at least two vehicle detected over 50% confidence within 20seconds.
The STOP detection works well, it shall not detect any vehicle with confidence over 40% within any 50seconds period. 

There is an "Event" button which can be used to mark your journey and later check with the detected ones.

The Journey log is a file on the root filesystem (user accessible) "journey_log.txt", this shall only contain START and STOP events, and user button presses. A second log collects activity informations as well, it is located at the same place, called "Activity_log.txt"


**Limitations/Known issues:**

- The app uses battery in a moderate way, however it is not tested on different units and in long terms.
- After a while it looks like the activity monitoring stops, no log is written and no Journey detected. This is tipically fixed once monitoring stopped and started again.
- At slow traffic the app might detect the journey in segments, breaking up the whole into multiple shorter journey
- Start detection does not work as good in slow speed in bumpy roads, especially in the city.
