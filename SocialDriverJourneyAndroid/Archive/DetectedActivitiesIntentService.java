/**
 * Copyright 2014 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.risk.socialdriver.journeyapp;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.risk.socialdriver.journeyapp.events.ActivitiesDetectedEvent;
import com.risk.socialdriver.journeyapp.loggers.Logging;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 *  IntentService for handling incoming intents that are generated as a result of requesting
 *  activity updates using
 *  {@link com.google.android.gms.location.ActivityRecognitionApi#requestActivityUpdates}.
 */
public class DetectedActivitiesIntentService extends IntentService {

    protected static final String TAG = "activity-detection-intent-service";
    protected static boolean log_init = false;
    /**
     * This constructor is required, and calls the super IntentService(String)
     * constructor with the name for a worker thread.
     */
    public DetectedActivitiesIntentService() {
        // Use the TAG to name the worker thread.
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * Handles incoming intents.
     * @param intent The Intent is provided (inside a PendingIntent) when requestActivityUpdates()
     *               is called.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
        Intent localIntent = new Intent(Constants.BROADCAST_ACTION);

        int type;
        int confidence;
        String typeStr;
        int maxconfidence=0;
        int maxconfidence_type=-1;

        String logString = "";
        // Get the list of the probable activities associated with the current state of the
        // device. Each activity is associated with a confidence level, which is an int between
        // 0 and 100.
        ArrayList<DetectedActivity> detectedActivities = (ArrayList) result.getProbableActivities();

        // Log each activity.
        Log.i(TAG, "activities detected");
        for (DetectedActivity da: detectedActivities) {

            type = da.getType();
            typeStr = Constants.getActivityString(getApplicationContext(), da.getType());
            confidence = da.getConfidence();

            Log.i(TAG, typeStr + " " + confidence + "%");

            // notify Journey manager
            JourneyDetection.ActivityDetected(this, type, confidence);

            logString += ",";
            logString += Constants.getActivityString(getApplicationContext(),type);
            logString += ",";
            logString += type;
            logString += ",";
            logString += confidence;

            if(maxconfidence < confidence){
                maxconfidence = confidence;
                maxconfidence_type = type;
            }
        }

        //Logging.i("Activity", "," + maxconfidence_type + "," + Constants.getActivityString(getApplicationContext(), maxconfidence_type) + "," + maxconfidence);
        Logging.i("Activity", logString);

        EventBus.getDefault().post(new ActivitiesDetectedEvent(detectedActivities));
    }
}