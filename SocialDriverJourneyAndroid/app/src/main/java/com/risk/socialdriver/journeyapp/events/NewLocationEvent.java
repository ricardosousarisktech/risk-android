package com.risk.socialdriver.journeyapp.events;

import android.location.Location;

/**
 * Created by zsolt on 02/07/2015.
 */
/*
 * Event sent when the GPS gave a new location update.
 * It can be sent when the service is running, even when in auto mode but START not detected yet.
 * Sent data is an Android Location structure
 */
public class NewLocationEvent {
    public final Location loc;

    public NewLocationEvent(Location location){
        loc = location;
    }
    public String toString() {
        return "New Location available";
    }
}
