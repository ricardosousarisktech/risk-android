package com.risk.socialdriver.journeyapp.events;

import com.risk.socialdriver.journeyapp.JourneyService;

/**
 * Created by zsolt on 30/06/2015.
 */

/*
 * Event sent when either Journey START (true) or STOP (false) detected
 */
public class JourneyStartEvent {
    public final boolean start;

    public JourneyStartEvent(boolean status)
    {
        start = status;
    }
    public String toString(){
        return start ? "Start" : "Stop";
    }
}
