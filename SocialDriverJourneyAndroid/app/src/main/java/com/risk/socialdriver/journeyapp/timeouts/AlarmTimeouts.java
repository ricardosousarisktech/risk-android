package com.risk.socialdriver.journeyapp.timeouts;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.risk.socialdriver.journeyapp.events.GpsGetNewFixEvent;
import com.risk.socialdriver.journeyapp.events.JourneyTimeoutEvent;
import com.risk.socialdriver.journeyapp.loggers.Logging;

import de.greenrobot.event.EventBus;

/**
 * Created by zsolt on 19/06/2015.
 */
public class AlarmTimeouts {
    private static final String TAG = "AlarmTimeouts";

    private static AlarmTimeouts mAlarm = null;
    protected static Context mContext;

    private static long mJourneyPeriod;
    private static PendingIntent mJourneyWakeUpOperation = null;

    private static long mGpsPeriod;
    private static PendingIntent mGpsWakeUpOperation = null;

    protected AlarmTimeouts(){

    }

    private static void init(Context context) {

        if (mAlarm == null)
            mAlarm = new AlarmTimeouts();

        mContext = context;
    }

    protected static PendingIntent setWakeUpAlarm(PendingIntent pIntent, long timeout, Class<?> cls) {

        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(mContext,cls);

        intent.putExtra("Timeout", true);

        cancelWakeUpAlarm(pIntent);
        pIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + mJourneyPeriod, mJourneyPeriod, mJourneyWakeUpOperation);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + timeout, pIntent);
        //Logging.i(TAG, "Timeout started with " + mJourneyPeriod + " ms timeout");


        return pIntent;
    }

    private static PendingIntent cancelWakeUpAlarm(PendingIntent pIntent) {
        //Logging.i(TAG, "Timeout cancelled");
        if (pIntent != null) {
            AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pIntent);
            pIntent = null;
        }
        return pIntent;
    }

    /********************           Journey Timeout functions       ***************************/

    public static void startJourneyTimeout(Context context, long Period) {
        init(context);
        mJourneyPeriod = Period;
        //Logging.d(TAG, "Timeout created with " + Period);
        mJourneyWakeUpOperation = setWakeUpAlarm(mJourneyWakeUpOperation,mJourneyPeriod, AlarmBroadcastReceiverJourney.class);
    }

    public static void stopJourneyTimeout() {
        mJourneyWakeUpOperation = cancelWakeUpAlarm(mJourneyWakeUpOperation);
    }

    public static boolean isJourneyTimeoutActive(){
        if(mJourneyWakeUpOperation == null){
            return false;
        }
        return true;
    }
    public static void JourneyTimeoutReached(){
        mJourneyWakeUpOperation = null;
        Logging.i(TAG, "Journey timeout: " + mJourneyPeriod / 1000 + "sec");
        EventBus.getDefault().post(new JourneyTimeoutEvent());
    }

    /********************           GPS Timeout functions       ***************************/
    public static void startGpsTimeout(Context context, long Period) {
        init(context);
        mGpsPeriod = Period;
        //Logging.d(TAG, "Timeout created with " + Period);
        mGpsWakeUpOperation = setWakeUpAlarm(mGpsWakeUpOperation,mGpsPeriod, AlarmBroadcastReceiverGps.class);
    }

    public static void stopGpsTimeout() {
        mGpsWakeUpOperation = cancelWakeUpAlarm(mGpsWakeUpOperation);
    }

    public static boolean isGpsTimeoutActive(){
        if(mGpsWakeUpOperation == null){
            return false;
        }
        return true;
    }

    public static void GpsTimeoutReached(){
        mGpsWakeUpOperation = null;
        Logging.i(TAG, "Gps timeout: " + mGpsPeriod / 1000 + "sec");
        EventBus.getDefault().post(new GpsGetNewFixEvent());
    }
}
