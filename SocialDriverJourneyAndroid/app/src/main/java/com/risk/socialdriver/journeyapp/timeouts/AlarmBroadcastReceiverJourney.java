package com.risk.socialdriver.journeyapp.timeouts;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class AlarmBroadcastReceiverJourney extends BroadcastReceiver{
	private static final String TAG = "AlarmReceiverJourney";

	@Override
	public void onReceive(Context context, Intent intent) {		
		//Logging.i(TAG, "Intent: "+intent);
		//Toast.makeText(context.getApplicationContext(), "Intent received: " + intent, Toast.LENGTH_LONG).show();		

		/*if (intent.getBooleanExtra("wakeup", false)) {
			Logging.i(TAG, "got wake up intent");
		}
		else {
			Logging.i(TAG, "got some other event: "+intent+" of type:  "+intent.getStringExtra("wakeup"));
		}*/
		Log.i(TAG, "expired");
		AlarmTimeouts.JourneyTimeoutReached();
	}

}
