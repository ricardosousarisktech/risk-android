package com.risk.socialdriver.journeyapp.events;

import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;

/**
 * Created by zsolt on 30/06/2015.
 */
public class ActivitiesDetectedEvent {
    public final ArrayList<DetectedActivity> mDetectedActivities;

    public ActivitiesDetectedEvent(ArrayList<DetectedActivity> detectedActivities) {
        mDetectedActivities = detectedActivities;
    }

    public String toString() {
        return "";
    }

}
