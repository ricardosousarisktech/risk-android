package com.risk.socialdriver.journeyapp.events;

import com.risk.socialdriver.journeyapp.JourneyData;

/**
 * Created by gareth on 07/07/2015.
 */
/*
 * Event sent when any Journey events detected (Start,Stop, High Acceleration, timed GPS locations)
 * Sent data is a structure with JourneyData including the details of the journey event
 */
public class JourneyDataEvent {
    public final JourneyData journey;

    public JourneyDataEvent(JourneyData newJourney)
    {
        journey = newJourney;
    }
    public String toString(){
        return "";
    }
}
