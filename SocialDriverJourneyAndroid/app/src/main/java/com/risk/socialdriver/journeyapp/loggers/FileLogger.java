//
//Copyright 2015 Jasteq
//

package com.risk.socialdriver.journeyapp.loggers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.risk.socialdriver.journeyapp.Constants;


public class FileLogger {
 private static FileLogger mLogger = null;
 private static FileWriter mLogWriter = null;
 private static FileWriter mLogWriter2 = null;

 public synchronized static FileLogger getLogger (Context c) {
     if(mLogger == null) {
         mLogger = new FileLogger();
     }
     return mLogger;
 }

 private FileLogger() {
     try {
         mLogWriter = new FileWriter(Constants.LOG_FILE_NAME, true);
         mLogWriter2 = new FileWriter(Constants.LOG_FILE_NAME_2, true);
     } catch (Exception e) {
     	Log.e("FileLogger", "Error creating log file ", e);
     }
 }

 static public synchronized void close() {
     if (mLogWriter != null) {
         try {
             mLogWriter.close();
         } catch (IOException e) {
         }
         mLogWriter = null;
     }
     if (mLogWriter2 != null) {
         try {
             mLogWriter2.close();
         } catch (IOException e) {
         }
         mLogWriter2 = null;
     }

     mLogger = null;
 }

 static public synchronized void log(Exception e) {
     if (mLogWriter != null) {
         log("Exception", "Stack trace follows...");
         PrintWriter pw = new PrintWriter(mLogWriter);
         e.printStackTrace(pw);
         pw.flush();
     }
 }

 static private synchronized void log(FileWriter logWriter,String prefix, String str) {
     if (mLogger == null) {
         mLogger = new FileLogger();
         log("Logger", "\r\n\r\n --- New Log ---");
     }
     Calendar cal = Calendar.getInstance();
     
     int day = cal.get(Calendar.DAY_OF_MONTH);
     int month = cal.get(Calendar.MONTH) + 1;
     int year = cal.get(Calendar.YEAR);
     int hr = cal.get(Calendar.HOUR_OF_DAY);
     int min = cal.get(Calendar.MINUTE);
     int sec = cal.get(Calendar.SECOND);

     // Don't use DateFormat here because it's much slower
     StringBuffer sb = new StringBuffer(256);
     sb.append(year);
     sb.append(':');
     sb.append(month);
     sb.append(':');
     sb.append(day);
     sb.append('-');
     sb.append(hr);
     sb.append(':');
     if (min < 10)
         sb.append('0');
     sb.append(min);
     sb.append(':');
     if (sec < 10) {
         sb.append('0');
     }
     sb.append(sec);
     sb.append('-');        
     if (prefix != null) {
         sb.append(prefix);
         sb.append("-");
     }
     sb.append(str);
     sb.append("\r\n");
     String s = sb.toString();

     if (logWriter != null) {
         try {
             logWriter.write(s);
             logWriter.flush();
         } catch (IOException e) {
             // Something might have happened to the sdcard
             if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                 // If the card is mounted and we can create the writer, retry
                 mLogger = new FileLogger();
                 if (logWriter != null) {
                     try {
                         log("FileLogger", "Exception writing log; recreating...");
                         log(prefix, str);
                     } catch (Exception e1) {
                     }
                 }
             }
         }
     }
 }


 static public synchronized void log(String prefix, String str){
    log(mLogWriter,prefix,str);
 }

    static public synchronized void log2(String prefix, String str){
        log(mLogWriter2,prefix,str);
    }
}
