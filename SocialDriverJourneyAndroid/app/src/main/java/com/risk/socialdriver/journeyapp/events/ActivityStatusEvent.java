package com.risk.socialdriver.journeyapp.events;

/**
 * Created by Gareth on 6/07/2015.
 */

/*
 * Event sent only in Automatic Journey detection mode.
 * When the OS reports detected Activity the highest confidence one is sent over this event.
 * Sent data is the detected activity relates to a number under Android/DetectedActivity class
 */
public class ActivityStatusEvent {
    public int mStatus;
    public ActivityStatusEvent(int status)
    {
        mStatus = status;
    }
    public String toString(){
        return "";
    }
}
