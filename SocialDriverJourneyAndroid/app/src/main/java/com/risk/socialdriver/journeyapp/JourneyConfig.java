package com.risk.socialdriver.journeyapp;

import android.content.Context;
import android.util.Log;

import com.risk.socialdriver.journeyapp.utils.PersistentSettings;

/**
 * Created by zsolt on 24/08/2015.
 */
public class JourneyConfig {
    private static final String TAG = "Configuration";

    private static PersistentSettings mSettings;

    public interface config {
        String JourneyTimeoutStart   = "ConfJourneyTimeoutStart";
        String JourneyTimeoutStop    = "ConfJourneyTimeoutStop";

        String ActivityConfidenceHigh    = "ConfActivityConfidenceHigh";
        String ActivityConfidenceLow     = "ConfActivityConfidenceLow";

        String GpsSpeedHigh       = "ConfGpsSpeedHigh";
        String GpsSpeedLow        = "ConfGpsSpeedLow";
        String GpsUpdateInterval  = "ConfGpsUpdateInterval";

        String AccelThreshold     = "ConfAccelThreshold";
        String BrakingThreshold = "ConfBrakingThreshold";

        String AccelerationExceedTime    = "ConfAccelerationExceedTimeout";
        String AccelerationIgnoreTime    = "ConfAccelerationIgnoreTime";

        String GpsFixTimeoutLow =   "ConfGpsFixTimeoutLow";
        String GpsFixTimeoutHigh =  "ConfGpsFixTimeoutHigh";
        String GpsTimeCheckSnr  =   "ConfGpsTimeCheckSnr";
        String GpsSnrThreshold  =   "ConfGpsSnrThreshold";
        String GpsMinSatCnt     =   "ConfGpsMinSatCnt";
        String GpsOldValidTime  =   "ConfGpsOldValidTime";
    }

    private static GpsConfig mGpsConfig = null;


    public JourneyConfig(){
    }

    public static void init(Context context){
        // make sure we have initialized all
        mSettings = PersistentSettings.getInstance(context);
        if(mGpsConfig == null) {
            mGpsConfig = new GpsConfig();
        }
    }

    /***********************************************************/
    public static long getJourneyTimeoutStart(){
        return mSettings.getLong(config.JourneyTimeoutStart, Constants.JOURNEY_ACTIVITY_TimeoutStart);
    }
    public static boolean setJourneyTimeoutStart(Long value){
        if(value.equals(getJourneyTimeoutStart())){
            return true;
        }
        return mSettings.setLong(config.JourneyTimeoutStart, value);
    }

    /***********************************************************/
    public static long getJourneyTimeoutStop(){
        return mSettings.getLong(config.JourneyTimeoutStop, Constants.JOURNEY_ACTIVITY_TimeoutStop);
    }
    public static boolean setJourneyTimeoutStop(Long value){
        if(value.equals(getJourneyTimeoutStop())){
            return true;
        }
        return mSettings.setLong(config.JourneyTimeoutStop, value);
    }

    /***********************************************************/
    public static int getActivityConfidenceHigh(){
        return mSettings.getInteger(config.ActivityConfidenceHigh, Constants.JOURNEY_ACTIVITY_confidenceThresholdStart);
    }
    public static boolean setActivityConfidenceHigh(int value){
        if(value == getActivityConfidenceHigh()){
            return true;
        }
        return mSettings.setInteger(config.ActivityConfidenceHigh, value);
    }

    /***********************************************************/
    public static int getActivityConfidenceLow(){
        return mSettings.getInteger(config.ActivityConfidenceLow, Constants.JOURNEY_ACTIVITY_confidenceThresholdStop);
    }
    public static boolean setActivityConfidenceLow(int value){
        if(value == getActivityConfidenceLow()){
            return true;
        }
        return mSettings.setInteger(config.ActivityConfidenceLow, value);
    }

    /***********************************************************/
    public static float getGpsSpeedStartThreshold(){
        return mSettings.getFloat(config.GpsSpeedHigh, Constants.JOURNEY_GPS_speedThresholdStart_ms);
    }
    public static boolean setGpsSpeedStartThreshold(float value){
        if(value == getGpsSpeedStartThreshold()){
            return true;
        }
        return mSettings.setFloat(config.GpsSpeedHigh, value);
    }

    /***********************************************************/
    public static float getGpsSpeedStopThershold(){
        return mSettings.getFloat(config.GpsSpeedLow, Constants.JOURNEY_GPS_speedThresholdStop_ms);
    }
    public static boolean setGpsSpeedStopThershold(float value){
        if(value == getGpsSpeedStopThershold()){
            return true;
        }
        return mSettings.setFloat(config.GpsSpeedLow, value);
    }

    /***********************************************************/
    public static float getAccelerationThreshold(){
        return mSettings.getFloat(config.AccelThreshold, Constants.ACCELERATION_THRESHOLD);
    }
    public static boolean setAccelerationThreshold(float value){
        if(value == getAccelerationThreshold()){
            return true;
        }
        return mSettings.setFloat(config.AccelThreshold, value);
    }

    /***********************************************************/
    public static float getBrakingThreshold(){
        return mSettings.getFloat(config.BrakingThreshold, Constants.BRAKING_THRESHOLD);
    }
    public static boolean setBrakingThreshold(float value){
        if(value == getBrakingThreshold()){
            return true;
        }
        return mSettings.setFloat(config.BrakingThreshold, value);
    }

    /***********************************************************/
    public static int getAccelerationExceedTime(){
        return mSettings.getInteger(config.AccelerationExceedTime, Constants.ACCEL_DETECTION_TIMEOUT);
    }
    public static boolean setAccelerationExceedTime(int value){
        if(value == getAccelerationExceedTime()){
            return true;
        }
        return mSettings.setInteger(config.AccelerationExceedTime, value);
    }

    /***********************************************************/
    public static int getAccelerationIgnoreTime(){
        return mSettings.getInteger(config.AccelerationIgnoreTime, Constants.ACCEL_EVENT_REPEAT_TIMEOUT);
    }
    public static boolean setAccelerationIgnoreTime(int value){
        if(value == getAccelerationIgnoreTime()){
            return true;
        }
        return mSettings.setInteger(config.AccelerationIgnoreTime, value);
    }

    /***********************************************************/
    public static class GpsConfig {
        public float snrThreshold;   // level below short timeout used, unlikely to get fix
        public int timeCheckSnr; // seconds, after the SNR checked and timeout set
        public int minSatsCnt;   // minimum satellites to be above SNR threshold to use longer timeout
        public int fixTimeoutLow;  // timeout to get a fix if signal is low
        public int fixTimeoutHigh;  // timeout to get a fix if signal is high
        public int updateInterval;  // seconds, how often get a location update
        public int oldGpsValidTime; // seconds, how long an old fix considered valid

        private GpsConfig(){}
    }

    public static GpsConfig getGpsConfiguration(){
        mGpsConfig.fixTimeoutLow = mSettings.getInteger(config.GpsFixTimeoutLow, Constants.GPS_TIMEOUT_LOW_SIGNAL);
        mGpsConfig.fixTimeoutHigh = mSettings.getInteger(config.GpsFixTimeoutHigh, Constants.GPS_TIMEOUT_HIGH_SIGNAL);
        mGpsConfig.snrThreshold = mSettings.getFloat(config.GpsSnrThreshold, Constants.GPS_SIGNAL_THRESHOLD);
        mGpsConfig.minSatsCnt = mSettings.getInteger(config.GpsMinSatCnt, Constants.GPS_MIN_SATS);
        mGpsConfig.timeCheckSnr = mSettings.getInteger(config.GpsTimeCheckSnr, Constants.GPS_TIME_CHECK_SNR);
        mGpsConfig.updateInterval = mSettings.getInteger(config.GpsUpdateInterval, Constants.GPS_UPDATE_INTERVAL);
        mGpsConfig.oldGpsValidTime = mSettings.getInteger(config.GpsOldValidTime, Constants.GPS_OLD_FIX_VALID_TIME);
        return mGpsConfig;
    }

    public static boolean setGpsConfiguration(GpsConfig cfg){
        if(isGpsConfigChanged(cfg)) {
            mGpsConfig = cfg;
            if (mSettings.setInteger(config.GpsFixTimeoutLow, cfg.fixTimeoutLow)) {
                if (mSettings.setInteger(config.GpsFixTimeoutHigh, cfg.fixTimeoutHigh)) {
                    if (mSettings.setFloat(config.GpsSnrThreshold, cfg.snrThreshold)) {
                        if (mSettings.setInteger(config.GpsMinSatCnt, cfg.minSatsCnt)) {
                            if (mSettings.setInteger(config.GpsTimeCheckSnr, cfg.timeCheckSnr)) {
                                if (mSettings.setInteger(config.GpsUpdateInterval, cfg.updateInterval)) {
                                    if (mSettings.setInteger(config.GpsOldValidTime, cfg.oldGpsValidTime)) {
                                        Log.i(TAG, "Gps config updated");
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }else{
            Log.i(TAG, "Gps config same as before");
            return true;
        }
    }

    private static boolean isGpsConfigChanged(GpsConfig cfg){
        boolean changed = false;

        if(cfg.minSatsCnt != mSettings.getInteger(config.GpsMinSatCnt, Constants.GPS_MIN_SATS)) {
            changed = true;
        }
        if(cfg.oldGpsValidTime != mSettings.getInteger(config.GpsOldValidTime, Constants.GPS_OLD_FIX_VALID_TIME)){
            changed = true;
        }
        if(cfg.timeCheckSnr != mSettings.getInteger(config.GpsTimeCheckSnr, Constants.GPS_TIME_CHECK_SNR)){
            changed = true;
        }
        if(cfg.snrThreshold != mSettings.getFloat(config.GpsSnrThreshold, Constants.GPS_SIGNAL_THRESHOLD)){
            changed = true;
        }
        if(cfg.fixTimeoutLow != mSettings.getInteger(config.GpsFixTimeoutLow, Constants.GPS_TIMEOUT_LOW_SIGNAL)){
            changed = true;
        }
        if(cfg.fixTimeoutHigh != mSettings.getInteger(config.GpsFixTimeoutHigh, Constants.GPS_TIMEOUT_HIGH_SIGNAL)){
            changed = true;
        }
        if(cfg.updateInterval != mSettings.getInteger(config.GpsUpdateInterval, Constants.GPS_UPDATE_INTERVAL)){
            changed = true;
        }

        return changed;
    }
}
