package com.risk.socialdriver.journeyapp.events;

import com.risk.socialdriver.journeyapp.Constants;

/**
 * Created by zsolt on 15/07/2015.
 */
/* NOT USED!!!
 * Event sent when high acceleration force detected
 * Sent data is the detected g force in 'g'
 */
public class HighAccelerationEvent {
    public final float value;

    public HighAccelerationEvent(float accelValue){
        value = accelValue;
    }

    public String toString(){
        return "High Acceleration detected!";
    }
}
