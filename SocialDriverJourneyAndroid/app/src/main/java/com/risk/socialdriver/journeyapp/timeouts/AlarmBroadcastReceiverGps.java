package com.risk.socialdriver.journeyapp.timeouts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by zsolt on 25/08/2015.
 */
public class AlarmBroadcastReceiverGps extends BroadcastReceiver {
    private static final String TAG = "AlarmReceiverGps";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "expired");
        AlarmTimeouts.GpsTimeoutReached();
    }
}
