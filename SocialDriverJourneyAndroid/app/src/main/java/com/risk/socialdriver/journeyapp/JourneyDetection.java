package com.risk.socialdriver.journeyapp;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.DetectedActivity;
import com.risk.socialdriver.journeyapp.events.JourneyStartEvent;
import com.risk.socialdriver.journeyapp.events.JourneyTimeoutEvent;
import com.risk.socialdriver.journeyapp.timeouts.AlarmTimeouts;
import com.risk.socialdriver.journeyapp.loggers.Logging;

import de.greenrobot.event.EventBus;

/**
 * Created by zsolt on 19/06/2015.
 */
public class JourneyDetection {
    private final String TAG = "Journey";

    //private FileLogger mLogger = null;
    private boolean inJourney = false;
    private boolean forcedMode = false;

    private int confidenceThresholdStart;
    private int confidenceThresholdStop;

    private long journeyStartTiemout;
    private long journeyStopTiemout;

    Context mContext = null;

    public JourneyDetection(Context context){
        mContext = context;
        EventBus.getDefault().register(this);

        confidenceThresholdStart = JourneyConfig.getActivityConfidenceHigh();
        confidenceThresholdStop  = JourneyConfig.getActivityConfidenceLow();

        journeyStartTiemout = JourneyConfig.getJourneyTimeoutStart();
        journeyStopTiemout = JourneyConfig.getJourneyTimeoutStop();
    }

    public void ActivityDetected(int type, int confidence){
        if(forcedMode)
            return;

        if(inJourney == false) {
            if (type == DetectedActivity.IN_VEHICLE) {
                if (confidence >= confidenceThresholdStart) {
                    // check if this is the first good detection
                    if (AlarmTimeouts.isJourneyTimeoutActive()) {
                        //no, so at least two detected -> START condition
                        AlarmTimeouts.stopJourneyTimeout();
                        AlarmTimeouts.startJourneyTimeout(mContext, journeyStopTiemout);
                        startDetected();
                        inJourney = true;
                    } else {
                        // this is the first one, lets see if we get any more within a given time
                        AlarmTimeouts.startJourneyTimeout(mContext, journeyStartTiemout);
                    }
                }
            }
        } else {
            if (type == DetectedActivity.IN_VEHICLE) {
                if (confidence >= confidenceThresholdStop) {
                    AlarmTimeouts.startJourneyTimeout(mContext, journeyStopTiemout);
                }
            }
        }

    }

    // return true if the location generated an event, so calling class knows that there will be START/STOP
    public boolean newGpsLocation(Location loc){
        boolean generated = false;

        if(forcedMode)
            return false;

        if(loc == null)
            return false;

        if(inJourney == false){
            if(loc.getSpeed() >= JourneyConfig.getGpsSpeedStartThreshold()){
                AlarmTimeouts.startJourneyTimeout(mContext, journeyStopTiemout);
                startDetected();
                generated = true;
            }
        }else{
            if(loc.getSpeed() >= JourneyConfig.getGpsSpeedStopThershold()){
                AlarmTimeouts.startJourneyTimeout(mContext, journeyStopTiemout);
            }
        }

        return generated;
    }

    public void onEvent(JourneyTimeoutEvent event) {
        Log.i(TAG, event.toString());
        if(inJourney == true) {
            stopDetected();
        }
    }

    private void startDetected(){
        Logging.i2(TAG, "Journey START");
        EventBus.getDefault().post(new JourneyStartEvent(true));
        inJourney = true;
    }

    private void stopDetected(){
        inJourney = false;
        Logging.i2(TAG, "Journey STOP");
        EventBus.getDefault().post(new JourneyStartEvent(false));
    }

    public boolean isInJourney(){
        return inJourney;
    }

    public void forceStart(){
        forcedMode = true;
        Log.i(TAG,"forced startJourneyTimeout");
        startDetected();
    }

    public void forceStop(){
        forcedMode = true;
        Log.i(TAG,"forced stopJourneyTimeout");
        stopDetected();
    }

    public void setForcedMode(boolean forced){
        forcedMode = forced;
    }
}
