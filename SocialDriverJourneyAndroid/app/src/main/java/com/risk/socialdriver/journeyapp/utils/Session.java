package com.risk.socialdriver.journeyapp.utils;


import android.location.Location;
import android.os.PowerManager;

import com.risk.socialdriver.journeyapp.loggers.Logging;

public class Session {

	private static final String TAG = "RuntimeParameters";
	private static Session sInstance;

	private static PowerManager.WakeLock wl=null;



	public static Session getInstance() {
		if (sInstance == null) {
			sInstance = new Session();
			Logging.i(TAG, "initialized");
		}
		return (sInstance);
	}
	
	private Session() {
	}
	
	public static void setWakeLockInstance(PowerManager.WakeLock wakeLock){
		wl = wakeLock;
	}
	
	public static PowerManager.WakeLock getWakeLockInstance(){
		return wl;
	}


}
