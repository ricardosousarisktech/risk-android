package com.risk.socialdriver.journeyapp;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;

//MATTC: updated to v7 support library, v4 is not needed for MinSDK:9
import android.support.v7.app.NotificationCompat;

import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationServices;

import com.risk.socialdriver.journeyapp.events.ActivityStatusEvent;
import com.risk.socialdriver.journeyapp.events.GpsGetNewFixEvent;
import com.risk.socialdriver.journeyapp.events.HighBrakingEvent;
import com.risk.socialdriver.journeyapp.events.JourneyDataEvent;
import com.risk.socialdriver.journeyapp.events.JourneyListEvent;
import com.risk.socialdriver.journeyapp.events.JourneyStartEvent;
import com.risk.socialdriver.journeyapp.events.NewLocationEvent;
import com.risk.socialdriver.journeyapp.events.SpeedEvent;
import com.risk.socialdriver.journeyapp.gps.GpsManager;
import com.risk.socialdriver.journeyapp.loggers.Logging;
import com.risk.socialdriver.journeyapp.utils.Session;
import com.risk.socialdriver.journeyapp.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class JourneyService extends Service {// implements
    //GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    private static final String TAG = "JourneyService";

    private PowerManager.WakeLock wl;
    private static Service mService;
    //private GpsManagerFused mGpsManager=null;
    private static GpsManager mGpsManager = null;
    private static JourneyDetection mJourneyDetection = null;
    protected GoogleApiClient mGoogleApiClient = null;
    private PendingIntent mActivityDetectionPendingIntent;
    private AccelDetection mAccelDetection = null;

    //MATTC: set to return false be default.
    //       This should be set to true once the system is told to start monitoring only
    private static boolean automaticJourneyDetectionEnabled = false;

    private static boolean meRunning = false;
    private static boolean initialized = false;

    //MATTC: added for notification customisation
    private static String sNotificationAppName = "Risk Android App";
    private static String sNotificationStartMessageForJourney = "Risk Android App Started";
    private static String sNotificationInProgressMessageForJourney = "Risk Android App in Journey";
    private static int sNotificationSmallIconId = R.drawable.ic_launcher;
    private static int sNotificationIconId = R.drawable.ic_launcher;

    public static ArrayList<JourneyData> sDetectedJourneys = new ArrayList<JourneyData>();

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    private Notification getCompatNotification() {

        //MATTC: if the name hasn't been set, try to auto resolve
        if (sNotificationAppName == null) {
            sNotificationAppName = getString(R.string.app_name);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        Intent notificationIntent = new Intent(Constants.ACTION.MAIN_ACTION);
        //notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        //MATTC: create large icon bitmap
        Bitmap bm = BitmapFactory.decodeResource(getResources(), sNotificationIconId);

        //MATTC: populate builder
        builder.setContentTitle(sNotificationAppName)
                .setTicker(sNotificationStartMessageForJourney)
                .setContentText(sNotificationInProgressMessageForJourney)
                .setLargeIcon(bm)
                .setSmallIcon(sNotificationSmallIconId);

        builder.setContentIntent(pendingIntent);
        //.setOngoing(true);

        Notification notification = builder.build();
        return notification;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "created");
        int eventBusPriority = 3;
        EventBus.getDefault().register(this, eventBusPriority);

        // make sure config is initialized
        JourneyConfig.init(this);

        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        }

        if (mGpsManager == null) {
            mGpsManager = new GpsManager(this);
        }

        if (mAccelDetection == null) {
            mAccelDetection = new AccelDetection(this);
        }

        // this has to be the last one as it can immediately issue a START event
        if (mJourneyDetection == null) {
            mJourneyDetection = new JourneyDetection(this);
        }
        if (automaticJourneyDetectionEnabled) {
            mJourneyDetection.setForcedMode(false);
        } else {
            mJourneyDetection.setForcedMode(true);
            mJourneyDetection.forceStart();
        }

        Logging.i(TAG, "battery level: " + Utils.getBatteryLevel(this) + "%");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logging.i(TAG, "battery level: " + Utils.getBatteryLevel(this) + "%");
        Logging.i(TAG, "onDestroy");
        mService = null;
        initialized = false;

        EventBus.getDefault().unregister(this);

        if (mAccelDetection != null) {
            mAccelDetection.stop();
        }
    }

    private void stopThisService() {
        Logging.i(TAG, "stopping our service");
        stopForeground(true);
        stopSelf();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        init(this);
        /*if(!PersistentSettings.getInstance(this).getBoolean("ServiceEnabled", true)){
            Logging.e(TAG, "we are not starting, service disabled");
			stopThisService();
			return START_NOT_STICKY;
		}*/

        if ((intent != null)) {
            if (Constants.ACTION.MONITORING_START.equals(intent.getAction())) {
                Logging.i(TAG, "Received Start Intent ");
                if (mService == null) {
                    mService = this;
                    Logging.d(TAG, "Service started");

                    startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, getCompatNotification());

                    startMonitoring();

                    // acquire wakelock
                    wl = Session.getWakeLockInstance();
                    if (wl == null) {
                        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ActivityDetectionWakeLock");
                        Session.setWakeLockInstance(wl);
                    }
                    wl.acquire();
                }

            } else if (Constants.ACTION.MONITORING_STOP.equals(intent.getAction())) {
                Logging.d(TAG, "Received Stop Intent");
                if (mService != null) {
                    stopMonitoring();
                    stopThisService();
                    if (wl.isHeld()) {
                        wl.release();
                    }
                }
            } else {
                ActivityRecognitionResult arr = ActivityRecognitionResult.extractResult(intent);
                if (arr != null) {
                    //Logging.i(TAG, "Activity received: " + arr.getMostProbableActivity().toString());
                    handleDetectedActivities((ArrayList) arr.getProbableActivities());
                } else {
                    Logging.i(TAG, "Normal Intent received");
                }
            }
            return START_STICKY;
        }
        return START_NOT_STICKY;
    }

    private static void startLogging(Context context) {
        Logging.getLogging(context);
        if (!Logging.getLoggingState(context)) {
            //Logging.enableLogging(true);
        }
    }


    /*******************************************************************
     * Activity Detections
     ****************************/
    private void buildGoogleApiClient() {
        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(ActivityRecognition.API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {

                    @Override
                    public void onConnectionSuspended(int arg) {
                        Logging.w(TAG, "Requested activity recognition update Suspended");
                        try {
                            mGoogleApiClient.connect();
                        } catch (Throwable e) {

                        }
                    }

                    @Override
                    public void onConnected(Bundle arg0) {
                        try {
                            Logging.i(TAG, "Requesting activity recognition updates");
                            //Intent intent = new Intent(getApplicationContext(), DetectedActivitiesIntentService.class);
                            Intent intent = new Intent(getApplicationContext(), JourneyService.class);
                            mActivityDetectionPendingIntent = PendingIntent.getService(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


                            // Request activity updates can be disabled if required
                            if (Constants.USE_ACTIVITY_MONITORING && automaticJourneyDetectionEnabled) {
                                ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mGoogleApiClient, Constants.DETECTION_INTERVAL_IN_MILLISECONDS, mActivityDetectionPendingIntent);
                            }

                            mGpsManager.startGpsMonitoring();

                        } catch (Throwable t) {
                            Logging.e(TAG, "Can't connect to activity recognition service", t);
                        }

                    }
                })

                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult arg0) {
                        Logging.e(TAG, "Requested activity recognition update failed!!!");
                    }
                });

        mGoogleApiClient = builder.build();
    }

    private void startMonitoring() {
        mGoogleApiClient.connect();
    }

    private void stopMonitoring() {
        try {
            Logging.i(TAG, "Stopping activity recognition updates");
            ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mGoogleApiClient, mActivityDetectionPendingIntent);
            mGpsManager.stopGpsMonitoring();
            mGoogleApiClient.disconnect();
        } catch (Throwable t) {
            Logging.e(TAG, "Tried to stop activity recognition updates", t);
        }

    }

    private void handleDetectedActivities(ArrayList<DetectedActivity> detectedActivities) {
        int type;
        int confidence;
        String typeStr;
        int maxconfidence = 0;
        int maxconfidence_type = -1;

        String logString = "";


        // Log each activity.
        //Log.i(TAG, "activities detected");
        for (DetectedActivity da : detectedActivities) {

            type = da.getType();
            typeStr = Utils.getActivityString(getApplicationContext(), da.getType());
            confidence = da.getConfidence();

            //Log.i(TAG, typeStr + " " + confidence + "%");

            // notify Journey manager
            mJourneyDetection.ActivityDetected(type, confidence);

            logString += ",";
            logString += Utils.getActivityString(getApplicationContext(), type);
            logString += ",";
            logString += type;
            logString += ",";
            logString += confidence;

            if (maxconfidence < confidence) {
                maxconfidence = confidence;
                maxconfidence_type = type;
            }
        }

        //Logging.i("Activity", "," + maxconfidence_type + "," + Constants.getActivityString(getApplicationContext(), maxconfidence_type) + "," + maxconfidence);
        Logging.i(TAG, logString);

        // Update the UI with the highest activity threshold
        EventBus.getDefault().post(new ActivityStatusEvent(maxconfidence_type));

        //EventBus.getDefault().post(new ActivitiesDetectedEvent(detectedActivities));
    }

    /*******************************************************************
     * END   Activity Detections
     ****************************/

    private String getLogFromLocation(Location loc) {
        String logString;
        if (loc == null) {
            logString = "NO Location: " + (float) mAccelDetection.getLastValue() + "," + Utils.getBatteryLevel(this);
        } else {
            double latitude = loc.getLatitude();
            double longitude = loc.getLongitude();
            float speed = loc.getSpeed();
            int battery = Utils.getBatteryLevel(this);

            float accel = (float) mAccelDetection.getLastValue();

            int satsUsedInFix = loc.getExtras().getInt("SATSUSED");

            int heading = 0;
            if (loc.hasBearing()) {
                heading = (int) loc.getBearing();
            }
            logString = "Location: " + latitude + "," + longitude + "," + speed + "," + heading + "," + accel + "," + battery + "%," + satsUsedInFix;
        }

        return logString;
    }

    /*******************************************************************
     * Event handler
     ****************************/
    public void onEvent(JourneyStartEvent event) {
        Log.i(TAG, "new Journey event");

        Location loc = mGpsManager.getLastGps();
        JourneyData journey = new JourneyData(loc, Utils.getBatteryLevel(this), mAccelDetection.getLastValue());
        if (event.start) {

            journey.setJourneyType(Constants.JOURNEY_TYPE.START);

            if (mAccelDetection != null) {
                mAccelDetection.start();
            }
        } else {
            journey.setJourneyType(Constants.JOURNEY_TYPE.STOP);

            if (mAccelDetection != null) {
                mAccelDetection.stop();
            }
        }

        String logString = getLogFromLocation(loc);
        Logging.i2(TAG, journey.journey + "-" + logString);

        //Add journey to list
        sDetectedJourneys.add(journey);
        saveJsonListData();


        // check if we shall stop our operation and exit this service
        if ((event.start == false) && (automaticJourneyDetectionEnabled == false)) {
            sendStopMonitoringIntent(this);
        }

        EventBus.getDefault().post(new JourneyDataEvent(journey));

        // check if we shall post the full Journey data on EventBus on stop
        if (event.start == false) {
            EventBus.getDefault().post(new JourneyListEvent(sDetectedJourneys));
        }
    }

    public void onEvent(NewLocationEvent event) {
        if (event.loc != null) {
            String logString = getLogFromLocation(event.loc);

            // feed to automatic journey detection
            // we only do further action if this is not generating START/STOP
            // it it does, we will get another event anyway
            if (!mJourneyDetection.newGpsLocation(event.loc)) {

                if (mJourneyDetection.isInJourney()) {
                    Logging.i2(TAG, logString);
                    JourneyData journey = new JourneyData(event.loc, Utils.getBatteryLevel(this), mAccelDetection.getLastValue());
                    journey.setJourneyType(Constants.JOURNEY_TYPE.TIMED);
                    // Add journey to list
                    sDetectedJourneys.add(journey);
                    saveJsonListData();
                    EventBus.getDefault().post(new JourneyDataEvent(journey));

                } else {
                    Logging.i(TAG, logString);
                }
            }
            EventBus.getDefault().post(new SpeedEvent(Math.round(event.loc.getSpeed())));
        }
    }

    public void onEvent(HighBrakingEvent event) {
        Location loc = mGpsManager.getLastGps();
        JourneyData journey = new JourneyData(loc, Utils.getBatteryLevel(this), mAccelDetection.getLastValue());
        journey.setJourneyType(Constants.JOURNEY_TYPE.HIGH_BRAKING);

        //Add journey to list
        sDetectedJourneys.add(journey);
        saveJsonListData();
        EventBus.getDefault().post(new JourneyDataEvent(journey));
    }

    public void onEvent(GpsGetNewFixEvent event) {
        mGpsManager.startGpsMonitoring();
    }

    /*******************************************************************
     * saving JSON data
     ****************************/
    private void saveJsonListData() {

        JSONArray data = new JSONArray();
        JSONObject journeyJSON;

        for (int i = 0; i < sDetectedJourneys.size(); i++) {
            JourneyData journeyData = sDetectedJourneys.get(i);
            journeyJSON = new JSONObject();
            JourneyData.fillJSONobject(journeyJSON, journeyData);
            data.put(journeyJSON);
        }

        String text = data.toString();

        try {
            FileOutputStream fos = getApplicationContext().openFileOutput(Constants.JSON_FILE_NAME, MODE_PRIVATE);
            fos.write(text.getBytes());
            fos.close();
        } catch (Exception e) {
            Logging.e(TAG, "File access error", e);
        }
    }

    private static void loadJsonListData(Context context) {
        try {
            FileInputStream fis = context.openFileInput(Constants.JSON_FILE_NAME);

            BufferedInputStream bis = new BufferedInputStream(fis);
            StringBuffer b = new StringBuffer();
            try {
                while (bis.available() != 0) {
                    char c = (char) bis.read();
                    b.append(c);
                }
                bis.close();
                fis.close();
            } catch (IOException e) {
                Logging.e(TAG, "IOException", e);
            }

            JSONArray data;
            try {
                data = new JSONArray(b.toString());
            } catch (JSONException e) {
                Logging.e(TAG, "JSONException", e);
                return;
            }
            // Clear list
            JourneyService.sDetectedJourneys.clear();
            for (int i = 0; i < data.length(); i++) {
                //Logging.d(TAG, "Json data found");
                JourneyData temp = null;
                try {
                    temp = new JourneyData(data.getJSONObject(i).getString("timestamp"), data.getJSONObject(i).getString("journey"));
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.lat = data.getJSONObject(i).getString("lat");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.lon = data.getJSONObject(i).getString("lon");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.speed = data.getJSONObject(i).getString("speed");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.accuracy = data.getJSONObject(i).getString("accuracy");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.battery = data.getJSONObject(i).getString("battery");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.heading = data.getJSONObject(i).getString("heading");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.accel = data.getJSONObject(i).getString("accel");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.sats = data.getJSONObject(i).getString("sats_used");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }
                try {
                    temp.type = data.getJSONObject(i).getString("type");
                } catch (JSONException e) {
                    Logging.e(TAG, "JSON exception", e);
                }


                // Add to arraylist
                JourneyService.sDetectedJourneys.add(temp);
            }
        } catch (FileNotFoundException e) {
            Logging.d(TAG, "File not found");
        }
    }

    /*******************************************************************
     * Public functions
     ****************************/
	/*  Initialises the library, including the logging functionality. Can be called multiple times
	 *	Param:	context - main activity context in order to issue system wise intents to start the background service
	 *	Ret: 	-
	 */
    public static void init(Context context) {
        if (initialized == false) {
            startLogging(context);
            loadJsonListData(context);
            // make sure config is initialized
            JourneyConfig.init(context);
            initialized = true;
        }
    }


    //MATTC: function for configuring notifications
    public static void ConfigureNotification(String notificationAppName,
                                             String notificationStartMessageForJourney,
                                             String notificationInProgressMessageForJourney,
                                             int notificationSmallIconId,
                                             int notificationIconId) {
        sNotificationAppName = notificationAppName;
        sNotificationStartMessageForJourney = notificationStartMessageForJourney;
        sNotificationInProgressMessageForJourney = notificationInProgressMessageForJourney;
        sNotificationIconId = notificationIconId;
        sNotificationSmallIconId = notificationSmallIconId;
    }

    /*  Starts the monitoring in either automatic Journey detection mod, or in user mode
     *	Param:  context - main activity context in order to issue system wise intents to start the background service
     *			autoMode - defines if service 	starts in auto mode (true) or user (false) mode for Journey detection
     *	Ret: -
     */
    public static void StartMonitoring(Context context, boolean autoMode) {
        automaticJourneyDetectionEnabled = autoMode;
        if (autoMode) {
            Logging.i(TAG, "StartService in auto mode");
        } else {
            Logging.i(TAG, "StartService in user mode");
        }
        // make sure config is initialized
        JourneyConfig.init(context);

        Intent mIntent = new Intent(context, JourneyService.class);
        mIntent.setAction(Constants.ACTION.MONITORING_START);
        context.startService(mIntent);

        meRunning = true;
    }

    /*  Stops the monitoring
     *	Param:	-
     *	Ret: 	-
     */
    public static void StopMonitoring(Context context) {
        if (automaticJourneyDetectionEnabled) {
            sendStopMonitoringIntent(context);
        } else {
            if (mJourneyDetection != null)
                mJourneyDetection.forceStop();
        }
        meRunning = false;
        automaticJourneyDetectionEnabled = false;
    }

    private static void sendStopMonitoringIntent(Context context) {
        Intent mIntent = new Intent(context, JourneyService.class);
        mIntent.setAction(Constants.ACTION.MONITORING_STOP);
        context.startService(mIntent);
        Logging.i(TAG, "StopService");

        meRunning = false;
        automaticJourneyDetectionEnabled = false;
    }

    /*  Query the current running status of the service
     *	Param:	-
     *	Ret: 	true if service is running, if not false
     */
    public static boolean isRunning() {
        return meRunning;
    }

    /*  Query the current mode of the service
     *	Param:	-
     *	Ret: 	true if service was last set in auto Journey detection mode, false if it was in user mode
     */
    public static boolean isInAutoMode() {
        return automaticJourneyDetectionEnabled;
    }

    /*  Deletes all Journey data stored by the library
     *	Param:	context - main activity context in order to issue system wise intents to start the background service
     *	Ret: 	-
     */
    public static void deleteAllJourneys(Context context) {
        context.deleteFile(Constants.JSON_FILE_NAME);
        JourneyService.sDetectedJourneys.clear();
        Logging.i(TAG, "all journey data deleted");
    }

    /*  requests all the journey data to be handed back as an Array
     *	Param:	context - main activity context in order to issue system wise intents to start the background service
     *	Ret: 	Array of the JourneyData
     */
    public static ArrayList<JourneyData> getAllJourneys(Context context) {
        return JourneyService.sDetectedJourneys;
    }


    /*  Queries the last known location
     *	Param:	-
     *	Ret: 	"Location" of the last knows position by the Android OS. Can be GPS or network based and null
     */
    public static Location getLastPosition() {
        if (mGpsManager == null) {
            return null;
        }
        return mGpsManager.getLastGps();
    }

    //MATTC: getters and setters for notification configuration
    public static String getNotificationAppName() {
        return sNotificationAppName;
    }

    public static void setNotificationAppName(String notificationAppName) {
        sNotificationAppName = notificationAppName;
    }

    public static String getNotificationStartMessageForJourney() {
        return sNotificationStartMessageForJourney;
    }

    public static void setNotificationStartMessageForJourney(String notificationStartMessageForJourney) {
        sNotificationStartMessageForJourney = notificationStartMessageForJourney;
    }

    public static String getNotificationInProgressMessageForJourney() {
        return sNotificationInProgressMessageForJourney;
    }

    public static void setNotificationInProgressMessageForJourney(String notificationInProgressMessageForJourney) {
        sNotificationInProgressMessageForJourney = notificationInProgressMessageForJourney;
    }

    public static int getNotificationSmallIconId() {
        return sNotificationSmallIconId;
    }

    public static void setNotificationSmallIconId(int notificationSmallIconId) {
        sNotificationSmallIconId = notificationSmallIconId;
    }

    public static int getNotificationIconId() {
        return sNotificationIconId;
    }

    public static void setNotificationIconId(int notificationIconId) {
        sNotificationIconId = notificationIconId;
    }
}
