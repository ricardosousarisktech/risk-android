package com.risk.socialdriver.journeyapp.gps;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import com.risk.socialdriver.journeyapp.Constants;
import com.risk.socialdriver.journeyapp.JourneyConfig;
import com.risk.socialdriver.journeyapp.events.NewLocationEvent;
import com.risk.socialdriver.journeyapp.loggers.Logging;
import com.risk.socialdriver.journeyapp.timeouts.AlarmTimeouts;
import com.risk.socialdriver.journeyapp.utils.Utils;

import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

/**
 * Created by zsolt on 02/07/2015.
 */
public class GpsManager implements LocationListener, GpsStatus.Listener, GpsStatus.NmeaListener{
    private final static String TAG = "GpsManager";

    protected Timer timerTimeout = null;

    private static final String listenerName = "GPS";
    protected String latestHdop;
    protected String latestPdop;
    protected String latestVdop;
    protected String geoIdHeight;
    protected String ageOfDgpsData;
    protected String dgpsId;
    protected Context mContext;

    private int lastSatellitesInFix =0;
    private int elapsedSeconds = 0;
    private int gpsTimeoutForFix = 120;

    protected LocationManager gpsLocationManager = null;


    /***************    Config options          ***************/
    private JourneyConfig.GpsConfig gpsConfig;

    public GpsManager(Context context) {
        mContext=context;
        gpsLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        gpsConfig = JourneyConfig.getGpsConfiguration();
    }

    /**
     * Event raised when a new fix is received.
     */
    public void onLocationChanged(Location loc) {

        try {
            if (loc != null) {
                removeGpsListener();
                AlarmTimeouts.startGpsTimeout(mContext, gpsConfig.updateInterval * 1000);

                Bundle b = new Bundle();
                b.putString("HDOP", this.latestHdop);
                b.putString("PDOP", this.latestPdop);
                b.putString("VDOP", this.latestVdop);
                b.putString("GEOIDHEIGHT", this.geoIdHeight);
                b.putString("AGEOFDGPSDATA", this.ageOfDgpsData);
                b.putString("DGPSID", this.dgpsId);

                b.putBoolean("PASSIVE", listenerName.equalsIgnoreCase("PASSIVE"));
                b.putString("LISTENER", listenerName);
                b.putInt("SATSUSED",lastSatellitesInFix);

                loc.setExtras(b);
                EventBus.getDefault().post(new NewLocationEvent(loc));

                this.latestHdop = "";
                this.latestPdop = "";
                this.latestVdop = "";
            }

        } catch (Exception ex) {
            Log.e(TAG, "GeneralLocationListener.onLocationChanged", ex);
        }

    }

    public void onProviderDisabled(String provider) {
        Log.i(TAG, "Provider disabled: " + provider);
        //loggingService.RestartGpsManagers();
    }

    public void onProviderEnabled(String provider) {

        Log.i(TAG, "Provider enabled: " + provider);
        //loggingService.RestartGpsManagers();
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        if (status == LocationProvider.OUT_OF_SERVICE) {
            Log.i(TAG, provider + " is out of service");
            //loggingService.StopManagerAndResetAlarm();
        }

        if (status == LocationProvider.AVAILABLE) {
            Log.i(TAG, provider + " is available");
        }

        if (status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
            Log.i(TAG, provider + " is temporarily unavailable");
            //loggingService.StopManagerAndResetAlarm();
        }
    }

    public void onGpsStatusChanged(int event) {
        int satellitesInFix = 0;
        int count = 0;
        int highSnrCount = 0;
        String satInfo = "";
        switch (event) {
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                Log.i(TAG, "GPS Event First Fix");
                //loggingService.SetStatus(loggingService.getString(R.string.fix_obtained));
                break;

            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:

                GpsStatus status = gpsLocationManager.getGpsStatus(null);

                for (GpsSatellite sat : status.getSatellites()) {
                    if(sat.getSnr() >3.0f) {
                        if (sat.usedInFix()) {
                            satellitesInFix++;
                        }
                        satInfo += sat.getPrn();
                        satInfo += "-";
                        satInfo += sat.getSnr();
                        if(sat.hasAlmanac()){
                            satInfo += "A";
                        }
                        if(sat.hasAlmanac()){
                            satInfo += "E";
                        }
                        satInfo += ", ";
                        count++;
                    }
                    if(sat.getSnr() >= gpsConfig.snrThreshold){
                        highSnrCount ++;
                    }
                }

                if( (elapsedSeconds > gpsConfig.timeCheckSnr) && (highSnrCount >= gpsConfig.minSatsCnt)){
                    gpsTimeoutForFix = gpsConfig.fixTimeoutHigh;
                }

                if(satellitesInFix >0){
                    lastSatellitesInFix = satellitesInFix;
                }

                /*int maxSatellites = status.getMaxSatellites();
                Iterator<GpsSatellite> it = status.getSatellites().iterator();
                int count = 0;

                while (it.hasNext() && count <= maxSatellites) {
                    it.next();
                    count++;
                    if(it)
                }*/

                Log.d(TAG, String.valueOf(count) + " seen, " + satellitesInFix + " used");
                if(count >0 ){
                    Log.d(TAG,satInfo);
                }

                //loggingService.SetSatelliteInfo(count);
                break;

            case GpsStatus.GPS_EVENT_STARTED:
                Log.i(TAG, "GPS started, waiting for fix");
                //loggingService.SetStatus(loggingService.getString(R.string.started_waiting));
                break;

            case GpsStatus.GPS_EVENT_STOPPED:
                Log.i(TAG, "GPS Event Stopped");
                //loggingService.SetStatus(loggingService.getString(R.string.gps_stopped));
                break;

        }
    }

    @Override
    public void onNmeaReceived(long timestamp, String nmeaSentence) {
        //loggingService.OnNmeaSentence(timestamp, nmeaSentence);

        if(Utils.IsNullOrEmpty(nmeaSentence)){
            return;
        }

        String[] nmeaParts = nmeaSentence.split(",");

        if (nmeaParts[0].equalsIgnoreCase("$GPGSA")) {

            if (nmeaParts.length > 15 && !Utils.IsNullOrEmpty(nmeaParts[15])) {
                this.latestPdop = nmeaParts[15];
            }

            if (nmeaParts.length > 16 &&!Utils.IsNullOrEmpty(nmeaParts[16])) {
                this.latestHdop = nmeaParts[16];
            }

            if (nmeaParts.length > 17 &&!Utils.IsNullOrEmpty(nmeaParts[17]) && !nmeaParts[17].startsWith("*")) {

                this.latestVdop = nmeaParts[17].split("\\*")[0];
            }
        }


        if (nmeaParts[0].equalsIgnoreCase("$GPGGA")) {
            if (nmeaParts.length > 8 &&!Utils.IsNullOrEmpty(nmeaParts[8])) {
                this.latestHdop = nmeaParts[8];
            }

            if (nmeaParts.length > 11 &&!Utils.IsNullOrEmpty(nmeaParts[11])) {
                this.geoIdHeight = nmeaParts[11];
            }

            if (nmeaParts.length > 13 &&!Utils.IsNullOrEmpty(nmeaParts[13])) {
                this.ageOfDgpsData = nmeaParts[13];
            }

            if (nmeaParts.length > 14 &&!Utils.IsNullOrEmpty(nmeaParts[14]) && !nmeaParts[14].startsWith("*")) {
                this.dgpsId = nmeaParts[14].split("\\*")[0];
            }
        }
    }

    public void startGpsMonitoring(){
        Log.i(TAG, "Requesting GPS location updates");
        // gps satellite based
        gpsLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, gpsConfig.updateInterval * 1000, 0, this);
        //MATTC: added wifi and cell tower detection for improved accuracy
        gpsLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, gpsConfig.updateInterval * 1000, 0, this);
        gpsLocationManager.addGpsStatusListener(this);
        gpsLocationManager.addNmeaListener(this);

        startTimeoutForFix();
    }

    public void stopGpsMonitoring() {
        AlarmTimeouts.stopGpsTimeout();
        removeGpsListener();
    }

    private void removeGpsListener(){
        Log.i(TAG, "Stopping GPS location updates");
        stopTimeoutForFix();
        gpsLocationManager.removeUpdates(this);
        gpsLocationManager.removeGpsStatusListener(this);
        gpsLocationManager.removeNmeaListener(this);
    }

    public Location getLastGps(){
        Location loc = gpsLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(isLocationValid(loc) == false)
        {
            loc = gpsLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(loc == null){
                Logging.i(TAG, "GPS and Network position not available");
            }else{
                Logging.i(TAG, "GPS position not available, using network");
            }
        }

        if(isLocationValid(loc) == false){
            return null;
        }

        return loc;
    }

    private boolean isLocationValid(Location loc){
        if(loc == null){
            return false;
        }

        long timediff = System.currentTimeMillis() - loc.getTime();
        //Log.i(TAG, "systime: " + System.currentTimeMillis());
        //Log.i(TAG, "Gps time: " + loc.getTime());
        //Log.i(TAG, "old location old by: " + timediff);
        if ( timediff <= ((long)gpsConfig.oldGpsValidTime*(long)1000)) {
            return true;
        }
        return false;
    }


    private void startTimeoutForFix(){
        if(timerTimeout == null){
            timerTimeout = new Timer();
        }else{
            stopTimeoutForFix();
            timerTimeout = new Timer();
        }
        elapsedSeconds = 0;
        gpsTimeoutForFix = gpsConfig.fixTimeoutLow;
        timerTimeout.schedule(new TimerTask() {
            @Override
            public void run() {
                elapsedSeconds++;
                if( elapsedSeconds >= gpsTimeoutForFix)
                {
                    Log.i(TAG,"wait for fix timeout reached");
                    removeGpsListener();
                    AlarmTimeouts.startGpsTimeout(mContext, gpsConfig.updateInterval*1000);
                }
            }
        }, 1000,1000);
    }


    private void stopTimeoutForFix(){
        if(timerTimeout != null) {
            timerTimeout.cancel();
            timerTimeout.purge();
            timerTimeout = null;
        }
    }
}
