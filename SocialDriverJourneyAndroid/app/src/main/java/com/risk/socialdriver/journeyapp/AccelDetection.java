package com.risk.socialdriver.journeyapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.util.Log;

import com.risk.socialdriver.journeyapp.events.HighBrakingEvent;
import com.risk.socialdriver.journeyapp.loggers.Logging;

import de.greenrobot.event.EventBus;

/**
 * Created by zsolt on 15/07/2015.
 */
public class AccelDetection {

    private final static  String    TAG = "AccelDetection";

    private double accelValue=0;
    private float accelValueG=0;
    private float accelValueEventG=0;
    private boolean accelHigh = false;

    private SensorManager mySensorManager;

    private CountDownTimer mTimerDetection;
    private CountDownTimer mTimerIgnore;

    private boolean highAccelerationWaitForTimeout  =   false;
    private boolean ignoreAccelEvents   =   false;

    private float ACCELERATION_THRESHOLD;
    private int ACCEL_DETECTION_TIMEOUT;
    private int ACCEL_EVENT_REPEAT_TIMEOUT;

    public AccelDetection(Context context){
        mySensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        ACCELERATION_THRESHOLD = JourneyConfig.getBrakingThreshold();
        ACCEL_DETECTION_TIMEOUT = JourneyConfig.getAccelerationExceedTime();
        ACCEL_EVENT_REPEAT_TIMEOUT = JourneyConfig.getAccelerationIgnoreTime();
    }

    public void start(){
        mySensorManager.registerListener(mySensorEventListener, mySensorManager
                        .getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);

        Logging.i(TAG, "started");

        mTimerDetection = new CountDownTimer(ACCEL_DETECTION_TIMEOUT, 100) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Log.i(TAG, "Event timeout reached");

                highAccelerationWaitForTimeout = false;

                // harsh acceleration detected
                Logging.i2(TAG, "high braking: " + accelValueEventG);
                EventBus.getDefault().post(new HighBrakingEvent(accelValueEventG));

                // ignore acceleration for a while
                ignoreAccelEvents = true;
                mTimerIgnore.start();
                Log.i(TAG, "ignoring high accel for a while");
            }
        };


        mTimerIgnore = new CountDownTimer(ACCEL_EVENT_REPEAT_TIMEOUT, 100) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                ignoreAccelEvents = false;
                Log.i(TAG,"Ignore timeout reached");
            }
        };
    }

    public void stop(){
        mySensorManager.unregisterListener(mySensorEventListener);
        Logging.i(TAG, "stopped");
    }

    public float getLastValue(){
        return accelValueG;
    }

    private final SensorEventListener mySensorEventListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent se) {
            //Log.i(TAG,"change detected");
            float x;
            float y;
            float z;

            x = se.values[0];
            y = se.values[1];
            z = se.values[2];

            accelValue = Math.sqrt( x*x + y*y + z*z) - Constants.ACCELERATION_G_VALUE;
            accelValueG = Math.abs((float) (accelValue/Constants.ACCELERATION_G_VALUE));
            //Log.i(TAG, String.valueOf(accelValue));


            if(Math.abs(accelValueG) > ACCELERATION_THRESHOLD){
                // check if we just got over it
                if(accelHigh == false){
                    accelHigh = true;

                    // check if we shall start the timeout and eventually store an event
                    if((highAccelerationWaitForTimeout == false) && (ignoreAccelEvents == false)) {
                        accelValueEventG = accelValueG;
                        startEventTimeout();
                    }
                }

                // keep track of the highest acceleration
                if(highAccelerationWaitForTimeout){
                    if(accelValueG > accelValueEventG) {
                        accelValueEventG = accelValueG;
                    }
                }
            }else{ // we are below threshold
                // check if we was above before
                if(accelHigh == true){
                    accelHigh = false;
                    // cancel the event timer
                    stopEventTimeout();
                }
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }


        private void startEventTimeout(){
            highAccelerationWaitForTimeout = true;
            mTimerDetection.start();
            Log.i(TAG, "Event timeout started");
        }
        private void stopEventTimeout(){
            highAccelerationWaitForTimeout = false;
            mTimerDetection.cancel();
            Log.i(TAG, "Event timeout stopped");
        }
    };
}
