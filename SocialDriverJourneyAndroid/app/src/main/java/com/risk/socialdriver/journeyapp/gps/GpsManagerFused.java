package com.risk.socialdriver.journeyapp.gps;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.risk.socialdriver.journeyapp.Constants;
import com.risk.socialdriver.journeyapp.JourneyConfig;
import com.risk.socialdriver.journeyapp.events.NewLocationEvent;
import com.risk.socialdriver.journeyapp.loggers.Logging;

import de.greenrobot.event.EventBus;

/**
 * Created by zsolt on 30/06/2015.
 */
public class GpsManagerFused implements LocationListener {
    private final String TAG = "GpsManager";

    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;


    public GpsManagerFused(GoogleApiClient googleApiClient) {
        mGoogleApiClient = googleApiClient;
    }


    @Override
    public void onLocationChanged(Location location) {
        // Assign the new location
        if (location != null) {
            mLastLocation = location;

            Log.i(TAG, "location provider: " + mLastLocation.getProvider());
            if(location.getExtras() != null){
                Log.i(TAG, "satellite count: " + location.getExtras().getInt("satellites"));
            }

                    //double latitude = mLastLocation.getLatitude();
                    //double longitude = mLastLocation.getLongitude();
                    //float speed = mLastLocation.getSpeed() * 3.6f;
                    //Logging.i(TAG, "Location: " + latitude + "," + longitude + "," + speed);

                    EventBus.getDefault().post(new NewLocationEvent(mLastLocation));
        }
    }

    public void startGpsMonitoring(){
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(JourneyConfig.getGpsConfiguration().updateInterval* 1000);
        mLocationRequest.setFastestInterval(Constants.GPS_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(Constants.GPS_DISPLACEMENT);


        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

        Logging.i(TAG, "GPS monitoring started");
    }


    public void stopGpsMonitoring() {
        if(mGoogleApiClient == null)
            return;

        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

            Logging.i(TAG, "GPS monitoring stopped");
        }catch (Throwable e){
            Logging.e(TAG,"error stopping GPS");
        }
    }

    public Location getLastGps(){
        if(mGoogleApiClient == null)
            return null;

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        return mLastLocation;
    }
}
