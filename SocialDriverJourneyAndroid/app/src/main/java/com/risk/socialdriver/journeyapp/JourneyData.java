package com.risk.socialdriver.journeyapp;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.risk.socialdriver.journeyapp.loggers.Logging;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Gareth Carter on 07/07/2015.
 */
public class JourneyData implements Parcelable {
    public String lat;
    public String lon;
    public String timestamp;
    public String speed;
    public String accuracy;
    public String journey;  // Start/Stop/HighAccel/In Progress/HighBraking
    public String battery;
    public String heading;
    public String accel;
    public String sats;
    public String type;

    protected JourneyData(String timestamp, String journey) {
        this.timestamp = timestamp;
        this.journey = journey;
        this.fillTypeField();
    }

    protected JourneyData(Location loc, int battLevel, float accelValue) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        String formatted = format.format(date);
        this.timestamp = formatted;

        this.accel = String.valueOf(accelValue);
        this.battery = String.valueOf(battLevel);

        if(loc == null){
            this.lat = "0";
            this.lon = "0";
            this.accuracy = "0";
            this.speed = "0";
            this.heading = "0";
            this.sats = "0";
        }else {
            this.lat = String.valueOf(loc.getLatitude());
            this.lon = String.valueOf(loc.getLongitude());

            if(loc.hasAccuracy()) {
                this.accuracy = String.valueOf(loc.getAccuracy());
            }else{
                this.accuracy = "";
            }
            if(loc.hasSpeed()) {
                this.speed = String.valueOf(loc.getSpeed());
            }else{
                this.speed ="";
            }
            if(loc.hasBearing()) {
                this.heading = String.valueOf(loc.getBearing());
            }else{
                this.heading = "";
            }

            Bundle extras = loc.getExtras();
            int satsUsed =0;
            if( extras != null){
                satsUsed = extras.getInt("SATSUSED");
            }
            this.sats = String.valueOf(satsUsed);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(lat);
        dest.writeString(lon);
        dest.writeString(timestamp);
        dest.writeString(speed);
        dest.writeString(accuracy);
        dest.writeString(journey);
        dest.writeString(battery);
        dest.writeString(heading);
        dest.writeString(accel);
        dest.writeString(sats);
        dest.writeString(type);
    }

    /*
    public static final Parcelable.Creator<JourneyData> CREATOR = new Parcelable.Creator<JourneyData>() {
        @Override
        public JourneyData createFromParcel(Parcel in) {
            return new JourneyData(in);
        }

        @Override
        public JourneyData[] newArray(int size) {
            return new JourneyData[size];
        }
    };
    */

    public static void fillJSONobject(JSONObject journeyJSON, JourneyData journeyData){
        try {
            journeyJSON.put("timestamp",journeyData.timestamp);
            journeyJSON.put("journey",journeyData.journey);
            journeyJSON.put("lat", journeyData.lat);
            journeyJSON.put("lon", journeyData.lon);
            journeyJSON.put("speed", journeyData.speed);
            journeyJSON.put("accuracy", journeyData.accuracy);
            journeyJSON.put("heading", journeyData.heading);
            journeyJSON.put("battery", journeyData.battery);
            journeyJSON.put("accel", journeyData.accel);
            journeyJSON.put("sats_used", journeyData.sats);
            journeyJSON.put("type", journeyData.type);
        } catch (JSONException e) {
            Logging.e("Journey Data save", "JSON error", e);
        }
    }

    public void fillJourneyField(){
        if( Constants.JOURNEY_TYPE.STOP.equals(this.type)){
            this.journey = Constants.JOURNEY_STINGS.STOP;
        }
        if( Constants.JOURNEY_TYPE.START.equals(this.type)){
            this.journey = Constants.JOURNEY_STINGS.START;
        }
        if( Constants.JOURNEY_TYPE.TIMED.equals(this.type)){
            this.journey = Constants.JOURNEY_STINGS.TIMED;
        }
        if( Constants.JOURNEY_TYPE.HIGH_ACCEL.equals(this.type)){
            this.journey = Constants.JOURNEY_STINGS.HIGH_ACCEL;
        }
        if( Constants.JOURNEY_TYPE.HIGH_BRAKING.equals(this.type)){
            this.journey = Constants.JOURNEY_STINGS.HIGH_BRAKING;
        }
    }

    public void fillTypeField(){
        if( Constants.JOURNEY_STINGS.STOP.equals(this.journey)){
            this.type = Constants.JOURNEY_TYPE.STOP;
        }
        if( Constants.JOURNEY_STINGS.START.equals(this.journey)){
            this.type = Constants.JOURNEY_TYPE.START;
        }
        if( Constants.JOURNEY_STINGS.TIMED.equals(this.journey)){
            this.type = Constants.JOURNEY_TYPE.TIMED;
        }
        if( Constants.JOURNEY_STINGS.HIGH_ACCEL.equals(this.journey)){
            this.type = Constants.JOURNEY_TYPE.HIGH_ACCEL;
        }
        if( Constants.JOURNEY_STINGS.HIGH_BRAKING.equals(this.journey)){
            this.type = Constants.JOURNEY_TYPE.HIGH_BRAKING;
        }
    }

    public void setJourneyType(String mType){
        this.type = mType;
        this.fillJourneyField();
    }
}