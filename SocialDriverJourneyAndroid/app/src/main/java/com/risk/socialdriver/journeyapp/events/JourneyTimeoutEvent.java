package com.risk.socialdriver.journeyapp.events;

/**
 * Created by zsolt on 02/07/2015.
 */
public class JourneyTimeoutEvent {

    public JourneyTimeoutEvent(){

    }

    public String toString() {
        return "Journey Timeout reached";
    }
}
