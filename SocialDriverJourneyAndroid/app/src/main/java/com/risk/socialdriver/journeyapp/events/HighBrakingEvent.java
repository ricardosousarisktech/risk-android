package com.risk.socialdriver.journeyapp.events;

/**
 * Created by zsolt on 24/08/2015.
 */
/*
 * Event sent when high braking force detected
 * Sent data is the detected g force in 'g'
 */
public class HighBrakingEvent {
    public final float value;

    public HighBrakingEvent(float breakValue){
        value = breakValue;
    }

    public String toString(){
        return "High Braking detected!";
    }
}
