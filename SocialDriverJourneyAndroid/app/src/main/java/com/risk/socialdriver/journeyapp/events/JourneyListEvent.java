package com.risk.socialdriver.journeyapp.events;

import com.risk.socialdriver.journeyapp.JourneyData;

import java.util.ArrayList;

/**
 * Created by zsolt on 04/08/2015.
 */
/*
 * Event sent when Journey STOP condition detected.
 * Sent data is the full Journey data list in an array
 */
public class JourneyListEvent {
    public final ArrayList<JourneyData> journeyList;

    public JourneyListEvent(ArrayList<JourneyData> journeys){
        journeyList = journeys;
    }
    public String toString(){
        return "";
    }
}
