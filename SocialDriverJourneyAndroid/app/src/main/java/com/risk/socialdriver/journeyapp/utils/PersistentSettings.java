//
// Copyright 2015 Jasteq
//

package com.risk.socialdriver.journeyapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.risk.socialdriver.journeyapp.loggers.Logging;

import java.util.Date;

public class PersistentSettings {

	private static final String TAG = "PersistentSettings";
	private static PersistentSettings sInstance;
	private SharedPreferences mSharedPreferences;

	private static final boolean logOnGetEnabled = false;
	private static final boolean logOnSetEnabled = false;
		
	public static PersistentSettings getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new PersistentSettings(context);
		}
		return (sInstance);
	}
	
	private PersistentSettings(Context context) {
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	}
		
	public boolean setInteger(String key, int value) {
		if(logOnSetEnabled) Logging.i(TAG, "Setting " + key + " to " + value);
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putInt(key, value);
		return (editor.commit());
	}

	public boolean setLong(String key, long value) {
		if(logOnSetEnabled) Logging.i(TAG, "Setting " + key + " to " + value);
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putLong(key, value);
		return (editor.commit());
	}

	public boolean setBoolean(String key, boolean value) {
		if(logOnSetEnabled) Logging.i(TAG, "Setting " + key + " to " + value);
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putBoolean(key, value);
		return (editor.commit());
	}

	public boolean setFloat(String key, float value) {
		if(logOnSetEnabled) Logging.i(TAG, "Setting " + key + " to " + value);
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putFloat(key, value);
		return (editor.commit());
	}

	public boolean setDate(String key, Date value) {
		if(logOnSetEnabled) Logging.i(TAG, "Setting " + key + " to " + value.toString());
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putLong(key, value.getTime());
		return (editor.commit());
	}
	
	public boolean setString(String key, String value) {
		if(logOnSetEnabled) Logging.i(TAG, "Setting " + key + " to " + value);
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putString(key, value);
		return (editor.commit());
	}

	public int getInteger(String key, int defaultReturnValue) {
		int value = mSharedPreferences.getInt(key, defaultReturnValue);
		if(logOnGetEnabled) Logging.i(TAG, "Getting " + key + " as " + value);
		return value;
	}

	public long getLong(String key, long defaultReturnValue) {
		long value = mSharedPreferences.getLong(key, defaultReturnValue);
		if(logOnGetEnabled) Logging.i(TAG, "Getting " + key + " as " + value);
		return value;
	}

	public boolean getBoolean(String key, boolean defaultReturnValue) {
		boolean value = mSharedPreferences.getBoolean(key, defaultReturnValue);
		if(logOnGetEnabled) Logging.i(TAG, "Getting " + key + " as " + value);
		return value;
	}

	public float getFloat(String key, float defaultReturnValue) {
		float value = mSharedPreferences.getFloat(key, defaultReturnValue);
		if(logOnGetEnabled) Logging.i(TAG, "Getting " + key + " as " + value);
		return value;
	}

	public String getString(String key, String defaultReturnValue, boolean hide) {
		String value = mSharedPreferences.getString(key, defaultReturnValue);
		if(logOnGetEnabled) {
			if (hide == true) {
				Logging.i(TAG, "Getting " + key);
			} else {
				Logging.i(TAG, "Getting " + key + " as " + value);
			}
		}
		return value;
	}

	public Date getDate(String key, Date defaultReturnValue) {
		Date ret = defaultReturnValue;
		Long value = mSharedPreferences.getLong(key, -1);
		if(logOnGetEnabled) Logging.i(TAG, "Getting " + key + " as " + value);
		if (value != -1)
			ret = new Date(value);
		if(logOnGetEnabled) {
			if (ret != null)
				Logging.i(TAG, "Returning " + key + " as " + ret.toString());
			else
				Logging.i(TAG, "Returning " + key + " as null");
		}
		return ret;
	}

    // This is used for Obfuscation, albeit not that complex
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}