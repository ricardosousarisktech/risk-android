/**
 * Copyright 2014 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.risk.socialdriver.journeyapp;

import android.content.Context;
import android.content.res.Resources;
import android.os.Environment;

import com.google.android.gms.location.DetectedActivity;
import com.risk.socialdriver.journeyapp.R;

import java.io.File;

/**
 * Constants used in this sample.
 */
public final class Constants {

    private Constants() {
    }

    public interface ACTION {
        public static String MAIN_ACTION        = "com.risk.socialdriver.journeyapp.action.mainActivity";
        public static String MONITORING_START   = "com.risk.socialdriver.journeyapp.journeymonitoring.action.start";
        public static String MONITORING_STOP    = "com.risk.socialdriver.journeyapp.journeymonitoring.action.stop";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 10133;
    }

    // determines if Activity monitoring used or not in combination with GPS
    public final static boolean USE_ACTIVITY_MONITORING = true;

    public final static double ACCELERATION_G_VALUE   = 9.9f;                   // value of g, some device reports 10 in stationary as gravity

    /**********************     Configurable Items, these are the default values      **********************************/
    // Journey detection related constans
    public final static long    JOURNEY_ACTIVITY_TimeoutStart   =   (20L*1000L);   // ms, within more than one Vehicle activity shall be detected
    public final static long    JOURNEY_ACTIVITY_TimeoutStop   =   (3L*60L*1000L); // ms, within no Vehicle or GPS speed shall be detected
    public final static int     JOURNEY_ACTIVITY_confidenceThresholdStart    =   95;// in %
    public final static int     JOURNEY_ACTIVITY_confidenceThresholdStop    =   85; // in %
    public final static float   JOURNEY_GPS_speedThresholdStart_ms  =   7.0f;   // m/s
    public final static float   JOURNEY_GPS_speedThresholdStop_ms  =   2.5f;    // m/s
    public final static int     GPS_UPDATE_INTERVAL = 60;                       // sec
    public final static float   ACCELERATION_THRESHOLD   = 0.375f;              // g above it detects high acceleration
    public final static float   BRAKING_THRESHOLD = 0.375f;                  // g above it detects high braking
    public final static int     ACCEL_DETECTION_TIMEOUT = 300;                  // in ms, time till the acceleration has to exceed the threshold to generate an event
    public final static int     ACCEL_EVENT_REPEAT_TIMEOUT  =   30000;          // in ms, time within another event can not be generated

    public final static int     GPS_TIMEOUT_LOW_SIGNAL          =   20;         // sec timeout for fix if signal is low
    public final static int     GPS_TIMEOUT_HIGH_SIGNAL         =   60;         // sec timeout for fix if signal is high
    public final static float   GPS_SIGNAL_THRESHOLD            =   15.0f;      // SNR of satellites, below this short timeout used as unlikely to get fix
    public final static int     GPS_TIME_CHECK_SNR              =   10;         // time after check snr
    public final static int     GPS_MIN_SATS                    =   4;          // min sats count to use high timeout
    public final static int     GPS_OLD_FIX_VALID_TIME          =   300;        // sec, till old fix considered valid
    /************************************************************************************/

    public static int GPS_FASTEST_INTERVAL = 10000; // ms, used only in the Fused library which does not work well
    public static int GPS_DISPLACEMENT = 10; // meters, used only in the Fused library which does not work well

    /*
    public static final String PACKAGE_NAME = "com.risk.socialdriver.journeyapp";
    public static final String BROADCAST_ACTION = PACKAGE_NAME + ".BROADCAST_ACTION";
    public static final String ACTIVITY_EXTRA = PACKAGE_NAME + ".ACTIVITY_EXTRA";
    public static final String SHARED_PREFERENCES_NAME = PACKAGE_NAME + ".SHARED_PREFERENCES";
    public static final String ACTIVITY_UPDATES_REQUESTED_KEY = PACKAGE_NAME + ".ACTIVITY_UPDATES_REQUESTED";
    public static final String DETECTED_ACTIVITIES = PACKAGE_NAME + ".DETECTED_ACTIVITIES";*/


    public static final String LOG_FILE_NAME  =   Environment.getExternalStorageDirectory() + "/Activity_log.txt";
    public static final String LOG_FILE_NAME_2  =   Environment.getExternalStorageDirectory() + "/Journey_log.txt";

    // file to stora the Journey data internally
    public static final String JSON_FILE_NAME = "JourneyJson.txt";


    /**
     * The desired time between activity detections. Larger values result in fewer activity
     * detections while improving battery life. A value of 0 results in activity detections at the
     * fastest possible rate. Getting frequent updates negatively impact battery life and a real
     * app may prefer to request less frequent updates.
     */
    public static final long DETECTION_INTERVAL_IN_MILLISECONDS = 2000;

    public interface  JOURNEY_TYPE {
        public final static String START = "3";
        public final static String STOP = "4";
        public final static String TIMED = "1";
        public final static String HIGH_ACCEL = "51";
        public final static String HIGH_BRAKING = "52";
    }
    public interface  JOURNEY_STINGS {
        public final static String START = "Start";
        public final static String STOP = "Stop";
        public final static String TIMED = "In Progress";
        public final static String HIGH_ACCEL = "HighAccel";
        public final static String HIGH_BRAKING = "HighBraking";
    }
}
