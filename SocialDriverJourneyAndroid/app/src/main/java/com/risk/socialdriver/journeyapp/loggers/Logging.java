//
//Copyright 2015 Jasteq
//

package com.risk.socialdriver.journeyapp.loggers;

import android.content.Context;
import android.util.Log;

import com.risk.socialdriver.journeyapp.utils.PersistentSettings;

public class Logging {
	private static Logging mLogging = null;
	private static String LOGGINGONKEY = "LoggingOn";
	private static Boolean mLoggingOn = false;
	private static Context mContext;

	private static final boolean	DEFAULT_STATE	=	false;

	private final static boolean iEnabled =true; 

	public static Logging getLogging(Context context) {
		if(mLogging == null){
			mLogging = new Logging();
			mContext = context;
			mLoggingOn = getLoggingState(context);
			if(mLoggingOn) {
				FileLogger.getLogger(mContext);
			}
		}
		return mLogging;
	}

	private Logging() {
	}

    public static void restartLogging() {
        mLogging = new Logging ();
        FileLogger.getLogger(mContext);
    }

	public static void enableLogging(Boolean onOff) {
		mLoggingOn = onOff;
		PersistentSettings.getInstance(mContext).setBoolean(LOGGINGONKEY, onOff);

		if(mLoggingOn) {
			FileLogger.getLogger(mContext);
		}
	}

	public static Boolean getLoggingState(Context context){
		return PersistentSettings.getInstance(context).getBoolean(LOGGINGONKEY, DEFAULT_STATE);
	}

	public static void i(String prefix, String str) {
		Log.i(prefix, str);
		if(iEnabled){
			if (mLogging != null && mLoggingOn) {
				FileLogger.log(prefix, str);
			}
		}
	}

	public static void i2(String prefix, String str) {
		Log.i(prefix, str);
		if(iEnabled){
			if (mLogging != null && mLoggingOn) {
				FileLogger.log2(prefix, str);
				FileLogger.log(prefix, str);
			}
		}
	}

	public static void e(String prefix, String str) {
		Log.e(prefix, str);
		if (mLogging != null && mLoggingOn) {
			FileLogger.log(prefix, str);
		}
		// Level 'e' logs are sent to the system (for diagnostics etc)
		postToEventBus(prefix, str, ""); // We weren't passed an exception so don't pass one on
	}

	public static void d(String prefix, String str) {
		Log.d(prefix, str);
		if (mLogging != null && mLoggingOn) {
			FileLogger.log(prefix, str);
		}
	}

	public static void w(String prefix, String str) {
		Log.w(prefix, str);
		if (mLogging != null && mLoggingOn) {
			FileLogger.log(prefix, str);
		}
	}

	public static void e(String prefix, String str, Throwable e) {
		Log.e(prefix, str, e);
		if (mLogging != null) {
			FileLogger.log(prefix, str);
			FileLogger.log(prefix, e.toString());
			FileLogger.log(prefix, Log.getStackTraceString(e));
		}
		// Level 'e' logs are sent to the system (for diagnostics etc)
		postToEventBus(prefix, str, e.getMessage());
	}

	private static void postToEventBus(String tag, String description, String exceptionString) {
		// Todo: get all the extra locat and system info
		//EventBus.getDefault().post(new ErrorEvent(tag, description, exceptionString, "", "", ""));
	}
}