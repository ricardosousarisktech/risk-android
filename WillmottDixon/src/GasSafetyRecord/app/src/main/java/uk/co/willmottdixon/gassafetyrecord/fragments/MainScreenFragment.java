package uk.co.willmottdixon.gassafetyrecord.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;

import uk.co.willmottdixon.gassafetyrecord.R;
import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.adapters.CompletedFormsListAdapter;
import uk.co.willmottdixon.gassafetyrecord.callbacks.PDFCallbacks;
import uk.co.willmottdixon.gassafetyrecord.core.FormManager;
import uk.co.willmottdixon.gassafetyrecord.fragments.forms.FormDetailsFragment;
import uk.co.willmottdixon.gassafetyrecord.models.FormModel;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.DimmableButton;
import uk.co.willmottdixon.gassafetyrecord.utilities.PDFHelper;

/**
 * Created by Dev on 07/08/2015.
 */
public class MainScreenFragment extends BaseFragment implements View.OnClickListener, PDFCallbacks
{

    DimmableButton mFormActionButton;
    ListView mCompletedForms;
    private CompletedFormsListAdapter mCompletedFormsListAdapter;
    private Handler mHandler = new Handler();

    @Override
    public void onResume()
    {
        super.onResume();
        if (FormManager.getStaticInstance().getCurrentForm() == null)
        {
            mFormActionButton.setText("Start New Form");
        }
        else
        {
            mFormActionButton.setText("Continue Form");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment

        setTitle("Home");

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mFormActionButton = (DimmableButton) view.findViewById(R.id.formActionButton);
        mCompletedForms = (ListView) view.findViewById(R.id.completedForms);
        mCompletedFormsListAdapter = new CompletedFormsListAdapter(getActivity(), R.layout.completed_forms_list_item, R.id.menuItemLayout, FormManager.getStaticInstance().getCompletedForms());
        mCompletedForms.setAdapter(mCompletedFormsListAdapter);
        mCompletedForms.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

                String[] options = new String[]{"Edit Form", "View PDF", "Upload PDF", "Delete PDF"};
                final FormModel model = mCompletedFormsListAdapter.getItem(position);
                AlertDialog.Builder optionBuilder = new AlertDialog.Builder(getActivity());

                optionBuilder.setTitle(model.getTenantDetails().getTitle() + " " + model.getTenantDetails().getName() + " - " + model.getCertificateSerialNumber())
                        .setItems(options, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, final int which)
                            {
                                mHandler.post(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        FormManager.getStaticInstance().setPDFCallbacks(MainScreenFragment.this);
                                        String pdfFilePath = model.getPDFFilePath();
                                        if (pdfFilePath == null)
                                        {
                                            Toast.makeText(getActivity(), "The pdf does not exist, creating it now.", Toast.LENGTH_SHORT).show();
                                            try
                                            {
                                                FormManager.getStaticInstance().generatePDFWithForm(model, getActivity());
                                            } catch (Exception e)
                                            {
                                                e.printStackTrace();
                                            }
                                            return;
                                        }

                                        File pdfFile = new File(pdfFilePath);
                                        if (!pdfFile.exists())
                                        {
                                            Toast.makeText(getActivity(), "The pdf does not exist, creating it now.", Toast.LENGTH_SHORT).show();
                                            try
                                            {
                                                FormManager.getStaticInstance().generatePDFWithForm(model, getActivity());
                                            } catch (Exception e)
                                            {
                                                e.printStackTrace();
                                            }
                                            return;
                                        }


                                        switch (which)
                                        {
                                            case 0:
                                            {
                                                FormManager.getStaticInstance().moveFormBackToCurrent(model, getActivity());
                                                break;
                                            }
                                            case 1:
                                            {
                                                PDFHelper.showPDF(getActivity(), pdfFile);
                                                break;
                                            }
                                            case 2:
                                            {
                                                FormManager.getStaticInstance().uploadToS3(pdfFile, getActivity(), model);
                                                break;
                                            }
                                            case 3:
                                            {
                                                FormManager.getStaticInstance().deleteForm(model, getActivity(), mCompletedFormsListAdapter);
                                                break;
                                            }
                                        }
                                    }
                                });
                                dialog.dismiss();
                            }
                        }).create();
                optionBuilder.show();
            }
        });

        if (FormManager.getStaticInstance().getCurrentForm() == null)
        {
            mFormActionButton.setText("Start New Form");
        }
        else
        {
            mFormActionButton.setText("Continue Form");

        }

        mFormActionButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v)
    {
        if (v == mFormActionButton)
        {
            if (FormManager.getStaticInstance().getCurrentForm() == null)
            {
                FormManager.getStaticInstance().createNewForm();
                FormManager.getStaticInstance().saveCurrentForm();
                mFormActionButton.setText("Continue Form");
            }

            MainActivity.sharedInstance().showFragment(new FormDetailsFragment(), true);
        }
    }

    @Override
    public void pdfGenerated(File pdfFile)
    {
    }

    @Override
    public void pdfUploaded(FormModel formModel)
    {
        formModel.setUploaded(true);
        FormManager.getStaticInstance().archiveForm(formModel);
        mCompletedFormsListAdapter.notifyDataSetChanged();
    }

    @Override
    public void pdfUploadFailed(FormModel formModel)
    {
        formModel.setUploaded(false);
        FormManager.getStaticInstance().archiveForm(formModel);
        Toast.makeText(getActivity(), "Upload Failed. Please ensure that you upload this pdf from the main screen.", Toast.LENGTH_SHORT).show();
        mCompletedFormsListAdapter.notifyDataSetChanged();
    }

}
