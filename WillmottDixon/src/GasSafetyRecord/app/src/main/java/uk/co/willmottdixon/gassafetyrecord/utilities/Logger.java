package uk.co.willmottdixon.gassafetyrecord.utilities;

import android.util.Log;

@SuppressWarnings("UnusedDeclaration")
public class Logger {

    public static final int ERROR_LEVEL_VERBOSE = 0;
    public static final int ERROR_LEVEL_INFO = 1;
    public static final int ERROR_LEVEL_DEBUG = 2;
    public static final int ERROR_LEVEL_WARNING = 3;
    public static final int ERROR_LEVEL_ERROR = 4;

    public static void LogDebugMessage(String tag, String message) {
        Log.d(tag, message);
    }

    public static void LogDebugMessage(String tag, String message, Throwable throwable) {
        Log.d(tag, message, throwable);
    }

    public static void LogMessage(String tag, String message) {
        Log.i(tag, message);
    }

    public static void LogMessage(String tag, String message, Throwable throwable) {
        Log.i(tag, message, throwable);
    }

    public static void LogErrorMessage(String tag, String message) {
        Log.e(tag, message);
    }

    public static void LogErrorMessage(String tag, String message, Throwable throwable) {
        Log.e(tag, message, throwable);
    }
}
