package uk.co.willmottdixon.gassafetyrecord.fragments.forms;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uk.co.willmottdixon.gassafetyrecord.callbacks.FragmentSubFormCompleteCallback;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.BaseFormItem;

/**
 * Created by Dev on 17/08/2015.
 */
public abstract class SubFormFragment extends BaseFormFragment
{
    BaseFormItem mFormItem;

    public static String SUB_FORM_CHILD_ITEM_KEY = "SUB_FORM_CHILD_ITEM_KEY";

    private FragmentSubFormCompleteCallback mFragmentSubFormCompleteCallback;

    public SubFormFragment()
    {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Bundle arguments = getArguments();
        if(arguments != null)
        {
            if (arguments.containsKey(SUB_FORM_CHILD_ITEM_KEY))
            {
                mFormItem = (BaseFormItem) arguments.get(SUB_FORM_CHILD_ITEM_KEY);
            }
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public BaseFormItem getFormItem()
    {
        return mFormItem;
    }

    public void setFormItem(BaseFormItem formItem)
    {
        mFormItem = formItem;
    }

    @Override
    protected void continueForm()
    {
        mFragmentSubFormCompleteCallback.onComplete();
    }

    public FragmentSubFormCompleteCallback getFragmentSubFormCompleteCallback()
    {
        return mFragmentSubFormCompleteCallback;
    }

    public void setFragmentSubFormCompleteCallback(FragmentSubFormCompleteCallback fragmentSubFormCompleteCallback)
    {
        mFragmentSubFormCompleteCallback = fragmentSubFormCompleteCallback;
    }
}
