package uk.co.willmottdixon.gassafetyrecord.utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressWarnings("UnusedDeclaration")
public class UiUtilities {

    private static Point sScreenSize;
    private static Typeface sTypefaceBold;
    private static Typeface sTypefaceLight;

    public static int dpToPixel(Context context, int dp)
    {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float fPixels = metrics.density * dp;
        return (int) (fPixels + 0.5f);
    }

    public static Point getScreenDimensions(Activity activity)
    {
        if(sScreenSize == null) {
            Display display = activity.getWindowManager().getDefaultDisplay();
            sScreenSize = new Point();
            display.getSize(sScreenSize);
        }
        return sScreenSize;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if (listItem != null) {
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void prepareFonts(Context context)
    {
        if(sTypefaceBold == null) {
            sTypefaceBold = Typeface.create("HelveticaNeue", Typeface.BOLD);
        }

        if(sTypefaceLight == null) {
            sTypefaceLight = Typeface.createFromAsset(context.getAssets(), "Fonts/HelveticaNeueUL.otf");
        }
    }

    public static void setMultifontStringOnTextView(TextView textView, Context context) {
        setMultifontStringOnTextView(textView, textView.getText().toString(), context);
    }

    public static void setMultifontStringOnTextView(TextView textView, String content, Context context) {

        prepareFonts(context);

        textView.setTypeface(sTypefaceBold, Typeface.BOLD);
        textView.setTypeface(sTypefaceLight, Typeface.NORMAL);

        textView.setText(Html.fromHtml(content), TextView.BufferType.SPANNABLE);
    }

    public static void setMultifontStringOnButton(Button button, Context context) {
        setMultifontStringOnButton(button, button.getText().toString(), context);
    }

    public static void setMultifontStringOnButton(Button button, String content, Context context) {
        prepareFonts(context);

        button.setTypeface(sTypefaceBold, Typeface.BOLD);
        button.setTypeface(sTypefaceLight, Typeface.NORMAL);

        button.setText(Html.fromHtml(button.getText().toString()), TextView.BufferType.SPANNABLE);
    }

    public static void setDefaultFontStringOnTextView(TextView textView, Context context) {

        prepareFonts(context);

        textView.setTypeface(sTypefaceBold);
    }
}
