package uk.co.willmottdixon.gassafetyrecord.core;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by Dev on 17/08/2015.
 */
public abstract class ExtendedTextWatcher implements TextWatcher
{
    private final EditText mEditText;

    public abstract void beforeTextChanged(CharSequence s, int start, int count, int after, EditText editText);
    public abstract void onTextChanged(CharSequence s, int start, int before, int count,  EditText editText);
    public abstract void afterTextChanged(Editable s, EditText editText);

    public ExtendedTextWatcher(EditText editText)
    {
        mEditText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {
        beforeTextChanged(s, start, count, after, mEditText);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        onTextChanged(s, start, before, count, mEditText);
    }

    @Override
    public void afterTextChanged(Editable s)
    {
        afterTextChanged(s, mEditText);
    }
}
