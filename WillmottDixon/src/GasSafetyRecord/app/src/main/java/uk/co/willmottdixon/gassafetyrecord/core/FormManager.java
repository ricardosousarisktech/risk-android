package uk.co.willmottdixon.gassafetyrecord.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.amazonmobileanalytics.AnalyticsEvent;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.gson.Gson;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.application.WDApplication;
import uk.co.willmottdixon.gassafetyrecord.callbacks.PDFCallbacks;
import uk.co.willmottdixon.gassafetyrecord.fragments.forms.FormDetailsFragment;
import uk.co.willmottdixon.gassafetyrecord.models.FormModel;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.ProgressHUD;
import uk.co.willmottdixon.gassafetyrecord.utilities.FileManager;

/**
 * Created by Dev on 07/08/2015.
 */
@SuppressWarnings("unused")
public class FormManager
{

    private final boolean kDeleteSavedForms = false;

    @SuppressWarnings("FieldCanBeLocal")
    private final String kCompletedFormsKey = "CompletedForms";
    private final String kCurrentFormKey = "CurrentForm";

    private ArrayList<FormModel> mCompletedForms;
    private static FormManager sStaticInstance;
    private FormModel mCurrentForm;
    private TransferObserver mUpload;
    private static AmazonS3Client mS3Client;
    private TransferUtility mTransferUtility;
    private Handler mHandler = new Handler();

    /**
     * Getter for property 'PDFCallbacks'.
     *
     * @return Value for property 'PDFCallbacks'.
     */
    public PDFCallbacks getPDFCallbacks()
    {
        return mPDFCallbacks;
    }

    /**
     * Setter for property 'PDFCallbacks'.
     *
     * @param PDFCallbacks Value to set for property 'PDFCallbacks'.
     */
    public void setPDFCallbacks(PDFCallbacks PDFCallbacks)
    {
        mPDFCallbacks = PDFCallbacks;
    }

    private PDFCallbacks mPDFCallbacks;


    /**
     * Getter for property 'completedForms'.
     *
     * @return Value for property 'completedForms'.
     */
    public ArrayList<FormModel> getCompletedForms()
    {
        return mCompletedForms;
    }

    /**
     * Setter for property 'completedForms'.
     *
     * @param completedForms Value to set for property 'completedForms'.
     */
    public void setCompletedForms(ArrayList<FormModel> completedForms)
    {
        mCompletedForms = completedForms;
    }


    public static FormManager getStaticInstance()
    {
        if (sStaticInstance == null)
        {
            sStaticInstance = new FormManager();
        }
        return sStaticInstance;
    }

    @SuppressWarnings("unchecked")
    private FormManager()
    {
        //noinspection ConstantConditions - User defined at compile time
        if (kDeleteSavedForms)
        {
            FileManager.deleteObject(kCompletedFormsKey, WDApplication.getAppContext());
        }

        Object loadedObject = FileManager.getObject(kCurrentFormKey, WDApplication.getAppContext());
        Gson gson = new Gson();

        if (loadedObject instanceof FormModel)
        {
            mCurrentForm = (FormModel) loadedObject;
        }
        else if (loadedObject instanceof String)
        {
            mCurrentForm = gson.fromJson((String) loadedObject, FormModel.class);
        }

        if (mCompletedForms == null)
        {
            mCompletedForms = new ArrayList<>();
        }

        loadedObject = FileManager.getObject(kCompletedFormsKey, WDApplication.getAppContext());
        if (loadedObject instanceof String)
        {
            try
            {
                //noinspection ConstantConditions - Try/Catch in palce
                FormModel[] formModels = gson.fromJson((String) loadedObject, FormModel[].class);
                Collections.addAll(mCompletedForms, formModels);

            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }

    public static FormModel currentForm()
    {
        return getStaticInstance().mCurrentForm;
    }

    /**
     * Getter for property 'currentForm'.
     *
     * @return Value for property 'currentForm'.
     */
    public FormModel getCurrentForm()
    {
        return mCurrentForm;
    }

    public void createNewForm()
    {
        mCurrentForm = new FormModel();
        saveCurrentForm();
    }

    public synchronized void saveCurrentForm()
    {
        if (mCurrentForm != null)
        {
            Gson gson = new Gson();
            String s = gson.toJson(getCurrentForm());
            FileManager.saveObject(s, kCurrentFormKey, WDApplication.getAppContext());
        }
    }

    public void generatePDFWithCurrentForm(Activity activity) throws IOException, DocumentException
    {
        generatePDFWithForm(getCurrentForm(), activity);
    }

    public void generatePDFWithForm(final FormModel form, final Activity activity) throws IOException, DocumentException
    {
        final ProgressHUD hud = ProgressHUD.show(activity, "Generating PDF", true, false, null);

        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    InputStream inputStream = WDApplication.getAppContext().getAssets().open("form.pdf");
                    PdfReader pdfReader = new PdfReader(inputStream);

                    Date date = getCurrentForm().getCompletedDate();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault());
                    String dateString = simpleDateFormat.format(date);

                    final File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" +
                            "WD_" + getCurrentForm().getCertificateSerialNumber() + "_" + getCurrentForm().getEngineerRegistrationNumber() + "_" + dateString + ".pdf");
                    FileOutputStream fileOutputStream = new FileOutputStream(outputFile, false);
                    PdfStamper stamper = new PdfStamper(pdfReader, fileOutputStream);

                    AcroFields fields = stamper.getAcroFields();
                    fields.setGenerateAppearances(true);

                    form.mapToPdfFields(stamper, fields);

                    stamper.setFormFlattening(true);
                    stamper.close();
                    fileOutputStream.close();

                    hud.dismiss();

                    form.setPDFFilePath(outputFile.getAbsolutePath());

                    if (mPDFCallbacks != null)
                    {
                        mPDFCallbacks.pdfGenerated(outputFile);
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();

    }

    public void archiveForm(FormModel formModel)
    {
        if (!mCompletedForms.contains(formModel))
        {
            mCompletedForms.add(formModel);
        }
        saveCompletedForms();
    }

    public void archiveCurrentForm()
    {
        archiveForm(getCurrentForm());
        deleteCurrentForm();
        saveCurrentForm();
    }

    private void deleteCurrentForm()
    {
        FileManager.deleteObject(kCurrentFormKey, WDApplication.getAppContext());
        mCurrentForm = null;
    }

    public synchronized void saveCompletedForms()
    {
        if (mCompletedForms != null)
        {
            Gson gson = new Gson();

            FormModel[] tmpModels;
            tmpModels = new FormModel[mCompletedForms.size()];
            for (int i = 0; i < mCompletedForms.size(); i++)
            {
                tmpModels[i] = mCompletedForms.get(i);
            }
            String s = gson.toJson(tmpModels);
            FileManager.saveObject(s, kCompletedFormsKey, WDApplication.getAppContext());
        }
    }

    public static AmazonS3Client getS3Client(Context context)
    {
        if (mS3Client == null)
        {
            AWSCredentials credentials = new AWSCredentials()
            {
                @Override
                public String getAWSAccessKeyId()
                {
                    return Constants.S3_KEY_ID;
                }

                @Override
                public String getAWSSecretKey()
                {
                    return Constants.S3_SECRET;
                }
            };

            CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                    context, // Context
                    "eu-west-1:aed8af88-0340-4199-ab0c-4821aa05ad7e", // Identity Pool ID
                    Regions.EU_WEST_1 // Region
            );

            mS3Client = new AmazonS3Client(credentials);
        }
        return mS3Client;
    }

    public void uploadToS3(final java.io.File myFile, final Context context, final FormModel formModel)
    {
        if (Looper.myLooper() != Looper.getMainLooper())
        {
            mHandler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    uploadToS3(myFile, context, formModel);
                }
            });
            return;
        }


        AnalyticsEvent uploadAnalytics = MainActivity.analytics.getEventClient().createEvent("Form Upload").withAttribute("Form Name",  myFile.getName());
        //Record the Level Complete event
        MainActivity.analytics.getEventClient().recordEvent(uploadAnalytics);

        final ProgressHUD hud = ProgressHUD.show(context, "Uploading Gas Safety Record", true, true, null);

        mTransferUtility = new TransferUtility(getS3Client(context), context);

        mUpload = mTransferUtility.upload("matrix-forms", myFile.getName(), myFile);

        mUpload.setTransferListener(new TransferListener()
        {
            @Override
            public void onStateChanged(int id, TransferState state)
            {
                Log.i("STATE", state.name());
                switch (state)
                {
                    case WAITING:
                        hud.setMessage("Preparing upload");
                        break;
                    case IN_PROGRESS:
                        hud.setMessage("Uploading Gas Safety Record");
                        break;
                    case PAUSED:
                        hud.setMessage("Paused");
                        break;
                    case RESUMED_WAITING:
                        hud.setMessage("Waiting to resume");
                        break;
                    case COMPLETED:
                        hud.setMessage("Uploaded");
                        hud.dismiss();
                        if (mPDFCallbacks != null)
                        {
                            mPDFCallbacks.pdfUploaded(formModel);
                        }
                        break;
                    case CANCELED:
                        hud.setMessage("Cancelled");
                        hud.dismiss();
                        if (mPDFCallbacks != null)
                        {
                            mPDFCallbacks.pdfUploadFailed(formModel);
                        }
                        break;
                    case FAILED:
                        hud.setMessage("Failed");
                        hud.dismiss();
                        if (mPDFCallbacks != null)
                        {
                            mPDFCallbacks.pdfUploadFailed(formModel);
                        }
                        break;
                    case WAITING_FOR_NETWORK:
                        hud.setMessage("Waiting for network connection");
                        break;
                    case PART_COMPLETED:
                        break;
                    case PENDING_CANCEL:
                        break;
                    case PENDING_PAUSE:
                        break;
                    case PENDING_NETWORK_DISCONNECT:
                        break;
                    case UNKNOWN:
                        break;
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal)
            {
            }

            @Override
            public void onError(int id, Exception ex)
            {
                hud.dismiss();
                Log.e("ERROR", ex.getLocalizedMessage());
                Toast.makeText(context, "Error - " + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void deleteForm(@NonNull final FormModel model, @NonNull final Activity activity, @Nullable final ArrayAdapter adapter)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Are you sure you want to delete this form.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface deleteDialog, int id)
            {
                String fileName = model.getPDFFilePath();
                if (fileName != null)
                {
                    File pdfFile = new File(fileName);
                    if (pdfFile != null && pdfFile.exists())
                    {
                        pdfFile.delete();
                    }
                }

                mCompletedForms.remove(model);
                saveCompletedForms();
                deleteDialog.dismiss();
                if (adapter != null)
                {
                    adapter.notifyDataSetChanged();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface deleteDialog, int id)
            {
                // User cancelled the dialog
                deleteDialog.dismiss();
            }
        });
        AlertDialog deleteDialog = builder.create();
        deleteDialog.show();
    }

    public void moveFormBackToCurrent(final FormModel model, Activity activity)
    {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage("Editing this form will replace your current form and will require uploading again.\nDo you want to continue");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface editDialog, int id)
                {
                    mCompletedForms.remove(model);
                    mCurrentForm = model;
                    saveCompletedForms();
                    saveCurrentForm();
                    MainActivity.sharedInstance().showFragment(new FormDetailsFragment(), true);
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface editDialog, int id)
                {
                    // User cancelled the dialog
                    editDialog.dismiss();
                }
            });
            AlertDialog deleteDialog = builder.create();
            deleteDialog.show();

    }
}
