package uk.co.willmottdixon.gassafetyrecord.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import uk.co.willmottdixon.gassafetyrecord.R;
import uk.co.willmottdixon.gassafetyrecord.core.FormManager;
import uk.co.willmottdixon.gassafetyrecord.models.FormModel;

public class CompletedFormsListAdapter extends ArrayAdapter<FormModel> {
    private final Context mContext;
    private ArrayList<FormModel> mOptions;

    public CompletedFormsListAdapter(Context context, int resource, int textViewResourceId, ArrayList<FormModel> objects) {
        //noinspection unchecked
        super(context, resource, textViewResourceId, objects);
        mContext = context;
        mOptions = objects;
    }

    public void setOptions(ArrayList<FormModel> options) {
        mOptions = options;
    }

    @Override
     public View getView(int position, View convertView, ViewGroup parent) {

        CompleteFormListItemHolder viewHolder;

        if(convertView==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.completed_forms_list_item, parent, false);

            viewHolder = new CompleteFormListItemHolder();

            viewHolder.setName((TextView) convertView.findViewById(R.id.tenantNameLabel));
            viewHolder.setCertificateNumber((TextView) convertView.findViewById(R.id.certificateNumberLabel));
            viewHolder.setDate((TextView) convertView.findViewById(R.id.dateLabel));
            viewHolder.setUploadedLabel((TextView) convertView.findViewById(R.id.uploadedLabel));

            convertView.setTag(viewHolder);

        }else{
            viewHolder = (CompleteFormListItemHolder) convertView.getTag();
        }

        FormModel model = getItem(position);

        Date completedDate = model.getCompletedDate();
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.getDefault());
        String dateString = simpleDateFormat.format(completedDate);

        viewHolder.getName().setText(model.getTenantDetails().getTitle() + " " + model.getTenantDetails().getName());
        viewHolder.getCertificateNumber().setText(model.getCertificateSerialNumber());
        viewHolder.getDate().setText(dateString);

        if(model.getUploaded())
        {
            viewHolder.getUploadedLabel().setText("Uploaded");
            viewHolder.getUploadedLabel().setTextColor(mContext.getResources().getColor(R.color.green));
        }
        else
        {
            viewHolder.getUploadedLabel().setText("Not Uploaded");
            viewHolder.getUploadedLabel().setTextColor(mContext.getResources().getColor(R.color.red));
        }

        return convertView;

    }

    private class CompleteFormListItemHolder {
        private TextView mName;
        private TextView mCertificateNumber;
        private TextView mDate;
        private TextView mUploadedLabel;

        /**
         * Getter for property 'name'.
         *
         * @return Value for property 'name'.
         */
        public TextView getName()
        {
            return mName;
        }

        /**
         * Setter for property 'name'.
         *
         * @param name Value to set for property 'name'.
         */
        public void setName(TextView name)
        {
            mName = name;
        }

        /**
         * Getter for property 'certificateNumber'.
         *
         * @return Value for property 'certificateNumber'.
         */
        public TextView getCertificateNumber()
        {
            return mCertificateNumber;
        }

        /**
         * Setter for property 'certificateNumber'.
         *
         * @param certificateNumber Value to set for property 'certificateNumber'.
         */
        public void setCertificateNumber(TextView certificateNumber)
        {
            mCertificateNumber = certificateNumber;
        }

        /**
         * Getter for property 'date'.
         *
         * @return Value for property 'date'.
         */
        public TextView getDate()
        {
            return mDate;
        }

        /**
         * Setter for property 'date'.
         *
         * @param date Value to set for property 'date'.
         */
        public void setDate(TextView date)
        {
            mDate = date;
        }

        public TextView getUploadedLabel()
        {
            return mUploadedLabel;
        }

        public void setUploadedLabel(TextView uploadedLabel)
        {
            mUploadedLabel = uploadedLabel;
        }
    }
}
