package uk.co.willmottdixon.gassafetyrecord.core;

@SuppressWarnings("UnusedDeclaration")
public class Constants {

    public static final String FacebookAppID = "1466974266956359";
    public static final String FacebookAppSecret = "289a93ff3f2147eaf1d73b8cefcf49fa";


    public static final String PDF_KEY_APPLIANCES_TESTED = "appliances_tested";
    public static final String PDF_KEY_BEHALF_LANDLORD = "behalf_landlord";
    public static final String PDF_KEY_CERTIFICATE_SERIAL_NUMBER = "certificate_serial_number";
    public static final String PDF_KEY_CHK_EARTH = "chk_earth";
    public static final String PDF_KEY_CHK_EMER_CONT = "chk_emer_cont";
    public static final String PDF_KEY_CHK_EQUIP_BOND = "chk_equip_bond";
    public static final String PDF_KEY_CHK_GAS_INSTALL = "chk_gas_install";
    public static final String PDF_KEY_CHK_PIPEWORK = "chk_pipework";
    public static final String PDF_KEY_CHK_TIGHTNESS = "chk_tightness";
    public static final String PDF_KEY_CHK_WARNING_ISSUED = "chk_warning_issued";
    public static final String PDF_KEY_CHK_WARNING_NUMBER = "chk_warning_number";
    public static final String PDF_KEY_CHK_WORK_CO = "chk_work_co";
    public static final String PDF_KEY_CHK_WORK_SMOKE = "chk_work_smoke";
    public static final String PDF_KEY_CHK_WORK_PRESS = "chk_work_press";
    public static final String PDF_KEY_DATE = "date";
    public static final String PDF_KEY_ENGINEER_REGISTRATION_NUMBER = "engineer_registration_number";
    public static final String PDF_KEY_FRONT_ADDRESS = "front_address";
    public static final String PDF_KEY_FRONT_NAME = "front_name";
    public static final String PDF_KEY_INSTALLER_ADDRESS = "installer_address";
    public static final String PDF_KEY_INSTALLER_NAME = "installer_name";
    public static final String PDF_KEY_INSTALLER_PHONE = "installer_phone";
    public static final String PDF_KEY_INSTALLER_SIG = "installer_sig";
    public static final String PDF_KEY_INS_APP_INS = "ins_app_ins_";
    public static final String PDF_KEY_INS_APP_SAFE = "ins_app_safe_";
    public static final String PDF_KEY_INS_APP_SERV = "ins_app_serv_";
    public static final String PDF_KEY_INS_COMB = "ins_comb_";
    public static final String PDF_KEY_INS_FAULT = "ins_fault_";
    public static final String PDF_KEY_INS_FLUE_TYPE = "ins_flue_type_";
    public static final String PDF_KEY_INS_F_FLOW = "ins_f_flow_";
    public static final String PDF_KEY_INS_F_SPILLAGE = "ins_f_spillage_";
    public static final String PDF_KEY_INS_F_TERM = "ins_f_term_";
    public static final String PDF_KEY_INS_F_VISUAL = "ins_f_visual_";
    public static final String PDF_KEY_INS_LANDLORD = "ins_landlord_";
    public static final String PDF_KEY_INS_LOC = "ins_loc_";
    public static final String PDF_KEY_INS_MAKE = "ins_make_";
    public static final String PDF_KEY_INS_MODEL = "ins_model_";
    public static final String PDF_KEY_INS_OPP_PRES = "ins_opp_pres_";
    public static final String PDF_KEY_INS_PRES_GAS = "ins_pres_gas_";
    public static final String PDF_KEY_INS_SAFETY_DEVICE = "ins_safety_device_";
    public static final String PDF_KEY_INS_TYPE = "ins_type_";
    public static final String PDF_KEY_INS_VENT_ADEQ = "ins_vent_adeq_";
    public static final String PDF_KEY_LANDLORD_ADDRESS = "landlord_address";
    public static final String PDF_KEY_LANDLORD_NAME = "landlord_name";
    public static final String PDF_KEY_RECORDED_BY = "recorded_by";
    public static final String PDF_KEY_SERVICE_TYPE = "service_type";
    public static final String PDF_KEY_TENANT_ADDRESS = "tenant_address";
    public static final String PDF_KEY_TENANT_NAME = "tenant_name";
    public static final String PDF_KEY_TENANT_PHONE = "tenant_phone";
    public static final String PDF_KEY_TENANT_SIG = "tenant_sig";

    public static final String S3_KEY_ID = "AKIAJOYEFCI5JKEGX7IA";
    public static final String S3_SECRET = "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";

    public enum PageIndexes
    {
        PAGE_INDEX_HOME,
        PAGE_INDEX_JOURNEYS,
        PAGE_INDEX_SETTINGS,
        PAGE_INDEX_MY_INFORMATION,
        PAGE_INDEX_LOGOUT,
        PAGE_INDEX_COUNT, //anthing below here will be ignored
    }

    public enum FormElements
    {
        FORM_ELEMENT_OPTION,
        FORM_ELEMENT_TEXT_FIELD,
        FORM_ELEMENT_TOGGLE,
        FORM_ELEMENT_FORM_ENTRY,
        FORM_ELEMENT_COUNT, //anthing below here will be ignored
    }

    public enum FormValidationTypes{
        FORM_VALIDATION_TYPE_NOT_NULL,
        FORM_VALIDATION_TYPE_NOT_NULL_TEXT,
        FORM_VALIDATION_TYPE_EMAIL,
        FORM_VALIDATION_TYPE_PHONE,
        FORM_VALIDATION_TYPE_SELECTION,
    }
}