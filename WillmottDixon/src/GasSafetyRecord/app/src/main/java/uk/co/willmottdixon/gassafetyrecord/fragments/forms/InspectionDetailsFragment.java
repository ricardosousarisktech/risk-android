package uk.co.willmottdixon.gassafetyrecord.fragments.forms;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.core.FormManager;
import uk.co.willmottdixon.gassafetyrecord.models.FormModel;
import uk.co.willmottdixon.gassafetyrecord.models.InspectionModel;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.BaseFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.FormEntryFormItem;

/**
 * Created by Dev on 14/08/2015.
 */
public class InspectionDetailsFragment extends BaseFormFragment
{
    @Override
    public void configureElements()
    {
        FormModel form = FormManager.getStaticInstance().getCurrentForm();

        ArrayList<InspectionModel> inspectionsList = form.getInspectionsList();
        ArrayList<BaseFormItem> inspectionDetails = new ArrayList<>();

        int count = 1;

        for(InspectionModel model : inspectionsList)
        {
            FormEntryFormItem formItem = new FormEntryFormItem(getActivity(), "Appliance " + count, model, InspectionFragment.class);
            inspectionDetails.add(formItem);
            count++;
        }

        addFieldToSection("Inspection Details", inspectionDetails);
    }

    @Override
    protected void continueForm()
    {
        CheckListFragment checkListFragment = new CheckListFragment();
        checkListFragment.setContinueFlow(true);
        MainActivity.sharedInstance().showFragment(checkListFragment, true);
    }
}
