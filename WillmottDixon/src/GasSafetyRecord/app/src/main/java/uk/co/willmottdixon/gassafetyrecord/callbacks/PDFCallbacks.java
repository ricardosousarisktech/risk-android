package uk.co.willmottdixon.gassafetyrecord.callbacks;

import java.io.File;

import uk.co.willmottdixon.gassafetyrecord.models.FormModel;

/**
 * Created by Dev on 24/08/2015.
 */
public interface PDFCallbacks
{
    void pdfGenerated(File pdfFile);
    void pdfUploaded(FormModel formModel);
    void pdfUploadFailed(FormModel formModel);
}
