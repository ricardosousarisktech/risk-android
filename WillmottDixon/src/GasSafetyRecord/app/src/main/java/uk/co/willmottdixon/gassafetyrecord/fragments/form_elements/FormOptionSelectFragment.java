package uk.co.willmottdixon.gassafetyrecord.fragments.form_elements;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.R;
import uk.co.willmottdixon.gassafetyrecord.adapters.FormOptionsListAdapter;
import uk.co.willmottdixon.gassafetyrecord.callbacks.FragmentOptionSelectionCallback;
import uk.co.willmottdixon.gassafetyrecord.fragments.BaseFragment;

public class FormOptionSelectFragment extends BaseFragment {
    private ListView mFormOptionsListView;
    private ArrayList<?> mOptions;
    private FormOptionsListAdapter mFormOptionsListAdapter;
    private FragmentOptionSelectionCallback mFragmentOptionSelectionCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_option_select, container, false);

        mFormOptionsListView = (ListView) view.findViewById(R.id.formOptionsListView);
        mFormOptionsListAdapter = new FormOptionsListAdapter(getActivity(), R.layout.formitem_text_entry, R.id.menuItemLayout, mOptions);
        mFormOptionsListView.setAdapter(mFormOptionsListAdapter);
        mFormOptionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mFragmentOptionSelectionCallback.onSelection(mOptions.get(position));
            }
        });

        return view;
    }

    public void setOptions(ArrayList<?> options) {
        mOptions = options;
        if(mFormOptionsListAdapter != null) {
            mFormOptionsListAdapter.setOptions(mOptions);
            ((BaseAdapter) mFormOptionsListView.getAdapter()).notifyDataSetChanged();
        }
    }

    public FragmentOptionSelectionCallback getFragmentOptionSelectionCallback() {
        return mFragmentOptionSelectionCallback;
    }

    public void setFragmentOptionSelectionCallback(FragmentOptionSelectionCallback fragmentOptionSelectionCallback) {
        mFragmentOptionSelectionCallback = fragmentOptionSelectionCallback;
    }
}

