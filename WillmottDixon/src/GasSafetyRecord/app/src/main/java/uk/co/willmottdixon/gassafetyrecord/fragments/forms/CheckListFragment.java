package uk.co.willmottdixon.gassafetyrecord.fragments.forms;

import android.text.InputType;
import android.util.Log;
import android.view.View;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.core.DefaultConfigManager;
import uk.co.willmottdixon.gassafetyrecord.core.FormManager;
import uk.co.willmottdixon.gassafetyrecord.models.CheckListModel;
import uk.co.willmottdixon.gassafetyrecord.models.FormModel;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.BaseFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.OptionSelectFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.TextFormItem;

/**
 * Created by Dev on 14/08/2015.
 */
public class CheckListFragment extends BaseFormFragment
{
    private boolean mContinueFlow = false;

    @Override
    public void configureElements()
    {
        FormModel form = FormManager.getStaticInstance().getCurrentForm();

        CheckListModel checklist = form.getChecklist();

        //CheckList Details


        ArrayList<BaseFormItem> checklistDetails = new ArrayList<>();
        OptionSelectFormItem warningNotice = new OptionSelectFormItem(getActivity(), "Warning Issued",
                DefaultConfigManager.getStaticInstance().getYesNoList(), checklist, "warningIssued");
        TextFormItem warningNumber = new TextFormItem(getActivity(), "Warning Number", checklist, "warningNumber");
        warningNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        OptionSelectFormItem pipeworkSatisfactory = new OptionSelectFormItem(getActivity(), "Gas Installation Pipework\nSatisfactory",
                DefaultConfigManager.getStaticInstance().getYesNoList(), checklist, "gasInstallationPipeworkSatisfactory");
        OptionSelectFormItem emergencyControlAccessible = new OptionSelectFormItem(getActivity(), "Emergency Control Accessible",
                DefaultConfigManager.getStaticInstance().getYesNoList(), checklist, "emergencyControlAccessible");
        OptionSelectFormItem equipotentionBonding = new OptionSelectFormItem(getActivity(), "Equipotential Bonding\nSatisfactory",
                DefaultConfigManager.getStaticInstance().getYesNoList(), checklist, "equipotentialBondingSatisfactory");
        OptionSelectFormItem pipeworkSized = new OptionSelectFormItem(getActivity(), "Pipework Sized Correctly",
                DefaultConfigManager.getStaticInstance().getYesNoList(), checklist, "pipeworkSizedCorrectly");
        OptionSelectFormItem earthFitted = new OptionSelectFormItem(getActivity(), "Earth Fitted",
                DefaultConfigManager.getStaticInstance().getYesNoList(), checklist, "earthFitted");
        OptionSelectFormItem tightnessTest = new OptionSelectFormItem(getActivity(), "Tightness Test",
                DefaultConfigManager.getStaticInstance().getPassFailList(), checklist, "tightnessTest");
        TextFormItem workingPressure = new TextFormItem(getActivity(), "Working Pressure at Meter mBar", checklist, "workingPressureAtMeter");
        TextFormItem workingSmokeAlarms = new TextFormItem(getActivity(), "Working Smoke Alarm(s)", checklist, "workingSmokeAlarms");
        workingSmokeAlarms.setInputType(InputType.TYPE_CLASS_NUMBER);
        TextFormItem workingCOAlarms = new TextFormItem(getActivity(), "Working CO Alarm(s)", checklist, "workingCOAlarms");
        workingCOAlarms.setInputType(InputType.TYPE_CLASS_NUMBER);

        checklistDetails.add(warningNotice);
        checklistDetails.add(warningNumber);
        checklistDetails.add(pipeworkSatisfactory);
        checklistDetails.add(emergencyControlAccessible);
        checklistDetails.add(equipotentionBonding);
        checklistDetails.add(pipeworkSized);
        checklistDetails.add(earthFitted);
        checklistDetails.add(tightnessTest);
        checklistDetails.add(workingPressure);
        checklistDetails.add(workingSmokeAlarms);
        checklistDetails.add(workingCOAlarms);

        warningNumber.setLinkedVisibilityElement(warningNotice);
        warningNumber.setVisibleValue("Yes");

        addFieldToSection("Checklist", checklistDetails);

        PropertyChangeListener listener = new PropertyChangeListener()
        {
            @Override
            public void propertyChange(PropertyChangeEvent event)
            {
                Log.i("PROPERTY", event.getPropertyName() + " = " + event.getNewValue().toString());
            }
        };

    }

    @Override
    public int getRequiredLabelVisibility()
    {
        return View.GONE;
    }

    @Override
    public String getContinueButtonText()
    {

        if(isContinueFlow())
        {
            return "Continue";
        }
        else
        {
            return "Close Checklist";
        }
    }

    @Override
    protected void continueForm()
    {
        if(isContinueFlow())
        {
            MainActivity.sharedInstance().showFragment(new SignatureFragment(), true);
        }
        else
        {
            getFragmentManager().popBackStack();
        }
    }

    public void setContinueFlow(boolean continueFlow)
    {
        mContinueFlow = continueFlow;
    }

    public boolean isContinueFlow()
    {
        return mContinueFlow;
    }
}
