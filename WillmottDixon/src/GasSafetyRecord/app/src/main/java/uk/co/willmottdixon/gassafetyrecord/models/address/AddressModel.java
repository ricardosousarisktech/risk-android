package uk.co.willmottdixon.gassafetyrecord.models.address;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.models.BaseModel;

/**
 * Created by Dev on 07/08/2015.
 */
public class AddressModel extends BaseModel {

    public String mName;
    public String mHouseNumber;
    public String mAddress1;
    public String mAddress2;
    public String mAddress3;
    public String mAddress4;
    public String mCity;
    public String mCounty;
    public String mPostcode;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getHouseNumber() {
        return mHouseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        mHouseNumber = houseNumber;
    }


    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCounty() {
        return mCounty;
    }

    public void setCounty(String county) {
        mCounty = county;
    }

    public String getPostcode() {
        return mPostcode;
    }

    public void setPostcode(String postcode) {
        mPostcode = postcode;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String address1) {
        mAddress1 = address1;
    }

    public String getAddress2() {
        return mAddress2;
    }

    public void setAddress2(String address2) {
        mAddress2 = address2;
    }

    public String getAddress3() {
        return mAddress3;
    }

    public void setAddress3(String address3) {
        mAddress3 = address3;
    }

    public String getAddress4() {
        return mAddress4;
    }

    public void setAddress4(String address4) {
        mAddress4 = address4;
    }

    public String getSimpleAddress()
    {
        ArrayList<String>firstLineAddress = new ArrayList<>();
        ArrayList<String>secondLineAddress = new ArrayList<>();
        ArrayList<String>lastLineAddress = new ArrayList<>();

        ArrayList<String>components = new ArrayList<>();

        if(mAddress1 != null && mAddress1.length() > 0)
        {
            firstLineAddress.add(mAddress1);
        }

        if(mAddress2 != null && mAddress2.length() > 0)
        {
            firstLineAddress.add(mAddress2);
        }

        if(mAddress3 != null && mAddress3.length() > 0)
        {
            firstLineAddress.add(mAddress3);
        }

        if(mAddress4 != null && mAddress4.length() > 0)
        {
            secondLineAddress.add(mAddress4);
        }

        if(mCity != null && mCity.length() > 0)
        {
            secondLineAddress.add(mCity);
        }

        if(mCounty != null && mCounty.length() > 0)
        {
            lastLineAddress.add(mCounty);
        }

        if(mPostcode != null && mPostcode.length() > 0)
        {
            lastLineAddress.add(mPostcode);
        }

        if(firstLineAddress.size() > 0)
        {
            components.add(StringUtils.join(firstLineAddress, ", "));
        }
        if(secondLineAddress.size() > 0)
        {
            components.add(StringUtils.join(secondLineAddress, ", "));
        }
        if(lastLineAddress.size() > 0)
        {
            components.add(StringUtils.join(lastLineAddress, ", "));
        }

        return StringUtils.join(components, "\n");
    }


    public String getLongerAddress()
    {
        ArrayList<String>firstLineAddress = new ArrayList<>();
        ArrayList<String>lastLineAddress = new ArrayList<>();

        ArrayList<String>components = new ArrayList<>();

        if(mAddress1 != null && mAddress1.length() > 0)
        {
            firstLineAddress.add(mAddress1);
        }

        if(mAddress2 != null && mAddress2.length() > 0)
        {
            firstLineAddress.add(mAddress2);
        }

        if(mAddress3 != null && mAddress3.length() > 0)
        {
            firstLineAddress.add(mAddress3);
        }

        if(firstLineAddress.size() > 0)
        {
            components.add(StringUtils.join(firstLineAddress, ", "));
        }

        if(mAddress4 != null && mAddress4.length() > 0)
        {
            components.add(mAddress4);
        }

        if(mCity != null && mCity.length() > 0)
        {
            components.add(mCity);
        }

        if(mCounty != null && mCounty.length() > 0)
        {
            lastLineAddress.add(mCounty);
        }

        if(mPostcode != null && mPostcode.length() > 0)
        {
            lastLineAddress.add(mPostcode);
        }

        if(lastLineAddress.size() > 0)
        {
            components.add(StringUtils.join(lastLineAddress, ", "));
        }

        return StringUtils.join(components, "\n");
    }
}
