package uk.co.willmottdixon.gassafetyrecord.models;

/**
 * Created by Dev on 07/08/2015.
 */
public class SignatureModel extends BaseModel {

    String mSignatureFileName;
    String mName;

    public String getSignatureFileName() {
        return mSignatureFileName;
    }

    public void setSignatureFileName(String signatureFileName) {
        mSignatureFileName = signatureFileName;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
