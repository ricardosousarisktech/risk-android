package uk.co.willmottdixon.gassafetyrecord.ui_elements.forms;

import android.content.Context;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.core.Constants;
import uk.co.willmottdixon.gassafetyrecord.models.BaseModel;

/**
 * Created by Dev on 03/08/2015.
 */
public class OptionSelectFormItem extends BaseFormItem {

    private ArrayList<?> mOptions;

    public OptionSelectFormItem(Context context, String itemName, ArrayList<?>options, BaseModel baseModel, String propertyName) {
        setFormElementType(Constants.FormElements.FORM_ELEMENT_OPTION);
        setFormItemName(itemName);
        setOptions(options);
        setContext(context);
        setBaseModel(baseModel);
        setPropertyName(propertyName);
    }

    public ArrayList<?> getOptions() {
        return mOptions;
    }

    public void setOptions(ArrayList<?> options) {
        mOptions = options;
    }
}
