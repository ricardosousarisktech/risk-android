package uk.co.willmottdixon.gassafetyrecord.fragments.forms;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

import uk.co.willmottdixon.gassafetyrecord.R;
import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.adapters.FormListAdapter;
import uk.co.willmottdixon.gassafetyrecord.fragments.BaseFragment;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.BaseFormItem;
import uk.co.willmottdixon.gassafetyrecord.utilities.Helpers;

/**
 * Created by Dev on 14/08/2015.
 */
public abstract class BaseFormFragment extends BaseFragment
{
    private View mRootView;


    private LinearLayout mFormLinearLayout;
    private HashMap<String, ArrayList<BaseFormItem>> mFormElements;
    private ArrayList<String> mFormHeaders;
    private Button mContinueButton;
    private TextView mErrorText;
    private ArrayList<BaseFormItem> mFields;
    private FormListAdapter mFormListAdapter;
    private View mFooterView;
    private TextView mRequiredLabel;

    public abstract void configureElements();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(mRootView == null)
        {
            mRootView = inflater.inflate(R.layout.fragment_generic_form, container, false);

            setDummyFocusLayout(new LinearLayout(getActivity()));

            getDummyFocusLayout().setLayoutParams(new LinearLayout.LayoutParams(1, 1));
            getDummyFocusLayout().setFocusable(true);
            getDummyFocusLayout().setFocusableInTouchMode(true);
            getDummyFocusLayout().setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));

            ((RelativeLayout)mRootView).addView(getDummyFocusLayout());

            mFooterView = inflater.inflate(R.layout.fragment_generic_form_footer, container, false);
            mContinueButton = (Button) mFooterView.findViewById(R.id.continueButton);
            mErrorText = (TextView) mFooterView.findViewById(R.id.errorText);
            mFormLinearLayout = (LinearLayout) mRootView.findViewById(R.id.formLinearLayout);
            mRequiredLabel = (TextView) mFooterView.findViewById(R.id.requiredLabel);
            mFields = new ArrayList<>();

            mContinueButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Boolean valid = validateForm();
                    if (valid)
                    {
                        continueForm();
                    }
                }
            });

            //noinspection ResourceType
            mRequiredLabel.setVisibility(getRequiredLabelVisibility());

            mContinueButton.setText(getContinueButtonText());

            mFormLinearLayout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    hideKeyboard();
                }
            });

            mFormElements = new HashMap<>();

            mFormHeaders = new ArrayList<>(); //these are kept seperate so that order is maintained

            configureElements();

            renderList();
        }
        else
        {
            ViewGroup parent = (ViewGroup) mRootView.getParent();
            if(parent != null)
            {
                parent.removeView(mRootView);
            }
        }

        if (!(this instanceof CheckListFragment))
        {
            setHasOptionsMenu(true);
        }

        if(getDummyFocusLayout() != null)
        {
            getDummyFocusLayout().requestFocus();
        }

        ViewTreeObserver observer = mRootView.getViewTreeObserver();
        observer.addOnGlobalFocusChangeListener(new ViewTreeObserver.OnGlobalFocusChangeListener() {
            @Override
            public void onGlobalFocusChanged(View oldFocus, View newFocus) {
                if(oldFocus == null)
                {
                    getDummyFocusLayout().requestFocus();
                }
            }
        });



        return mRootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(mFormListAdapter != null)
        {
            mFormListAdapter.setDummyFocusLayout(null);
            mFormListAdapter.setRootFragment(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        if(getDummyFocusLayout() != null)
        {
            getDummyFocusLayout().requestFocus();
            hideKeyboard();
        }

        super.onPause();
    }

    public int getRequiredLabelVisibility()
    {
        return TextView.VISIBLE;
    }

    public String getContinueButtonText()
    {
        return "Continue";
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_activity_actions, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        MainActivity.sharedInstance().showFragment(new CheckListFragment(), true);
        return true;
    }

    protected abstract void continueForm();

    private void renderList()
    {
        if(mFormLinearLayout == null)
        {
            return;
        }
        mFormLinearLayout.removeAllViews();

        mFormListAdapter = new FormListAdapter(getActivity(), mFormElements, mFormHeaders);
        mFormListAdapter.setDummyFocusLayout(getDummyFocusLayout());
        mFormListAdapter.setRootFragment(this);
        for(int i = 0; i < mFormListAdapter.getGroupCount(); i++)
        {
            mFormLinearLayout.addView(mFormListAdapter.getGroupView(i, true, null, mFormLinearLayout));
            for(int j = 0; j < mFormListAdapter.getChildrenCount(i); j++)
            {
                mFormLinearLayout.addView(mFormListAdapter.getChildView(i, j, (j == mFormListAdapter.getChildrenCount(i) - 1), null, mFormLinearLayout));
            }
        }

        mFormLinearLayout.addView(mFooterView);

        mFormListAdapter.processLinkedElements();

    }

    public void addFieldToSection(String fieldName, ArrayList<BaseFormItem> fields)
    {
        mFormHeaders.add(fieldName);
        mFormElements.put(fieldName, fields);
        for(BaseFormItem field : fields)
        {
            mFields.add(field);
        }
    }

    public Boolean validateForm()
    {
        ArrayList<String> validationErrors = new ArrayList<>();

        if(mFields == null)
        {
            return true;
        }


        for(BaseFormItem formItem : mFields)
        {
            if(formItem.isFieldRequired())
            {
                Object returnedValue = Helpers.invokeGetterMethod(formItem.getPropertyGetter(), formItem.getBaseModel());
                switch (formItem.getValidationType())
                {
                    case FORM_VALIDATION_TYPE_NOT_NULL:
                        if(returnedValue == null)
                        {
                            validationErrors.add(formItem.getValidationMessage());
                        }
                        break;
                    case FORM_VALIDATION_TYPE_NOT_NULL_TEXT:
                        if(returnedValue == null || ((String)returnedValue).length() == 0)
                        {
                            validationErrors.add(formItem.getValidationMessage());
                        }
                        break;
                    case FORM_VALIDATION_TYPE_EMAIL:
                        if(returnedValue == null)
                        {
                            validationErrors.add(formItem.getValidationMessage());
                        }
                        break;
                    case FORM_VALIDATION_TYPE_PHONE:
                        if(returnedValue == null)
                        {
                            validationErrors.add(formItem.getValidationMessage());
                        }
                        break;
                    case FORM_VALIDATION_TYPE_SELECTION:
                        if(returnedValue == null)
                        {
                            validationErrors.add(formItem.getValidationMessage());
                        }
                        break;
                }
            }
        }

        if(validationErrors.size() > 0)
        {
            String errorString = StringUtils.join(validationErrors, "\n");
            mErrorText.setText(errorString);
            mErrorText.setVisibility(View.VISIBLE);
            return false;
        }
        else
        {
            mErrorText.setVisibility(View.GONE);
            return true;
        }
    }

    public ArrayList<BaseFormItem> getFields()
    {
        return mFields;
    }

    public void setFields(ArrayList<BaseFormItem> fields)
    {
        mFields = fields;
    }


}

