package uk.co.willmottdixon.gassafetyrecord.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import junit.framework.Assert;

import java.util.Calendar;

import uk.co.willmottdixon.gassafetyrecord.R;
import uk.co.willmottdixon.gassafetyrecord.listeners.OnFragmentInteractionListener;


public class MainActivity extends BaseActivity
        implements OnFragmentInteractionListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private static MainActivity sMainActivity;
    private int mLastSeconds;
    private final int kExitTimeDelay = 2;


    public static MainActivity sharedInstance() {
        if(sMainActivity == null)
        {
            try {
                throw new Exception("MainActivity does not yet exist. Check that you are not trying to use this out of the main app flow");
            } catch (Exception e) {
                e.printStackTrace();
                Assert.assertNotNull(sMainActivity);
            }
        }
        return sMainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sMainActivity = this;

        setContentView(R.layout.activity_main);

    }

    public void showFragment(android.app.Fragment fragment, boolean addToBackStack) {
        showFragment(fragment, addToBackStack, false);
    }

    public void showFragment(android.app.Fragment fragment, boolean addToBackStack, @SuppressWarnings("UnusedParameters") boolean hideActionBar) {
        hideKeyboard();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        android.app.Fragment existingFragment = fragmentManager.findFragmentByTag(fragment.getClass().getName());

        if(!addToBackStack)
        {
            fragmentManager.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentTransaction = fragmentTransaction.replace(R.id.container, fragment, fragment.getClass().getName());
        }
        else
        {

            fragmentTransaction = fragmentTransaction.replace(R.id.container, fragment, fragment.getClass().getName());
        }
        if (addToBackStack) {
            fragmentTransaction = fragmentTransaction.addToBackStack("backstack");
        }
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    public void hideKeyboard()
    {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            view.clearFocus();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {

        hideKeyboard();

        FragmentManager fragmentManager = getFragmentManager();

        if(fragmentManager.getBackStackEntryCount() > 0)
        {
            fragmentManager.popBackStack();
        }
        else
        {
            Calendar c = Calendar.getInstance();
            int seconds = c.get(Calendar.SECOND);

            if(seconds - mLastSeconds <= kExitTimeDelay)
            {
                super.onBackPressed();
                mLastSeconds = 0;
            }
            else
            {
                Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
                mLastSeconds = seconds;
            }
        }
    }
}
