package uk.co.willmottdixon.gassafetyrecord.ui_elements.forms;

import android.app.Activity;
import android.content.Context;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.core.Constants;
import uk.co.willmottdixon.gassafetyrecord.models.BaseModel;
import uk.co.willmottdixon.gassafetyrecord.models.FormModel;

/**
 * Created by Dev on 03/08/2015.
 */
public class ToggleFormItem extends BaseFormItem {

    public ToggleFormItem(Context context, String itemName, BaseModel baseModel, String propertyName) {
        setFormElementType(Constants.FormElements.FORM_ELEMENT_TOGGLE);
        setFormItemName(itemName);
        setContext(context);
        setBaseModel(baseModel);
        setPropertyName(propertyName);
    }
}
