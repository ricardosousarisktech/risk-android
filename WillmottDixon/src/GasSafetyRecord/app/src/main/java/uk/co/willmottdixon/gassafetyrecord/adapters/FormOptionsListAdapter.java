package uk.co.willmottdixon.gassafetyrecord.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uk.co.willmottdixon.gassafetyrecord.R;
import uk.co.willmottdixon.gassafetyrecord.models.BaseModel;

public class FormOptionsListAdapter extends ArrayAdapter<Object> {
    private final Context mContext;
    private ArrayList<?> mOptions;

    public FormOptionsListAdapter(Context context, int resource, int textViewResourceId, List<?> objects) {
        //noinspection unchecked
        super(context, resource, textViewResourceId, (List<Object>) objects);
        mContext = context;
        mOptions = new ArrayList<>(objects);
    }

    public void setOptions(ArrayList<?> options) {
        mOptions = options;
    }

    @Override
     public View getView(int position, View convertView, ViewGroup parent) {

        OptionItemHolder viewHolder;

        if(convertView==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitem_option_select, parent, false);

            viewHolder = new OptionItemHolder();

            viewHolder.setOptionTitle((TextView) convertView.findViewById(R.id.optionLabel));

            convertView.setTag(viewHolder);

        }else{
            viewHolder = (OptionItemHolder) convertView.getTag();
        }

        Object optionObject = mOptions.get(position);

        viewHolder.getOptionTitle().setText(optionObject.toString());

        return convertView;

    }

    private class OptionItemHolder {
        private CheckBox mOptionSelected;
        private TextView mOptionTitle;

        public CheckBox getOptionSelected() {
            return mOptionSelected;
        }

        public void setOptionSelected(CheckBox optionSelected) {
            mOptionSelected = optionSelected;
        }

        public TextView getOptionTitle() {
            return mOptionTitle;
        }

        public void setOptionTitle(TextView optionTitle) {
            mOptionTitle = optionTitle;
        }
    }
}
