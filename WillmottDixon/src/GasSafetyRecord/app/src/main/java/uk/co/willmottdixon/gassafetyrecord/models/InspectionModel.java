package uk.co.willmottdixon.gassafetyrecord.models;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

/**
 * Created by Dev on 07/08/2015.
 */
@SuppressWarnings("unused")
public class InspectionModel extends BaseModel {

    private String mLocation;
    private String mType;
    private String mMake;
    private String mModel;
    private String mFlueType;
    private String mOperatingPressure;
    private String mApplianceInspected;
    private String mSafetyDeviceCorrectOperation;
    private String mVentilationAdequate;
    private String mLandlordAppliance;
    private String mVisualCondition;
    private String mFlueFlowTest;
    private String mSpillageTest;
    private String mTerminationSatisfactory;
    private String mApplianceSafeToUse;
    private String mCombustionAnalysisReading;
    private String mPressureAtGasInlet;
    private String mApplianceServiced;
    private String mFault;

    public InspectionModel() {
        mFault = "";
    }

    public String getLocation()
    {
        return mLocation;
    }

    public void setLocation(String location)
    {
        mLocation = location;
    }

    public String getType()
    {
        return mType;
    }

    public void setType(String type)
    {
        mType = type;
    }

    public String getMake()
    {
        return mMake;
    }

    public void setMake(String make)
    {
        mMake = make;
    }

    public String getModel()
    {
        return mModel;
    }

    public void setModel(String model)
    {
        mModel = model;
    }

    public String getFlueType()
    {
        return mFlueType;
    }

    public void setFlueType(String flueType)
    {
        mFlueType = flueType;
    }

    public String getOperatingPressure()
    {
        return mOperatingPressure;
    }

    public void setOperatingPressure(String operatingPressure)
    {
        mOperatingPressure = operatingPressure;
    }

    public String getApplianceInspected()
    {
        return mApplianceInspected;
    }

    public void setApplianceInspected(String applianceInspected)
    {
        mApplianceInspected = applianceInspected;
    }

    public String getSafetyDeviceCorrectOperation()
    {
        return mSafetyDeviceCorrectOperation;
    }

    public void setSafetyDeviceCorrectOperation(String safetyDeviceCorrectOperation)
    {
        mSafetyDeviceCorrectOperation = safetyDeviceCorrectOperation;
    }

    public String getVentilationAdequate()
    {
        return mVentilationAdequate;
    }

    public void setVentilationAdequate(String ventilationAdequate)
    {
        mVentilationAdequate = ventilationAdequate;
    }

    public String getLandlordAppliance()
    {
        return mLandlordAppliance;
    }

    public void setLandlordAppliance(String landlordAppliance)
    {
        mLandlordAppliance = landlordAppliance;
    }

    public String getVisualCondition()
    {
        return mVisualCondition;
    }

    public void setVisualCondition(String visualCondition)
    {
        mVisualCondition = visualCondition;
    }

    public String getFlueFlowTest()
    {
        return mFlueFlowTest;
    }

    public void setFlueFlowTest(String flueFlowTest)
    {
        mFlueFlowTest = flueFlowTest;
    }

    public String getSpillageTest()
    {
        return mSpillageTest;
    }

    public void setSpillageTest(String spillageTest)
    {
        mSpillageTest = spillageTest;
    }

    public String getTerminationSatisfactory()
    {
        return mTerminationSatisfactory;
    }

    public void setTerminationSatisfactory(String terminationSatisfactory)
    {
        mTerminationSatisfactory = terminationSatisfactory;
    }

    public String getApplianceSafeToUse()
    {
        return mApplianceSafeToUse;
    }

    public void setApplianceSafeToUse(String applianceSafeToUse)
    {
        mApplianceSafeToUse = applianceSafeToUse;
    }

    public String getCombustionAnalysisReading()
    {
        return mCombustionAnalysisReading;
    }

    public void setCombustionAnalysisReading(String combustionAnalysisReading)
    {
        mCombustionAnalysisReading = combustionAnalysisReading;
    }

    public String getPressureAtGasInlet()
    {
        return mPressureAtGasInlet;
    }

    public void setPressureAtGasInlet(String pressureAtGasInlet)
    {
        mPressureAtGasInlet = pressureAtGasInlet;
    }

    public String getApplianceServiced()
    {
        return mApplianceServiced;
    }

    public void setApplianceServiced(String applianceServiced)
    {
        mApplianceServiced = applianceServiced;
    }

    public String getFault()
    {
        return mFault;
    }

    public void setFault(String fault)
    {
        mFault = fault;
    }

    @Override
    public String toString()
    {
        ArrayList<String> components = new ArrayList<>();

        if(mLocation != null && mLocation.length() > 0)
        {
            components.add(mLocation);
        }

        if(mType != null && mType.length() > 0)
        {
            components.add(mType);
        }

        if(mMake != null && mMake.length() > 0)
        {
            components.add(mMake);
        }

        if(mModel != null && mModel.length() > 0)
        {
            components.add(mModel);
        }

        if(components.size() > 0)
        {
            return StringUtils.join(components, "\n");
        }
        else
        {
            return "None";
        }
    }
}
