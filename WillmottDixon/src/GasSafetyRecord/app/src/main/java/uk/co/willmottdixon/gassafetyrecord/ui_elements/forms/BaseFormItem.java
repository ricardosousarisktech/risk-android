package uk.co.willmottdixon.gassafetyrecord.ui_elements.forms;

import android.content.Context;
import android.view.View;

import java.io.Serializable;
import java.lang.reflect.Method;

import uk.co.willmottdixon.gassafetyrecord.callbacks.FormCallback;
import uk.co.willmottdixon.gassafetyrecord.core.Constants;
import uk.co.willmottdixon.gassafetyrecord.models.BaseModel;

/**
 * Created by Dev on 03/08/2015.
 */
@SuppressWarnings("unused")
public abstract class BaseFormItem  implements Serializable
{
    private FormCallback mFormCallback;
    private String mFormItemName;
    private Context mContext;
    private Constants.FormElements formElementType;

    private BaseModel mBaseModel;
    private Method mPropertySetter;
    private Method mPropertyGetter;
    private String mPropertyName;
    private boolean mFieldRequired = false;
    private String mValidationMessage;
    private Constants.FormValidationTypes mValidationType;

    private BaseFormItem mLinkedVisibilityElement;
    private Object mVisibleValue;
    private View mHolderView; //refrence to view displaying this item;

    /**
     * Getter for property 'holderView'.
     *
     * @return Value for property 'holderView'.
     */
    public View getHolderView()
    {
        return mHolderView;
    }

    /**
     * Setter for property 'holderView'.
     *
     * @param holderView Value to set for property 'holderView'.
     */
    public void setHolderView(View holderView)
    {
        mHolderView = holderView;
    }


    public void setBaseModel(BaseModel baseModel) {
        mBaseModel = baseModel;
    }

    public BaseModel getBaseModel() {
        return mBaseModel;
    }

    public void setPropertyName(String propertyName) {
        mPropertyName = propertyName;

        String key = propertyName.substring(0,1).toUpperCase() + propertyName.substring(1);
        String methodSetter = "set" + key; //capitalise string for method name
        String methodGetter = "get" + key; //capitalise string for method name

        //Note: This will only find the first available method with the selected name.

        Method[] methods = getBaseModel().getClass().getMethods();

        for(Method method : methods)
        {
            if(method.getName().equals(methodSetter))
            {
                setPropertySetter(method);
            }
            else if(method.getName().equals(methodGetter))
            {
                setPropertyGetter(method);
            }
        }
    }

    public Method getPropertySetter() {
        return mPropertySetter;
    }

    public void setPropertySetter(Method propertySetter) {
        mPropertySetter = propertySetter;
    }

    public Method getPropertyGetter() {
        return mPropertyGetter;
    }

    public void setPropertyGetter(Method propertyGetter) {
        mPropertyGetter = propertyGetter;
    }

    public String getPropertyName() {
        return mPropertyName;
    }

    public FormCallback getFormCallback() {
        return mFormCallback;
    }

    public void setFormCallback(FormCallback formCallback) {
        mFormCallback = formCallback;
    }

    public String getFormItemName() {
        return mFormItemName;
    }

    public void setFormItemName(String settingName) {
        mFormItemName = settingName;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public Constants.FormElements getFormElementType() {
        return formElementType;
    }

    public int getFormElementTypeAsInt()
    {
        return getFormElementType().ordinal();
    }

    public void setFormElementType(Constants.FormElements formElementType) {
        this.formElementType = formElementType;
    }

    public boolean isFieldRequired()
    {
        return mFieldRequired;
    }

    public void setFieldRequired(boolean fieldRequired)
    {
        mFieldRequired = fieldRequired;
    }

    public String getValidationMessage()
    {
        return mValidationMessage;
    }

    public void setValidationMessage(String validationMessage)
    {
        mValidationMessage = validationMessage;
    }

    public Constants.FormValidationTypes getValidationType()
    {
        return mValidationType;
    }

    public void setValidationType(Constants.FormValidationTypes validationType)
    {
        this.mValidationType = validationType;
    }

    public void setRequired(boolean required, String validationMessage, Constants.FormValidationTypes validationType)
    {
        mFieldRequired = required;
        mValidationMessage = validationMessage;
        mValidationType = validationType;
    }

    public BaseFormItem getLinkedVisibilityElement()
    {
        return mLinkedVisibilityElement;
    }

    public void setLinkedVisibilityElement(BaseFormItem linkedVisibilityElement)
    {
        mLinkedVisibilityElement = linkedVisibilityElement;
    }

    public Object getVisibleValue()
    {
        return mVisibleValue;
    }

    public void setVisibleValue(Object visibleValue)
    {
        mVisibleValue = visibleValue;
    }
}
