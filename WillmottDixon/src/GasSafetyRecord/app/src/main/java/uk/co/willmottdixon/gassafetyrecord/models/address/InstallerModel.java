package uk.co.willmottdixon.gassafetyrecord.models.address;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

/**
 * Created by Dev on 07/08/2015.
 */
public class InstallerModel extends AddressModel {

    private String mPhoneNumber;

    /**
     * Getter for property 'phoneNumber'.
     *
     * @return Value for property 'phoneNumber'.
     */
    public String getPhoneNumber()
    {
        return mPhoneNumber;
    }

    /**
     * Setter for property 'phoneNumber'.
     *
     * @param phoneNumber Value to set for property 'phoneNumber'.
     */
    public void setPhoneNumber(String phoneNumber)
    {
        mPhoneNumber = phoneNumber;
    }

    @Override
    public String toString()
    {

        ArrayList<String> components = new ArrayList<>();

        components.add(mName);

        if(mAddress1 != null && mAddress1.length() > 0)
        {
            components.add(mAddress1);
        }

        if(mAddress2 != null && mAddress2.length() > 0)
        {
            components.add(mAddress2);
        }

        if(mAddress3 != null && mAddress3.length() > 0)
        {
            components.add(mAddress3);
        }

        if(mAddress4 != null && mAddress4.length() > 0)
        {
            components.add(mAddress4);
        }

        if(mCity != null && mCity.length() > 0)
        {
            components.add(mCity);
        }
        if(mPostcode != null && mPostcode.length() > 0)
        {
            components.add(mPostcode);
        }

        components.add(mPhoneNumber);

        return StringUtils.join(components, "\n");
    }
}
