package uk.co.willmottdixon.gassafetyrecord.fragments.forms;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.core.DefaultConfigManager;
import uk.co.willmottdixon.gassafetyrecord.models.InspectionModel;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.BaseFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.OptionSelectFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.TextFormItem;

/**
 * Created by Dev on 17/08/2015.
 */
public class InspectionFragment extends SubFormFragment
{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void configureElements()
    {
        DefaultConfigManager configManager = DefaultConfigManager.getStaticInstance();
        InspectionModel inspectionModel = (InspectionModel) getFormItem().getBaseModel();

        ArrayList<BaseFormItem> applianceDetails = new ArrayList<>();

        OptionSelectFormItem locationOptionItem = new OptionSelectFormItem(getActivity(), "Location", configManager.getLocations(), inspectionModel, "location");
        OptionSelectFormItem typeOptionItem = new OptionSelectFormItem(getActivity(), "Type", configManager.getAppliances(), inspectionModel, "type");
        TextFormItem makeTextItem = new TextFormItem(getActivity(), "Make", inspectionModel, "make");
        makeTextItem.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem modelTextItem = new TextFormItem(getActivity(), "Model", inspectionModel, "model");
        modelTextItem.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        OptionSelectFormItem flueTypeOptionItem = new OptionSelectFormItem(getActivity(), "Flue Type", configManager.getFlueTypes(), inspectionModel, "flueType");

        applianceDetails.add(locationOptionItem);
        applianceDetails.add(typeOptionItem);
        applianceDetails.add(makeTextItem);
        applianceDetails.add(modelTextItem);
        applianceDetails.add(flueTypeOptionItem);

        ArrayList<BaseFormItem> inspectionDetails = new ArrayList<>();
        TextFormItem operationPressureTextItem = new TextFormItem(getActivity(), "Operating Pressure", inspectionModel, "operatingPressure");
        OptionSelectFormItem applianceInspectedItem = new OptionSelectFormItem(getActivity(), "Appliance Inspected", configManager.getYesNoList(), inspectionModel, "applianceInspected");
        OptionSelectFormItem safetyDeviceCorrectItem = new OptionSelectFormItem(getActivity(), "Safety Device Correct Operation", configManager.getYesNoNAList(), inspectionModel, "safetyDeviceCorrectOperation");
        OptionSelectFormItem ventilationAdequateItem = new OptionSelectFormItem(getActivity(), "Ventilation Adequate", configManager.getYesNoList(), inspectionModel, "ventilationAdequate");
        OptionSelectFormItem landlordApplianceItem = new OptionSelectFormItem(getActivity(), "Landlord Appliance", configManager.getYesNoList(), inspectionModel, "applianceInspected");
        inspectionDetails.add(operationPressureTextItem);
        inspectionDetails.add(applianceInspectedItem);
        inspectionDetails.add(safetyDeviceCorrectItem);
        inspectionDetails.add(ventilationAdequateItem);
        inspectionDetails.add(landlordApplianceItem);

        ArrayList<BaseFormItem> flueCheckDetails = new ArrayList<>();
        OptionSelectFormItem visualConditionOption = new OptionSelectFormItem(getActivity(), "Visual Condition of Flue", configManager.getPassFailNAList(), inspectionModel, "visualCondition");
        OptionSelectFormItem flueFlowTestOption = new OptionSelectFormItem(getActivity(), "Flue Flow Test", configManager.getPassFailNAList(), inspectionModel, "flueFlowTest");
        OptionSelectFormItem spillageTestOption = new OptionSelectFormItem(getActivity(), "Spillage Test", configManager.getPassFailNAList(), inspectionModel, "spillageTest");
        OptionSelectFormItem terminationSatisfactoryOption = new OptionSelectFormItem(getActivity(), "Termination Satisfactory", configManager.getPassFailNAList(), inspectionModel, "terminationSatisfactory");
        flueCheckDetails.add(visualConditionOption);
        flueCheckDetails.add(flueFlowTestOption);
        flueCheckDetails.add(spillageTestOption);
        flueCheckDetails.add(terminationSatisfactoryOption);

        ArrayList<BaseFormItem> moreInspectionDetails = new ArrayList<>();
        OptionSelectFormItem applianceSafeToUseOption = new OptionSelectFormItem(getActivity(), "Appliance Safe to use", configManager.getYesNoVIList(), inspectionModel, "applianceSafeToUse");
        TextFormItem combustionAnalysisReadingTextItem = new TextFormItem(getActivity(), "Combustion Analysis Reading", inspectionModel, "combustionAnalysisReading");
        combustionAnalysisReadingTextItem.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem pressureAtGasInletTextItem = new TextFormItem(getActivity(), "Pressure at Gas Inlet mBar", inspectionModel, "pressureAtGasInlet");
        pressureAtGasInletTextItem.setInputType(InputType.TYPE_CLASS_TEXT);
        OptionSelectFormItem applianceServicedOption = new OptionSelectFormItem(getActivity(), "Appliance Serviced", configManager.getYesNoList(), inspectionModel, "applianceServiced");
        moreInspectionDetails.add(applianceSafeToUseOption);
        moreInspectionDetails.add(combustionAnalysisReadingTextItem);
        moreInspectionDetails.add(pressureAtGasInletTextItem);
        moreInspectionDetails.add(applianceServicedOption);

        ArrayList<BaseFormItem> faultDetails = new ArrayList<>();
        TextFormItem fault = new TextFormItem(getActivity(), "Fault", inspectionModel, "fault");
        fault.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        faultDetails.add(fault);

        addFieldToSection("Appliance Details", applianceDetails);
        addFieldToSection("Inspection Details", inspectionDetails);
        addFieldToSection("Flue Performance Check", flueCheckDetails);
        addFieldToSection("More Inspection Details", moreInspectionDetails);
        addFieldToSection("Fault Details", faultDetails);
    }
}
