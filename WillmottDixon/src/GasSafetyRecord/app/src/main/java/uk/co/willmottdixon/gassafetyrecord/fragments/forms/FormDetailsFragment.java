package uk.co.willmottdixon.gassafetyrecord.fragments.forms;

import android.text.InputType;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.core.DefaultConfigManager;
import uk.co.willmottdixon.gassafetyrecord.core.FormManager;
import uk.co.willmottdixon.gassafetyrecord.models.FormModel;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.BaseFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.OptionSelectFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.TextFormItem;

import static uk.co.willmottdixon.gassafetyrecord.core.Constants.FormValidationTypes.FORM_VALIDATION_TYPE_NOT_NULL;
import static uk.co.willmottdixon.gassafetyrecord.core.Constants.FormValidationTypes.FORM_VALIDATION_TYPE_NOT_NULL_TEXT;
import static uk.co.willmottdixon.gassafetyrecord.core.Constants.FormValidationTypes.FORM_VALIDATION_TYPE_SELECTION;

/**
 * Created by Dev on 14/08/2015.
 */
public class FormDetailsFragment extends BaseFormFragment
{
    @Override
    public void configureElements()
    {
        FormModel form = FormManager.getStaticInstance().getCurrentForm();

        //Service Details
        ArrayList<BaseFormItem> serviceDetails = new ArrayList<>();
        OptionSelectFormItem serviceType = new OptionSelectFormItem(getActivity(), "Service Type", DefaultConfigManager.getStaticInstance().getServiceTypes(), form, "serviceType");
        TextFormItem certificateSerialNumber = new TextFormItem(getActivity(), "Certificate Number", form, "certificateSerialNumber");
        certificateSerialNumber.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        TextFormItem engineerRegistrationNumber = new TextFormItem(getActivity(), "Engineer Registration Number", form, "engineerRegistrationNumber");
        engineerRegistrationNumber.setInputType(InputType.TYPE_CLASS_NUMBER);

        serviceType.setRequired(true, "Please select a service type.", FORM_VALIDATION_TYPE_SELECTION);
        certificateSerialNumber.setRequired(true, "Please enter your certificate serial number.", FORM_VALIDATION_TYPE_NOT_NULL_TEXT);
        engineerRegistrationNumber.setRequired(true, "Please enter your engineer registration number", FORM_VALIDATION_TYPE_NOT_NULL_TEXT);

        serviceDetails.add(serviceType);
        serviceDetails.add(certificateSerialNumber);
        serviceDetails.add(engineerRegistrationNumber);

        //Installer Details
        ArrayList<BaseFormItem> installerDetails = new ArrayList<>();
        OptionSelectFormItem installer = new OptionSelectFormItem(getActivity(), "Installer", DefaultConfigManager.getStaticInstance().getInstallers(), form, "installerDetails");
        installer.setRequired(true, "Please select the installer.", FORM_VALIDATION_TYPE_NOT_NULL);
        installerDetails.add(installer);

        //Tenant Details
        ArrayList<BaseFormItem> tenantDetails = new ArrayList<>();
        OptionSelectFormItem titleItem  = new OptionSelectFormItem(getActivity(), "Title", DefaultConfigManager.getStaticInstance().getTitles(), form.getTenantDetails(), "title");
        TextFormItem name               = new TextFormItem(getActivity(), "Name",       form.getTenantDetails(), "name");
        name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem address1           = new TextFormItem(getActivity(), "Address 1",  form.getTenantDetails(), "address1");
        address1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem address2           = new TextFormItem(getActivity(), "Address 2",  form.getTenantDetails(), "address2");
        address2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem address3           = new TextFormItem(getActivity(), "Address 3",  form.getTenantDetails(), "address3");
        address3.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem address4           = new TextFormItem(getActivity(), "Address 4",  form.getTenantDetails(), "address4");
        address4.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem city               = new TextFormItem(getActivity(), "City",       form.getTenantDetails(), "city");
        city.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem county             = new TextFormItem(getActivity(), "County",     form.getTenantDetails(), "county");
        county.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        TextFormItem postcode           = new TextFormItem(getActivity(), "Postcode",   form.getTenantDetails(), "postcode");
        postcode.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS | InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS);
        TextFormItem telephone          = new TextFormItem(getActivity(), "Telephone",  form.getTenantDetails(), "phoneNumber");
        telephone.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS | InputType.TYPE_CLASS_PHONE);

        titleItem.setRequired(true, "Please select the tenant's title", FORM_VALIDATION_TYPE_NOT_NULL_TEXT);
        name.setRequired(true, "Please enter the tenant's name", FORM_VALIDATION_TYPE_NOT_NULL_TEXT);
        address1.setRequired(true, "Please enter the tenant's address", FORM_VALIDATION_TYPE_NOT_NULL_TEXT);
        city.setRequired(true, "Please enter the tenant's city", FORM_VALIDATION_TYPE_NOT_NULL_TEXT);
        county.setRequired(true, "Please enter the tenant's county", FORM_VALIDATION_TYPE_NOT_NULL_TEXT);
        postcode.setRequired(true, "Please enter the tenant's postcode", FORM_VALIDATION_TYPE_NOT_NULL_TEXT);

        tenantDetails.add(titleItem);
        tenantDetails.add(name);
        tenantDetails.add(address1);
        tenantDetails.add(address2);
        tenantDetails.add(address3);
        tenantDetails.add(address4);
        tenantDetails.add(city);
        tenantDetails.add(county);
        tenantDetails.add(postcode);
        tenantDetails.add(telephone);

        addFieldToSection("Service Details", serviceDetails);
        addFieldToSection("Installer Details", installerDetails);
        addFieldToSection("Tenant Details", tenantDetails);
    }

    @Override
    protected void continueForm()
    {
        MainActivity.sharedInstance().showFragment(new InspectionDetailsFragment(), true);
    }
}
