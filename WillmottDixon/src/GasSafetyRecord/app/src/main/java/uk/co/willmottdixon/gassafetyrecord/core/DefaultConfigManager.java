package uk.co.willmottdixon.gassafetyrecord.core;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.models.address.InstallerModel;
import uk.co.willmottdixon.gassafetyrecord.models.address.LandlordModel;

/**
 * Created by Dev on 13/08/2015.
 */
@SuppressWarnings("unused")
public class DefaultConfigManager {

    private static DefaultConfigManager sStaticInstance;
    private ArrayList<InstallerModel> mInstallers;
    private ArrayList<String> mServiceTypesArray;
    private ArrayList<String> mLocationsArray;
    private ArrayList<String> mAppliancesArray;
    private ArrayList<String> mFlueTypesArray;
    private ArrayList<String> mYesNoListArray;
    private ArrayList<String> mYesNoNAListArray;
    private ArrayList<String> mYesNoVIListArray;
    private ArrayList<String> mPassFailListArray;
    private ArrayList<String> mPassFailNAListArray;
    private ArrayList<String> mTitlesArray;

    public static DefaultConfigManager getStaticInstance() {
        if(sStaticInstance == null)
        {
            sStaticInstance = new DefaultConfigManager();
        }
        return sStaticInstance;
    }

    public ArrayList<String> getTitles()
    {
        if(mTitlesArray == null)
        {
            mTitlesArray = new ArrayList<>();
            mTitlesArray.add("Miss");
            mTitlesArray.add("Mrs");
            mTitlesArray.add("Ms");
            mTitlesArray.add("Mr");
            mTitlesArray.add("Other");
        }
        return mTitlesArray;
    }

    public LandlordModel getLandlord()
    {
        LandlordModel landlordModel = new LandlordModel();
        landlordModel.setName("Home Group");
        landlordModel.setAddress1("2 Gosforth Park Way");
        landlordModel.setAddress4("Gosforth Business Park");
        landlordModel.setCity("Newcastle upon Tyne");
        landlordModel.setPostcode("NE12 8ET");
        return landlordModel;
    }

    public ArrayList<InstallerModel> getInstallers()
    {
        if(mInstallers == null)
        {
            mInstallers = new ArrayList<>();

            /*
            Installer Details
            Name:       Willmott Dixon Partnerships Ltd
            Address:    First Floor, Swanage Court, Dodds Close
                        Bradmarsh Business Park
                        Rotherham
                        S60 1BX
            Tel:        08456 076 269
            */

            InstallerModel firstInstaller = new InstallerModel();
            firstInstaller.setName("Willmott Dixon Partnerships Ltd");
            firstInstaller.setAddress1("First Floor");
            firstInstaller.setAddress2("Swanage Court");
            firstInstaller.setAddress3("Dodds Close");
            firstInstaller.setAddress4("Bradmarsh Business Park");
            firstInstaller.setCity("Rotherham");
            firstInstaller.setPostcode("S60 1BX");
            firstInstaller.setPhoneNumber("08456 076 269");

            /*
            Installer Details
            Name:       Willmott Dixon Partnerships Ltd
            Address:    Patterson House
                        Dovenby Hall Estate
                        Cockermouth
                        CA13 0PN
            Tel: 08456 057 078
            */

            InstallerModel secondInstaller = new InstallerModel();
            secondInstaller.setName("Willmott Dixon Partnerships Ltd");
            secondInstaller.setAddress1("Patterson House");
            secondInstaller.setAddress4("Dovenby Hall Estate");
            secondInstaller.setCity("Cockermouth");
            secondInstaller.setPostcode("CA13 0PN");
            secondInstaller.setPhoneNumber("08456 057 078");

            mInstallers.add(firstInstaller);
            mInstallers.add(secondInstaller);
        }

        return mInstallers;
    }

    public ArrayList<String> getServiceTypes()
    {
        if(mServiceTypesArray == null)
        {
            mServiceTypesArray = new ArrayList<>();
            mServiceTypesArray.add("Gas Service");
            mServiceTypesArray.add("Gas Repair");
            mServiceTypesArray.add("Gas Cap Off");
            mServiceTypesArray.add("Gas Uncap and Test");
            mServiceTypesArray.add("Gas Pipleline Inspection");
        }
        return mServiceTypesArray;
    }

    public ArrayList<String> getLocations()
    {
        if(mLocationsArray == null)
        {
            mLocationsArray = new ArrayList<>();
            mLocationsArray.add("Kitchen");
            mLocationsArray.add("Hall");
            mLocationsArray.add("Bedroom");
            mLocationsArray.add("Loft");
            mLocationsArray.add("Utility");
            mLocationsArray.add("Living Room");
            mLocationsArray.add("Cellar");
            mLocationsArray.add("Bathroom");
            mLocationsArray.add("Toilet");
            mLocationsArray.add("Other");
        }
        return mLocationsArray;
    }

    public ArrayList<String> getAppliances()
    {
        if(mAppliancesArray == null)
        {
            mAppliancesArray = new ArrayList<>();
            mAppliancesArray.add("Boiler");
            mAppliancesArray.add("Back Boiler");
            mAppliancesArray.add("Cooker");
            mAppliancesArray.add("Fire");
            mAppliancesArray.add("Hob");
            mAppliancesArray.add("Oven");
            mAppliancesArray.add("WAU");
            mAppliancesArray.add("Water Heater");
            mAppliancesArray.add("Grill");
            mAppliancesArray.add("Other");
        }
        return mAppliancesArray;
    }

    public ArrayList<String> getFlueTypes()
    {
        if(mFlueTypesArray == null)
        {
            mFlueTypesArray = new ArrayList<>();
            mFlueTypesArray.add("OF");
            mFlueTypesArray.add("RS");
            mFlueTypesArray.add("FL");
        }

        return mFlueTypesArray;
    }

    public ArrayList<String> getYesNoList()
    {
        if(mYesNoListArray == null)
        {
            mYesNoListArray = new ArrayList<>();
            mYesNoListArray.add("Yes");
            mYesNoListArray.add("No");
        }
        return mYesNoListArray;
    }

    public ArrayList<String> getYesNoNAList()
    {
        if(mYesNoNAListArray == null)
        {
            mYesNoNAListArray = new ArrayList<>();
            mYesNoNAListArray.add("Yes");
            mYesNoNAListArray.add("No");
            mYesNoNAListArray.add("N/A");
        }
        return mYesNoNAListArray;
    }

    public ArrayList<String> getYesNoVIList()
    {
        if(mYesNoVIListArray == null)
        {
            mYesNoVIListArray = new ArrayList<>();
            mYesNoVIListArray.add("Yes");
            mYesNoVIListArray.add("No");
            mYesNoVIListArray.add("VI");
        }
        return mYesNoVIListArray;
    }

    public ArrayList<String> getPassFailList()
    {
        if(mPassFailListArray == null)
        {
            mPassFailListArray = new ArrayList<>();
            mPassFailListArray.add("Pass");
            mPassFailListArray.add("Fail");
        }
        return mPassFailListArray;
    }

    public ArrayList<String> getPassFailNAList()
    {
        if(mPassFailNAListArray == null)
        {
            mPassFailNAListArray = new ArrayList<>();
            mPassFailNAListArray.add("Pass");
            mPassFailNAListArray.add("Fail");
            mPassFailNAListArray.add("N/A");
        }
        return mPassFailNAListArray;
    }

    public ArrayList<String> getCountArray(int number)
    {
        ArrayList<String> countArray = new ArrayList<>();

        countArray.add("None");

        for(Integer i = 1; i <= number; i++)
        {
            countArray.add(i.toString());
        }

        return countArray;
    }
}
