package uk.co.willmottdixon.gassafetyrecord.models;

/**
 * Created by Dev on 07/08/2015.
 */
public class CheckListModel extends BaseModel {

    private String mWarningIssued;
    private String mWarningNumber;
    private String mGasInstallationPipeworkSatisfactory;
    private String mEmergencyControlAccessible;
    private String mEquipotentialBondingSatisfactory;
    private String mPipeworkSizedCorrectly;
    private String mEarthFitted;
    private String mTightnessTest;
    private String mWorkingPressureAtMeter;
    private String mWorkingSmokeAlarms;
    private String mWorkingCOAlarms;



    public boolean isCheckListComplete()
    {
        if(mWarningIssued == null || mWarningIssued.length() == 0 ||
                mGasInstallationPipeworkSatisfactory == null || mGasInstallationPipeworkSatisfactory.length() == 0 ||
                mEmergencyControlAccessible == null || mEmergencyControlAccessible.length() == 0 ||
                mEquipotentialBondingSatisfactory == null || mEquipotentialBondingSatisfactory.length() == 0 ||
                mPipeworkSizedCorrectly == null || mPipeworkSizedCorrectly.length() == 0 ||
                mEarthFitted == null || mEarthFitted.length() == 0 ||
                mTightnessTest == null || mTightnessTest.length() == 0 ||
                mWorkingPressureAtMeter == null || mWorkingPressureAtMeter.length() == 0 ||
                mWorkingSmokeAlarms == null || mWorkingSmokeAlarms.length() == 0 ||
                mWorkingCOAlarms == null || mWorkingCOAlarms.length() == 0)

        {
            return false;
        }

        if(mWarningIssued.equals("Yes") && (mWarningNumber == null || mWarningNumber.length() == 0))
        {
            return false;
        }

        return true;
    }

    /**
     * Getter for property 'warningIssued'.
     *
     * @return Value for property 'warningIssued'.
     */
    public String getWarningIssued()
    {
        return mWarningIssued;
    }

    /**
     * Setter for property 'warningIssued'.
     *
     * @param warningIssued Value to set for property 'warningIssued'.
     */
    public void setWarningIssued(String warningIssued)
    {
        mWarningIssued = warningIssued;
    }

    /**
     * Getter for property 'warningNumber'.
     *
     * @return Value for property 'warningNumber'.
     */
    public String getWarningNumber()
    {
        return mWarningNumber;
    }

    /**
     * Setter for property 'warningNumber'.
     *
     * @param warningNumber Value to set for property 'warningNumber'.
     */
    public void setWarningNumber(String warningNumber)
    {
        mWarningNumber = warningNumber;
    }

    /**
     * Getter for property 'gasInstallationPipeworkSatisfactory'.
     *
     * @return Value for property 'gasInstallationPipeworkSatisfactory'.
     */
    public String getGasInstallationPipeworkSatisfactory()
    {
        return mGasInstallationPipeworkSatisfactory;
    }

    /**
     * Setter for property 'gasInstallationPipeworkSatisfactory'.
     *
     * @param gasInstallationPipeworkSatisfactory Value to set for property 'gasInstallationPipeworkSatisfactory'.
     */
    public void setGasInstallationPipeworkSatisfactory(String gasInstallationPipeworkSatisfactory)
    {
        mGasInstallationPipeworkSatisfactory = gasInstallationPipeworkSatisfactory;
    }

    /**
     * Getter for property 'emergencyControlAccessible'.
     *
     * @return Value for property 'emergencyControlAccessible'.
     */
    public String getEmergencyControlAccessible()
    {
        return mEmergencyControlAccessible;
    }

    /**
     * Setter for property 'emergencyControlAccessible'.
     *
     * @param emergencyControlAccessible Value to set for property 'emergencyControlAccessible'.
     */
    public void setEmergencyControlAccessible(String emergencyControlAccessible)
    {
        mEmergencyControlAccessible = emergencyControlAccessible;
    }

    public String getEquipotentialBondingSatisfactory() {
        return mEquipotentialBondingSatisfactory;
    }

    public void setEquipotentialBondingSatisfactory(String equipotentialBondingSatisfactory) {
        mEquipotentialBondingSatisfactory = equipotentialBondingSatisfactory;
    }

    /**
     * Getter for property 'pipeworkSizedCorrectly'.
     *
     * @return Value for property 'pipeworkSizedCorrectly'.
     */
    public String getPipeworkSizedCorrectly()
    {
        return mPipeworkSizedCorrectly;
    }

    /**
     * Setter for property 'pipeworkSizedCorrectly'.
     *
     * @param pipeworkSizedCorrectly Value to set for property 'pipeworkSizedCorrectly'.
     */
    public void setPipeworkSizedCorrectly(String pipeworkSizedCorrectly)
    {
        mPipeworkSizedCorrectly = pipeworkSizedCorrectly;
    }

    /**
     * Getter for property 'earthFitted'.
     *
     * @return Value for property 'earthFitted'.
     */
    public String getEarthFitted()
    {
        return mEarthFitted;
    }

    /**
     * Setter for property 'earthFitted'.
     *
     * @param earthFitted Value to set for property 'earthFitted'.
     */
    public void setEarthFitted(String earthFitted)
    {
        mEarthFitted = earthFitted;
    }

    /**
     * Getter for property 'tightnessTest'.
     *
     * @return Value for property 'tightnessTest'.
     */
    public String getTightnessTest()
    {
        return mTightnessTest;
    }

    /**
     * Setter for property 'tightnessTest'.
     *
     * @param tightnessTest Value to set for property 'tightnessTest'.
     */
    public void setTightnessTest(String tightnessTest)
    {
        mTightnessTest = tightnessTest;
    }

    /**
     * Getter for property 'workingPressureAtMeter'.
     *
     * @return Value for property 'workingPressureAtMeter'.
     */
    public String getWorkingPressureAtMeter()
    {
        return mWorkingPressureAtMeter;
    }

    /**
     * Setter for property 'workingPressureAtMeter'.
     *
     * @param workingPressureAtMeter Value to set for property 'workingPressureAtMeter'.
     */
    public void setWorkingPressureAtMeter(String workingPressureAtMeter)
    {
        mWorkingPressureAtMeter = workingPressureAtMeter;
    }

    /**
     * Getter for property 'workingSmokeAlarms'.
     *
     * @return Value for property 'workingSmokeAlarms'.
     */
    public String getWorkingSmokeAlarms()
    {
        return mWorkingSmokeAlarms;
    }

    /**
     * Setter for property 'workingSmokeAlarms'.
     *
     * @param workingSmokeAlarms Value to set for property 'workingSmokeAlarms'.
     */
    public void setWorkingSmokeAlarms(String workingSmokeAlarms)
    {
        mWorkingSmokeAlarms = workingSmokeAlarms;
    }

    /**
     * Getter for property 'workingCOAlarms'.
     *
     * @return Value for property 'workingCOAlarms'.
     */
    public String getWorkingCOAlarms()
    {
        return mWorkingCOAlarms;
    }

    /**
     * Setter for property 'workingCOAlarms'.
     *
     * @param workingCOAlarms Value to set for property 'workingCOAlarms'.
     */
    public void setWorkingCOAlarms(String workingCOAlarms)
    {
        mWorkingCOAlarms = workingCOAlarms;
    }
}
