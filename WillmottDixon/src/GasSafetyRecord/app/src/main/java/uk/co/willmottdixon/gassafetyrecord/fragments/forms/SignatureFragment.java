package uk.co.willmottdixon.gassafetyrecord.fragments.forms;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import uk.co.willmottdixon.gassafetyrecord.R;
import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.callbacks.PDFCallbacks;
import uk.co.willmottdixon.gassafetyrecord.core.FormManager;
import uk.co.willmottdixon.gassafetyrecord.fragments.BaseFragment;
import uk.co.willmottdixon.gassafetyrecord.fragments.MainScreenFragment;
import uk.co.willmottdixon.gassafetyrecord.models.FormModel;
import uk.co.willmottdixon.gassafetyrecord.models.InspectionModel;
import uk.co.willmottdixon.gassafetyrecord.models.SignatureModel;
import uk.co.willmottdixon.gassafetyrecord.models.address.TenantModel;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.DimmableButton;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.SignatureView;

/**
 * Created by Dev on 18/08/2015.
 */
public class SignatureFragment extends BaseFragment implements View.OnClickListener, SignatureView.OnSignedListener, PDFCallbacks
{
    private View mRootView;
    private DimmableButton mFinishFormButton;
    private SignatureView mInstallerSignatureView;
    private SignatureView mTenantSignatureView;
    private DimmableButton mInstallerClearButton;
    private DimmableButton mTenantClearButton;
    private File mTenantSignatureFile;
    private File mInstallerSignatureFile;
    private SignatureModel mInstallerSignatureModel;
    private SignatureModel mTenantSignatureModel;
    private EditText mInstalledNameEditText;
    private TextView mAppliancesTestedLabel;
    private TextView mDateLabel;
    private TextView mTenantTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (mRootView == null)
        {
            File directory = new File(Environment.getExternalStorageDirectory() + "/signatures");
            //noinspection ResultOfMethodCallIgnored
            directory.mkdirs();

            mRootView = inflater.inflate(R.layout.fragment_signature_form, container, false);
            mFinishFormButton = (DimmableButton) mRootView.findViewById(R.id.finishForm);
            mInstallerClearButton = (DimmableButton) mRootView.findViewById(R.id.installerClearButton);
            mTenantClearButton = (DimmableButton) mRootView.findViewById(R.id.tenantClearButton);
            mInstallerSignatureView = (SignatureView) mRootView.findViewById(R.id.installerSignature);
            mTenantSignatureView = (SignatureView) mRootView.findViewById(R.id.tenantSignature);
            mInstalledNameEditText = (EditText) mRootView.findViewById(R.id.installerNameEditText);
            mAppliancesTestedLabel = (TextView) mRootView.findViewById(R.id.numberOfTestedAppliancesLabel);
            mTenantTextView = (TextView) mRootView.findViewById(R.id.tenantNameLabel);
            mDateLabel = (TextView) mRootView.findViewById(R.id.dateLabel);

            mInstalledNameEditText.setText(FormManager.currentForm().getEngineerName());
            mInstalledNameEditText.addTextChangedListener(new TextWatcher()
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after)
                {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {

                }

                @Override
                public void afterTextChanged(Editable s)
                {
                    FormManager.currentForm().setEngineerName(s.toString());
                    FormManager.getStaticInstance().saveCurrentForm();
                }
            });

            mInstallerSignatureModel = FormManager.getStaticInstance().getCurrentForm().getInstallerSignature();
            if(mInstallerSignatureModel == null)
            {
                mInstallerSignatureModel = new SignatureModel();
                FormManager.currentForm().setInstallerSignature(mInstallerSignatureModel);
            }

            TenantModel tenantModel = FormManager.currentForm().getTenantDetails();

            if(tenantModel.getTitle().equals("Other")) {
                mTenantTextView.setText(tenantModel.getName());
            }
            else
            {
                mTenantTextView.setText(tenantModel.getTitle() + " " + tenantModel.getName());
            }

            mTenantSignatureModel = FormManager.getStaticInstance().getCurrentForm().getTenantSignature();
            if(mTenantSignatureModel == null)
            {
                mTenantSignatureModel = new SignatureModel();
                FormManager.currentForm().setTenantSignature(mTenantSignatureModel);
            }

            Integer appliancesTested = 0;

            for(int i = 0; i < FormManager.currentForm().getInspectionsList().size(); i++)
            {
                InspectionModel inspectionModel = FormManager.currentForm().getInspectionsList().get(i);

                if (inspectionModel.getLocation() != null && inspectionModel.getLocation().length() > 0)
                {
                    appliancesTested++;
                }
            }

            mAppliancesTestedLabel.setText(appliancesTested.toString());

            Date date = FormManager.currentForm().getCompletedDate();

            if(date == null)

            {
                date = new Date();
                FormManager.currentForm().setCompletedDate(date);
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            String formattedDate = simpleDateFormat.format(date);

            mDateLabel.setText(formattedDate);

            mInstallerSignatureView.setOnSignedListener(this);
            mTenantSignatureView.setOnSignedListener(this);

            mInstallerClearButton.setOnClickListener(this);
            mTenantClearButton.setOnClickListener(this);
            mFinishFormButton.setOnClickListener(this);
        }
        else
        {
            ViewGroup parent = (ViewGroup) mRootView.getParent();
            if (parent != null)
            {
                parent.removeView(mRootView);
            }
        }

        setHasOptionsMenu(true);

        return mRootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        final ViewTreeObserver observer = view.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            public void onGlobalLayout()
            {
                observer.removeOnGlobalLayoutListener(this);

                File directory = new File(Environment.getExternalStorageDirectory() + "/signatures");
                //noinspection ResultOfMethodCallIgnored
                directory.mkdirs();

                if(mInstallerSignatureModel.getSignatureFileName() != null)
                {
                    mInstallerSignatureView.invalidate();
                    mInstallerSignatureFile = new File(mInstallerSignatureModel.getSignatureFileName());
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(mInstallerSignatureFile.getAbsolutePath(), options);
                    if(bitmap != null)
                    {
                        mInstallerSignatureView.setSignatureBitmap(bitmap);
                    }
                    else
                    {
                        mInstallerSignatureModel.setSignatureFileName(null);
                    }
                }
                else
                {
                    mInstallerSignatureFile = new File(directory, FormManager.currentForm().getCertificateSerialNumber() +
                            "_installer.png");
                }

                if(mTenantSignatureModel.getSignatureFileName() != null)
                {
                    mTenantSignatureView.invalidate();
                    mTenantSignatureFile = new File(mTenantSignatureModel.getSignatureFileName());
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(mTenantSignatureFile.getAbsolutePath(), options);
                    if(bitmap != null)
                    {
                        mTenantSignatureView.setSignatureBitmap(bitmap);
                    }
                    else
                    {
                        mTenantSignatureModel.setSignatureFileName(null);
                    }

                }
                else
                {
                    mTenantSignatureFile = new File(directory, FormManager.currentForm().getCertificateSerialNumber() +
                            "_tenant.png");
                }
            }
        });

    }

    @Override
    public void onClick(View v)
    {
        if(v == mFinishFormButton)
        {

            if(!FormManager.getStaticInstance().getCurrentForm().getChecklist().isCheckListComplete())
            {
                Toast.makeText(getActivity(), "Please complete the checklist", Toast.LENGTH_SHORT).show();
                MainActivity.sharedInstance().showFragment(new CheckListFragment(), true);
                return;
            }

            try
            {
                if(mInstallerSignatureFile.exists() && mTenantSignatureFile.exists())
                {
                    if(FormManager.currentForm().getEngineerName() != null && FormManager.currentForm().getEngineerName().length() > 0)
                    {
                        FormManager.getStaticInstance().setPDFCallbacks(this);
                        FormManager.getStaticInstance().generatePDFWithCurrentForm(getActivity());
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Please enter the engineer's name", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), "Please sign this form", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(v == mInstallerClearButton)
        {
            mInstallerSignatureView.clear();
        }
        else if(v == mTenantClearButton)
        {
            mTenantSignatureView.clear();
        }
    }

    @Override
    public void onSigned(SignatureView view)
    {
        File signatureFile;
        SignatureModel signatureModel;

        if(view == mTenantSignatureView)
        {
            signatureFile = mTenantSignatureFile;
            signatureModel = mTenantSignatureModel;
        }
        else if(view == mInstallerSignatureView)
        {
            signatureFile = mInstallerSignatureFile;
            signatureModel = mInstallerSignatureModel;
        }
        else
        {
            return;
        }

        FileOutputStream stream = null;
        try
        {
            stream = new FileOutputStream(signatureFile, false);
            view.getSignatureBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if(stream != null)
            {
                try
                {
                    stream.close();
                    signatureModel.setSignatureFileName(signatureFile.getAbsolutePath());
                    FormManager.getStaticInstance().saveCurrentForm();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClear(SignatureView view)
    {
        File signatureFile;
        SignatureModel signatureModel;

        if(view == mTenantSignatureView)
        {
            signatureFile = mTenantSignatureFile;
            signatureModel = mTenantSignatureModel;
        }
        else if(view == mInstallerSignatureView)
        {
            signatureFile = mInstallerSignatureFile;
            signatureModel = mInstallerSignatureModel;
        }
        else
        {
            return;
        }

        if(signatureFile.exists())
        {
            boolean delete = signatureFile.delete();
            if(delete)
            {
                signatureModel.setSignatureFileName(null);
                FormManager.getStaticInstance().saveCurrentForm();
            }
        }
    }

    @Override
    public void pdfGenerated(File pdfFile)
    {
        FormModel form = FormManager.getStaticInstance().getCurrentForm();
        FormManager.getStaticInstance().archiveCurrentForm();
        FormManager.getStaticInstance().uploadToS3(pdfFile, getActivity(), form);
    }

    @Override
    public void pdfUploaded(FormModel formModel)
    {
        formModel.setUploaded(true);
        FormManager.getStaticInstance().archiveForm(formModel);
        MainActivity.sharedInstance().showFragment(new MainScreenFragment(), false);
    }

    @Override
    public void pdfUploadFailed(FormModel formModel)
    {
        formModel.setUploaded(false);
        FormManager.getStaticInstance().archiveForm(formModel);
        Toast.makeText(getActivity(), "Upload Failed. Please ensure that you upload this pdf from the main screen.", Toast.LENGTH_SHORT);
        MainActivity.sharedInstance().showFragment(new MainScreenFragment(), false);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        FormManager.getStaticInstance().setPDFCallbacks(null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_activity_actions, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        MainActivity.sharedInstance().showFragment(new CheckListFragment(), true);
        return true;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mInstallerSignatureView.setOnSignedListener(null);
        mTenantSignatureView.setOnSignedListener(null);

        mInstallerClearButton.setOnClickListener(null);
        mTenantClearButton.setOnClickListener(null);
        mFinishFormButton.setOnClickListener(null);
        FormManager.getStaticInstance().setPDFCallbacks(null);
    }
}
