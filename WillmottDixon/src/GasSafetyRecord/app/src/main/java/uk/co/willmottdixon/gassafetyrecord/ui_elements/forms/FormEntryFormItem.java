package uk.co.willmottdixon.gassafetyrecord.ui_elements.forms;

import android.content.Context;

import java.util.ArrayList;

import uk.co.willmottdixon.gassafetyrecord.core.Constants;
import uk.co.willmottdixon.gassafetyrecord.fragments.forms.BaseFormFragment;
import uk.co.willmottdixon.gassafetyrecord.models.BaseModel;

/**
 * Created by Dev on 17/08/2015.
 */
public class FormEntryFormItem extends BaseFormItem
{
    private Class<? extends BaseFormFragment> mAttachedFormFragment;

    public FormEntryFormItem(Context context, String itemName, BaseModel formModel, Class<? extends BaseFormFragment> attachedFormFragment) {
        setFormElementType(Constants.FormElements.FORM_ELEMENT_FORM_ENTRY);
        setFormItemName(itemName);
        setContext(context);
        setBaseModel(formModel);
        setAttachedFormFragment(attachedFormFragment);
    }

    public Class<? extends BaseFormFragment> getAttachedFormFragment()
    {
        return mAttachedFormFragment;
    }

    public void setAttachedFormFragment(Class<? extends BaseFormFragment> attachedFormFragment)
    {
        mAttachedFormFragment = attachedFormFragment;
    }
}
