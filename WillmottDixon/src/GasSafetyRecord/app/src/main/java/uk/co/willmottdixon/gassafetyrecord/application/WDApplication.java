package uk.co.willmottdixon.gassafetyrecord.application;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Dev on 24/07/2015.
 */
public class WDApplication extends Application {

    private static Context context;

    public void onCreate(){
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        WDApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return WDApplication.context;
    }
}