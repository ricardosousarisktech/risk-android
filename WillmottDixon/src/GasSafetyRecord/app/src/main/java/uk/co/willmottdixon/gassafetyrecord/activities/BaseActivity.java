package uk.co.willmottdixon.gassafetyrecord.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.amazonaws.mobileconnectors.amazonmobileanalytics.InitializationException;
import com.amazonaws.mobileconnectors.amazonmobileanalytics.MobileAnalyticsManager;

import uk.co.willmottdixon.gassafetyrecord.R;

/**
 * Created by Dev on 21/07/2015.
 */
public class BaseActivity extends FragmentActivity {

    public static MobileAnalyticsManager analytics;

    @Override
    protected void onPause() {
        super.onPause();
        if (analytics != null) {
            analytics.getSessionClient().pauseSession();
            analytics.getEventClient().submitEvents();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (analytics == null) {
            try {
                analytics = MobileAnalyticsManager.getOrCreateInstance(
                        this.getApplicationContext(),
                        "5ee7bc45db1a42f6b66100cbf2b4f1de",
                        "us-east-1:7398ef84-dcf5-4947-9da5-38af6dfc6fe7"
                );

            } catch (InitializationException ex) {
                Log.e(this.getClass().getName(), "Failed to initialize Amazon Mobile Analytics", ex);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (analytics != null) {
            analytics.getSessionClient().resumeSession();
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
}
