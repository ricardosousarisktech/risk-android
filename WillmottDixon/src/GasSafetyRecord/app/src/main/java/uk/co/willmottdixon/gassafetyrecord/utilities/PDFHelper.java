package uk.co.willmottdixon.gassafetyrecord.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.DrawableRes;

import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfStamper;

import java.io.File;

/**
 * Created by Dev on 20/08/2015.
 */
public class PDFHelper
{
    public static void addImage(PdfStamper stamper, AcroFields form, String fieldName, @DrawableRes String imagePath)
    {
        try
        {
            java.util.List<AcroFields.FieldPosition> imageField = form.getFieldPositions(fieldName);
            if (imageField != null && imageField.size() > 0)
            {
                AcroFields.FieldPosition fieldPosition = imageField.get(0);
                Rectangle rect = fieldPosition.position;

                Image img = Image.getInstance(imagePath);
                img.scaleToFit(rect.getWidth(), rect.getHeight());
                img.setBorder(2);
                img.setAbsolutePosition(rect.getLeft() + ((rect.getWidth() / 2) - (img.getScaledWidth() / 2)),
                                        rect.getBottom() + ((rect.getHeight() / 2) - (img.getScaledHeight() / 2)));
                PdfContentByte cb = stamper.getOverContent(fieldPosition.page);
                cb.addImage(img);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static void showPDF(Activity activity, File outputFile)
    {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(outputFile);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            activity.startActivity(intent);
        } catch (Exception e) {
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            builder.setMessage(
                    "Please install an application which allows viewing of pdf files.")
                    .setTitle("Cannot view PDF");
            builder.setPositiveButton("OK", null);

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
}
