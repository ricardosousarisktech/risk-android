package uk.co.willmottdixon.gassafetyrecord.utilities;

import android.graphics.Typeface;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;

import org.xml.sax.XMLReader;

import uk.co.willmottdixon.gassafetyrecord.application.WDApplication;

public class MultipleFontTagHandler implements Html.TagHandler {

    @Override
    public void handleTag(final boolean opening, final String tag, Editable output, final XMLReader xmlReader) {
        if (tag.equals("b")) {
            handleBoldTag(output);
        }
        else if (tag.equals("p"))
        {
            handlePTag(output);
        }
    }

    private void handleBoldTag(Editable output) {
        Typeface tfBold = Typeface.create("HelveticaNeue", Typeface.BOLD);
        output.setSpan(new CustomTypefaceSpan("", tfBold), 0, output.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
    }


    private void handlePTag(Editable output) {
        Typeface tfLight = Typeface.createFromAsset(WDApplication.getAppContext().getAssets(), "Fonts/HelveticaNeueUL.otf");
        output.setSpan(new CustomTypefaceSpan("", tfLight), 0, output.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
    }
}