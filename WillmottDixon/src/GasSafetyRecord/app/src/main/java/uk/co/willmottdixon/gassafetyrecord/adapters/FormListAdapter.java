package uk.co.willmottdixon.gassafetyrecord.adapters;

/**
 * Created by Dev on 03/08/2015.
 */

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;

import uk.co.willmottdixon.gassafetyrecord.R;
import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.callbacks.FragmentOptionSelectionCallback;
import uk.co.willmottdixon.gassafetyrecord.callbacks.FragmentSubFormCompleteCallback;
import uk.co.willmottdixon.gassafetyrecord.core.Constants;
import uk.co.willmottdixon.gassafetyrecord.core.ExtendedTextWatcher;
import uk.co.willmottdixon.gassafetyrecord.core.FormManager;
import uk.co.willmottdixon.gassafetyrecord.fragments.BaseFragment;
import uk.co.willmottdixon.gassafetyrecord.fragments.form_elements.FormOptionSelectFragment;
import uk.co.willmottdixon.gassafetyrecord.fragments.forms.SubFormFragment;
import uk.co.willmottdixon.gassafetyrecord.models.BaseModel;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.ExtendedEditText;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.BaseFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.FormEntryFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.OptionSelectFormItem;
import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.TextFormItem;
import uk.co.willmottdixon.gassafetyrecord.utilities.Helpers;

public class FormListAdapter extends BaseExpandableListAdapter
{

    private Context mContext;

    private ArrayList<String> mFormHeaders; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<BaseFormItem>> mFormItems;
    private int mIncId = 1;
    private BaseFormItem mLastChildItem;
    private View mLastConvertView;
    private View mDummyFocusLayout;
    private BaseFragment mRootFragment;

    public FormListAdapter(Context context, HashMap<String, ArrayList<BaseFormItem>> formItems, ArrayList<String> formHeaders)
    {
        mContext = context;
        mFormItems = formItems;
        mFormHeaders = formHeaders;
    }

    private View generateOptionField(View convertView, ViewGroup parent, final BaseFormItem childItem)
    {
        final FormItemOptionHolder optionEntryHolder;

        /** Get holder **/
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.formitem_option_entry, parent, false);
            optionEntryHolder = new FormItemOptionHolder();
            optionEntryHolder.setSettingLabel((TextView) convertView.findViewById(R.id.settingLabel));
            optionEntryHolder.setSettingValueLabel((TextView) convertView.findViewById(R.id.settingValueLabel));
            optionEntryHolder.getSettingLabel().setFocusableInTouchMode(false);
            optionEntryHolder.getSettingLabel().setFocusable(false);
            optionEntryHolder.getSettingValueLabel().setFocusableInTouchMode(false);
            optionEntryHolder.getSettingValueLabel().setFocusable(false);
            convertView.setTag(optionEntryHolder);
        }
        else
        {
            optionEntryHolder = (FormItemOptionHolder) convertView.getTag();
        }

        /** Add listeners **/
        convertView.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                final FormOptionSelectFragment fragment = new FormOptionSelectFragment();
                fragment.setOptions(((OptionSelectFormItem) childItem).getOptions());
                MainActivity.sharedInstance().showFragment(fragment, true);
                final FormItemOptionHolder fHolder = (FormItemOptionHolder) v.getTag();

                fragment.setFragmentOptionSelectionCallback(new FragmentOptionSelectionCallback()
                {
                    @Override
                    public void onSelection(Object selection)
                    {

                        BaseModel baseModel = childItem.getBaseModel();

                        for(String key : mFormItems.keySet())
                        {
                            for(BaseFormItem item : mFormItems.get(key))
                            {
                                if(item.getLinkedVisibilityElement() != null && item.getLinkedVisibilityElement().equals(childItem))
                                {
                                    if(item.getVisibleValue().equals(selection))
                                    {
                                        item.getHolderView().setVisibility(View.VISIBLE);
                                    }
                                    else
                                    {
                                        item.getHolderView().setVisibility(View.GONE);
                                        Helpers.invokeSetterMethod(item.getPropertySetter(), item.getBaseModel(), null);
                                        FormManager.getStaticInstance().saveCurrentForm();
                                    }
                                }
                            }
                        }

                        Helpers.invokeSetterMethod(childItem.getPropertySetter(), baseModel, selection);
                        fHolder.getSettingValueLabel().setText(selection.toString());
                        FormManager.getStaticInstance().saveCurrentForm();
                        FragmentManager fragmentManager = ((Activity) mContext).getFragmentManager();
                        fragmentManager.popBackStack();
                    }
                });

                if(mRootFragment != null)
                {
                    mRootFragment.hideKeyboard();
                }
                if(mDummyFocusLayout != null) {
                    mDummyFocusLayout.requestFocus();
                }
            }
        });

        /** Populate cell **/
        BaseModel baseModel = childItem.getBaseModel();
        Object value = Helpers.invokeGetterMethod(childItem.getPropertyGetter(), baseModel);

        if (value != null)
        {
            optionEntryHolder.getSettingValueLabel().setText(value.toString());
        }
        else
        {
            optionEntryHolder.getSettingValueLabel().setText("");
        }

        if (childItem.isFieldRequired())
        {
            optionEntryHolder.getSettingLabel().setText(childItem.getFormItemName() + " *");
        }
        else
        {
            optionEntryHolder.getSettingLabel().setText(childItem.getFormItemName());
        }

        return convertView;
    }

    private View generateFormEntryField(View convertView, ViewGroup parent, final BaseFormItem childItem)
    {
        final FormItemFormEntryHolder formEntryHolder;

        /** Get holder **/
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.formitem_option_entry, parent, false);
            formEntryHolder = new FormItemFormEntryHolder();
            formEntryHolder.setSettingLabel((TextView) convertView.findViewById(R.id.settingLabel));
            formEntryHolder.setSettingValueLabel((TextView) convertView.findViewById(R.id.settingValueLabel));
            convertView.setTag(formEntryHolder);
        }
        else
        {
            formEntryHolder = (FormItemFormEntryHolder) convertView.getTag();
        }

        /** Add listeners **/
        convertView.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                if(mDummyFocusLayout != null) {
                    mDummyFocusLayout.requestFocus();
                }
                if(mRootFragment != null)
                {
                    mRootFragment.hideKeyboard();
                }
                Class<?> nextFragmentClass = ((FormEntryFormItem) childItem).getAttachedFormFragment();

                Constructor<?> constructor;
                try
                {
                    constructor = nextFragmentClass.getConstructor();

                    SubFormFragment subFormFragment;
                    if (constructor != null)
                    {
                        subFormFragment = (SubFormFragment) constructor.newInstance();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(SubFormFragment.SUB_FORM_CHILD_ITEM_KEY, childItem);
                        subFormFragment.setArguments(bundle);
                        MainActivity.sharedInstance().showFragment(subFormFragment, true);
                        final FormItemFormEntryHolder fHolder = (FormItemFormEntryHolder) v.getTag();

                        subFormFragment.setFragmentSubFormCompleteCallback(new FragmentSubFormCompleteCallback()
                        {
                            @Override
                            public void onComplete()
                            {
                                BaseModel baseModel = childItem.getBaseModel();
                                FragmentManager fragmentManager = ((Activity) mContext).getFragmentManager();
                                fragmentManager.popBackStack();
                                FormManager.getStaticInstance().saveCurrentForm();
                                fHolder.getSettingValueLabel().setText(baseModel.toString());
                            }
                        });

                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        /** Populate cell **/
        BaseModel baseModel = childItem.getBaseModel();

        if (baseModel != null)
        {
            formEntryHolder.getSettingValueLabel().setText(baseModel.toString());
        }
        else
        {
            formEntryHolder.getSettingValueLabel().setText("");
        }

        if (childItem.isFieldRequired())
        {
            formEntryHolder.getSettingLabel().setText(childItem.getFormItemName() + " *");
        }
        else
        {
            formEntryHolder.getSettingLabel().setText(childItem.getFormItemName());
        }

        return convertView;
    }

    public View generateTextField(View convertView, ViewGroup parent, final BaseFormItem childItem)
    {
        FormItemTextEntryHolder textEntryHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.formitem_text_entry, parent, false);

            textEntryHolder = new FormItemTextEntryHolder();

            textEntryHolder.setSettingLabel((TextView) convertView.findViewById(R.id.settingLabel));
            textEntryHolder.setSettingTextfield((ExtendedEditText) convertView.findViewById(R.id.settingTextfield));

            convertView.setTag(textEntryHolder);
        }
        else
        {
            textEntryHolder = (FormItemTextEntryHolder) convertView.getTag();
        }
        int inputType = ((TextFormItem) childItem).getInputType();

        if(inputType == 0)
        {
            inputType = InputType.TYPE_CLASS_TEXT;
        }

        textEntryHolder.getSettingTextfield().setInputType(inputType);


        int rawInputType = ((TextFormItem) childItem).getRawInputType();

        if(rawInputType == 0)
        {
            rawInputType = InputType.TYPE_CLASS_TEXT;
        }

        textEntryHolder.getSettingTextfield().setRawInputType(rawInputType);

        BaseModel baseModel = childItem.getBaseModel();

        Object value = Helpers.invokeGetterMethod(childItem.getPropertyGetter(), baseModel);

        if (value != null)
        {
            textEntryHolder.getSettingTextfield().setText(value.toString());
        }
        else
        {
            textEntryHolder.getSettingTextfield().setText("");
        }

        if (childItem.isFieldRequired())
        {
            textEntryHolder.getSettingLabel().setText(childItem.getFormItemName() + " *");
        }
        else
        {
            textEntryHolder.getSettingLabel().setText(childItem.getFormItemName());
        }

        textEntryHolder.getSettingTextfield();

        textEntryHolder.getSettingTextfield().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ((ExtendedEditText) v).setSelection(((ExtendedEditText) v).getText().length());
                    ((ExtendedEditText) v).addTextChangedListener(new ExtendedTextWatcher(((ExtendedEditText) v)) {

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after, EditText editText) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count, EditText editText) {

                        }

                        @Override
                        public void afterTextChanged(Editable s, EditText editText) {
                            try {
                                BaseModel baseModel = childItem.getBaseModel();
                                Helpers.invokeSetterMethod(childItem.getPropertySetter(), baseModel, s.toString());

                            } finally {
                                FormManager.getStaticInstance().saveCurrentForm();
                            }
                        }
                    });
                } else {
                    ((ExtendedEditText) v).clearTextChangedListeners();
                }
            }
        });

        textEntryHolder.getSettingTextfield().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    v.clearFocus();
                    if(mDummyFocusLayout != null) {
                        mDummyFocusLayout.requestFocus();
                    }
                    if(mRootFragment != null)
                    {
                        mRootFragment.hideKeyboard();
                    }
                }
                return false;
            }
        });

        return convertView;
    }

    public View generateToggleField(View convertView, ViewGroup parent, final BaseFormItem childItem)
    {
        FormItemToggleHolder toggleEntryHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.formitem_toggle_entry, parent, false);

            toggleEntryHolder = new FormItemToggleHolder();

            toggleEntryHolder.setSettingLabel((TextView) convertView.findViewById(R.id.settingLabel));
            toggleEntryHolder.setSettingToggle((Switch) convertView.findViewById(R.id.settingToggle));

            convertView.setTag(toggleEntryHolder);
        }
        else
        {
            toggleEntryHolder = (FormItemToggleHolder) convertView.getTag();
        }


        if (childItem.isFieldRequired())
        {
            toggleEntryHolder.getSettingLabel().setText(childItem.getFormItemName() + " *");
        }
        else
        {
            toggleEntryHolder.getSettingLabel().setText(childItem.getFormItemName());
        }

        Boolean checked = (Boolean) Helpers.invokeGetterMethod(childItem.getPropertyGetter(), childItem.getBaseModel());

        if (checked == null)
        {
            checked = false;
        }

        toggleEntryHolder.getSettingToggle().setChecked(checked.booleanValue());

        toggleEntryHolder.getSettingToggle().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                BaseModel baseModel = childItem.getBaseModel();
                Helpers.invokeSetterMethod(childItem.getPropertySetter(), baseModel, isChecked);
                FormManager.getStaticInstance().saveCurrentForm();
                if(mDummyFocusLayout != null) {
                    mDummyFocusLayout.requestFocus();
                }
                if(mRootFragment != null)
                {
                    mRootFragment.hideKeyboard();
                }
            }
        });

        return convertView;
    }

    /**
     * OVERRIDES
     */

    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
        return this.mFormItems.get(this.mFormHeaders.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return childPosition;
    }


    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent)
    {

        Constants.FormElements viewType = Constants.FormElements.values()[getChildType(groupPosition, childPosition)];

        BaseFormItem childItem = (BaseFormItem) getChild(groupPosition, childPosition);

        switch (viewType)
        {
            case FORM_ELEMENT_FORM_ENTRY:
                convertView = generateFormEntryField(convertView, parent, childItem);
                //FormItemOptionHolder optionEntryHolder = (FormItemOptionHolder) convertView.getTag();
                break;
            case FORM_ELEMENT_OPTION:
                convertView = generateOptionField(convertView, parent, childItem);
                //FormItemOptionHolder optionEntryHolder = (FormItemOptionHolder) convertView.getTag();
                break;
            case FORM_ELEMENT_TEXT_FIELD:
                convertView = generateTextField(convertView, parent, childItem);
                //FormItemTextEntryHolder textEntryHolder = (FormItemTextEntryHolder) convertView.getTag();
                break;
            case FORM_ELEMENT_TOGGLE:
                convertView = generateToggleField(convertView, parent, childItem);
                //FormItemToggleHolder toggleHolder = (FormItemToggleHolder) convertView.getTag();
                break;
            case FORM_ELEMENT_COUNT:
                break;
        }

        if(mLastConvertView != null && mLastConvertView.getTag() instanceof FormItemTextEntryHolder)
        {
            if (!(convertView.getTag() instanceof FormItemTextEntryHolder))
            {
                FormItemTextEntryHolder holder = (FormItemTextEntryHolder) mLastConvertView.getTag();
                holder.getSettingTextfield().setImeActionLabel("Done", EditorInfo.IME_ACTION_DONE);
            }
        }

        mLastConvertView = convertView;

        childItem.setHolderView(convertView);

        return convertView;
    }

    @Override
    public int getChildTypeCount()
    {
        return Constants.FormElements.FORM_ELEMENT_COUNT.ordinal();
    }

    @Override
    public int getChildType(int groupPosition, int childPosition)
    {
        BaseFormItem childItem = (BaseFormItem) getChild(groupPosition, childPosition);
        return childItem.getFormElementTypeAsInt();
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        return this.mFormItems.get(this.mFormHeaders.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        return this.mFormHeaders.get(groupPosition);
    }

    @Override
    public int getGroupCount()
    {
        return this.mFormHeaders.size();
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent)
    {

        SettingsHeaderViewHolderItem viewHolder;
        if (convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.formitem_header, parent, false);

            viewHolder = new SettingsHeaderViewHolderItem();

            viewHolder.setTitleLabel((TextView) convertView.findViewById(R.id.titleLabel));

            convertView.setTag(viewHolder);

        }
        else
        {
            viewHolder = (SettingsHeaderViewHolderItem) convertView.getTag();
        }

        String headerTitle = mFormHeaders.get(groupPosition);
        viewHolder.getTitleLabel().setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }

    public void processLinkedElements()
    {
        for(String baseKey : mFormItems.keySet())
        {
            for(BaseFormItem baseItem : mFormItems.get(baseKey))
            {
                for (String key : mFormItems.keySet())
                {
                    for (BaseFormItem item : mFormItems.get(key))
                    {
                        if (item.getLinkedVisibilityElement() != null && item.getLinkedVisibilityElement().equals(baseItem))
                        {
                            if (item.getVisibleValue().equals(Helpers.invokeGetterMethod(baseItem.getPropertyGetter(), baseItem.getBaseModel())))
                            {
                                item.getHolderView().setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                item.getHolderView().setVisibility(View.GONE);
                                Helpers.invokeSetterMethod(item.getPropertySetter(), item.getBaseModel(), null);
                                FormManager.getStaticInstance().saveCurrentForm();
                            }
                        }
                    }
                }
            }
        }
    }

    public void setDummyFocusLayout(View dummyFocusLayout) {
        mDummyFocusLayout = dummyFocusLayout;
    }


    /**
     * HOLDERS
     **/

    private class SettingsHeaderViewHolderItem
    {
        private TextView mTitleLabel;

        public TextView getTitleLabel()
        {
            return mTitleLabel;
        }

        public void setTitleLabel(TextView titleLabel)
        {
            mTitleLabel = titleLabel;
        }
    }

    @SuppressWarnings("unused")
    private class FormItemTextEntryHolder
    {
        private ImageView mSettingsIcon;
        private TextView mSettingLabel;
        private ExtendedEditText mSettingTextfield;

        public void setSettingsIcon(ImageView settingsIcon)
        {
            mSettingsIcon = settingsIcon;
            //noinspection ResourceType
            mSettingsIcon.setId(mIncId);
            mIncId++;
        }

        public TextView getSettingLabel()
        {
            return mSettingLabel;
        }

        public void setSettingLabel(TextView settingLabel)
        {
            mSettingLabel = settingLabel;
            //noinspection ResourceType
            mSettingLabel.setId(mIncId);
            mIncId++;
        }

        public ExtendedEditText getSettingTextfield()
        {
            return mSettingTextfield;
        }

        public void setSettingTextfield(ExtendedEditText settingTextfield)
        {
            mSettingTextfield = settingTextfield;
            //noinspection ResourceType
            mSettingTextfield.setId(mIncId);
            mIncId++;
        }
    }

    public BaseFragment getRootFragment() {
        return mRootFragment;
    }

    public void setRootFragment(BaseFragment rootFragment) {
        mRootFragment = rootFragment;
    }

    @SuppressWarnings("unused")
    private class FormItemToggleHolder
    {
        private ImageView mSettingsIcon;
        private TextView mSettingLabel;
        private Switch mSettingToggle;

        public ImageView getSettingsIcon()
        {
            return mSettingsIcon;
        }

        public void setSettingsIcon(ImageView settingsIcon)
        {
            mSettingsIcon = settingsIcon;
        }

        public TextView getSettingLabel()
        {
            return mSettingLabel;
        }

        public void setSettingLabel(TextView settingLabel)
        {
            mSettingLabel = settingLabel;
        }

        public Switch getSettingToggle()
        {
            return mSettingToggle;
        }

        public void setSettingToggle(Switch settingToggle)
        {
            mSettingToggle = settingToggle;
        }
    }

    @SuppressWarnings("unused")
    private class FormItemOptionHolder
    {
        private ImageView mSettingsIcon;
        private TextView mSettingLabel;
        private TextView mSettingValueLabel;

        public ImageView getSettingsIcon()
        {
            return mSettingsIcon;
        }

        public void setSettingsIcon(ImageView settingsIcon)
        {
            mSettingsIcon = settingsIcon;
        }

        public TextView getSettingLabel()
        {
            return mSettingLabel;
        }

        public void setSettingLabel(TextView settingLabel)
        {
            mSettingLabel = settingLabel;
        }

        public TextView getSettingValueLabel()
        {
            return mSettingValueLabel;
        }

        public void setSettingValueLabel(TextView settingValueLabel)
        {
            mSettingValueLabel = settingValueLabel;
        }
    }

    @SuppressWarnings("unused")
    private class FormItemFormEntryHolder
    {
        private ImageView mSettingsIcon;
        private TextView mSettingLabel;
        private TextView mSettingValueLabel;

        public ImageView getSettingsIcon()
        {
            return mSettingsIcon;
        }

        public void setSettingsIcon(ImageView settingsIcon)
        {
            mSettingsIcon = settingsIcon;
        }

        public TextView getSettingLabel()
        {
            return mSettingLabel;
        }

        public void setSettingLabel(TextView settingLabel)
        {
            mSettingLabel = settingLabel;
        }

        public TextView getSettingValueLabel()
        {
            return mSettingValueLabel;
        }

        public void setSettingValueLabel(TextView settingValueLabel)
        {
            mSettingValueLabel = settingValueLabel;
        }
    }
}