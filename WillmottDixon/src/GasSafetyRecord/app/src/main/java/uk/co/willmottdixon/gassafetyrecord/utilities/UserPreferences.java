package uk.co.willmottdixon.gassafetyrecord.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserPreferences {
    private static UserPreferences sharedInstance;
    private SharedPreferences mSharedPreferences;
    private Context mContext;

    private UserPreferences() {
    }

    public static synchronized UserPreferences getSharedInstance(Context context) {
        if(sharedInstance == null)
        {
            sharedInstance = new UserPreferences();
        }

        sharedInstance.mContext = context;

        if(context != null) {
            sharedInstance.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }

        return sharedInstance;
    }

    public void clearAll()
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void putString(String value, String key)
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void putInt(int value, String key)
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void putLong(long value, String key)
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public void putFloat(float value, String key)
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public void putBoolean(boolean value, String key)
    {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public String getStringForKey(String key)
    {
        return getOptStringForKey(key, "");
    }

    public String getOptStringForKey(String key, String value)
    {
        return mSharedPreferences.getString(key, value);
    }

    public int getIntForKey(String key)
    {
        return getOptIntForKey(key, -1);
    }

    public int getOptIntForKey(String key, int value)
    {
        return mSharedPreferences.getInt(key, value);
    }

    public float getFloatForKey(String key)
    {
        return getOptFloatForKey(key, -1.0f);
    }

    public float getOptFloatForKey(String key, float value)
    {
        return mSharedPreferences.getFloat(key, value);
    }

    public boolean getBoolForKey(String key)
    {
        return getOptBoolForKey(key, false);
    }

    public boolean getOptBoolForKey(String key, boolean value)
    {
        return mSharedPreferences.getBoolean(key, value);
    }
}
