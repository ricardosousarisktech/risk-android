package uk.co.willmottdixon.gassafetyrecord.models;

import android.view.View;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfStamper;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Dev on 07/08/2015.
 */
public class BaseModel extends java.lang.Object implements Serializable {


    public void mapToPdfFields(PdfStamper stamper, AcroFields fields) throws IOException, DocumentException
    {

    }

    public BaseModel() {
    }

    @Override
    public String toString()
    {
        return super.toString();
    }

}
