package uk.co.willmottdixon.gassafetyrecord.callbacks;

import uk.co.willmottdixon.gassafetyrecord.ui_elements.forms.BaseFormItem;

/**
 * Created by Dev on 03/08/2015.
 */
public interface FormCallback {

    void configOptionSelected(BaseFormItem configItem);
    void configValueChanged(BaseFormItem configItem);

}
