package uk.co.willmottdixon.gassafetyrecord.models.address;

/**
 * Created by Dev on 07/08/2015.
 */
public class TenantModel extends AddressModel {

    private String mTitle;
    private String mPhoneNumber;

    /**
     * Getter for property 'phoneNumber'.
     *
     * @return Value for property 'phoneNumber'.
     */
    public String getPhoneNumber()
    {
        return mPhoneNumber;
    }

    /**
     * Setter for property 'phoneNumber'.
     *
     * @param phoneNumber Value to set for property 'phoneNumber'.
     */
    public void setPhoneNumber(String phoneNumber)
    {
        mPhoneNumber = phoneNumber;
    }

    /**
     * Getter for property 'title'.
     *
     * @return Value for property 'title'.
     */
    public String getTitle()
    {
        return mTitle;
    }

    /**
     * Setter for property 'title'.
     *
     * @param title Value to set for property 'title'.
     */
    public void setTitle(String title)
    {
        mTitle = title;
    }
}
