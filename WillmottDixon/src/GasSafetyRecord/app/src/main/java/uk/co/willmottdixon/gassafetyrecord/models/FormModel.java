package uk.co.willmottdixon.gassafetyrecord.models;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfStamper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import uk.co.willmottdixon.gassafetyrecord.core.Constants;
import uk.co.willmottdixon.gassafetyrecord.core.DefaultConfigManager;
import uk.co.willmottdixon.gassafetyrecord.core.FormManager;
import uk.co.willmottdixon.gassafetyrecord.models.address.InstallerModel;
import uk.co.willmottdixon.gassafetyrecord.models.address.LandlordModel;
import uk.co.willmottdixon.gassafetyrecord.models.address.TenantModel;
import uk.co.willmottdixon.gassafetyrecord.utilities.PDFHelper;

/**
 * Created by Dev on 07/08/2015.
 */
@SuppressWarnings("unused")
public class FormModel extends BaseModel {

    private InstallerModel mInstallerDetails;
    private TenantModel mTenantDetails;
    private LandlordModel mLandlordDetails;

    private ArrayList<InspectionModel> mInspectionsList;
    private CheckListModel mChecklist;
    private SignatureModel mInstallerSignature;
    private SignatureModel mTenantSignature;

    private String mServiceType;
    private String mCertificateSerialNumber;
    private String mEngineerRegistrationNumber;
    private String mPDFFilePath;

    /**
     * Getter for property 'engineerName'.
     *
     * @return Value for property 'engineerName'.
     */
    public String getEngineerName()
    {
        return mEngineerName;
    }

    /**
     * Setter for property 'engineerName'.
     *
     * @param engineerName Value to set for property 'engineerName'.
     */
    public void setEngineerName(String engineerName)
    {
        mEngineerName = engineerName;
    }

    /**
     * Getter for property 'createdDate'.
     *
     * @return Value for property 'createdDate'.
     */
    public Date getCreatedDate()
    {
        return mCreatedDate;
    }

    /**
     * Setter for property 'createdDate'.
     *
     * @param createdDate Value to set for property 'createdDate'.
     */
    public void setCreatedDate(Date createdDate)
    {
        mCreatedDate = createdDate;
    }

    /**
     * Getter for property 'completedDate'.
     *
     * @return Value for property 'completedDate'.
     */
    public Date getCompletedDate()
    {
        return mCompletedDate;
    }

    /**
     * Setter for property 'completedDate'.
     *
     * @param completedDate Value to set for property 'completedDate'.
     */
    public void setCompletedDate(Date completedDate)
    {
        mCompletedDate = completedDate;
    }

    /**
     * Getter for property 'uploadedDate'.
     *
     * @return Value for property 'uploadedDate'.
     */
    public Date getUploadedDate()
    {
        return mUploadedDate;
    }

    /**
     * Setter for property 'uploadedDate'.
     *
     * @param uploadedDate Value to set for property 'uploadedDate'.
     */
    public void setUploadedDate(Date uploadedDate)
    {
        mUploadedDate = uploadedDate;
    }

    private String mEngineerName;
    private Boolean mUploaded = false;
    private Date mCreatedDate;
    private Date mCompletedDate;
    private Date mUploadedDate;

    public FormModel() {
        mInstallerDetails = new InstallerModel();
        mTenantDetails = new TenantModel();
        mLandlordDetails = DefaultConfigManager.getStaticInstance().getLandlord();

        mInspectionsList = new ArrayList<>();
        mChecklist = new CheckListModel();
        mTenantSignature = new SignatureModel();
        mInstallerSignature = new SignatureModel();
    }

    @Override
    public void mapToPdfFields(PdfStamper stamper, AcroFields fields) throws IOException, DocumentException
    {
        fields.setField(Constants.PDF_KEY_SERVICE_TYPE, mServiceType);
        fields.setField(Constants.PDF_KEY_CERTIFICATE_SERIAL_NUMBER, mCertificateSerialNumber);
        fields.setField(Constants.PDF_KEY_ENGINEER_REGISTRATION_NUMBER, mEngineerRegistrationNumber);
        //todo: get completion date
        //fields.setField(Constants.PDF_KEY_ENGINEER_REGISTRATION_NUMBER, mCompletedDate);
        fields.setField(Constants.PDF_KEY_INSTALLER_NAME, mInstallerDetails.getName());
        fields.setField(Constants.PDF_KEY_INSTALLER_PHONE, mInstallerDetails.getPhoneNumber());
        fields.setField(Constants.PDF_KEY_INSTALLER_ADDRESS, mInstallerDetails.getSimpleAddress());

        ArrayList<String> titles = DefaultConfigManager.getStaticInstance().getTitles();
        String tenantName;
        if(mTenantDetails.getTitle() == titles.get(titles.size() - 1))
        {
            tenantName = mTenantDetails.getName();
        }
        else
        {
            tenantName = mTenantDetails.getTitle() + " " + mTenantDetails.getName();
        }
        fields.setField(Constants.PDF_KEY_TENANT_NAME, tenantName);
        fields.setField(Constants.PDF_KEY_TENANT_PHONE, mTenantDetails.getPhoneNumber());
        fields.setField(Constants.PDF_KEY_TENANT_ADDRESS, mTenantDetails.getSimpleAddress());

        if(mLandlordDetails.getName() == null || mLandlordDetails.getName().length() == 0)
        {
            mLandlordDetails = DefaultConfigManager.getStaticInstance().getLandlord();
        }

        Integer appliancesTested = 0;

        for(int i = 1; i <= 4; i++)
        {
            InspectionModel inspectionModel = mInspectionsList.get(i - 1);

            if(inspectionModel.getLocation() != null && inspectionModel.getLocation().length() > 0)
            {
                appliancesTested++;
            }

            fields.setField(Constants.PDF_KEY_INS_LOC + i, inspectionModel.getLocation());
            fields.setField(Constants.PDF_KEY_INS_TYPE + i, inspectionModel.getType());
            fields.setField(Constants.PDF_KEY_INS_MAKE + i, inspectionModel.getMake());
            fields.setField(Constants.PDF_KEY_INS_MODEL + i, inspectionModel.getModel());
            fields.setField(Constants.PDF_KEY_INS_FLUE_TYPE + i, inspectionModel.getFlueType());
            fields.setField(Constants.PDF_KEY_INS_OPP_PRES + i, inspectionModel.getOperatingPressure());
            fields.setField(Constants.PDF_KEY_INS_APP_INS + i, inspectionModel.getApplianceInspected());
            fields.setField(Constants.PDF_KEY_INS_SAFETY_DEVICE + i, inspectionModel.getSafetyDeviceCorrectOperation());
            fields.setField(Constants.PDF_KEY_INS_VENT_ADEQ + i, inspectionModel.getVentilationAdequate());
            fields.setField(Constants.PDF_KEY_INS_LANDLORD + i, inspectionModel.getLandlordAppliance());
            fields.setField(Constants.PDF_KEY_INS_F_VISUAL + i, inspectionModel.getVisualCondition());
            fields.setField(Constants.PDF_KEY_INS_F_FLOW + i, inspectionModel.getFlueFlowTest());
            fields.setField(Constants.PDF_KEY_INS_F_SPILLAGE + i, inspectionModel.getSpillageTest());
            fields.setField(Constants.PDF_KEY_INS_F_TERM + i, inspectionModel.getTerminationSatisfactory());
            fields.setField(Constants.PDF_KEY_INS_APP_SAFE + i, inspectionModel.getApplianceSafeToUse());
            fields.setField(Constants.PDF_KEY_INS_COMB + i, inspectionModel.getCombustionAnalysisReading());
            fields.setField(Constants.PDF_KEY_INS_PRES_GAS + i, inspectionModel.getPressureAtGasInlet());
            fields.setField(Constants.PDF_KEY_INS_APP_SERV + i, inspectionModel.getApplianceServiced());

            if(inspectionModel.getLocation() == null || inspectionModel.getLocation().length() == 0)
            {
                fields.setField(Constants.PDF_KEY_INS_FAULT + i, "");
            }
            else
            {
                if(inspectionModel.getFault() == null || inspectionModel.getFault().length() == 0)
                {
                    fields.setField(Constants.PDF_KEY_INS_FAULT + i, "None");
                }
                else
                {
                    fields.setField(Constants.PDF_KEY_INS_FAULT + i, inspectionModel.getFault());
                }
            }
        }




        fields.setField(Constants.PDF_KEY_LANDLORD_NAME, mLandlordDetails.getName());
        fields.setField(Constants.PDF_KEY_LANDLORD_ADDRESS, mLandlordDetails.getLongerAddress());

        fields.setField(Constants.PDF_KEY_FRONT_NAME, tenantName);
        fields.setField(Constants.PDF_KEY_FRONT_ADDRESS, mTenantDetails.getSimpleAddress());

        fields.setField(Constants.PDF_KEY_BEHALF_LANDLORD, tenantName);
        fields.setField(Constants.PDF_KEY_RECORDED_BY, getEngineerName());
        fields.setField(Constants.PDF_KEY_APPLIANCES_TESTED, appliancesTested.toString());

        fields.setField(Constants.PDF_KEY_CHK_WARNING_ISSUED, mChecklist.getWarningIssued());
        if(mChecklist.getWarningNumber() != null && mChecklist.getWarningNumber().length() > 0)
        {
            fields.setField(Constants.PDF_KEY_CHK_WARNING_NUMBER, mChecklist.getWarningNumber());
        }
        else
        {
            fields.setField(Constants.PDF_KEY_CHK_WARNING_NUMBER, "");
        }
        fields.setField(Constants.PDF_KEY_CHK_GAS_INSTALL, mChecklist.getGasInstallationPipeworkSatisfactory());
        fields.setField(Constants.PDF_KEY_CHK_EMER_CONT, mChecklist.getEmergencyControlAccessible());
        fields.setField(Constants.PDF_KEY_CHK_EQUIP_BOND, mChecklist.getEquipotentialBondingSatisfactory());
        fields.setField(Constants.PDF_KEY_CHK_PIPEWORK, mChecklist.getPipeworkSizedCorrectly());
        fields.setField(Constants.PDF_KEY_CHK_EARTH, mChecklist.getEarthFitted());
        fields.setField(Constants.PDF_KEY_CHK_TIGHTNESS, mChecklist.getTightnessTest());
        fields.setField(Constants.PDF_KEY_CHK_WORK_PRESS, mChecklist.getWorkingPressureAtMeter());
        fields.setField(Constants.PDF_KEY_CHK_WORK_SMOKE, mChecklist.getWorkingSmokeAlarms());
        fields.setField(Constants.PDF_KEY_CHK_WORK_CO, mChecklist.getWorkingCOAlarms());

        Date date = FormManager.currentForm().getCompletedDate();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = simpleDateFormat.format(date);

        fields.setField(Constants.PDF_KEY_DATE, formattedDate);

        PDFHelper.addImage(stamper, fields, Constants.PDF_KEY_INSTALLER_SIG, mInstallerSignature.getSignatureFileName());
        PDFHelper.addImage(stamper, fields, Constants.PDF_KEY_TENANT_SIG, mTenantSignature.getSignatureFileName());

    }


    // BASE GETTERS AND SETTERS ONLY BELOW HERE

    public InstallerModel getInstallerDetails() {
        return mInstallerDetails;
    }

    public void setInstallerDetails(InstallerModel installerDetails) {
        mInstallerDetails = installerDetails;
    }

    public TenantModel getTenantDetails() {
        return mTenantDetails;
    }

    public void setTenantDetails(TenantModel tenantDetails) {
        mTenantDetails = tenantDetails;
    }

    public LandlordModel getLandlordDetails() {
        return mLandlordDetails;
    }

    public void setLandlordDetails(LandlordModel landlordDetails) {
        mLandlordDetails = landlordDetails;
    }

    public ArrayList<InspectionModel> getInspectionsList() {

        while(mInspectionsList.size() < 4)
        {
            mInspectionsList.add(new InspectionModel());
        }
        return mInspectionsList;
    }

    public void setInspectionsList(ArrayList<InspectionModel> inspectionsList) {
        mInspectionsList = inspectionsList;
    }

    public CheckListModel getChecklist() {
        return mChecklist;
    }

    public void setChecklist(CheckListModel checklist) {
        mChecklist = checklist;
    }

    public SignatureModel getInstallerSignature() {
        return mInstallerSignature;
    }

    public void setInstallerSignature(SignatureModel installerSignature) {
        mInstallerSignature = installerSignature;
    }

    public SignatureModel getTenantSignature() {
        return mTenantSignature;
    }

    public void setTenantSignature(SignatureModel tenantSignature) {
        mTenantSignature = tenantSignature;
    }

    public String getServiceType() {
        return mServiceType;
    }

    public void setServiceType(String serviceType) {
        mServiceType = serviceType;
    }

    public String getCertificateSerialNumber() {
        return mCertificateSerialNumber;
    }

    public void setCertificateSerialNumber(String certificateSerialNumber) {
        mCertificateSerialNumber = certificateSerialNumber;
    }

    public String getEngineerRegistrationNumber() {
        return mEngineerRegistrationNumber;
    }

    public void setEngineerRegistrationNumber(String engineerRegistrationNumber) {
        mEngineerRegistrationNumber = engineerRegistrationNumber;
    }

    public Boolean getUploaded() {
        return mUploaded;
    }

    public void setUploaded(Boolean uploaded) {
        mUploaded = uploaded;
    }

    public void setPDFFilePath(String PDFFilePath)
    {
        mPDFFilePath = PDFFilePath;
    }

    public String getPDFFilePath()
    {
        return mPDFFilePath;
    }
}
