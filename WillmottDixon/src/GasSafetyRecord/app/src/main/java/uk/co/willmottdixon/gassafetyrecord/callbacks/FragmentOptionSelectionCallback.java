package uk.co.willmottdixon.gassafetyrecord.callbacks;

/**
 * Created by Dev on 12/08/2015.
 */

public interface FragmentOptionSelectionCallback
{
    void onSelection(Object selection);
}
