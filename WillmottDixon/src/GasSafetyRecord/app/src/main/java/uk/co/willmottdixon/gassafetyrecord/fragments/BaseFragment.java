package uk.co.willmottdixon.gassafetyrecord.fragments;

import android.animation.Animator;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.amazonaws.mobileconnectors.amazonmobileanalytics.AnalyticsEvent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import uk.co.willmottdixon.gassafetyrecord.activities.MainActivity;
import uk.co.willmottdixon.gassafetyrecord.listeners.OnFragmentInteractionListener;


public class BaseFragment extends Fragment {

    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();

    private OnFragmentInteractionListener mListener;
    private OnFragmentInteractionListener mFragmentListener; //used to allow a fragment to listen to the response
    private View mDummyFocusLayout;

    public void setFragmentListener(OnFragmentInteractionListener fragmentListener) {
        mFragmentListener = fragmentListener;
    }

    public BaseFragment() {
    }

    public OnFragmentInteractionListener getListener() {
        return (mFragmentListener != null ? mFragmentListener : mListener);
    }

    public void setTitle(String title) {
        if(getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(title);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        AnalyticsEvent fragmentAnalytics = MainActivity.analytics.getEventClient().createEvent("Fragment Displayed").withAttribute("Fragment Name",  getClass().getName());
        //Record the Level Complete event
        MainActivity.analytics.getEventClient().recordEvent(fragmentAnalytics);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        hideKeyboard();

        mListener = null;
        mFragmentListener = null;

    }

    @Override
    public void onPause()
    {
        super.onPause();
        hideKeyboard();
        AnalyticsEvent fragmentAnalytics = MainActivity.analytics.getEventClient().createEvent("Fragment Hidden").withAttribute("Fragment Name",  getClass().getName());
        //Record the Level Complete event
        MainActivity.analytics.getEventClient().recordEvent(fragmentAnalytics);
        MainActivity.analytics.getEventClient().submitEvents();
    }

    public void showLoadingDialog() {
        hideLoadingDialog();
    }

    public void showLoadingDialog(String message)
    {
        hideLoadingDialog();
    }

    public void hideLoadingDialog()
    {

    }

    public void hideKeyboard()
    {
        View view = this.getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

            if(mDummyFocusLayout != null)
            {
                mDummyFocusLayout.requestFocus();
            }
            else {
                view.clearFocus();
            }
        }

        if(mDummyFocusLayout != null)
        {
            mDummyFocusLayout.requestFocus();
        }
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {

        View view = getView();

        if(view != null) {
            if (enter) {
                view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else {
                view.setLayerType(View.LAYER_TYPE_NONE, null);
            }
        }

        return super.onCreateAnimator(transit, enter, nextAnim);
    }

    public View getDummyFocusLayout() {
        return mDummyFocusLayout;
    }

    public void setDummyFocusLayout(View dummyFocusLayout) {
        mDummyFocusLayout = dummyFocusLayout;
    }
}
