package uk.co.willmottdixon.gassafetyrecord.ui_elements.forms;

import android.content.Context;

import uk.co.willmottdixon.gassafetyrecord.core.Constants;
import uk.co.willmottdixon.gassafetyrecord.models.BaseModel;

/**
 * Created by Dev on 03/08/2015.
 */
public class TextFormItem extends BaseFormItem {


    private int mInputType;
    private int mRawInputType;

    public TextFormItem(Context context, String itemName, BaseModel baseModel, String propertyName) {
        setFormElementType(Constants.FormElements.FORM_ELEMENT_TEXT_FIELD);
        setFormItemName(itemName);
        setContext(context);
        setBaseModel(baseModel);
        setPropertyName(propertyName);
    }

    public int getInputType()
    {
        return mInputType;
    }

    public void setInputType(int inputType)
    {
        mInputType = inputType;
    }

    public int getRawInputType() {
        return mRawInputType;
    }

    public void setRawInputType(int mRawInputType) {
        this.mRawInputType = mRawInputType;
    }
}
